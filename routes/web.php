<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/contact', function () {
    return view('website/contact');
});
Route::get('/cart', function () {
    return view('website/cart');
});*/

/*Route::get('/', function () {
    return view('website/index');
});*/



/*Route::get('/cart', function () {
    return view('website/cart');
});*/
Route::get('test_email', 'HomeController@test_email')->name('test_email');
Route::get('/signin', function () {
    return view('website/signin');
});

Route::get('/forgot-password','ForgotPasswordController@add')->name('forgot-password');
Route::post('/forgot-password','ForgotPasswordController@store')->name('forgot-password.store');
Route::get('/reset-password/{token}','ForgotPasswordController@reset')->name('reset-password');
Route::post('/reset-password/store','ForgotPasswordController@resetStore')->name('reset-password.store');

Route::get('/signup', function () {
    return view('website/signup');
});


//extra sign in
Route::get('/sign-in', function () {
    return view('website//customer/signin');
});
Route::get('/login', function () {
    return view('auth/login');
});
Route::get('/cms', function () {
    return view('auth/login');
});

Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//Auth::routes();
Route::group(['prefix' => 'admin', 'middleware' => ['role:admin', 'auth'],], function () {
	Route::get('search', 'Admin\SearchController@adminSearch')->name('admin.search');
		Route::get('/', 'Admin\LeadController@index')->name('admin.home');
	Route::get('home/', 'Admin\LeadController@index')->name('admin.home');
			Route::get('/sub-menu', 'Admin\MenuController@header_menu')->name('admin.header.sub.menu');

	Route::group(['prefix' => 'menu'], function () {

		Route::group(['prefix' => 'footer-menu'], function () {
			Route::get('/', 'Admin\MenuController@footer_menu')->name('admin.footer.menu');
			Route::get('add', 'Admin\MenuController@footer_menu_add')->name('admin.footer.menu.add');
			Route::post('post', 'Admin\MenuController@footer_menu_post')->name('admin.footer.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@footer_menu_edit')->name('admin.footer.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@footer_menu_delete')->name('admin.footer.menu.delete');
		    Route::post('status', 'Admin\MenuController@footer_menu_status')->name('admin.footer.menu.status');
		});

		Route::group(['prefix' => 'footer-menu-bottom'], function () {
			Route::get('/', 'Admin\MenuController@footer_menu_bottom')->name('admin.footer.menu.bottom');
			Route::get('add', 'Admin\MenuController@footer_menu_bottom_add')->name('admin.footer.menu.bottom.add');
			Route::post('post', 'Admin\MenuController@footer_menu_bottom_post')->name('admin.footer.menu.bottom.post');
			Route::get('edit/{id}', 'Admin\MenuController@footer_menu_bottom_edit')->name('admin.footer.menu.bottom.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@footer_menu_bottom_delete')->name('admin.footer.menu.bottom.delete');
		    Route::post('status', 'Admin\MenuController@footer_menu_bottom_status')->name('admin.footer.menu.bottom.status');
		});

		Route::group(['prefix' => 'sub-footer-menu'], function () {
			Route::get('/{id}', 'Admin\MenuController@footer_sub_menu')->name('admin.footer.sub.menu');
			Route::get('add/{id}', 'Admin\MenuController@footer_sub_menu_add')->name('admin.footer.sub.menu.add');
			Route::post('post', 'Admin\MenuController@footer_sub_menu_post')->name('admin.footer.sub.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@footer_sub_menu_edit')->name('admin.footer.sub.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@footer_sub_menu_delete')->name('admin.footer.sub.menu.delete');
		    Route::post('status', 'Admin\MenuController@footer_sub_menu_status')->name('admin.footer.sub.menu.status');
		});

		Route::group(['prefix' => 'sidebar-menu'], function () {
			Route::get('/', 'Admin\MenuController@sidebar_menu')->name('admin.sidebar.menu');
			Route::get('add', 'Admin\MenuController@sidebar_menu_add')->name('admin.sidebar.menu.add');
			Route::post('post', 'Admin\MenuController@sidebar_menu_post')->name('admin.sidebar.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@sidebar_menu_edit')->name('admin.sidebar.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@sidebar_menu_delete')->name('admin.sidebar.menu.delete');
		    Route::post('status', 'Admin\MenuController@sidebar_menu_status')->name('admin.sidebar.menu.status');
		});

		Route::group(['prefix' => 'sub-sidebar-menu'], function () {
			Route::get('/{id}', 'Admin\MenuController@sidebar_sub_menu')->name('admin.sidebar.sub.menu');
			Route::get('add/{id}', 'Admin\MenuController@sidebar_sub_menu_add')->name('admin.sidebar.sub.menu.add');
			Route::post('post', 'Admin\MenuController@sidebar_sub_menu_post')->name('admin.sidebar.sub.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@sidebar_sub_menu_edit')->name('admin.sidebar.sub.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@sidebar_sub_menu_delete')->name('admin.sidebar.sub.menu.delete');
		    Route::post('status', 'Admin\MenuController@sidebar_sub_menu_status')->name('admin.sidebar.sub.menu.status');
		});

		Route::group(['prefix' => 'section-menu'], function () {
			Route::get('/', 'Admin\MenuController@section_menu')->name('admin.section.menu');
			Route::get('add', 'Admin\MenuController@section_menu_add')->name('admin.section.menu.add');
			Route::post('post', 'Admin\MenuController@section_menu_post')->name('admin.section.menu.post');
			Route::get('edit/{id}', 'Admin\MenuController@section_menu_edit')->name('admin.section.menu.edit');
		    Route::post('delete/{id}', 'Admin\MenuController@section_menu_delete')->name('admin.section.menu.delete');
		    Route::post('status', 'Admin\MenuController@section_menu_status')->name('admin.section.menu.status');

		    //image
	    	Route::post('upload/image', 'Admin\MenuController@sectionImageUpload')->name('admin.section.menu.upload.image');
	    	Route::get('remove/image', 'Admin\MenuController@removeImage')->name('admin.section.menu.remove.image');
		});
	});

	
	//localization
	Route::group(['prefix' => 'lang'], function () {
		Route::get('/{lang}', 'LocalizationController@index')->name('admin.lang');
	});

	//leads
	Route::group(['prefix' => 'lead'], function () {
		Route::get('/', 'Admin\LeadController@index')->name('admin.lead');
	});
	//tags
	Route::group(['prefix' => 'tag'], function () {
		Route::get('/', 'Admin\TagController@index')->name('admin.tag.manage');
		Route::get('add', 'Admin\TagController@add')->name('admin.tag.add');
		Route::get('edit/{id}', 'Admin\TagController@edit')->name('admin.tag.edit');
		Route::post('post', 'Admin\TagController@post')->name('admin.tag.post');
		Route::post('delete/{id}', 'Admin\TagController@delete')->name('admin.tag.delete');
	});
	//variants
	Route::group(['prefix' => 'variant'], function () {
		Route::get('/', 'Admin\VariantController@index')->name('admin.variant.manage');
		Route::get('add', 'Admin\VariantController@add')->name('admin.variant.add');
		Route::get('edit/{id}', 'Admin\VariantController@edit')->name('admin.variant.edit');
		Route::post('post', 'Admin\VariantController@post')->name('admin.variant.post');
		Route::post('delete/{id}', 'Admin\VariantController@delete')->name('admin.variant.delete');
	});
	//brands
	Route::group(['prefix' => 'brand'], function () {
		Route::get('/', 'Admin\BrandController@index')->name('admin.brand.manage');
		Route::get('add', 'Admin\BrandController@add')->name('admin.brand.add');
		Route::get('edit/{id}', 'Admin\BrandController@edit')->name('admin.brand.edit');
		Route::post('post', 'Admin\BrandController@post')->name('admin.brand.post');
		Route::post('delete/{id}', 'Admin\BrandController@delete')->name('admin.brand.delete');
	});
	Route::group(['prefix' => 'ourstory'], function () {
		Route::get('add', 'Admin\OurStoryController@create')->name('admin.ourstory.add');
		Route::post('post', 'Admin\OurStoryController@store')->name('admin.ourstory.post');
		Route::post('upload/image', 'Admin\OurStoryController@imageUpload')->name('admin.ourstory.upload.image');
	    Route::get('remove/image', 'Admin\OurStoryController@removeImage')->name('admin.ourstory.remove.image');
	});

	//settings
	Route::group(['prefix' => 'setting'], function () {
		Route::get('edit', 'Admin\SettingController@edit')->name('admin.setting.edit');
		Route::post('post', 'Admin\SettingController@post')->name('admin.setting.post');
        Route::get('email', 'Admin\SettingController@edit_email')->name('admin.setting.edit.email');
		Route::post('email/post', 'Admin\SettingController@post_email')->name('admin.setting.post.email');

		Route::get('edit_contact', 'Admin\SettingController@edit_contact')->name('admin.setting.edit_contact');
		Route::post('post_contact', 'Admin\SettingController@post_contact')->name('admin.setting.post_contact');
		Route::post('upload/image', 'Admin\SettingController@imageUpload')->name('admin.setting.upload.image.contact');
	    Route::get('remove/image', 'Admin\SettingController@removeImage')->name('admin.setting.remove.image.contact');
	});

	//shipping
	Route::group(['prefix' => 'shipping'], function () {
		Route::get('edit', 'Admin\ShippingController@edit')->name('admin.shipping.edit');
		Route::post('post', 'Admin\ShippingController@post')->name('admin.shipping.post');
	});

	//pages
	Route::group(['prefix' => 'page'], function () {
		Route::get('/', 'Admin\PageController@index')->name('admin.page.manage');
		Route::get('edit/{id}', 'Admin\PageController@edit')->name('admin.page.edit');
		Route::post('post', 'Admin\PageController@post')->name('admin.page.post');
	});

	//customers
	Route::group(['prefix' => 'customer'], function () {
		Route::get('/', 'Admin\CustomerController@index')->name('admin.customer.manage');
    	Route::get('report', 'Admin\CustomerController@report')->name('admin.customer.report');
		
	});

	//Sign up customers
	Route::group(['prefix' => 'sign_up_customer'], function () {
		Route::get('/', 'Admin\SignUpCustomerController@index')->name('admin.sign_up_customer.manage');		
		Route::get('/export', 'Admin\SignUpCustomerController@export')->name('admin.sign_up_customer.export');	
	});

	//orders
	Route::group(['prefix' => 'order'], function () {
		Route::get('/', 'Admin\OrderController@index')->name('admin.order.manage');
   		Route::get('order/details/{id}', 'Admin\OrderController@details')->name('admin.order.details');
    	Route::post('orderDelete', 'Admin\OrderController@delete')->name('admin.order.delete');
    	Route::post('orderShipping', 'Admin\OrderController@shipping')->name('admin.order.shipping');
    	Route::get('report', 'Admin\OrderController@report')->name('admin.order.report');
		Route::get('orderInvoice', 'Admin\OrderController@invoice')->name('admin.order.invoice');
		Route::get('statusChange/{id}/{status}', 'Admin\OrderController@statusChange')->name('admin.order.status_change');

	});
	//subscribe
	Route::group(['prefix' => 'subscribe'], function () {
		Route::get('/', 'Admin\SubscribeController@index')->name('admin.subscribe.manage');
		Route::post('post', 'Admin\SubscribeController@post')->name('admin.subscribe.post');
		Route::get('edit', 'Admin\SubscribeController@edit')->name('admin.subscribe.edit');

		Route::post('post_subscribe', 'Admin\SubscribeController@post_subscribe')->name('admin.subscribe.post_subscribe');
		Route::get('edit_subscribe/{id}', 'Admin\SubscribeController@edit_subscribe')->name('admin.subscribe.edit_subscribe');

		Route::post('delete', 'Admin\SubscribeController@delete')->name('admin.subscribe.delete');
		//image
	    Route::post('upload/image', 'Admin\SubscribeController@imageUpload')->name('admin.subscribe.upload.image');
	    	Route::get('remove/image', 'Admin\SubscribeController@removeImage')->name('admin.subscribe.remove.image');
	});
	//product
	Route::group(['prefix' => 'product'], function () {
		Route::get('/', 'Admin\ProductController@index')->name('admin.product.manage');
		Route::get('add', 'Admin\ProductController@add')->name('admin.product.add');
		Route::get('edit/{id}', 'Admin\ProductController@edit')->name('admin.product.edit');
		Route::post('post', 'Admin\ProductController@post')->name('admin.product.post');
		Route::post('delete/{id}', 'Admin\ProductController@delete')->name('admin.product.delete');
		Route::post('add_inventory', 'Admin\ProductController@add_inventory')->name('admin.product.add_inventory');

    	
		//image
    	Route::post('upload/image', 'Admin\ProductController@productImageUpload')->name('admin.product.upload.image');
    	Route::get('remove/image', 'Admin\ProductController@removeImage')->name('admin.product.remove.image');		
		Route::get('details', 'Admin\ProductController@details')->name('admin.product.details');

	});

	//coupons
	Route::group(['prefix' => 'coupon'], function () {
		Route::get('/', 'Admin\CouponController@index')->name('admin.coupon.manage');
		Route::get('add', 'Admin\CouponController@add')->name('admin.coupon.add');
		Route::get('edit/{id}', 'Admin\CouponController@edit')->name('admin.coupon.edit');
		Route::post('post', 'Admin\CouponController@post')->name('admin.coupon.post');
		Route::get('details', 'Admin\CouponController@details')->name('admin.coupon.details');
		Route::post('delete/{id}', 'Admin\CouponController@delete')->name('admin.coupon.delete');
	});

	//store
	Route::group(['prefix' => 'store'], function () {
		Route::get('/', 'Admin\StoreController@index')->name('admin.store.manage');
		Route::get('add', 'Admin\StoreController@add')->name('admin.store.add');
		Route::get('edit/{id}', 'Admin\StoreController@edit')->name('admin.store.edit');
		Route::post('post', 'Admin\StoreController@post')->name('admin.store.post');
		Route::get('details', 'Admin\StoreController@details')->name('admin.store.details');
		Route::post('delete/{id}', 'Admin\StoreController@delete')->name('admin.store.delete');

		//image
		Route::post('upload/image', 'Admin\StoreController@ImageUpload')->name('admin.store.upload.image');
		Route::get('remove/image', 'Admin\StoreController@removeImage')->name('admin.store.remove.image');
	});
   


    // Route::get('assign-role', 'SuperAdmin\RoleController@assign_role')->name('admin.search');
	Route::get('add-role', 'SuperAdmin\RoleController@add_role')->name('add.role');

});

Route::group(['prefix' => 'superadmin', 'middleware' => ['role:superadmin', 'auth'],], function () {
	//roles
    Route::get('add-role', 'SuperAdmin\RoleController@add_role')->name('add.role');
	Route::get('assign-role', 'SuperAdmin\RoleController@assign_role')->name('assign.role');

});

Route::group(['prefix' => 'page'], function () {
	Route::group(['prefix' => 'detail'], function () {
		Route::get('/{page_name}', 'PageController@detail')->name('page.detail');
	});
});
Route::group(['prefix' => 'brand'], function () {
	Route::get('/{brand}', 'ProductController@brand')->name('products.brand');
});
Route::group(['prefix' => 'product'], function () {
Route::group(['prefix' => 'detail'], function () {
	Route::get('/{product}', 'ProductController@detail')->name('products.detail');
});

	Route::get('{sidebar}', 'ProductController@sidebar')->name('products.category.sidebar');
	Route::get('{sidebar}/{submenu}', 'ProductController@sidebar')->name('products.category.sidebar.submenu');
	Route::get('/', 'ProductController@detail')->name('product.products');
});
Route::group(['prefix' => 'category'], function () {
	Route::get('/{setion}', 'ProductController@section_menu')->name('products.category.section');
});
Route::group(['prefix' => 'products'], function () {
	Route::get('/', 'ProductController@products')->name('products');
	Route::get('search/{tag_id}/{brand_id}/{min_amount?}/{max_amount?}', 'ProductController@search')->name('products.search');
});
Route::post('subscribe', 'SubscribtionController@add_subscribe')->name('web.subscribe');
Route::get('header_search', 'HomeController@header_search')->name('header.search');
Route::get('/', 'HomeController@index')->name('web.home');
Route::get('update/payment/status', 'CartController@update_payment_status')->name('update.payment.status');
Route::get('cart', 'CartController@cart')->name('cart');
Route::post('post_checkout', 'CartController@post_checkout')->name('post.checkout');
Route::get('checkout', 'CartController@checkout')->name('web.checkout');
Route::post('redeem', 'CartController@redeem')->name('web.coupon.redeem');
Route::post('payment_response', 'CartController@payment_response')->name('post.checkout.payment_response');

Route::get('thank-you', 'HomeController@thankyou')->name('thankyou');
Route::get('404', 'HomeController@page_not_found')->name('web.404');
Route::get('privacy-policy', 'HomeController@privacy_policy')->name('privacy.policy');
Route::get('faqs', 'HomeController@faqs')->name('faqs');
Route::get('terms-and-condition', 'HomeController@terms_and_condition')->name('terms.and.condition');
Route::get('about', 'HomeController@about')->name('about');
Route::get('contact-us', 'HomeController@contact')->name('contact');
Route::get('brands', 'HomeController@brands')->name('brands');
Route::get('our-story', 'HomeController@ourStory')->name('our-story');
Route::get('stores', 'HomeController@stores')->name('stores');
Route::post('post_lead', 'LeadController@post_lead')->name('post.lead');
Route::get('best_seller', 'HomeController@best_seller')->name('best_seller');
Route::get('exclusive', 'HomeController@exclusive')->name('exclusive');




Route::get('registration', 'UserController@index')->name('registration');
Route::post('registration-post', 'UserController@post')->name('registration-post');
Route::post('customer-login', 'UserController@checklogin')->name('customer-login');
Route::get('customer-logout', 'UserController@logout')->name('customer-logout');
// Route::get('checkIfUserLoggedin', 'UserController@checkIfUserLoggedin')->name('checkIfUserLoggedin');


Route::group(['prefix' => 'customer', 'middleware' => ['role:customer', 'auth'],], function () {
	Route::get('profile', 'Customer\HomeController@index')->name('customer.profile');
	Route::post('add_customer_address', 'Customer\HomeController@add_address')->name('customer.add_address');
	Route::post('delete_customer_address/{id}', 'Customer\HomeController@delete_customer_address')->name('customer.delete_address');
	Route::post('customer_name_change', 'Customer\HomeController@customer_name_change')->name('customer.customer_name_change');
	Route::post('customer_detail_change', 'Customer\HomeController@customer_detail_change')->name('customer.customer_detail_change');
	Route::post('cancel_order/{id}', 'Customer\HomeController@cancel_order')->name('customer.cancel_order');
});


//localization
Route::group(['prefix' => 'lang'], function () {
	Route::get('/{lang}', 'LocalizationController@index')->name('admin.lang');
});

Route::post('wishlist', 'WishlistController@post')->name('web.wishlist');