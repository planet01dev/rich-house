@extends('layouts.layout')
@section('content')
<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">@lang('lang.manage') {{@$title}}</h4>
            <ol class="breadcrumb">
              <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
              <li class="breadcrumb-item active" aria-current="page">
               <a href="{{route('admin.tag.manage')}}">@lang('lang.manage')</a>
             </li> 
            </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          
        </div>
        <div class="col-lg-12">
            @include('includes.alert')
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">@lang('lang.all') {{@$title}}</h5>
              <div class="table">
               <table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col" colspan="2">@lang('lang.title')</th>
                      <th scope="col">@lang('lang.action')</th>
                    </tr>
                  </thead>
                  @php($i= $data->perPage() * ($data->currentPage() - 1)+1)
                  <tbody>
                    @if(isset($error))
                    <tr>
                      <th colspan="6">
                        @lang('lang.data_not_found')
                      </th>
                    </tr>
                    @else
                    @foreach($data as $dt)
                    <tr>
                      <th scope="row">{{$i++}}</th>
                      <td class="pro-title" colspan="2">{{@$dt->page_name}}</td>
                      
                      <td class="action-buttons">
                        <a href="{{route('admin.page.edit', $dt->id)}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.edit')"> <i class="fa fa-pen"></i></button>
                        </a>
                      </td>
                    </tr>
                    
                    @endforeach
                    @endif
                  </tbody>
                </table>

            </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
 <?php if(isset($data)){?>    
 {!! $data->render() !!}
 <?php }?>
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
@endsection