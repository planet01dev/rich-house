@extends('layouts.layout')

@section('content')
<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">@lang('lang.product') @lang('lang.detail')</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.product')</li>
          <li class="breadcrumb-item active" aria-current="page">
            <a href="{{route('admin.product.manage')}}">@lang('lang.manage')</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.detail')</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-primary">
              <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#tabe-1"><i class="icon-home"></i> <span class="hidden-xs">@lang('lang.detail')</span></a>
              </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div id="tabe-1" class="container tab-pane active show">
                <div class="col-lg-12">
                  <div class="card card-primary" style="width: 100px;">
                    <img src="{{env('PRODUCT_IMAGES')}}{{ (file_exists(storage_path('products/').@$data->images[0]->image) && @$data->images[0]->image != '') ?@$data->images[0]->image:'dummy.jpeg'}}" alt="{{@$data->product_text_all[0]->title}}" class="card-img-top">
                  </div>
                </div>
                <table class="table table-bordered">
                  <tbody>
                    <?php if (isset($error)) { ?>
                      <tr>
                        <th colspan="4">
                          No Result Found
                        </th>
                      </tr>
                    <?php } else { ?>
                      <tr>
                        <th scope="col">@lang('lang.title')</th>
                        <td colspan='3'>{{ @$data->product_text->title }}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.price')</th>
                        <?php
                        if (isset($data->price)) {
                          foreach ($data->price as $v) {
                            if ($v['variant_id'] == NULL) {
                              $price = $v['price'];
                            }
                          }
                        }
                        ?>
                        <td colspan='3'>{{ @$price }} </td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.inventory_stock')</th>
                        <td colspan='3'>{{$data->inventory}}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.discount_type')</th>
                        <td><?= (@$data->discount_type == 0)?trans('lang.percentage'):trans('lang.amount')?> </td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.discount')</th>
                        <td colspan='3'>{{$data->discount}}<?= (@$data->discount_type == 0)?'%':''?></td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.weight')</th>
                        <td colspan='3'>
                          @foreach($weight_type as $wtype)
                          @if($data->weight_type_id == $wtype->id)
                          {{$data->weight_value}} {{$wtype->title}}
                          @else
                          @endif
                          @endforeach

                        </td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.brand')</th>
                        <td colspan='3'>{{@$data->brand->brand_text->title}}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.tag')</th>
                        <td colspan='3'>
                          @foreach($data->category as $v)
                          @if($v->tag_id != NULL)
                          @foreach($v->tag as $k)
                          {{$k->tag_text->title}}
                          @endforeach
                          @else
                          @endif
                          @endforeach
                        </td>
                      </tr>

                      <tr>
                        <th scope="col">@lang('lang.section_menu')</th>
                        <td colspan='3'>
                          @foreach(@$section as $v)
                          @foreach(@$data->category as $v1)
                          @if(@$v1->section_id != NULL)


                          @if(@$v1->section_id == @$v->id)
                          {{$v->section_menu_text->title}}
                          @endif

                          @endif
                          @endforeach
                          @endforeach
                        </td>
                      </tr>

                      <tr>
                        <th scope="col">@lang('lang.description')</th>
                        <td colspan='3'>{!! @$data->product_text->short_description !!}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.brief_detail')</th>
                        <td colspan='3'>{!! @$data->product_text->brief_description !!}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.ingredients')</th>
                        <td colspan='3'>{!! @$data->product_text->ingredients !!}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.notes')</th>
                        <td colspan='3'>{!! @$data->product_text->notes !!}</td>
                      </tr>
                      <tr>
                        <th>@lang('lang.image')</th>
                        <td align="center" colspan='3'>
                          <div class="col-lg-4">
                            <div id="carousel-3" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                @for($i = 0; $i< sizeof($data->images); $i++)
                                  @if($i == 0)
                                  <div class="carousel-item active">
                                    @else
                                    <div class="carousel-item">
                                      @endif
                                      <img class="d-block w-100" src="{{env('PRODUCT_IMAGES')}}{{ (file_exists(storage_path('products/').@$data->images[$i]->image) && @$data->images[$i]->image != '') ? @$data->images[$i]->image : 'dummy.jpeg' }}" alt="">
                                    </div>
                                    @endfor
                                  </div>
                                  <a class="carousel-control-prev" href="#carousel-3" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carousel-3" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                              </div>
                            </div>
                        </td>
                      </tr>
                    <?php } ?>

                    <tr>
                      <th scope="col">@lang('lang.sidebar_menu')</th>
                      <td colspan='3'>
                        @foreach($data->category as $v)
                        @foreach($v->sidebar as $k)
                        {{$k->sidebar_menu_text->title}}
                        @endforeach
                        @if($i == 0)

                        @endif
                        @endforeach
                      </td>
                    </tr>

                    <tr>
                      <th scope="col">@lang('lang.sidebar_sub_menu')</th>
                      <td colspan='3'>
                        @foreach($sidebar_menu as $v)
                        @foreach($v->sidebar_sub_menu as $k)

                        @if(isset($data))
                        @foreach($data->category as $v1)


                        @if($k->id == $v1->sidebar_child_id)
                        {{$k->sidebar_sub_menu_text->title}}
                        @endif


                        @endforeach
                        @endif

                        @endforeach
                        @endforeach


                      </td>
                    </tr>

                    <!-- Variant -->
                    <tr>
                      <th colspan='4' scope="col">@lang('lang.variant')</th>

                    </tr>

                    @foreach(@$data->productvariant as $k => $v)
                    <tr>
                      <th>{{ @$v->variant->variant_text->title }}</th>
                      <td>{{ @$v->product_variant_text->title }}</td>
                      <?php
                      if (isset($data->price)) {
                        foreach ($data->price as $v2) {
                          if ($v2['variant_id'] == $v['id']) {
                            $price = $v2['price'];
                          }
                        }
                      }
                      ?>
                      <td><b>@lang('lang.price'):</b> {{ @$price }}</td>
                      <td><b>@lang('lang.inventory_stock'):</b> {{ @$v->inventory }}</td>
                    </tr>
                    @endforeach


                    <!-- <tr>
                    <th>@lang('lang.variant') @lang('lang.price')</th>
                    <?php
                    if (isset($data->price)) {
                      foreach ($data->price as $v) {
                        if ($v['variant_id'] != NULL) {
                          $price = $v['price'];
                        }
                      }
                    }
                    ?>
                        <td>{{ @$price }} </td>
                    </tr> -->
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endsection