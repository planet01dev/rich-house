@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">{{@$title}}</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.product')</li>
          <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        @include('includes.alert')
        <div class="card">
          <div class="card-header text-uppercase">{{@$title}}</div>
          <div class="card-body">
            <form enctype="multipart/form-data" method="post" action="{{route('admin.product.upload.image')}}" class="dropzone" id="dropzone">
              @csrf
              <div class="fallback dropzone">
                <input name="file[]" id="file" type="file" multiple="multiple">
              </div>
              <input type="hidden" name="new_name" value="">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
            <label>Note: image format should be jpg & png, image dimentions should be 500 x 500</label>
            <br />
            <form id="product-create" class="ajaxForm validate" method="post" action="{{route('admin.product.post')}}">
              @csrf
              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.title') (@lang('lang.english'))</p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="title1" value="{{@$data->product_text_all[0]->title}}">
                  </div>
                </div>

                <div class="col-md-6">
                  <p>@lang('lang.title') (@lang('lang.arabic'))</p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="title2" value="{{@$data->product_text_all[1]->title}}" dir="rtl">
                  </div>
                </div>
              </div>
              <input type="hidden" name="fileName" id="fileName" value="" />
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>@lang('lang.sidebar_menu')</label>
                    <select class="form-control multiple-select select2" multiple="multiple" name="sidebar_menu[]">
                      @foreach($sidebar_menu as $v)
                      <?php
                      if (count($v->sidebar_sub_menu) == 0) {
                        $selected = '';
                      ?>
                        @if(isset($data))
                        @foreach($data->category as $v1)
                        @if($v1->sidebar_id != NULL && $v1->sidebar_child_id == NULL)
                        @foreach($v1->sidebar as $k)
                        @if($k->id == $v->id)
                        <?php $selected = 'selected'; ?>
                        @endif
                        @endforeach
                        @else
                        @endif
                        @endforeach
                        @endif
                        <option {{ @$selected }} value="{{$v->id}}">{{$v->sidebar_menu_text->title}}</option>
                      <?php
                      } else {
                      ?>
                        <optgroup label="{{$v->sidebar_menu_text->title}}">
                        <?php
                      }

                        ?>





                        @foreach($v->sidebar_sub_menu as $k)
                        <?PHP $selected = ''; ?>
                        @if(isset($data))
                        @foreach($data->category as $v1)


                        @if($k->id == $v1->sidebar_child_id)
                        <?php $selected = 'selected'; ?>
                        @endif


                        @endforeach
                        @endif
                        <option {{ @$selected }} value="{{$v->id}}_{{$k->id}}">{{$k->sidebar_sub_menu_text->title}}</option>
                        @endforeach
                        <?php
                        if (count($v->sidebar_sub_menu) == 0) {
                        ?>

                        <?php
                        } else {
                        ?>
                        </optgroup>
                      <?php
                        }
                      ?>

                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-6">
                  <p>@lang('lang.price')</p>
                  <div class="input-group mb-3">
                    <?php
                    if (isset($data->price)) {
                      foreach ($data->price as $v) {
                        if ($v['variant_id'] == NULL) {
                          $price = $v['price'];
                        }
                      }
                    }
                    ?>
                    <input type="text" class="form-control allow_decimal" name="price" value="{{@$price}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.weight')</p>
                  <div class="input-group mb-3">
                    <input type="text" name="weight_value" class="form-control allow_decimal" value="{{ @$data->weight_value }}">
                    <div class="input-group-append">
                      <select name="weight_type_id" class="btn btn-outline-primary joint-select">
                        @foreach($weight_type as $wtype)
                        <option <?= (@$data->weight_type_id == $wtype->id) ? 'selected' : ''; ?> value="{{$wtype->id}}">{{$wtype->title}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <p>@lang('lang.discount_type')</p>
                  <div class="input-group mb-3">
                    <select name="discount_type" class="form-control">
                      <option <?= (@$data->discount_type == 0) ? 'selected' : '' ?> value="0">@lang('lang.percentage')</option>
                      <option <?= (@$data->discount_type == 1) ? 'selected' : '' ?> value="1">@lang('lang.amount')</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.discount')</p>
                  <div class="input-group mb-3">
                    <input type="text" name="discount" class="form-control allow_decimal" value="{{@$data->discount}}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>@lang('lang.select_tag')</label>
                <select class="form-control multiple-select" name="tag[]" multiple="multiple">

                  @foreach(@$tag as $v)
                  <?php $selected = ''; ?>
                  @if(isset($data))
                  @foreach(@$data->category as $v1)
                  @if(@$v1->tag_id != NULL)
                  @foreach(@$v1->tag as $k1)
                  @if(@$k1->id == @$v->id)
                  <?php $selected = 'selected'; ?>
                  @endif
                  @endforeach
                  @else
                  @endif
                  @endforeach
                  @endif
                  <option {{ @$selected }} value="{{$v->id}}">{{$v->tag_text->title}}</option>
                  @endforeach
                </select>
              </div>
              <div class="row">

                <div class="col-md-6">
                  <p>@lang('lang.brand')</p>
                  <div class="input-group mb-3">
                    <select class="form-control multiple-select" name="brand">
                      <option></option>
                      @foreach($brand as $v)
                      <option <?= (@$data->brand->id == $v->id) ? 'selected' : '' ?> value="{{$v->id}}">{{$v->brand_text->title}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="row">
                    <div class="col-lg-12">
                      <p>@lang('lang.permalink')</p>
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" name="permalink" value="<?= ($add == 1) ? permalink_product() : @$data->permalink ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12">
                      <p>Example: prduct-name (DON'T include /)</p>
                    </div>
                  </div>


                </div>
              </div>

              <div class="row">

                <div class="col-md-12">
                  <p>@lang('lang.section_menu')</p>
                  <div class="input-group mb-3">
                    <select class="form-control multiple-select" name="section[]" multiple="multiple">

                      @foreach(@$section as $v)
                      <?php $selected = ''; ?>
                      @if(isset($data))
                      @foreach(@$data->category as $v1)
                      @if(@$v1->section_id != NULL)


                      @if(@$v1->section_id == @$v->id)
                      <?php $selected = 'selected'; ?>
                      @endif

                      @endif
                      @endforeach
                      @endif
                      <option {{ @$selected }} value="{{$v->id}}">{{$v->section_menu_text->title}}</option>
                      @endforeach
                    </select>

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.category') (@lang('lang.english'))</p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="short_description1" id="short_description" value="{{@$data->product_text_all[0]->short_description}}">
                  </div>
                </div>

                <div class="col-md-6">
                  <p>@lang('lang.category') (@lang('lang.arabic'))</p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" name="short_description2" id="short_description" value="{{@$data->product_text_all[1]->short_description}}" dir="rtl">
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.brief_detail') (@lang('lang.english'))</p>
                  <textarea name="brief_description1" class="form-control invt" id="brief_description1">{{@$data->product_text_all[0]->brief_description}}</textarea>
                </div>
                <div class="col-md-6">
                  <p>@lang('lang.brief_detail') (@lang('lang.arabic'))</p>
                  <textarea name="brief_description2" class="form-control invt" id="brief_description2" dir="rtl">{{@$data->product_text_all[1]->brief_description}}</textarea>

                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.ingredients') (@lang('lang.english'))</p>
                  <textarea name="ingredients1" class="form-control invt" id="ingredients1">{{@$data->product_text_all[0]->ingredients}}</textarea>
                </div>
                <div class="col-md-6">
                  <p>@lang('lang.ingredients') (@lang('lang.arabic'))</p>
                  <textarea name="ingredients2" class="form-control invt" id="ingredients2" dir="rtl">{{@$data->product_text_all[1]->ingredients}}</textarea>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-12">
                      <p>@lang('lang.notes') (@lang('lang.english'))</p>
                      <textarea name="notes1" class="form-control invt" id="notes1">{{@$data->product_text_all[0]->notes}}</textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>To add new line in "notes" just add &lt;br> in the end of the text</label>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-12">
                    <p>@lang('lang.notes') (@lang('lang.arabic'))</p>
                  <textarea name="notes2" class="form-control invt" id="notes2" dir="rtl">{{@$data->product_text_all[1]->notes}}</textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>To add new line in "notes" just add &lt;br> in the end of the text</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                <div class="row ">
                <div class="col-md-3">
                  <div class="input-group mb-3">
                    <input type="checkbox" id="best_seller" name="best_seller" {{ (isset($data->best_seller) && $data->best_seller == 1)? 'checked' : ''}} />
                    <label for="best_seller">@lang('lang.best_seller')</label>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="input-group mb-3">
                    <input type="checkbox" class="checkbox_group exclusive" data-id="1" id="exclusive" name="exclusive" {{ (isset($data->exclusive) && $data->exclusive == 1)? 'checked' : ''}} />
                    <label for="exclusive">@lang('lang.exclusive')</label>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="input-group mb-3">
                    <input type="checkbox" class="checkbox_group new_in" data-id="2" id="new_in" name="new_in" {{ (isset($data->new_in) && $data->new_in == 1)? 'checked' : ''}} />
                    <label for="new_in">@lang('lang.new_in')</label>
                  </div>
                </div>
              </div>
              </div>
                
              </div>

              <!-- Variant -->
              <div class="card border border-primary">
                <div class="card-header">
                  <i class="fa fa-plus"></i>
                  <button type="button" class="btn btn-link shadow-none text-primary collapsed" data-toggle="collapse" data-target="#collapse-variant" aria-expanded="false" aria-controls="collapse-variant">
                    @lang('lang.add_variant')
                  </button>
                </div>
                <div id="collapse-variant" class="<?= (@$edit == 1) ? 'collapse show' : 'collapse' ?>">
                  <div class="card-body">
                    <p>@lang('lang.priceable_variant')</p>
                    <div class="input-group mb-3" name="bodyStyle">
                      <div class="demo-radio-button">
                        @foreach($variant as $v)
                        <option value="{{$v->id}}"></option>
                        <input checked="true" name="variant_has_price" type="radio" id="variant_{{$v->id}}" value="{{$v->id}}" />
                        <label for="variant_{{$v->id}}">{{$v->variant_text->title}}</label>
                        @endforeach
                      </div>
                    </div>

                    <div class="input-group mb-3" name="bodyStyle">
                      <div class="input-group-append select_parent">
                        <select name="variant_1" class="multiple-select">
                          <option value="0">@lang('lang.choose')</option>
                          @foreach($variant as $v)
                          <option selected value="{{$v->id}}">{{$v->variant_text->title}}</option>
                          @endforeach
                        </select>
                      </div>
                      <?php $value_eng = ''; ?>
                      <?php $value_ar = ''; ?>
                      @if(isset($data))
                      @foreach(@$data->productvariant as $v)
                      <?php $value_eng .= $v->product_variant_text_all[0]->title . ','; ?>
                      <?php $value_ar .= $v->product_variant_text_all[1]->title . ','; ?>
                      @endforeach
                      @endif
                      <input type="text" value="<?= $value_eng ?>" placeholder="@lang('lang.english')" data-role="tagsinput" class="form-control invt" id="size" name="value_1" />
                      <input type="text" value="<?= $value_ar ?>" placeholder="@lang('lang.arabic')" data-role="tagsinput" class="form-control invt" id="size" name="value_ar_1" dir="rtl" />
                    </div>

                    <!-- <div class="input-group mb-3" name="bodyStyle">
                               <div class="input-group-append select_parent">
                                  <select name="variant_2" class="multiple-select">
                                     <option value="0">@lang('lang.choose')</option>
                                     @foreach($variant as $v)
                                      <option value="{{$v->id}}">{{$v->variant_text->title}}</option>
                                     @endforeach
                                  </select>
                               </div> 
                               <input type="text" placeholder="@lang('lang.english')" data-role="tagsinput" class="form-control invt" id="color" name="value_2"/>
                               <input type="text" placeholder="@lang('lang.arabic')" data-role="tagsinput" class="form-control invt" id="color" name="value_ar_2"/> -->
                    <!-- </div>
                              <div class="input-group mb-3" name="bodyStyle">
                               <div class="input-group-append select_parent">
                                  <select name="variant_3" class="multiple-select">
                                     <option value="0">@lang('lang.choose')</option>
                                     @foreach($variant as $v)
                                      <option value="{{$v->id}}">{{$v->variant_text->title}}</option>
                                     @endforeach
                                  </select>
                               </div> 
                               <input type="text" placeholder="@lang('lang.english')" data-role="tagsinput" class="form-control invt" id="color" name="value_3"/>
                               <input type="text" placeholder="@lang('lang.arabic')" data-role="tagsinput" class="form-control invt" id="color" name="value_ar_3"/>
                             </div> -->
                    <p>@lang('lang.prices')</p>
                    <div class="input-group mb-3" name="bodyStyle">
                      <?php
                      $value_prc = '';
                      if (isset($data->price)) {
                        foreach ($data->price as $v) {
                          if ($v['variant_id'] != NULL) {
                            $value_prc .= $v['price'] . ',';
                          }
                        }
                      }
                      ?>
                      <input type="text" value="{{ @$value_prc }}" placeholder="@lang('lang.price')" data-role="tagsinput" class="form-control invt" id="variant_price" name="variant_price" />
                      <!-- <input type="text"  class="form-control allow_decimal" id="variant_price" name="variant_price" value="{{ @$price }}"> -->
                    </div>

                  </div>
                </div>
              </div>
              <input type="hidden" name="id" value="{{@$data->id}}">
              <button type="submit" class="btn btn-primary ajaxFormSubmit create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
            </form>
          </div>
        </div>
      </div>

    </div>
    <!-- End container-fluid-->
  </div>
  <!--End content-wrapper-->
  @endsection
  @section('footer')
  <script>
    var allFile = new Array();
    var findImage = "{{@$data->images}}";
    var image_path = "{{env('PRODUCT_IMAGES')}}";
    <?php
    if (isset($data->images)) {
      foreach (@$data->images as $key => $val) { ?>
        allFile.push('<?php echo $val; ?>');
    <?php }
    } ?>
    var images = Array();
    for (var i = 0; i < JSON.parse(allFile.length); i++) {
      images.push(JSON.parse(allFile[i]).image);
      // document.getElementById('fileName').value += JSON.parse(allFile[i]).image+"~~";
    }
    // console.log(images);


    $(document).ready(function() {

      $( ".checkbox_group" ).click(function() {
          var id = $(this).data('id'); //1 exclusive, 2 new in
          if(id == 1)
          {
            $('.new_in').prop('checked', false);
          }
          else
          {
            $('.exclusive').prop('checked', false);
          }
      });

      $("form.validate").validate({
        rules: {
          title1: {
            required: true
          },
          title2: {
            required: true
          },
          price: {
            required: true
          },
          permalink: {
            required: true
          },
          weight_value: {
            required: true,
            min: 0,

          },
          brand: {
            required: true
          },
          'sidebar_menu[]': {
            required: true
          },
        },
        messages: {
          title1: "This field is required.",
          title2: "This field is required.",
          price: "This field is required.",
          permalink: "This field is required.",
          weight_value: "This field is required.",
          brand: "This field is required.",
          "sidebar_menu[]": {
            required: "This field is required.",
          },
        },
        invalidHandler: function(event, validator) {
          //display error alert on form submit 
          $('#fail').show();
          $('#fail-text').html("Please fill all mandatory fields.");

        },
        errorPlacement: function(label, element) { // render error placement for each input type  
          $(element).addClass("border-red");
        },
        highlight: function(element) { // hightlight error inputs
          $(element).removeClass('border-green').addClass("border-red");
        },
        unhighlight: function(element) { // revert the change done by hightlight
          $(element).removeClass('border-red').addClass("border-green");
        },
        success: function(label, element) {
          $(element).removeClass('border-red').addClass("border-green");

        }
        // submitHandler: function (form) {
        // }
      });
    });
    image_delete("{{route('admin.product.remove.image')}}", 5);
  </script>
  @endsection