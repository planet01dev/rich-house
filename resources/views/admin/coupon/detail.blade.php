@extends('layouts.layout')

@section('content')
<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">@lang('lang.coupon') @lang('lang.detail')</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.coupon')</li>
          <li class="breadcrumb-item active" aria-current="page">
            <a href="{{route('admin.product.manage')}}">@lang('lang.manage')</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.detail')</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-primary">
              <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#tabe-1"><i class="icon-home"></i> <span class="hidden-xs">@lang('lang.detail')</span></a>
              </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div id="tabe-1" class="container tab-pane active show">
                <table class="table table-bordered">
                  <tbody>
                    <?php if (isset($error)) { ?>
                      <tr>
                        <th colspan="3">
                          No Result Found
                        </th>
                      </tr>
                    <?php } else { ?>
                      <tr>
                        <th scope="col">@lang('lang.title')</th>
                        <td colspan='2'>{{ @$data->coupon_text->Title }}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.coupon_code')</th>
                        <td>{{ @$data->CouponCode }} </td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.discount_type')</th>
                        <td><?= (@$data->discount_type == 0)?trans('lang.percentage'):trans('lang.amount')?> </td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.discount')</th>
                        <td>{{ @$data->DiscountPercentage }} </td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.description')</th>
                        <td colspan='2'>{!! @$data->coupon_text->Description !!}</td>
                      </tr>
                      <tr>
                      <th scope="col">@lang('lang.active')</th>
                        <td colspan='2'><?= (@$data->IsActive == 1 )?'Active':'Disable' ?></td>
                      </tr>
                      
                      
                    <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endsection