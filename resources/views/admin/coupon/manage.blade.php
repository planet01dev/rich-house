@extends('layouts.layout')
@section('content')
<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">@lang('lang.manage') @lang('lang.coupon')</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.coupon')</li>
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.manage')</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <a href="{{route('admin.coupon.add')}}">
          <button type="button" class="pull-right btn btn-secondary waves-effect waves-light m-1" title="@lang('lang.add')">
            @lang('lang.add')
          </button>
        </a>
      </div>
      <div class="col-lg-12">
        @include('includes.alert')
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">@lang('lang.all') @lang('lang.coupon')</h5>
            <div class="table">
              <table class="table table-striped">
                <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1); ?>
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col" colspan="2">@lang('lang.title')</th>
                    <th scope="col" colspan="2">@lang('lang.description')</th>
                    <th scope="col" colspan="2">@lang('lang.useage')</th>
                    <th scope="col">@lang('lang.action')</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (isset($error)) { ?>
                    <tr>
                      <th colspan="6">
                        @lang('lang.data_not_found')
                      </th>
                    </tr>
                  <?php } else { ?>
                    @foreach($data as $products)
                    <tr>
                      <td>{{$i++}}</td>
                      
                      <td colspan="2" class="pro-title">{{@$products->coupon_text->Title}}</td>
                      <td colspan="2" class="pro-title">{{@$products->coupon_text->Description}}</td>
                      <td colspan="2" class="pro-title">{{@$products->UsageCount}}</td>
                      <td class="action-buttons">
                        <a href="{{route('admin.coupon.details').'?id='.$products->id}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.view')"> <i class="fa fa-eye"></i> </button>
                        </a>
                        <a href="{{route('admin.coupon.edit', $products->id)}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.edit')"> <i class="fa fa-pen"></i></button>
                        </a>
                        <a>
                        <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$products->id}}, '{{route('admin.coupon.delete', $products->id)}}')" title="@lang('lang.delete')"> <i class="fa fa-trash"></i></button>
                        </a>
                      </td>
                    </tr>
                    @endforeach
                  <?php } ?>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>

    <!--End Row-->

    <div class="modal_display">
      <div class="modal fade" id="inventory_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content my-modal-controls">
            <div class="modal-header">
              <div class="float-left">
                <h4 id="myModalLabel" class="semi-bold mymodal-title">@lang('lang.add_inventory')</h4>
              </div>
              <div class="float-right">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>


            </div>
            <form class='ajaxForm validate' action="{{route('admin.product.add_inventory')}}" method='post'>
              @csrf
              <div class="modal-body mymodal-body" id="myModalDescription">
                <div class="row">
                  <div class="col-md-12 po_box">
                    <div class="form-group">
                      <div class="row form-row">
                        <div class="col-md-12">
                          <label class="form-label">@lang('lang.inventory_stock')</label>
                        </div>
                        <div class="col-md-12">
                          <div class="input-with-icon right controls">
                            <i class=""></i>
                            <input type="text" name="inventory" id="inventory" class="form-control txtboxToFilter" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <input type="hidden" value="" name="product_id" id="product_id">
                <button class='btn-sm btn-dark waves-effect waves-light m-1 ajaxFormSubmit my-bttn mt-2' type='button'>@lang('lang.submit')</button>
                <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" data-dismiss="modal">@lang('lang.close')</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    </div>

    <?php if (isset($product)) { ?>
      {!! $product->render() !!}
    <?php } ?>
  </div>
  <!-- End container-fluid-->

</div>
<!--End content-wrapper-->
@endsection
@section('footer')
<script>
  $(document).ready(function() {
    $(".table").on("click", ".add_inventory", function(event) {
      event.preventDefault();
      $('#inventory').val(0);
      var product_id = $(this).data('product-id');
      var inventory = $(this).data('inventory');
      $('#inventory').val(inventory);
      $('#product_id').val(product_id);
      $('#inventory_modal').modal('show');

    });
  });
</script>
@endsection