@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">{{@$title}}</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.coupon')</li>
          <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        @include('includes.alert')
        <div class="card">
          <div class="card-header text-uppercase">{{@$title}}</div>
          <div class="card-body">

            <br />
            <form id="product-create" class="ajaxForm validate" method="post" action="{{route('admin.coupon.post')}}">
              @csrf

              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.coupon_code') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="CouponCode" value="{{@$data->CouponCode}}">
                  </div>
                </div>

                <div class="col-md-6">
                  <p>@lang('lang.discount_type') </p>
                  <div class="input-group mb-3">
                  <select name="discount_type" class="form-control">
                      <option <?= (@$data->discount_type == 0)?'selected':''?> value="0">@lang('lang.percentage')</option>
                      <option <?= (@$data->discount_type == 1)?'selected':''?> value="1">@lang('lang.amount')</option>
                    </select>
                  </div>
                </div>

                
              </div>
              <div class="row">
              <div class="col-md-6">
                  <p>@lang('lang.discount') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="DiscountPercentage" value="{{@$data->DiscountPercentage}}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.title') (@lang('lang.english'))</p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="title1" value="{{@$data->coupon_text_all[0]->Title}}">
                  </div>
                </div>

                <div class="col-md-6">
                  <p>@lang('lang.title') (@lang('lang.arabic'))</p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="title2" value="{{@$data->coupon_text_all[1]->Title}}">
                  </div>
                </div>
              </div>



              <div class="row mb-3">
                <div class="col-md-6">
                  <p>@lang('lang.description') (@lang('lang.english'))</p>
                  <textarea name="Description1" class="form-control invt" id="Description1">{{@$data->coupon_text_all[0]->Description}}</textarea>
                </div>
                <div class="col-md-6">
                  <p>@lang('lang.description') (@lang('lang.arabic'))</p>
                  <textarea name="Description2" class="form-control invt" id="Description2">{{@$data->coupon_text_all[1]->Description}}</textarea>
                </div>
              </div>

              <div class="row ">
                <div class="col-md-6">
                  <div class="input-group mb-3">
                    <input type="checkbox" id="basic_checkbox_2" name="IsActive" {{ (isset($data->IsActive) && $data->IsActive == 1)? 'checked' : ''}} />
                    <label for="basic_checkbox_2">@lang('lang.active')</label>
                  </div>
                </div>
              </div>


              <input type="hidden" name="id" value="{{@$data->id}}">
              <button type="submit" class="btn btn-primary ajaxFormSubmit create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
            </form>
          </div>
        </div>
      </div>

    </div>
    <!-- End container-fluid-->
  </div>
  <!--End content-wrapper-->
  @endsection
  @section('footer')
  <script>
    $(document).ready(function() {
      $("form.validate").validate({
        rules: {
          title1: {
            required: true
          },
          title2: {
            required: true
          },
          descrption1: {
            required: true
          },
          descrption2: {
            required: true
          },

        },
        messages: {
          title1: "This field is required.",
          title2: "This field is required.",

        },
        invalidHandler: function(event, validator) {
          //display error alert on form submit 
          $('#fail').show();
          $('#fail-text').html("Please fill all mandatory fields.");

        },
        errorPlacement: function(label, element) { // render error placement for each input type  
          $(element).addClass("border-red");
        },
        highlight: function(element) { // hightlight error inputs
          $(element).removeClass('border-green').addClass("border-red");
        },
        unhighlight: function(element) { // revert the change done by hightlight
          $(element).removeClass('border-red').addClass("border-green");
        },
        success: function(label, element) {
          $(element).removeClass('border-red').addClass("border-green");

        }
        // submitHandler: function (form) {
        // }
      });
    });
  </script>
  @endsection