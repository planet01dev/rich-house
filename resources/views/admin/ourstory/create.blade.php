@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">{{@$title}}</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">@lang('lang.our_story')</li>
            <li class="breadcrumb-item active" aria-current="page">
              <a href="{{route('admin.ourstory.add')}}">@lang('lang.add')</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
       <div class="row">
        <div class="col-lg-12">
          <div class="card">
             <div class="card-header text-uppercase">{{@$title}}</div>
             <div class="card-body">
             <div class="row">
                    <div class="col-md-12">
                    <p>@lang('lang.image')</p>   
                    <form enctype="multipart/form-data" method="post" action="{{route('admin.ourstory.upload.image')}}" class="dropzone" id="dropzone">
                      @csrf
                        <div class="fallback dropzone">
                          <input name="file[]" id="file" type="file" multiple="multiple">
                        </div>
                        <input type="hidden" name="new_name" value="">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      </form> 
                    </div>
                  </div>
                  <label>Note: image format should be jpg & png, image dimentions should be 950 x 1050</label>
            <br />
                <form id="form-create" class="form" action="{{route('admin.ourstory.post')}}" method="post" enctype="multipart/form-data"> 
                 @csrf
                  
                  <p>@lang('lang.title') (@lang('lang.english'))</p>   
                  <div class="input-group mt-3">
                    <input type="text" class="form-control" name="title1" value="{{@$ourstory->our_story_text_all[0]->title}}">
                  </div>
                  @error('title1')
                  <div class="text-danger">This field is required</div>
                  @enderror
                  
                   <p class="mt-3">@lang('lang.title') (@lang('lang.arabic'))</p>   
                  <div class="input-group mt-3">
                    <input type="text" class="form-control" name="title2" dir="rtl" value="{{@$ourstory->our_story_text_all[1]->title}}">
                  </div>
                  @error('title2')
                  <div class="text-danger">This field is required</div>
                  @enderror
                  <p class="mt-3">@lang('lang.content') (@lang('lang.english'))</p>   
                  <div class="input-group mt-3">
                    <textarea rows="5" class="form-control" name="content1">{{@$ourstory->our_story_text_all[0]->content}}</textarea>
                  </div>
                  @error('content1')
                  <div class="text-danger">This field is required</div>
                  @enderror
                   <p class="mt-3">@lang('lang.content') (@lang('lang.arabic'))</p>   
                  <div class="input-group mt-3">
                    <textarea rows="5" class="form-control" dir="rtl" name="content2">{{@$ourstory->our_story_text_all[1]->content}}</textarea>
                  </div>
                  @error('content2')
                  <div class="text-danger">This field is required</div>
                  @enderror
                  
                  <input type="hidden" name="id" value="{{@$ourstory->id}}">
                  <?php
                    $image = ''; 
                    (isset($ourstory->image) && $ourstory->image != '') ? $image.=$ourstory->image.'~~' : '';
                  ?>
                  <input type="hidden" name="fileName"  id="fileName" value="{{ $image }}"/>
                  <button type="submit" class="btn btn-primary btn-lg btn-block waves-effect waves-light m-1 mt-3">@lang('lang.submit')</button>
                </form>                
            </div>
          </div>
        </div>

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <script>
   var allFile = new Array();
    var findImage  = "{{@$ourstory->image}}";
    var image_path = "{{env('STORY_IMAGES')}}";
    allFile.push(findImage);
    
    var images = Array();
    for (var i = 0; i < allFile.length; i++) {
      images.push(allFile[i]);
    }
    
  
</script>
@endsection
@section('footer')

<script>


 $(document).ready(function() {
  Dropzone.options.dropzone = {
  maxFiles: 1,
  accept: function(file, done) {
    console.log("uploaded");
    done();
  },
  init: function() {
    this.on("maxfilesexceeded", function(file){
        alert("No more files please!");
    });
  }
};
image_delete("{{route('admin.ourstory.remove.image')}}", 1);
});
</script>
@endsection
