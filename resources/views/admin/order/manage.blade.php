@extends('layouts.layout')
@section('content')
<link href="{{ asset('public/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<style>
.nav-tabs>li {
    float: left;
    margin-bottom: -1px;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #555;
    cursor: default;
    background-color: #fff;
    border: 1px solid #ddd;
    border-bottom-color: transparent;
}
ul.test2 a {
    color: #337ab7;
    text-decoration: none !important;
    border-radius: 4px;
}
.nav-tabs>li>a {
    margin-right: 2px;
    line-height: 1.42857143;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
}
.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 15px;
}
</style>
<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">@lang('lang.manage') @lang('lang.order')</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.order')</li>
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.manage')</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        @include('includes.alert')
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">@lang('lang.all') @lang('lang.orders')</h5>
            <div class="row">
              <div class="col-lg-4">
                <form id="report_form" action="{{route('admin.order.manage')}}" method="GET" target="_blank">

                  <label>@lang('lang.select') @lang('lang.daterange')</label>
                  <div id="dateragne-picker">
                    <div class="input-daterange input-group">
                      <input type="text" class="form-control" value="{{isset($from)? $from : ''}}" name="from" />
                      <div class="input-group-prepend">
                        <span class="input-group-text">@lang('lang.to')</span>
                      </div>
                      <input type="text" class="form-control" name="to" value="{{isset($to)? $to : ''}}" />
                    </div>
                    <button type="button" title="@lang('lang.list') @lang('lang.report')" onclick="report('{{route('admin.order.manage')}}')" class="btn-sm btn-dark waves-effect waves-light m-1"> <i class="fa fa-search"></i></button>
                    <button type="button" onclick="report('{{route('admin.order.manage')}}/report')" title="PDF @lang('lang.report')" class="btn-sm btn-dark waves-effect waves-light m-1"> <i class="fa fa fa-file-pdf-o"></i></button>
                  </div>
                </form>
              </div>
            </div>
            <br />

            <ul class="nav nav-tabs navigation_tabs">
              <li class="active"><a data-toggle="tab" href="#home" class="show active">Processing</a></li>
              <li><a data-toggle="tab" href="#menu1">Shipped</a></li>
              <li><a data-toggle="tab" href="#menu2">Delivered</a></li>
              <li><a data-toggle="tab" href="#menu3">Cancelled</a></li>
              <li><a data-toggle="tab" href="#menu4">Payment Failed</a></li>
            </ul>

            <div class="tab-content">
              <div id="home" class="tab-pane fade in active show">
                <div class="table">
                  <table class="table table-striped">
                    <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1); ?>
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">@lang('lang.order_no')</th>
                        <th scope="col">@lang('lang.name')</th>
                        <th scope="col">@lang('lang.user')</th>
                        <th scope="col">@lang('lang.phone')</th>
                        <th scope="col">@lang('lang.email')</th>
                        <th scope="col">@lang('lang.no_of_products')</th>
                        <th scope="col">@lang('lang.quantity')</th>
                        <th scope="col">@lang('lang.amount')</th>
                        <th scope="col">@lang('lang.status')</th>
                        <th scope="col">@lang('lang.action')</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (isset($error)) { ?>
                        <tr>
                          <th colspan="6">
                            @lang('lang.data_not_found')
                          </th>
                        </tr>
                      <?php } else { ?>
                        @foreach($data as $dt)
                        <?php if ($dt->status == 0) { ?>
                          <tr>
                            <th scope="row">{{$i++}}</th>
                            <td>@lang('lang.order_no') {{$dt->id}} </td>
                            <td>{{$dt->billing_name}} </td>
                            <td>{{ ($dt->user_id == 0)?'Guest':'Customer'}}</td>
                            <td>{{$dt->billing_phone }}</td>
                            <td>{{$dt->billing_email }}</td>
                            <td>{{$dt->orderDetail->count('order_id')}}</td>
                            <td>{{$dt->orderDetail->sum('quantity')}}</td>
                            <td>@lang('lang.currency'){{num_format($dt->total)}}</td>
                            <td>
                              <?php
                              $status = '';
                              $class = '';
                              if ($dt->status == 0) {
                                $status = 'Processing';
                                $class = 'label-warning';
                              } else if ($dt->status == 1) {
                                $status = 'Shipped';
                                $class = 'label-primary';
                              } else if ($dt->status == 2) {
                                $status = 'Delivered';
                                $class = 'label-success';
                              } else if ($dt->status == 3) {
                                $status = 'Cancelled';
                                $class = 'label-danger';
                              }
                              ?>
                              <label class="label <?= $class ?>"><?= $status ?></label>
                            </td>
                            <td>
                              <a href="{{route('admin.order.details', $dt->id)}}">
                                <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.view')"> <i class="fa fa-eye"></i> </button>
                              </a>

                              <div class="btn-group ">
                                <button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>
                                <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                <ul class="dropdown-menu">
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,0]) }}">Processing</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,1]) }}">Shipped</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,2]) }}">Delivered</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,3]) }}">Cancelled</a></li>

                                </ul>
                              </div>

                              <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$dt->id}}, '{{route('admin.order.delete', $dt->id)}}')" title="@lang('lang.delete')"> <i class="fa fa-trash"></i></button>
                              </a>
                            </td>
                          </tr>
                        <?php } ?>
                        @endforeach
                      <?php } ?>
                    </tbody>
                  </table>

                </div>
              </div>
              <div id="menu1" class="tab-pane fade">
                <div class="table">
                  <table class="table table-striped">
                    <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1); ?>
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">@lang('lang.order_no')</th>
                        <th scope="col">@lang('lang.name')</th>
                        <th scope="col">@lang('lang.user')</th>
                        <th scope="col">@lang('lang.phone')</th>
                        <th scope="col">@lang('lang.email')</th>
                        <th scope="col">@lang('lang.no_of_products')</th>
                        <th scope="col">@lang('lang.quantity')</th>
                        <th scope="col">@lang('lang.amount')</th>
                        <th scope="col">@lang('lang.status')</th>
                        <th scope="col">@lang('lang.action')</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (isset($error)) { ?>
                        <tr>
                          <th colspan="6">
                            @lang('lang.data_not_found')
                          </th>
                        </tr>
                      <?php } else { ?>
                        @foreach($data as $dt)
                        <?php if ($dt->status == 1) { ?>
                          <tr>
                            <th scope="row">{{$i++}}</th>
                            <td>@lang('lang.order_no') {{$dt->id}} </td>
                            <td>{{$dt->billing_name}} </td>
                            <td>{{ ($dt->user_id == 0)?'Guest':'Customer'}}</td>
                            <td>{{$dt->billing_phone }}</td>
                            <td>{{$dt->billing_email }}</td>
                            <td>{{$dt->orderDetail->count('order_id')}}</td>
                            <td>{{$dt->orderDetail->sum('quantity')}}</td>
                            <td>@lang('lang.currency'){{num_format($dt->total)}}</td>
                            <td>
                              <?php
                              $status = '';
                              $class = '';
                              if ($dt->status == 0) {
                                $status = 'Processing';
                                $class = 'label-warning';
                              } else if ($dt->status == 1) {
                                $status = 'Shipped';
                                $class = 'label-primary';
                              } else if ($dt->status == 2) {
                                $status = 'Delivered';
                                $class = 'label-success';
                              } else if ($dt->status == 3) {
                                $status = 'Cancelled';
                                $class = 'label-danger';
                              }
                              ?>
                              <label class="label <?= $class ?>"><?= $status ?></label>
                            </td>
                            <td>
                              <a href="{{route('admin.order.details', $dt->id)}}">
                                <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.view')"> <i class="fa fa-eye"></i> </button>
                              </a>

                              <div class="btn-group ">
                                <button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>
                                <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                <ul class="dropdown-menu">
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,0]) }}">Processing</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,1]) }}">Shipped</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,2]) }}">Delivered</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,3]) }}">Cancelled</a></li>

                                </ul>
                              </div>

                              <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$dt->id}}, '{{route('admin.order.delete', $dt->id)}}')" title="@lang('lang.delete')"> <i class="fa fa-trash"></i></button>
                              </a>
                            </td>
                          </tr>
                        <?php } ?>
                        @endforeach
                      <?php } ?>
                    </tbody>
                  </table>

                </div>
              </div>
              <div id="menu2" class="tab-pane fade">
                <div class="table">
                  <table class="table table-striped">
                    <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1); ?>
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">@lang('lang.order_no')</th>
                        <th scope="col">@lang('lang.name')</th>
                        <th scope="col">@lang('lang.user')</th>
                        <th scope="col">@lang('lang.phone')</th>
                        <th scope="col">@lang('lang.email')</th>
                        <th scope="col">@lang('lang.no_of_products')</th>
                        <th scope="col">@lang('lang.quantity')</th>
                        <th scope="col">@lang('lang.amount')</th>
                        <th scope="col">@lang('lang.status')</th>
                        <th scope="col">@lang('lang.action')</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (isset($error)) { ?>
                        <tr>
                          <th colspan="6">
                            @lang('lang.data_not_found')
                          </th>
                        </tr>
                      <?php } else { ?>
                        @foreach($data as $dt)
                        <?php if ($dt->status == 2) { ?>
                          <tr>
                            <th scope="row">{{$i++}}</th>
                            <td>@lang('lang.order_no') {{$dt->id}} </td>
                            <td>{{$dt->billing_name}} </td>
                            <td>{{ ($dt->user_id == 0)?'Guest':'Customer'}}</td>
                            <td>{{$dt->billing_phone }}</td>
                            <td>{{$dt->billing_email }}</td>
                            <td>{{$dt->orderDetail->count('order_id')}}</td>
                            <td>{{$dt->orderDetail->sum('quantity')}}</td>
                            <td>@lang('lang.currency'){{num_format($dt->total)}}</td>
                            <td>
                              <?php
                              $status = '';
                              $class = '';
                              if ($dt->status == 0) {
                                $status = 'Processing';
                                $class = 'label-warning';
                              } else if ($dt->status == 1) {
                                $status = 'Shipped';
                                $class = 'label-primary';
                              } else if ($dt->status == 2) {
                                $status = 'Delivered';
                                $class = 'label-success';
                              } else if ($dt->status == 3) {
                                $status = 'Cancelled';
                                $class = 'label-danger';
                              }
                              ?>
                              <label class="label <?= $class ?>"><?= $status ?></label>
                            </td>
                            <td>
                              <a href="{{route('admin.order.details', $dt->id)}}">
                                <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.view')"> <i class="fa fa-eye"></i> </button>
                              </a>

                              <div class="btn-group ">
                                <button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>
                                <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                <ul class="dropdown-menu">
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,0]) }}">Processing</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,1]) }}">Shipped</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,2]) }}">Delivered</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,3]) }}">Cancelled</a></li>

                                </ul>
                              </div>

                              <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$dt->id}}, '{{route('admin.order.delete', $dt->id)}}')" title="@lang('lang.delete')"> <i class="fa fa-trash"></i></button>
                              </a>
                            </td>
                          </tr>
                        <?php } ?>
                        @endforeach
                      <?php } ?>
                    </tbody>
                  </table>

                </div>
              </div>
              <div id="menu3" class="tab-pane fade">
                <div class="table">
                  <table class="table table-striped">
                    <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1); ?>
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">@lang('lang.order_no')</th>
                        <th scope="col">@lang('lang.name')</th>
                        <th scope="col">@lang('lang.user')</th>
                        <th scope="col">@lang('lang.phone')</th>
                        <th scope="col">@lang('lang.email')</th>
                        <th scope="col">@lang('lang.no_of_products')</th>
                        <th scope="col">@lang('lang.quantity')</th>
                        <th scope="col">@lang('lang.amount')</th>
                        <th scope="col">@lang('lang.status')</th>
                        <th scope="col">@lang('lang.action')</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (isset($error)) { ?>
                        <tr>
                          <th colspan="6">
                            @lang('lang.data_not_found')
                          </th>
                        </tr>
                      <?php } else { ?>
                        @foreach($data as $dt)
                        <?php if ($dt->status == 3) { ?>
                          <tr>
                            <th scope="row">{{$i++}}</th>
                            <td>@lang('lang.order_no') {{$dt->id}} </td>
                            <td>{{$dt->billing_name}} </td>
                            <td>{{ ($dt->user_id == 0)?'Guest':'Customer'}}</td>
                            <td>{{$dt->billing_phone }}</td>
                            <td>{{$dt->billing_email }}</td>
                            <td>{{$dt->orderDetail->count('order_id')}}</td>
                            <td>{{$dt->orderDetail->sum('quantity')}}</td>
                            <td>@lang('lang.currency'){{num_format($dt->total)}}</td>
                            <td>
                              <?php
                              $status = '';
                              $class = '';
                              if ($dt->status == 0) {
                                $status = 'Processing';
                                $class = 'label-warning';
                              } else if ($dt->status == 1) {
                                $status = 'Shipped';
                                $class = 'label-primary';
                              } else if ($dt->status == 2) {
                                $status = 'Delivered';
                                $class = 'label-success';
                              } else if ($dt->status == 3) {
                                $status = 'Cancelled';
                                $class = 'label-danger';
                              }
                              ?>
                              <label class="label <?= $class ?>"><?= $status ?></label>
                            </td>
                            <td>
                              <a href="{{route('admin.order.details', $dt->id)}}">
                                <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.view')"> <i class="fa fa-eye"></i> </button>
                              </a>

                              <div class="btn-group ">
                                <button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>
                                <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                <ul class="dropdown-menu">
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,0]) }}">Processing</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,1]) }}">Shipped</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,2]) }}">Delivered</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,3]) }}">Cancelled</a></li>

                                </ul>
                              </div>

                              <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$dt->id}}, '{{route('admin.order.delete', $dt->id)}}')" title="@lang('lang.delete')"> <i class="fa fa-trash"></i></button>
                              </a>
                            </td>
                          </tr>
                        <?php } ?>
                        @endforeach
                      <?php } ?>
                    </tbody>
                  </table>

                </div>
              </div>
              <div id="menu4" class="tab-pane fade">
                <div class="table">
                  <table class="table table-striped">
                    <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1); ?>
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">@lang('lang.order_no')</th>
                        <th scope="col">@lang('lang.name')</th>
                        <th scope="col">@lang('lang.user')</th>
                        <th scope="col">@lang('lang.phone')</th>
                        <th scope="col">@lang('lang.email')</th>
                        <th scope="col">@lang('lang.no_of_products')</th>
                        <th scope="col">@lang('lang.quantity')</th>
                        <th scope="col">@lang('lang.amount')</th>
                        <th scope="col">@lang('lang.status')</th>
                        <th scope="col">@lang('lang.action')</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (isset($error)) { ?>
                        <tr>
                          <th colspan="6">
                            @lang('lang.data_not_found')
                          </th>
                        </tr>
                      <?php } else { ?>
                        @foreach($data as $dt)
                        <?php if ($dt->status == 4) { ?>
                          <tr>
                            <th scope="row">{{$i++}}</th>
                            <td>@lang('lang.order_no') {{$dt->id}} </td>
                            <td>{{$dt->billing_name}} </td>
                            <td>{{ ($dt->user_id == 0)?'Guest':'Customer'}}</td>
                            <td>{{$dt->billing_phone }}</td>
                            <td>{{$dt->billing_email }}</td>
                            <td>{{$dt->orderDetail->count('order_id')}}</td>
                            <td>{{$dt->orderDetail->sum('quantity')}}</td>
                            <td>{{env('CURRENCY')}}{{num_format($dt->total)}}</td>
                            <td>
                              <?php
                              $status = '';
                              $class = '';
                              if ($dt->status == 0) {
                                $status = 'Processing';
                                $class = 'label-warning';
                              } else if ($dt->status == 1) {
                                $status = 'Shipped';
                                $class = 'label-primary';
                              } else if ($dt->status == 2) {
                                $status = 'Delivered';
                                $class = 'label-success';
                              } else if ($dt->status == 3) {
                                $status = 'Cancelled';
                                $class = 'label-danger';
                              }
                              else if ($dt->status == 4) {
                                $status = 'Payment Failed';
                                $class = 'label-danger';
                              }
                              ?>
                              <label class="label <?= $class ?>"><?= $status ?></label>
                            </td>
                            <td>
                              <a href="{{route('admin.order.details', $dt->id)}}">
                                <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.view')"> <i class="fa fa-eye"></i> </button>
                              </a>

                              <!-- <div class="btn-group ">
                                <button class="btn btn-white btn-demo-space"> <i class="fa fa-calendar"></i> Change Status</button>
                                <button class="btn btn-white dropdown-toggle btn-demo-space" data-toggle="dropdown"> <span class="caret"></span> </button>
                                <ul class="dropdown-menu">
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,0]) }}">Processing</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,1]) }}">Shipped</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,2]) }}">Delivered</a></li>
                                  <li class="divider"></li>
                                  <li class="dropdown-item"><a href="{{ route('admin.order.status_change',[$dt->id,3]) }}">Cancelled</a></li>

                                </ul>
                              </div> -->

                              <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$dt->id}}, '{{route('admin.order.delete', $dt->id)}}')" title="@lang('lang.delete')"> <i class="fa fa-trash"></i></button>
                              </a>
                            </td>
                          </tr>
                        <?php } ?>
                        @endforeach
                      <?php } ?>
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!--End Row-->
  <?php if (isset($data)) { ?>
    {!! $data->render() !!}
  <?php } ?>
</div>
<!-- End container-fluid-->

</div>
<!--End content-wrapper-->
@endsection
@section('footer')
<script src="{{ asset('public/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script>
  $(function() {
    $(".navigation_tabs li").click(function() {
        $("li").removeClass("active");
        $(this).addClass("active");
    });
  });
  $('#dateragne-picker .input-daterange').datepicker({});

  function report(url) {
    $('#report_form').attr('action', url).submit();
    $("#report_form").submit();
  }
</script>
@endsection