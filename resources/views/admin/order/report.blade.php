<head>
    <title>Order Report {{@dt_format($from)}} - {{@dt_format($to)}}</title>
<style>
    .div-controls{
        margin-left: 2%;
        margin-right: 2%;
    }
    .font-controls{
        font-size:14px;
        font-family:'AvenirLTStd-Light';
    }
    .table_align{
        margin-left: 2%;
        margin-right: 2%;
    }
    .pb-10{
        padding-bottom:10px;
    }
    .receipt-table1-th-details {
        border: 1px solid #e1e1e1; 
        color: #000;
        font-size:14px;
        font-family:'AvenirLTStd-Light';
        font-weight:bold;
    }
    .receipt-table1-td-details {
        border:1px solid #e1e1e1;
        color: #000;
        font-size:14px;
        font-family:'AvenirLTStd-Light';
    }
    .receipt-table2-th-details {
        border: 1px solid #e1e1e1; 
        color: #000;
        font-size:14px;
        font-family:'AvenirLTStd-Light';
        font-weight:bold;
    }
    .receipt-table2-td-details {
        border:1px solid #e1e1e1;
        color: #000;
        font-size:14px;
        font-family:'AvenirLTStd-Light';
    }
    .customer-headng {
        color: #000;
        font-size:18px;
        font-family:'AvenirLTStd-Light';
    }

</style>
</head>
    <div class="div-controls" style="text-align: center" >
        
        <!-- <h1 style="text-align:center;color:#2af;font-family:'AvenirLTStd-Light';">Rich House Report</h1>-->
        
        <!-- <h1 style="text-align:center;color:#2af;font-family:'AvenirLTStd-Light';">Rich House Report</h1>-->
        <img src="<?= asset('public/website/assets/media/images/logo_report.jpg') ?>"  >    
    </div>
    

    <table style="border-collapse: collapse;" width="100%" class="table_align" >
        <tr> 
            <td class="font-controls" colspan="2" align="left" width="200px" style="padding:10px;"  >
                <h4 class="customer-headng">Orders Report</h4>
            </td>
        </tr>
        @if(isset($from) && $from != '' && $to != '' && isset($to))
  
        <tr>
            <td class="font-controls pb-10 " align="center" width="100px"  style="padding:10px;"  >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:900;">From Date </span>
            </td>

            <td class="font-controls pb-10 " align="center" width="100px"  style="padding:10px;"  >
            </td>
            
            <td class="font-controls pb-10" align="center" width="100px" style="padding:10px;"   >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:900;">To Date </span>
            </td>
        </tr>
        
        <tr>
            <td class="font-controls pb-10" align="center" width="100px" bgcolor="#d93" style="padding:10px;" >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:900;color:#fff;">
                    {{@dt_format($from)}}
                </span>
            </td>

            <td class="font-controls pb-10 " align="center" width="100px"  style="padding:10px;"  >
            </td>

            <td class="font-controls pb-10" align="center" width="100px" bgcolor="#d93" style="padding:10px;" >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:bold;color:#fff;">{{@dt_format($to)}}</span>  
            </td>
        </tr>
        @endif
    </table>

    <br/>
    
    <table style="border-collapse: collapse;" width="100%" class="table_align">
      <thead>
    
          <tr>
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">#</th>

            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">Name</th>
    
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">Phone</th>
            
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">No Of  Products</th>
    
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">Quantity</th>
            
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">Amount</th>
                
          
          </tr>
    
      </thead>
      <tbody>
        @php($i = 1)
        @php($total = 0)
        @foreach($data as $dt)
         @php($total = $total + $dt->total)
        
        <tr>
            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$i++}}</td>

            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->billing_name}}</td>

            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->billing_phone}}</td>
            
            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->orderDetail->count('order_id')}}</td>
            
            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->orderDetail->sum('quantity')}}</td>
          
            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">@lang('lang.currency') {{num_format($dt->total)}}</td>           
          
            
        </tr>

         @endforeach
      </tbody>
    
    </table>

    <br/>
    
    <table style="border-collapse: collapse;" width="100%" class="table_align">
        <tr>
            <td class="font-controls pb-10" align="left" width="250px" style="padding:10px;" >
            </td>
            
            <td class="font-controls pb-10 receipt-table1-td-details" align="center" width="100px" style="padding:10px;"  >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:bold;">Total Amount </span>
            </td>
        </tr>
        <tr>
            <td class="font-controls pb-10" align="left" width="100px" style="padding:10px;">   
            </td>
            <td class="font-controls pb-10 receipt-table1-td-details" align="center" width="100px" style="padding:10px;">
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:900;color:#000;">@lang('lang.currency') {{$total}}</span>  
            </td>
        </tr>
    </table>

<!-- <div class="table">
  @if(isset($from) && $from != '' && $to != '' && isset($to))
  @lang('lang.from') : {{@dt_format($from)}} - @lang('lang.to') : {{@dt_format($to)}}
  @endif
   <table border="1" cellpadding="10" class="table table-striped">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col" style="font-family:Amiri-Regular;">@lang('lang.name')</th>
          <th scope="col">@lang('lang.phone')</th>
          <th scope="col">@lang('lang.no_of_products')</th>
          <th scope="col">@lang('lang.quantity')</th>
          <th scope="col">@lang('lang.amount')</th>
        </tr>
      </thead>
      <tbody>
        <?php if(isset($error)){?>
        <tr>
          <th colspan="6">
           @lang('lang.data_not_found')
          </th>
        </tr>
        <?php } else{ $i=1;?>
        @foreach($data as $dt)
        <tr>
          <th scope="row">{{$i++}}</th>
          <td>{{$dt->billing_name}} </td>
          <td>{{$dt->billing_phone }}</td>
          <td>{{$dt->orderDetail->count('order_id')}}</td>
          <td>{{$dt->orderDetail->sum('quantity')}}</td>
          <td>@lang('lang.currency') {{num_format($dt->orderDetail->sum('amount'))}}</td>
        </tr>
        @endforeach
        <?php } ?>
      </tbody>
    </table>

</div> -->