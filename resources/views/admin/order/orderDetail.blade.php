@extends('layouts.layout')
@section('content')
<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">@lang('lang.order') @lang('lang.details')</h4>
            <ol class="breadcrumb">
              <li class="breadcrumb-item active" aria-current="page">@lang('lang.order')</li>
              <li class="breadcrumb-item active" aria-current="page">
               <a href="{{route('admin.order.manage')}}">@lang('lang.manage')</a>
              </li> 
              <li class="breadcrumb-item active" aria-current="page">@lang('lang.details')</li> 
            </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
            @include('includes.alert')
            <div class="card">
              <div class="card-body"> 
                <ul class="nav nav-tabs nav-tabs-primary">
                  <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#tabe-1"><i class="icon-user"></i> <span class="hidden-xs">@lang('lang.billing') @lang('lang.address')</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabe-2"><i class="icon-basket-loaded icons"></i> <span class="hidden-xs">@lang('lang.order') @lang('lang.details')</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tabe-3"><i class="icon-rocket icons"></i> <span class="hidden-xs">@lang('lang.shipping') @lang('lang.address')</span></a>
                  </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  <div id="tabe-1" class="container tab-pane active show">
                   <div class="col-lg-12">
                   
                   </div>
                    <table class="table table-bordered">
                      <tbody>
                        <?php if(isset($error)){?>
                        <tr>
                          <th colspan="2">
                            @lang('lang.data_not_found')
                          </th>
                        </tr>
                        <?php } else{
                         $client =  @$data[0]->order[0];
                         ?>
                        
                        <tr>
                          <th scope="col">@lang('lang.date')</th>
                          <td>{{date('d-M-Y', strtotime(@$client->created_at))}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.name')</th>
                          <td>{{@$client->billing_name}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.email')</th>
                          <td>{{@$client->billing_email}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.phone')</th>
                          <td>{{@$client->billing_phone}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.country')</th>
                          <td>
                          @if(isset($shipping_data->shipping_text_all))
                          @for($i = 0; $i< count(@$shipping_data->shipping_text_all); $i++)
                            <?php if(@$shipping_data->shipping_text_all[$i]['id'] == @$client->billing_country){ 
                              ?>

                              {{ @$shipping_data->shipping_text_all[$i]['country'] }}
                            <?php } ?>
                          @endfor  
                          @endif
                          </td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.address')</th>
                          <td>{{@$client->billing_address}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.city')</th>
                          <td>{{@$client->billing_city}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.state')</th>
                          <td>{{@$client->billing_state}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.zip')</th>
                          <td>{{@$client->billing_zip}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.gift_note')</th>
                          <td>{{@$client->billing_message}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.gift_message')</th>
                          <td><?= (@$client->billing_wrapping==1)?'Yes':'No'; ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  <div id="tabe-2" class="container tab-pane fade">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">@lang('lang.order_id')</th>
                          <th scope="col">@lang('lang.title')</th>
                          <th scope="col">@lang('lang.variant')</th>
                          <th scope="col">@lang('lang.quantity')</th>
                          <th scope="col">@lang('lang.payment_method')</th>
                          <th scope="col">@lang('lang.amount')</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php
                        $c =1;
                        @endphp
                        @if(isset($data))
                        @foreach($data as $orders)
                        <tr>
                          <th scope="row">{{$c++}}</th>
                          <td>{{$orders->order_id}}</td>
                          <td>
                            {{$orders->orderProduct->product_text->title}}
                            @if(isset($orders->OrderVariationDetail))
                              @foreach($orders->OrderVariationDetail as $val)
                                 <p><b>{{$val->var_key}}</b> {{$val->var_value}}</p>
                              @endforeach
                            @endif
                          </td>
                          <td>
                            <?php if($orders->variant_id != 0 && $orders->variant_id != NULL){ ?>
                          @foreach(@$orders->orderProduct->productvariant as $k => $v)
                              <?php
                                if($v['id'] == $orders->variant_id)
                                {
                                  echo $v->product_variant_text['title'].' '.@$orders->orderProduct->weight->title;
                                }
                              ?>
                          @endforeach
                              <?php }else
                              {
                                echo @$orders->orderProduct->weight_value.' '.@$orders->orderProduct->weight->title;
                              } ?>
                          </td>
                          <td>{{$orders->quantity}}</td>
                          <td>{{payment_method($client->has_payment)}}</td>
                          <td align="right">@lang('lang.currency') {{num_format($orders->amount)}}</td>
                        </tr>
                        @endforeach
                        @endif
                        <tr align="right">
                          <th colspan="6">@lang('lang.net_amount')</th>
                          <td>@lang('lang.currency') {{num_format($data->sum('amount'))}}</td>
                        </tr>
                        <tr align="right">
                          <th colspan="6">@lang('lang.tax')</th>
                          <td>{{@$client->tax}}%</td>
                        </tr>
                        <tr align="right">
                          <th colspan="6">@lang('lang.shipping') @lang('lang.method')</th>
                          <td>{{ $shipping_data->method }}</td>
                        </tr>
                        <tr align="right">
                          <th colspan="6">@lang('lang.shipping') @lang('lang.charges')</th>
                          <td>@lang('lang.currency') {{(@$client->shipping_charges)? num_format(@$client->shipping_charges) : 0.00}}</td>
                        </tr>
                        <tr align="right">
                          <th colspan="6">@lang('lang.total_amount')</th>
                          <td>@lang('lang.currency') {{num_format(@$client->total)}}</td>
                        </tr>
                        <tr align="right">
                         <th colspan="6">@lang('lang.status')</th>
                           <td>
                           <?php
                        $status = '';
                        $class='';
                          if(@$orders->status == 0)
                          {
                            $status = 'Processing';
                            $class='label-warning';
                          }
                          else if(@$orders->status == 1)
                          {
                            $status = 'Shipped';
                            $class='label-primary';
                          }else if(@$orders->status == 2)
                          {
                            $status = 'Delivered';
                            $class='label-success';
                          }else if(@$orders->status == 3)
                          {
                            $status = 'Cancelled';
                            $class='label-danger';
                          }
                        ?>
                        <label class="label <?= $class?>"><?= $status?></label>
                           </td>
                         </tr>
                         <tr align="right">
                          <th colspan="6">@lang('lang.coupon')</th>
                          <td><?= (@$order->coupon_id != 0 && @$order->coupon_id != NULL)?'Yes':'No'?></td>
                        </tr>
                        <?php if(@$order->coupon_id != 0 && @$order->coupon_id != NULL){ ?>
                        <tr align="right">
                        <th colspan="6">@lang('lang.coupon_code')</th>
                          <td><?= $order->coupon['CouponCode']?></td>
                        </tr>
                        <tr align="right">
                        <th colspan="6">@lang('lang.coupon') @lang('lang.discount')</th>
                          <td><?= $order->coupon['DiscountPercentage']?>%</td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                </div>
                <div id="tabe-3" class="container tab-pane fade">
                  <!--<form id="shipping-charges" class="ajaxForm validate" method="post"  action="{{route('admin.order.shipping')}}" >
                    @csrf
                    <p>@lang('lang.shipping') @lang('lang.charges')</p>   
                    <div class="input-group mb-3">
                      <input type="text" value="{{@$client->shipping_charges}}" class="form-control invt" name="shipping_charges">
                      <input type="hidden" name="id" value="{{@$client->id}}">
                    </div> 
                    <button type="submit" id="shipping_charges_create" class="ajaxFormSubmit  btn create btn-primary create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
                  </form> -->
                   <table class="table table-bordered">
                      <tbody>
                        <?php if(isset($error)){?>
                        <tr>
                          <th colspan="2">
                            @lang('lang.data_not_found')
                          </th>
                        </tr>
                        <?php } else{
                         $client =  @$data[0]->order[0];
                         ?>
                         @if(@$client->has_shipping_address == 0)
                         <tr  align="center">
                          <th colspan="2">
                            Shipping and billing address is same.
                          </th>
                        </tr>
                        @else
                         <tr>
                          <th scope="col">@lang('lang.name')</th>
                          <td>{{@$client->shipping_name}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.email')</th>
                          <td>{{@$client->shipping_email}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.phone')</th>
                          <td>{{@$client->shipping_phone}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.country')</th>
                          <td>{{@$client->shipping_country}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.address')</th>
                          <td>{{@$client->shipping_address}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.city')</th>
                          <td>{{@$client->shipping_city}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.state')</th>
                          <td>{{@$client->shipping_state}}</td>
                        </tr>
                        <tr>
                          <th scope="col">@lang('lang.zip')</th>
                          <td>{{@$client->shipping_zip}}</td>
                        </tr>
                        @endif
                        
                        <?php } ?>
                      </tbody>
                    </table> 
                </div> 
              </div>
           </div>
  </div>
  </div>

@endsection
