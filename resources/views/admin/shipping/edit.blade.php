@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">{{@$title}}</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.setting')</li>
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.edit') {{@$title}}</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        @include('includes.alert')
        <div class="card">
          <div class="card-header text-uppercase">{{@$title}}</div>
          <div class="card-body">
            <form id="product-create" class="ajaxForm validate" method="post" action="{{route('admin.shipping.post')}}">
              @csrf

              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.shipping') @lang('lang.method')</p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" value="{{@$data->method}}" name="method">
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>
              <div class="form-group">
                <div class="row">
                  <div class="col-md-12">
                    <div class="responsive-scroll" style="width: 100%;">
                      <table class="table table-bordered" style="overflow-x:scroll">
                        <thead>
                          <tr>
                            <th class="text-center" style="width:60px;">@lang('lang.add')/@lang('lang.remove') Row</th>
                            <th class="text-center" style="min-width:200px;">@lang('lang.country') (@lang('lang.english'))</th>
                            <th class="text-center" style="min-width:200px;">@lang('lang.country') (@lang('lang.arabic'))</th>
                            <th class="text-center" style="min-width:200px;">@lang('lang.rate')</th>
                            <th class="text-center" style="min-width:50px;">COD</th>
                          </tr>
                        </thead>
                        <tr class="txtMult">
                          <td class="text-center"><a href="javascript:void(0);" class="addCF">@lang('lang.add')</a></td>
                          <td colspan="5"></td>
                        </tr>

                        <tbody id="customFields">

                          @php($k = 1)
                          @if(!empty(@$data->id) && @$data->id != null)
                          @for($i = 0; $i< count(@$data->shipping_text_all); $i++)
                            <tr class="txtMult">
                              <td class="text-center" style="width:40px;"><a href="javascript:void(0);" class="remCF">@lang('lang.remove')</a></td>

                              <td>
                                <input type='text' class='' required value="{{ @$data->shipping_text_all[$i]['country'] }}" style='width:100%' id='country1" + x + "' data-optional='0' name='country1[]' />
                              </td>

                              <td>
                                <input type='text' dir="rtl" class='' required value="{{ @$data->shipping_text_all[$i+1]['country'] }}" style='width:100%' id='country2" + x + "' data-optional='0' name='country2[]' />
                              </td>

                              <td>
                                <input type='text' class='allow_decimal rate' value="{{ @$data->shipping_text_all[$i]['rate'] }}" required style='width:100%' id='rate" + x + "' data-optional='0' name='rate[]' />
                              </td>

                              <td align="center">
                                  <input type="checkbox" id='basic_checkbox_<?= $i?>' name="cod[]" {{ (isset($data->shipping_text_all[$i]['cod']) && $data->shipping_text_all[$i]['cod'] == 1)? 'checked' : ''}} />
                                  <label for='basic_checkbox_<?= $i?>'></label>
                              </td>



                            </tr>
                            @php($i ++)
                            @php($k ++)
                            @endfor
                            @endif
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

              <input type="hidden" name="id" value="{{@$data->id}}">
              <button type="submit" class="btn btn-primary ajaxFormSubmit create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
            </form>
          </div>
        </div>
      </div>

    </div>
    <!-- End container-fluid-->
  </div>
  <!--End content-wrapper-->
  @endsection
  @section('footer')
  <script>
    $(document).ready(function() {
      $("form.validate").validate({
        rules: {
          method: {
            required: true
          },
          "country1[]": {
            required: true,
          },
          "country2[]": {
            required: true,
          }


        },
        messages: {
          method: "This field is required.",
        },
        invalidHandler: function(event, validator) {
          //display error alert on form submit 
          //error("Please input all the mandatory values marked as red");

        },
        errorPlacement: function(label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');
        },
        highlight: function(element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control');
        },
        unhighlight: function(element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control');
        },
        success: function(label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
          icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');

        }
        // submitHandler: function (form) {
        // }
      });
    });

    var x = 0;
    var add_button = $(".addCF");
    $(add_button).click(function(e) {


      x++;
      e.preventDefault();
      $('form.validate').validate();
      var temp = '<tr class="txtMult">';
      temp += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';

      temp += '<td>';
      temp += "<input type='text' class='' required  style='width:100%' id='country1" + x + "' data-optional='0' name='country1[]' />";
      temp += '</td>';

      temp += '<td>';
      temp += "<input type='text' class='' required  style='width:100%' id='country2" + x + "' data-optional='0' name='country2[]' />";
      temp += '</td>';

      temp += '<td>';
      temp += "<input type='text' class='allow_decimal rate' required  style='width:100%' id='rate" + x + "' data-optional='0' name='rate[]' />";
      temp += '</td>';





      temp += '</tr>';

      $("#customFields").append(temp);
      $("#po_material_id" + x).select2();
      $("#unit" + x).select2();

    });

    $("#customFields").on('click', '.remCF', function() {
      $(this).parent().parent().remove();
    });
  </script>
  @endsection