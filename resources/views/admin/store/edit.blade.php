@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">{{@$title}}</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.store')</li>
          <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        @include('includes.alert')
        <div class="card">
          <div class="card-header text-uppercase">{{@$title}}</div>
          <div class="card-body">

          <div class="row">
                    <div class="col-md-12">
                    <p> @lang('lang.image')</p>   
                    <form enctype="multipart/form-data" method="post" action="{{route('admin.store.upload.image')}}" class="dropzone" id="dropzone">
                      @csrf
                        <div class="fallback dropzone">
                        <input name="file[]" id="file" type="file" multiple="multiple">
                        </div>
                        <input type="hidden" name="new_name" value="">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      </form> 
                    </div>
                  </div>
                  <label>Note: image format should be jpg & png, image dimentions should be 950 x 1050</label>
            <br />
            <form id="product-create" class="ajaxForm validate" method="post" action="{{route('admin.store.post')}}">
              @csrf

              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.name') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="name" value="{{@$data->name}}">
                  </div>
                </div>

                <div class="col-md-6">
                  <p>@lang('lang.email') </p>
                  <div class="input-group mb-3">
                    <input type="email" class="form-control invt" name="email" value="{{@$data->email}}">
                  </div>
                </div>


              </div>
              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.phone') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="phone" value="{{@$data->phone}}">
                  </div>
                </div>

                <div class="col-md-6">
                  <p>@lang('lang.whatsapp') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="whatsapp" value="{{@$data->whatsapp}}">
                  </div>
                </div>


              </div>

              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.address') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="address" value="{{@$data->address}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <p>@lang('lang.city') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="city" value="{{@$data->city}}">
                  </div>
                </div>
              </div>
              
              <div class="row">

                <div class="col-md-6">
                  <p>@lang('lang.days') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="days" value="{{@$data->days}}">
                  </div>
                </div>

                <div class="col-md-6">
                  <p>@lang('lang.timing') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="timing" value="{{@$data->timing}}">
                  </div>
                </div>

              </div>

              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.latitude') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="lat" value="{{@$data->lat}}">
                  </div>
                </div>

                <div class="col-md-6">
                  <p>@lang('lang.longitude') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="long" value="{{@$data->long}}">
                  </div>
                </div>

              </div>
              


              <input type="hidden" name="id" value="{{@$data->id}}">
              <input type="hidden" name="fileName"  id="fileName" value="{{isset($data->image) ? $data->image.'~~' : ''}}"/>
              <button type="submit" class="btn btn-primary ajaxFormSubmit create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
            </form>
          </div>
        </div>
      </div>

    </div>
    <!-- End container-fluid-->
  </div>
  <!--End content-wrapper-->
  <script>
   var allFile = new Array();
    var findImage  = "{{@$data->image}}";
    var image_path = "{{env('STORE_IMAGES')}}";
    allFile.push(findImage);
    var images = Array();
    images.push(findImage);
    if(images.size > 0){
      document.getElementById('fileName').value += findImage+"~~";
    }
   
</script>
  @endsection
  @section('footer')
  <script>
    $(document).ready(function() {
      $("form.validate").validate({
        rules: {
          title1: {
            required: true
          },
          title2: {
            required: true
          },
          descrption1: {
            required: true
          },
          descrption2: {
            required: true
          },

        },
        messages: {
          title1: "This field is required.",
          title2: "This field is required.",

        },
        invalidHandler: function(event, validator) {
          //display error alert on form submit 
          $('#fail').show();
          $('#fail-text').html("Please fill all mandatory fields.");

        },
        errorPlacement: function(label, element) { // render error placement for each input type  
          $(element).addClass("border-red");
        },
        highlight: function(element) { // hightlight error inputs
          $(element).removeClass('border-green').addClass("border-red");
        },
        unhighlight: function(element) { // revert the change done by hightlight
          $(element).removeClass('border-red').addClass("border-green");
        },
        success: function(label, element) {
          $(element).removeClass('border-red').addClass("border-green");

        }
        // submitHandler: function (form) {
        // }
      });
      image_delete("{{route('admin.store.remove.image')}}", 1);
    });
  </script>
  @endsection