@extends('layouts.layout')

@section('content')
<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">@lang('lang.store') @lang('lang.detail')</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.store')</li>
          <li class="breadcrumb-item active" aria-current="page">
            <a href="{{route('admin.product.manage')}}">@lang('lang.manage')</a>
          </li>
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.detail')</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-primary">
              <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#tabe-1"><i class="icon-home"></i> <span class="hidden-xs">@lang('lang.detail')</span></a>
              </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div id="tabe-1" class="container tab-pane active show">
                <table class="table table-bordered">
                  <tbody>
                    <?php if (isset($error)) { ?>
                      <tr>
                        <th colspan="3">
                          No Result Found
                        </th>
                      </tr>
                    <?php } else { ?>
                      <tr>
                        <th scope="col">@lang('lang.name')</th>
                        <td colspan='2'>{{ @$data->name }}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.email')</th>
                        <td>{{ @$data->email }} </td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.phone')</th>
                        <td><?= @$data->phone?> </td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.address')</th>
                        <td>{{ @$data->address }} </td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.timing')</th>
                        <td colspan='2'>{{ @$data->timing }}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.longitude')</th>
                        <td colspan='2'>{{ @$data->long }}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.latitude')</th>
                        <td colspan='2'>{{ @$data->lat }}</td>
                      </tr>
                      <tr>
                        <th scope="col">@lang('lang.image')</th>
                        <td colspan='2'>
                        <img style="width:300px;height:200px" class="d-block " src="{{env('STORE_IMAGES')}}{{ (file_exists(storage_path('store/').@$data->image) && @$data->image != '') ? @$data->image : 'dummy.jpeg' }}" alt="">
                        </td>
                      </tr>
                      
                      
                      
                    <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endsection