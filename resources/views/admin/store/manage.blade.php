@extends('layouts.layout')
@section('content')
<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">@lang('lang.manage') @lang('lang.store')</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.store')</li>
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.manage')</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <a href="{{route('admin.store.add')}}">
          <button type="button" class="pull-right btn btn-secondary waves-effect waves-light m-1" title="@lang('lang.add')">
            @lang('lang.add')
          </button>
        </a>
      </div>
      <div class="col-lg-12">
        @include('includes.alert')
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">@lang('lang.all') @lang('lang.store')</h5>
            <div class="table">
              <table class="table table-striped">
                <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1); ?>
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col" colspan="2">@lang('lang.name')</th>
                    <th scope="col" colspan="2">@lang('lang.phone')</th>
                    <th scope="col" colspan="2">@lang('lang.email')</th>
                    <th scope="col">@lang('lang.action')</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (isset($error)) { ?>
                    <tr>
                      <th colspan="6">
                        @lang('lang.data_not_found')
                      </th>
                    </tr>
                  <?php } else { ?>
                    @foreach($data as $products)
                    <tr>
                      <td>{{$i++}}</td>
                      
                      <td colspan="2" class="pro-title">{{@$products->name}}</td>
                      <td colspan="2" class="pro-title">{{@$products->phone}}</td>
                      <td colspan="2" class="pro-title">{{@$products->email}}</td>
                      <td class="action-buttons">
                        <a href="{{route('admin.store.details').'?id='.$products->id}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.view')"> <i class="fa fa-eye"></i> </button>
                        </a>
                        <a href="{{route('admin.store.edit', $products->id)}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.edit')"> <i class="fa fa-pen"></i></button>
                        </a>
                        <a>
                        <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$products->id}}, '{{route('admin.store.delete', $products->id)}}')" title="@lang('lang.delete')"> <i class="fa fa-trash"></i></button>
                        </a>
                      </td>
                    </tr>
                    @endforeach
                  <?php } ?>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>

    <!--End Row-->


    <?php if (isset($product)) { ?>
      {!! $product->render() !!}
    <?php } ?>
  </div>
  <!-- End container-fluid-->

</div>
<!--End content-wrapper-->
@endsection
@section('footer')
<script>
  $(document).ready(function() {
   
  });
</script>
@endsection