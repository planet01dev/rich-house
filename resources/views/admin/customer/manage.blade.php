@extends('layouts.layout')
@section('content')
<link href="{{ asset('public/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">@lang('lang.customers')</h4>
            <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item active" aria-current="page">Lead</li> -->
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">@lang('lang.all') @lang('lang.customers')</h5>
               <div class="row">
                   <div class="col-lg-4">
                   <form id="report_form" action="{{route('admin.customer.manage')}}" method="GET" target="_blank">
                     
                    <label>@lang('lang.select') @lang('lang.daterange')</label>
                    <div id="dateragne-picker">
                     <div class="input-daterange input-group">
                      <input type="text" class="form-control" value="{{isset($from)? $from : ''}}" name="from" />
                      <div class="input-group-prepend">
                       <span class="input-group-text">@lang('lang.to')</span>
                      </div>
                      <input type="text" class="form-control" name="to" value="{{isset($to)? $to : ''}}" />
                     </div>
                      <button  type="button" title="@lang('lang.list') @lang('lang.report')" onclick="report('{{route('admin.customer.manage')}}')" class="btn-sm btn-dark waves-effect waves-light m-1"> <i class="fa fa-search"></i></button>
                      <button  type="button"  onclick="report('{{route('admin.customer.manage')}}/report')" title="PDF @lang('lang.report')" class="btn-sm btn-dark waves-effect waves-light m-1"> <i class="fa fa fa-file-pdf-o"></i></button>
                   </div>
                   </form>
                 </div>
               </div>
               <br/>
              <div class="table">
               <table class="table table-striped">
               <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 20) - 20 + 1) : 1); ?>
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">@lang('lang.name')</th>
                      <th scope="col">@lang('lang.user')</th>
                      <th scope="col">@lang('lang.phone')</th>
                      <th scope="col">@lang('lang.email')</th>
                      <th scope="col">@lang('lang.country')</th>
                      <th scope="col">@lang('lang.city')</th>
                      <th scope="col">@lang('lang.state')</th>
                      <th scope="col">@lang('lang.address')</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php if (isset($error)) { ?>
                    <tr>
                      <th colspan="6">
                        @lang('lang.data_not_found')
                      </th>
                    </tr>
                  <?php } else { ?>
                    @foreach($data as $dt)
                    <tr>
                      <th scope="row">{{$i++}}</th>
                      <td>{{$dt->billing_name}}</td>
                      <td>{{ ($dt->user_id == 0)?'Guest':'Customer'}}</td>
                      <td>{{$dt->billing_phone}}</td>
                      <td>{{$dt->billing_email}}</td>
                      <td>{{$dt->billing_country}}</td>
                      <td>{{$dt->billing_city}}</td>
                      <td>{{$dt->billing_state}}</td>
                      <td>
                        <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1 trigger_modal" data-toggle="modal" data-target="#modal"> <i class="fa fa-search"></i> </button>
                        <textarea class="message" style="display: none;">{!! $dt->billing_address !!}</textarea> 
                      </td>
                    </tr>
                    @endforeach
                    <?php } ?>
                  </tbody>
                </table>

            </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
     
      <?php
    
    if (isset($data)) { ?>
      {!! $data->render() !!}
    <?php } ?>
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
@endsection
@section('footer')
<script src="{{ asset('public/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script>
  $('.trigger_modal').click(function(event) {
    var message = $(this).siblings('.message').text();
    var data = "<p>"+message+"</p>";
    $('.modal_body').html(data);
    $('.modal_header_icon').html('<i class="fa fa-map-marker"></i>');
    $('.modal_header_text').html("@lang('lang.address')");
  });
  $('#dateragne-picker .input-daterange').datepicker({
  });
  function report(url) {
    $('#report_form').attr('action', url).submit();
    $("#report_form").submit();
  }
</script>
@endsection
