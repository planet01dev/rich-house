@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">{{@$title}}</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">@lang('lang.setting')</li>
            <li class="breadcrumb-item active" aria-current="page">@lang('lang.edit') {{@$title}}</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
       <div class="row">
        <div class="col-lg-12">
         @include('includes.alert')
          <div class="card">
             <div class="card-header text-uppercase">{{@$title}}</div>
             <div class="card-body">
                 <form id="product-create" class="ajaxForm validate" method="post"  action="{{route('admin.setting.post.email')}}"> 
                  @csrf
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center" width="60px">@lang('lang.add')/@lang('lang.remove') </th>
                        <th class="text-center" width="200px">@lang('lang.email')</th>
                      </tr>
                    </thead>
                    <tbody id="customFields">
                      <tr class="txtMult">
                        <td class="text-center"><a href="javascript:void(0);" class="addCF">@lang('lang.add')</a></td>
                        <td colspan="6"></td>
                      </tr>                      
                      @if(@$data != null && !empty(@$data))
                        @php($k = 1)
                        @foreach(@$data as $row)
                          <tr class="txtMult">
                              <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">@lang('lang.remove')</a></td>
                              <td>
                                  <input type="text" class="email form-control invt" required style="width:100%" id="email_old{{$k}}" name="email[]" 
                                  value="{{@$row['email']}}" placeholder="Email" />
                              </td>
                          </tr>
                        @php($k++)
                        @endforeach
                      @endif
                    </tbody>
                  </table>
                  <button type="submit" class="btn btn-primary ajaxFormSubmit create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
                </form>                
            </div>
          </div>
        </div>

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
@endsection
@section('footer')
<script>
 $(document).ready(function() {
        jQuery.validator.addMethod("emailValidation", function( value, element ) {
            var regex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
            var key = value;

            if (!regex.test(key)) {
               return false;
            }
            return true;
        }, "Please enter a valid email address.");

        $("form.validate").validate({
            rules: {
                "email[]": {
                    required: true,
                    emailValidation : true,
                }
            },
            messages: {
                
            },
            invalidHandler: function(event, validator) {
                //display error alert on form submit
                error("Please input all the mandatory values marked as red");
    
            },
            errorPlacement: function(label, element) { // render error placement for each input type   
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                $('<span class="error"></span>').insertAfter(element).append(label);
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function(element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
                var parent = $(element).parent();
                parent.removeClass('success-control').addClass('error-control');
            },
            unhighlight: function(element) { // revert the change done by hightlight
                var icon = $(element).parent('.input-with-icon').children('i');
                icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                var parent = $(element).parent();
                parent.removeClass('error-control').addClass('success-control');
            },
            success: function(label, element) {
                    var icon = $(element).parent('.input-with-icon').children('i');
                    icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control');

                }
                // submitHandler: function (form) {
                // }
        });
        $('.select2', "form.validate").change(function() {
            $('form.validate').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });
    });
    $(document).ready(function(){
        var max_fields      = 6; 
        var add_button      = $("#customFields .addCF");
        var x = 1; 
        $(add_button).click(function(e){
          e.preventDefault();
          $('form.validate').validate();
            x++;
            var temp = '<tr class="txtMult">';
            temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
            temp    += "<td>";
            temp    += '<input type="text" class="email form-control invt" required  style="width:100%" id="email'+x+'" data-optional="0" name="email[]" value="" placeholder="" />';
            temp    += '</td>';
            temp    += '</tr>';
            $("#customFields").append(temp);
        });
        $("#customFields").on('click','.remCF',function(){
            $(this).parent().parent().remove();
        });
      
    });

</script>
@endsection
