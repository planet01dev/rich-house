@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">{{@$title}}</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">@lang('lang.setting')</li>
            <li class="breadcrumb-item active" aria-current="page">@lang('lang.edit') {{@$title}}</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
       <div class="row">
        <div class="col-lg-12">
         @include('includes.alert')
          <div class="card">
             <div class="card-header text-uppercase">{{@$title}}</div>
             <div class="card-body">
             <div class="row">
                    <div class="col-md-12">
                    <p>@lang('lang.section') @lang('lang.image')</p>   
                    <form enctype="multipart/form-data" method="post" action="{{route('admin.setting.upload.image.contact')}}" class="dropzone" id="dropzone">
                      @csrf
                        <div class="fallback dropzone">
                          <input name="file[]" id="file" type="file" multiple="multiple">
                        </div>
                        <input type="hidden" name="new_name" value="">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      </form> 
                    </div>
                  </div>
                  <label>Note: image format should be jpg & png, image dimentions should be 950 x 1050</label>
            <br />
                 <form id="product-create" class="ajaxForm validate" method="post"  action="{{route('admin.setting.post_contact')}}"> 
                  @csrf
                  <div class="row">
                      <div class="col-md-6">
                          <p>@lang('lang.phone')</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->phone}}" name="phone">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <p>Whatsapp</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->whatsapp}}" name="whatsapp">
                          </div>
                      </div>
                  </div>
                
                  <div class="row">
                      <div class="col-md-6">
                          <p>@lang('lang.timing')</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{@$data->timing}}" name="timing">
                          </div>
                      </div>
                  </div>
                  
                   <input type="hidden" name="id" value="{{@$data->id}}">
                   <?php
                    $image = ''; 
                    (isset($data->contact_image) && $data->contact_image != '') ? $image.=$data->contact_image.'~~' : '';
                  ?>
                   <input type="hidden" name="fileName"  id="fileName" value="{{ $image }}"/>
                  <button type="submit" class="btn btn-primary ajaxFormSubmit create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
                </form>                
            </div>
          </div>
        </div>

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <script>
   var allFile = new Array();
    var findImage  = "{{@$data->contact_image}}";
    var image_path = "{{env('CONTACT_IMAGES')}}";
    allFile.push(findImage);
    
    var images = Array();
    for (var i = 0; i < allFile.length; i++) {
      images.push(allFile[i]);
    }
    
  
</script>
@endsection
@section('footer')
<script>


 $(document).ready(function() {
  Dropzone.options.dropzone = {
  maxFiles: 1,
  accept: function(file, done) {
    console.log("uploaded");
    done();
  },
  init: function() {
    this.on("maxfilesexceeded", function(file){
        alert("No more files please!");
    });
  }
};
    $("form.validate").validate({
      rules: {
        phone:{
          required: true
        },
        timing:{
          required: true
        },
       
      }, 
      messages: {
       
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        //error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
      image_delete("{{route('admin.setting.remove.image.contact')}}", 1);
  });


</script>
@endsection
