@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">{{@$title}}</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">@lang('lang.setting')</li>
            <li class="breadcrumb-item active" aria-current="page">@lang('lang.edit') {{@$title}}</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
       <div class="row">
        <div class="col-lg-12">
         @include('includes.alert')
          <div class="card">
             <div class="card-header text-uppercase">{{@$title}}</div>
             <div class="card-body">
             <div class="row">
                    <div class="col-md-12">
                    <p>@lang('lang.section') @lang('lang.image')</p>   
                    <form enctype="multipart/form-data" method="post" action="{{route('admin.section.menu.upload.image')}}" class="dropzone" id="dropzone">
                      @csrf
                        <div class="fallback dropzone">
                          <input name="file[]" id="file" type="file" multiple="multiple">
                        </div>
                        <input type="hidden" name="new_name" value="">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      </form> 
                    </div>
                  </div>
                  <label>Note: image format should be jpg & png, image dimentions should be 950 x 1050</label>
            <br />
                 <form id="product-create" class="ajaxForm validate" method="post"  action="{{route('admin.setting.post')}}"> 
                  @csrf
                  <div class="row">
                      <div class="col-md-6">
                        <p>@lang('lang.company_name') (@lang('lang.english'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->setting_text_all[0]->title}}" name="title1">
                        </div> 
                      </div>
                      <div class="col-md-6">
                        <p>@lang('lang.company_name') (@lang('lang.arabic'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" value="{{@$data->setting_text_all[1]->title}}" class="form-control" name="title2" dir="rtl">
                        </div> 
                      </div>
                  </div>  
                  <div class="row">
                      <div class="col-md-6">
                          <p>@lang('lang.email')</p>   
                          <div class="input-group mb-3">
                            <input type="text" value="{{@$data->email}}" class="form-control invt" name="email">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <p>@lang('lang.website')</p>   
                          <div class="input-group mb-3">
                            <input type="text" value="{{@$data->website}}" class="form-control invt" name="website">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <p>@lang('lang.phone')</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->phone}}" name="phone">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <p>Whatsapp</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->whatsapp}}" name="whatsapp">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <p>Facebook URL</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->facebook}}" name="facebook">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <p>Instagram URL</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->instagram}}" name="instagram">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <p>Pinterest URL</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->pinterest}}" name="pinterest">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <p>Twitter URL</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->twitter}}" name="twitter">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <p>Youtube URL</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->youtube}}" name="youtube">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <p>Linkedin URL</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control invt" value="{{@$data->linkedin}}" name="linkedin">
                          </div>
                      </div>
                  </div>
                  
                  <div class="row">
                      <div class="col-md-6">
                          <p>@lang('lang.tax')%</p>   
                          <div class="input-group mb-3">
                            <input type="text" class=" num_field form-control invt" value="{{@$data->tax}}" name="tax">
                          </div>
                      </div>
                   
                       <div class="col-md-6">
                        <p></p>
                          <div class="input-group mb-3">
                            <input type="checkbox" name="is_cod" id="basic_checkbox_2" class="form-control" value="1" {{(@$data->is_cod == 1)? 'checked' : ''}} />
                           <label for="basic_checkbox_2">@lang('lang.active') COD</label>
                          </div>

                      </div>
                  </div>
                  
                  <div class="row">
                      <div class="col-md-6">
                          <p>@lang('lang.ticker_1')</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{@$data->ticker_1}}" name="ticker_1">
                          </div>
                      </div>
                      <div class="col-md-6">
                        <p>@lang('lang.ticker_2')</p>
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{@$data->ticker_2}}" name="ticker_2">
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <p>@lang('lang.ticker_3')</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{@$data->ticker_3}}" name="ticker_3">
                          </div>
                      </div>
                      <div class="col-md-6">
                        <p>@lang('lang.ticker_4')</p>
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{@$data->ticker_4}}" name="ticker_4">
                          </div>
                      </div>
                  </div>
                  
                  <div class="row">
                      <div class="col-md-6">
                          <p>@lang('lang.timing')</p>   
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" value="{{@$data->timing}}" name="timing">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <p>@lang('lang.below_section_heading') (@lang('lang.english'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->setting_text_all[0]->below_section_heading}}" name="below_section_heading1">
                        </div> 
                      </div>
                      <div class="col-md-6">
                        <p>@lang('lang.below_section_heading') (@lang('lang.arabic'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" value="{{@$data->setting_text_all[1]->below_section_heading}}" class="form-control" name="below_section_heading2" dir="rtl">
                        </div> 
                      </div>
                  </div>  
                  <div class="row">
                      <div class="col-md-6">
                        <p>@lang('lang.below_section_text') (@lang('lang.english'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->setting_text_all[0]->below_section_text}}" name="below_section_text1">
                        </div> 
                      </div>
                      <div class="col-md-6">
                        <p>@lang('lang.below_section_text') (@lang('lang.arabic'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" value="{{@$data->setting_text_all[1]->below_section_text}}" class="form-control" name="below_section_text2" dir="rtl">
                        </div> 
                      </div>
                  </div>  
                  <div class="row">
                      <div class="col-md-6">
                        <p>@lang('lang.below_section_button') (@lang('lang.english'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->setting_text_all[0]->below_section_button}}" name="below_section_button1">
                        </div> 
                      </div>
                      <div class="col-md-6">
                        <p>@lang('lang.below_section_button') (@lang('lang.arabic'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" value="{{@$data->setting_text_all[1]->below_section_button}}" class="form-control" name="below_section_button2" dir="rtl">
                        </div> 
                      </div>
                  </div>  
                  <p>@lang('lang.address')</p>   
                  <textarea class="summernoteEditor" class="form-control invt" name="address" id="address"> {!! @$data->address !!} </textarea>
                  <div class="row">
                      <div class="col-md-12">
                      <p>@lang('lang.section') @lang('lang.link') 1</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->section_1_link}}" name="section_1_link">
                        </div> 
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <p>@lang('lang.section') 1 (@lang('lang.english'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->setting_text_all[0]->section_1_text}}" name="section_1_text1">
                        </div> 
                      </div>
                      <div class="col-md-6">
                        <p>@lang('lang.section') 1 (@lang('lang.arabic'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" value="{{@$data->setting_text_all[1]->section_1_text}}" class="form-control" name="section_1_text2" dir="rtl">
                        </div> 
                      </div>
                  </div>  

                  <div class="row">
                      <div class="col-md-12">
                      <p>@lang('lang.section') @lang('lang.link') 2</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->section_2_link}}" name="section_2_link">
                        </div> 
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <p>@lang('lang.section') 2 (@lang('lang.english'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->setting_text_all[0]->section_2_text}}" name="section_2_text1">
                        </div> 
                      </div>
                      <div class="col-md-6">
                        <p>@lang('lang.section') 2 (@lang('lang.arabic'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" value="{{@$data->setting_text_all[1]->section_2_text}}" class="form-control" name="section_2_text2" dir="rtl">
                        </div> 
                      </div>
                  </div>  

                  <div class="row">
                      <div class="col-md-12">
                      <p>@lang('lang.section') @lang('lang.link') 3</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->section_3_link}}" name="section_3_link">
                        </div> 
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <p>@lang('lang.section') 3 (@lang('lang.english'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->setting_text_all[0]->section_3_text}}" name="section_3_text1">
                        </div> 
                      </div>
                      <div class="col-md-6">
                        <p>@lang('lang.section') 3 (@lang('lang.arabic'))</p>   
                        <div class="input-group mb-3">
                          <input type="text" dir="rtl" value="{{@$data->setting_text_all[1]->section_3_text}}" class="form-control" name="section_3_text2">
                        </div> 
                      </div>
                  </div>

                  <div class="row">
                  <!-- <div class="col-md-6">
                        <p>@lang('lang.product_label')</p>   
                        <div class="input-group mb-3">
                          <input type="text" class="form-control invt" value="{{@$data->product_label}}" name="product_label">
                        </div> 
                      </div>
                  </div>   -->
                  
                   <input type="hidden" name="id" value="{{@$data->id}}">
                   <?php
                    $image = ''; 
                    (isset($data->about_image) && $data->about_image != '') ? $image.=$data->about_image.'~~' : '';
                    (isset($data->section_1_image) && $data->section_1_image!='') ? $image.=$data->section_1_image.'~~' : '';
                    (isset($data->section_2_image) && $data->section_2_image!='') ? $image.=$data->section_2_image.'~~' : '';
                    (isset($data->section_3_image) && $data->section_3_image!='') ? $image.=$data->section_3_image.'~~' : '';
                  ?>
                   <input type="hidden" name="fileName"  id="fileName" value="{{ $image }}"/>
                  <button type="submit" class="btn btn-primary ajaxFormSubmit create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
                </form>                
            </div>
          </div>
        </div>

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <script>
   var allFile = new Array();
    var findImage  = "{{@$data->about_image}}";
    var findImage1  = "{{@$data->section_1_image}}";
    var findImage2  = "{{@$data->section_2_image}}";
    var findImage3  = "{{@$data->section_3_image}}";
    var image_path = "{{env('SECTION_IMAGES')}}";
    allFile.push(findImage);
    allFile.push(findImage1);
    allFile.push(findImage2);
    allFile.push(findImage3);
    
    var images = Array();
    for (var i = 0; i < allFile.length; i++) {
      images.push(allFile[i]);
    }
    
    // if(images.size > 0){
    //   document.getElementById('fileName').value += findImage1+"~~";
    //   document.getElementById('fileName').value += findImage2+"~~";
    //   document.getElementById('fileName').value += findImage3+"~~";
    // }
  
</script>
@endsection
@section('footer')
<script>


 $(document).ready(function() {
  Dropzone.options.dropzone = {
  maxFiles: 4,
  accept: function(file, done) {
    console.log("uploaded");
    done();
  },
  init: function() {
    this.on("maxfilesexceeded", function(file){
        alert("No more files please!");
    });
  }
};
    $("form.validate").validate({
      rules: {
        title1:{
          required: true
        },
        title2:{
          required: true
        },
       
      }, 
      messages: {
        category_name: "This field is required.",
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        //error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
      image_delete("{{route('admin.section.menu.remove.image')}}", 4);
  });


</script>
@endsection
