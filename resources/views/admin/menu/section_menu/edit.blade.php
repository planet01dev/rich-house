@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">{{@$title}}</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">@lang('lang.menu')</li>
            <li class="breadcrumb-item active" aria-current="page">
              <a href="{{route('admin.section.menu')}}">@lang('lang.section_menu')</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
       <div class="row">
        <div class="col-lg-12">
         @include('includes.alert')
          <div class="card">
             <div class="card-header text-uppercase">{{@$title}}</div>
             <div class="card-body">
                <form enctype="multipart/form-data" method="post" action="{{route('admin.section.menu.upload.image')}}" class="dropzone" id="dropzone">
                @csrf
                  <div class="fallback dropzone">
                    <input name="file[]" id="file" type="file" multiple="multiple">
                  </div>
                  <input type="hidden" name="new_name" value="">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form> 
                <label>Note: image format should be jpg & png, image dimentions should be 1080 x 1080</label>
                <br/>
                <form id="form-create" class="form validate ajaxForm" action="{{route('admin.section.menu.post')}}" method="post"> 
                 @csrf
                  
                  <p>@lang('lang.title')  (@lang('lang.english'))</p>   
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="title1" value="{{@$data->section_menu_text_all[0]->title}}">
                  </div>
                  <p>@lang('lang.title')  (@lang('lang.arabic'))</p>   
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="title2" dir="rtl" value="{{@$data->section_menu_text_all[1]->title}}">
                  </div>
                  <p>@lang('lang.section_no')</p>   
                  <div class="input-group mb-3">
                    <select name="section_no" class="form-control">
                      @for($c=1; $c<6; $c++)
                        <option value="{{$c}}" {{ (isset($data->section_no) && $data->section_no == $c)? 'selected' : '' }} > @lang('lang.section') {{$c}}  </option>
                      @endfor
                    </select>
                  </div>
                  <p>@lang('lang.permalink')</p>   
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="permalink" value="{{@$data->permalink}}">
                  </div>
                  <div class="input-group mb-3">
                    <input type="checkbox" id="basic_checkbox_2" name="is_active" {{ (isset($data->is_active) && $data->is_active == 1)? 'checked' : ''}} />
                    <label for="basic_checkbox_2">@lang('lang.active')</label>
                  </div>
                  <div class="input-group mb-3">
                      <input type="checkbox" id="other" name="other" {{ (isset($data->other) && $data->other == 1)? 'checked' : ''}} />
                      <label for="other">@lang('lang.other')</label>
                  </div>
                  <input type="hidden" name="id" value="{{@$data->id}}">
                  <input type="hidden" name="fileName"  id="fileName" value="{{isset($data->image) ? $data->image.'~~' : ''}}"/>
                  <button type="button" class="btn btn-primary create ajaxFormSubmit btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
                </form>                
            </div>
          </div>
        </div>

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
<script>
   var allFile = new Array();
    var findImage  = "{{@$data->image}}";
    var image_path = "{{env('SECTION_IMAGES')}}";
    allFile.push(findImage);
    var images = Array();
    images.push(findImage);
    if(images.size > 0){
      document.getElementById('fileName').value += findImage+"~~";
    }
   
</script>
@endsection
@section('footer')
<script>
   $(document).ready(function() {

    $("form.validate").validate({
      rules: {
        title:{
          required: true
        },
      }, 
      messages: {
        category_name: "This field is required.",
        category_is_active: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        //error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
      image_delete("{{route('admin.section.menu.remove.image')}}", 1);

  });
</script>

@endsection
