@extends('layouts.layout')
@section('content')
<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">@lang('lang.subscribe')</h4>
            <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item active" aria-current="page">Lead</li> -->
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">@lang('lang.all') @lang('lang.subscribe')</h5>
              <div class="table">
               <table class="table table-striped">
                 <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1);?>
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">@lang('lang.email')</th>
                      <th scope="col">@lang('lang.action')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $dt)
                    <tr>
                      <th scope="row">{{$i++}}</th>
                      <td>{{$dt->email}}</td>
                      <td>
                        <a href="{{route('admin.subscribe.edit_subscribe', $dt->id)}}">
                          <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" title="@lang('lang.edit')"> <i class="fa fa-pen"></i></button>
                        </a>
                        <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$dt->id}}, '{{route('admin.subscribe.delete', $dt->id)}}')" title="@lang('lang.delete')"> <i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>

            </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
     
 {!! $data->render() !!}
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
@endsection
@section('footer')

@endsection
