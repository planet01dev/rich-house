@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">{{@$title}}</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">@lang('lang.subscribe')</li>
            <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
       <div class="row">
        <div class="col-lg-12">
         @include('includes.alert')
          <div class="card">
             <div class="card-header text-uppercase">{{@$title}}</div>
             <div class="card-body">
                <form enctype="multipart/form-data" method="post" action="{{route('admin.subscribe.upload.image')}}" class="dropzone" id="dropzone">
                @csrf
                  <div class="fallback dropzone">
                    <input name="file[]" id="file" type="file" multiple="multiple">
                  </div>
                  <input type="hidden" name="new_name" value="">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form> 
                <label>SNote: image format should be jpg & png, image dimentions should be 500 x 500</label>
                <br/>
                <form id="form-create" class="form validate ajaxForm" action="{{route('admin.subscribe.post')}}" method="post"> 
                 @csrf
                  
                  <p>@lang('lang.title')  (@lang('lang.english'))</p>   
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="title1" value="{{@$data->subscribe_setting_text_all[0]->title}}">
                  </div>
                  <p>@lang('lang.title')  (@lang('lang.arabic'))</p>   
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="title2" dir="rtl" value="{!! @$data->subscribe_setting_text_all[1]->title !!}">
                  </div>
                  <p>@lang('lang.description') (@lang('lang.english'))</p>   
                  <textarea class="summernoteEditor" class="form-control invt" name="description1" id="description1"> {!! @$data->subscribe_setting_text_all[0]->description !!} </textarea>
                  <p>@lang('lang.description') (@lang('lang.arabic'))</p>   
                  <textarea class="summernoteEditor" class="form-control invt" dir="rtl" name="description2" id="description2"> {!! @$data->subscribe_setting_text_all[1]->description !!} </textarea>
                  <div class="input-group mb-3">
                    <input type="checkbox" id="basic_checkbox_2" name="is_active" {{ (isset($data->is_active) && $data->is_active == 1)? 'checked' : ''}} />
                    <label for="basic_checkbox_2">@lang('lang.active')</label>
                  </div>
                  <input type="hidden" name="id" value="{{@$data->id}}">
                  <input type="hidden" name="fileName"  id="fileName" value="{{isset($data->image) ? $data->image.'~~' : ''}}"/>
                  <button type="button" class="btn btn-primary create ajaxFormSubmit btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
                </form>                
            </div>
          </div>
        </div>

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
<script>
   var allFile = new Array();
    var findImage  = "{{@$data->image}}";
    var image_path = "{{env('SECTION_IMAGES')}}";
    allFile.push(findImage);
    var images = Array();
    images.push(findImage);
    if(images.size > 0){
      document.getElementById('fileName').value += findImage+"~~";
    }
   
</script>
@endsection
@section('footer')
<script>
   $(document).ready(function() {

    $("form.validate").validate({
      rules: {
        title:{
          required: true
        },
      }, 
      messages: {
        category_name: "This field is required.",
        category_is_active: "This field is required."
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        //error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
      image_delete("{{route('admin.subscribe.remove.image')}}", 1);

  });
</script>

@endsection
