@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">{{@$title}}</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">@lang('lang.coupon')</li>
          <li class="breadcrumb-item active" aria-current="page">{{@$title}}</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        @include('includes.alert')
        <div class="card">
          <div class="card-header text-uppercase">{{@$title}}</div>
          <div class="card-body">

            <br />
            <form id="product-create" class="ajaxForm validate" method="post" action="{{route('admin.subscribe.post_subscribe')}}">
              @csrf

              <div class="row">
                <div class="col-md-6">
                  <p>@lang('lang.email') </p>
                  <div class="input-group mb-3">
                    <input type="text" class="form-control invt" name="email" value="{{@$data->email}}">
                  </div>
                </div>

              <input type="hidden" name="id" value="{{@$data->id}}">
              <button type="submit" class="btn btn-primary ajaxFormSubmit create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.submit')</button>
            </form>
          </div>
        </div>
      </div>

    </div>
    <!-- End container-fluid-->
  </div>
  <!--End content-wrapper-->
  @endsection
  @section('footer')
  <script>
    $(document).ready(function() {
      $("form.validate").validate({
        rules: {
          email: {
            required: true
          },
          

        },
        messages: {
          

        },
        invalidHandler: function(event, validator) {
          //display error alert on form submit 
          $('#fail').show();
          $('#fail-text').html("Please fill all mandatory fields.");

        },
        errorPlacement: function(label, element) { // render error placement for each input type  
          $(element).addClass("border-red");
        },
        highlight: function(element) { // hightlight error inputs
          $(element).removeClass('border-green').addClass("border-red");
        },
        unhighlight: function(element) { // revert the change done by hightlight
          $(element).removeClass('border-red').addClass("border-green");
        },
        success: function(label, element) {
          $(element).removeClass('border-red').addClass("border-green");

        }
        // submitHandler: function (form) {
        // }
      });
    });
  </script>
  @endsection