<title>Customer Report {{@dt_format($from)}} - {{@dt_format($to)}}</title>
<style>
    .div-controls{
        margin-left: 2%;
        margin-right: 2%;
    }
    .font-controls{
        font-size:14px;
        font-family:'AvenirLTStd-Light';
    }
    .table_align{
        margin-left: 2%;
        margin-right: 2%;
    }
    .pb-10{
        padding-bottom:10px;
    }
    .receipt-table1-th-details {
        border: 1px solid #e1e1e1; 
        color: #000;
        font-size:14px;
        font-family:'AvenirLTStd-Light';
        font-weight:bold;
    }
    .receipt-table1-td-details {
        border:1px solid #e1e1e1;
        color: #000;
        font-size:14px;
        font-family:'AvenirLTStd-Light';
    }
    .receipt-table2-th-details {
        border: 1px solid #e1e1e1; 
        color: #000;
        font-size:14px;
        font-family:'AvenirLTStd-Light';
        font-weight:bold;
    }
    .receipt-table2-td-details {
        border:1px solid #e1e1e1;
        color: #000;
        font-size:14px;
        font-family:'AvenirLTStd-Light';
    }
    .customer-headng {
        color: #000;
        font-size:18px;
        font-family:'AvenirLTStd-Light';
    }

</style>
    <div class="div-controls" style="text-align: center" >
        
        <!-- <h1 style="text-align:center;color:#2af;font-family:'AvenirLTStd-Light';">Rich House Report</h1>-->
        <img src="<?= asset('public/website/assets/media/images/logo_report.jpg') ?>"  >        
    </div>
    

    <table style="border-collapse: collapse;" width="100%" class="table_align" >
        <tr> 
            <td class="font-controls" colspan="2" align="left" width="200px" style="padding:10px;"  >
                <h4 class="customer-headng">Customer Report</h4>
            </td>
        </tr>
        @if(isset($from) && $from != '' && $to != '' && isset($to))
  
        <tr>
            <td class="font-controls pb-10 " align="center" width="100px"  style="padding:10px;"  >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:900;">From Date </span>
            </td>

            <td class="font-controls pb-10 " align="center" width="100px"  style="padding:10px;"  >
            </td>
            
            <td class="font-controls pb-10" align="center" width="100px" style="padding:10px;"   >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:900;">To Date </span>
            </td>
        </tr>
        
        <tr>
            <td class="font-controls pb-10" align="center" width="100px" bgcolor="#d93" style="padding:10px;" >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:900;color:#fff;">
                    {{@dt_format($from)}}
                </span>
            </td>

            <td class="font-controls pb-10 " align="center" width="100px"  style="padding:10px;"  >
            </td>

            <td class="font-controls pb-10" align="center" width="100px" bgcolor="#d93" style="padding:10px;" >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:bold;color:#fff;">{{@dt_format($to)}}</span>  
            </td>
        </tr>
        @endif
    </table>

    <br/>
    
    <table style="border-collapse: collapse;" width="100%" class="table_align">
      <thead>
    
          <tr>
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">S.No#</th>

            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">Name</th>
    
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">Phone</th>
            
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">Email</th>
    
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">Country</th>
            
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" style="">City</th>
    
            <th align="center" class="receipt-table1-th-details" height="50px" width="50px" >State</th>
            
          
          </tr>
    
      </thead>
      <tbody>
        @php($i = 1)
        @php($total = 0)
        @foreach($data as $dt)
         @php($total++)

        <tr>
            
            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$i++}}</td>

            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->billing_name}}</td>

            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->billing_phone}}</td>
            
            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->billing_email}}</td>
            
            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->billing_country}}</td>
          
            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->billing_city}}</td>
            
            <td align="center" class="receipt-table1-td-details" height="50px" width="50px" style="">{{$dt->billing_state}}</td>
            
          
            
        </tr>

         @endforeach
      </tbody>
    
    </table>

    <br/>
    
    <table style="border-collapse: collapse;" width="100%" class="table_align">
        <tr>
            <td class="font-controls pb-10" align="left" width="250px" style="padding:10px;" >
            </td>
            
            <td class="font-controls pb-10 receipt-table1-td-details" align="center" width="100px" style="padding:10px;"  >
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:bold;">Total Customer </span>
            </td>
        </tr>
        <tr>
            <td class="font-controls pb-10" align="left" width="100px" style="padding:10px;">   
            </td>
            <td class="font-controls pb-10 receipt-table1-td-details" align="center" width="100px" style="padding:10px;">
                <span style="font-size:14px;font-family:'AvenirLTStd-Light';font-weight:900;color:#000;">{{$total}}</span>  
            </td>
        </tr>
    </table>
