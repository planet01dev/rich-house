@extends('layouts.layout')
@section('content')
<link href="{{ asset('public/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <h4 class="page-title">@lang('lang.customers')</h4>
            <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item active" aria-current="page">Lead</li> -->
         </ol>
       </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <a href="{{route('admin.sign_up_customer.export')}}" title="Excel Report" class="btn-sm btn-dark waves-effect waves-light m-1"> <i class="fa fa-file-excel-o"></i> Export</a>
              <div class="table">
               <table class="table table-striped">
                 <?php $i = (isset($_GET['page']) ? (($_GET['page'] * 25) - 25 + 1) : 1);?>
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">@lang('lang.f_name')</th>
                      <th scope="col">@lang('lang.l_name')</th>
                      <th scope="col">@lang('lang.email')</th>
                      <th scope="col">@lang('lang.phone')</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $dt)
                    <tr>
                      <th scope="row">{{$i++}}</th>
                      <td>{{$dt->f_name}}</td>
                      <td>{{$dt->l_name}}</td>
                      <td>{{$dt->email}}</td>
                      <td>{{$dt->phone}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>

            </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->
     
 {!! $data->render() !!}
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
@endsection
@section('footer')
<script src="{{ asset('public/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script>
  $('.trigger_modal').click(function(event) {
    var message = $(this).siblings('.message').text();
    var data = "<p>"+message+"</p>";
    $('.modal_body').html(data);
    $('.modal_header_icon').html('<i class="fa fa-map-marker"></i>');
    $('.modal_header_text').html("@lang('lang.address')");
  });
  $('#dateragne-picker .input-daterange').datepicker({
  });
  function report(url) {
    $('#report_form').attr('action', url).submit();
    $("#report_form").submit();
  }
</script>
@endsection
