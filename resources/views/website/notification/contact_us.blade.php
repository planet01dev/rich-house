<!DOCTYPE html>
<html>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
      .div-controls{
          margin-left: 2%;
          margin-right: 2%;
      }
      .social-div1-controls{
        /*margin-left: 2%;*/
        display: inline-block;
        min-width: 370px;
        margin-right: -2px;
      }
      .social-div2-controls{
        display: inline-block;
        min-width: 370px;
        margin-right: -2px;
        margin-left: -2px;
      }
      .social-div3-controls{
        /*margin-right: 2%;*/
        display: inline-block;
        min-width: 370px;
        margin-left: -2px;
      }
      .font-controls{
          font-size:14px;
          font-family:'AvenirLTStd-Light'
      }
      .table_align{
          margin-left: 0;
          margin-right: 2%;
      }
      .pb-10{
          padding-bottom:10px;
      }
      .receipt-table1-th-details {
          border: 1px solid #e1e1e1; 
          color: #000;
          font-size:14px;
          font-family:'AvenirLTStd-Light';
          padding: 10px;
          font-weight:bold;
      }
      .receipt-table1-td-details {
          border:1px solid #e1e1e1;
          color: #000;
          font-size:14px;
          font-family:'AvenirLTStd-Light';
          padding: 10px;
      }
      .receipt-table2-th-details {
          border: 1px solid #e1e1e1; 
          color: #000;
          font-size:14px;
          font-family:'AvenirLTStd-Light';
          font-weight:bold;
      }
      .receipt-table2-td-details {
          border:1px solid #e1e1e1;
          color: #000;
          font-size:14px;
          font-family:'AvenirLTStd-Light';
      }
      .customer-headng {
          color: #000;
          font-size:18px;
          font-family:'AvenirLTStd-Light';
      }
      .address-th1{
        width: 400px;
      }
      .address-th2{
        width: 50px;
      }
      .address-th3{
        width: 400px;
      }
      .address-td1{
        width: 400px;
      }
      .address-td2{
        width: 50px;
      }
      .address-td3{
        width: 400px;
      }
      @media only screen and (max-width:428px){
        .address-th1{
          width: 150px;
        }
        .address-th2{
          width: 10px;
        }
        .address-th3{
          width: 150px;
        }
        .address-td1{
          width: 150px;
        }
        .address-td2{
          width: 10px;
        }
        .address-td3{
          width: 150px;
        }
      }

  </style>
<body style="margin:auto;"> 
    <div class="div-controls" style="text-align: center;padding: 5px;background-color: #A6A6A6;margin:auto;width:520px;" >
         <h1 style="text-align: center;color: #fff;font-size: 22px;">Contact Request Rich House.</h1>
    </div>

    <div class="div-controls" style="text-align: center;padding: 20px;background-color: #fff;height: auto;margin:auto;width:520px;">
        <table width="100%" cellpadding="5">
          <tbody>
            <tr>
              <th>Name</th>
              <td>{{$contactData->name}}</td> 
            </tr> 
            <tr>
              <th>Email</th>
              <td>{{$contactData->email}}</td> 
            </tr> 
            <tr>
              <th>Phone</th>
              <td>{{$contactData->phone}}</td> 
            </tr> 
            <tr>
              <th>Message</th>
              <td>{{$contactData->message}}</td> 
            </tr> 
          </tbody>  
        </table>   
    </div>

    <div class="div-controls" style="text-align: center;margin:auto;width:520px;" >
        <img class="" style="width:200px;max-width: 270px;" src="{{ asset('public/website/assets/media/images/logo.png') }}" alt=""/>    
    </div>
    
    <div class="div-controls" style="margin:auto;width:520px;">  
      <table style="width: 100%;border: 2px solid #A6A6A6;background-color: #A6A6A6;">
        <tr>
          <td width="600px;" align="right" style="background-color: #A6A6A6;color:#fff;border-bottom:1px solid #A6A6A6;padding: 5px;">
            <p style="font-size: 16px;margin:0;">
                Follow us on Instagram
            </p>
          </td>
          <td width="100px;" align="right" style="background-color: #A6A6A6; border-left:1px solid #A6A6A6; border-bottom:1px solid #A6A6A6;padding: 5px;">
            <a href="{{@$setting->instagram}}" target="_blank">
                <img src="{{ asset('public/website/assets/media/images/social-icons/instagram.png') }}" alt="">
            </a>
          </td>
        </tr>
      </table>
    </div>
    
</body>
</html>