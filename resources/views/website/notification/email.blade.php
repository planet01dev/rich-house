<!DOCTYPE html>
<html>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
      .div-controls{
          margin-left: 2%;
          margin-right: 2%;
      }
      .social-div1-controls{
        /*margin-left: 2%;*/
        display: inline-block;
        min-width: 370px;
        margin-right: -2px;
      }
      .social-div2-controls{
        display: inline-block;
        min-width: 370px;
        margin-right: -2px;
        margin-left: -2px;
      }
      .social-div3-controls{
        /*margin-right: 2%;*/
        display: inline-block;
        min-width: 370px;
        margin-left: -2px;
      }
      .font-controls{
          font-size:14px;
          /* font-family:'AvenirLTStd-Light'; */
      }
      .table_align{
          margin-left: 0;
          margin-right: 2%;
      }
      .pb-10{
          padding-bottom:10px;
      }
      .receipt-table1-th-details {
          border: 1px solid #e1e1e1; 
          color: #000;
          font-size:14px;
          /* font-family:'AvenirLTStd-Light'; */
          padding: 10px;
          font-weight:bold;
      }
      .receipt-table1-td-details {
          border:1px solid #e1e1e1;
          color: #000;
          font-size:14px;
          /* font-family:'AvenirLTStd-Light'; */
          padding: 10px;
      }
      .receipt-table2-th-details {
          border: 1px solid #e1e1e1; 
          color: #000;
          font-size:14px;
          /* font-family:'AvenirLTStd-Light'; */
          font-weight:bold;
      }
      .receipt-table2-td-details {
          border:1px solid #e1e1e1;
          color: #000;
          font-size:14px;
          /* font-family:'AvenirLTStd-Light'; */
      }
      .customer-headng {
          color: #000;
          font-size:18px;
          /* font-family:'AvenirLTStd-Light'; */
      }
      .address-th1{
        width: 400px;
      }
      .address-th2{
        width: 50px;
      }
      .address-th3{
        width: 400px;
      }
      .address-td1{
        width: 400px;
      }
      .address-td2{
        width: 50px;
      }
      .address-td3{
        width: 400px;
      }
      @media only screen and (max-width:428px){
        .address-th1{
          width: 150px;
        }
        .address-th2{
          width: 10px;
        }
        .address-th3{
          width: 150px;
        }
        .address-td1{
          width: 150px;
        }
        .address-td2{
          width: 10px;
        }
        .address-td3{
          width: 150px;
        }
      }
      #mi-width{
          width:50%;
      }
    

  </style>
<body style="margin:auto;" > 
    
    <div class="div-controls" style="text-align: center;width: 520px;margin: auto;" >
        <!-- <h1 style="text-align:center;color:#2af;font-family:'AvenirLTStd-Light';">Rich House Report</h1>-->
        <img class="" style="width:200px;max-width: 100%;" src="{{ asset('public/website/assets/media/images/logo.png') }}" alt="RICHHOUSE"/>    
    </div>
    @if ($isadmin == 0)
    <div class="div-controls" style="width: 520px;margin: auto;">
      <h2 style="font-weight: 600;text-align: center;font-size: 16px;text-decoration:capitalize;text-transform: capitalize;"><span>Hi {{ $order->billing_name }},</h2>
      <h3 style="font-weight: 600;text-align: center;font-size: 14px;"><span>Thank You for shopping with Richhouse</h3>        
    </div>
    @endif
   
    <div class="div-controls" style="text-align: center;padding: 0px 0px;background-color: #A9A9A9;width: 520px;margin: auto;" >
         <h1 style="text-align: center;color: #fff;font-size: 18px;">Order Number: {{$order->id}}</h1>
    </div>
    <div class="div-controls" style="text-align: center;padding: 5px 0px;background-color: #fff;width: 520px;margin: auto;" >
        <p style="text-align: left;color: #000;font-size: 12px;">@lang('lang.we_have_received_an_order')</p>
        <table style="border-collapse: collapse;" width="100%" class="table_align">
          <thead>      
              <tr>
                <th align="left" class="receipt-table1-th-details" height="30px" style="width:50px;min-width:50px;border-top: 1px solid #e1e1e1;border-left: 1px solid #e1e1e1;font-weight: bold;color: #000;font-size:14px;padding: 10px;">Item</th>

                <th height="30px" style="border: 1px solid #e1e1e1;border-right:none;border-left:none;min-width:100px;width:200px;">&nbsp;</th>

                <th align="right" class="receipt-table1-th-details" height="30px" style="min-width:100px;font-weight: bold;border: 1px solid #e1e1e1;color: #000;font-size:14px;padding: 10px;width:150px;">Price</th>
              </tr>
          </thead>
          <tbody>
            @foreach($order->orderDetail as $dt)
            <tr>
                <td align="" class="receipt-table1-td-details" height="30px" style="border: 1px solid #e1e1e1;color: #000;font-size:14px;padding: 10px;border-right:none;">
                    <img src="{{env('PRODUCT_IMAGES')}}{{@$dt->orderProduct->images[0]->image}}" width="100px" height="100px" alt=""> 
                </td>

                <td align="left" class="receipt-table1-td-details" height="30px" style="border: 1px solid #e1e1e1;color: #000;font-size:14px;padding: 10px;border-left:none;">
                    <p>{!! $dt->orderProduct->product_text->title !!}</p>
                    <p><?= limit_string(@$dt->orderProduct->product_text->brief_description, 30) ?></p>
                    <p>Size: <span style="font-weight:900;">
                    <?php if($dt->variant_id != 0 && $dt->variant_id != NULL){ ?>
                          @foreach(@$dt->orderProduct->productvariant as $k => $v)
                              <?php
                                if($v['id'] == $dt->variant_id)
                                {
                                  echo $v->product_variant_text['title'].' '.@$dt->orderProduct->weight->title;
                                }
                              ?>
                          @endforeach
                              <?php }else
                              {
                                echo @$dt->orderProduct->weight_value.' '.@$dt->orderProduct->weight->title;
                              } ?>
                    </span></p>
                    <p>Quantity: <span style="font-weight:900;">{{ $dt->quantity }}</span></p>
                    <p>Gift Note (Add-On): <span style="font-weight:900;">{{$order->billing_message}}</span> </p>
                    <p>Gift Wrapping (Add-On): <span style="font-weight:900;"><?= ($order->billing_wrapping==1)?'Yes':'No'; ?></span> </p> 
                </td>

                <td align="right" class="receipt-table1-td-details" height="30px" style="border: 1px solid #e1e1e1;color: #000;font-size:14px;padding: 10px;vertical-align:top;">
                    <p>{{num_format($dt->amount)}} @lang('lang.currency')</p> 
                </td>
            </tr>
            @endforeach
            <!--<tr>
                <td colspan="3" align="right">
                    <a href="{{route('web.home')}}" style="font-weight: 600;color: #fff;background: #db6;height: 50px;font-size: 14px;text-transform: uppercase;text-align: center;line-height: 50px;padding: 10px 20px;margin-top: 40px;border: 1px solid #db6;text-decoration:none;">VIEW ORDER DETAILS</a>
                </td>
            </tr>-->

            <tr>
                <td colspan="2" align="right">
                    <p style="font-weight:600;">@lang('lang.net_amount'):</p>
                    <p style="font-weight:600;">@lang('lang.shipping') @lang('lang.charges'):</p>
                    <p style="font-weight:600;">@lang('lang.tax'):</p>
                    <p style="font-weight:600;">Payment Method:</p>
                    <p style="font-weight:600;">@lang('lang.total_amount'):</p>
                    <p style="font-weight:600;">@lang('lang.status'):</p>
                </td>

                <td align="right">
                    <p><span>{{num_format($order->subtotal)}} @lang('lang.currency')</span></p>
                    <p><span>{{($order->shipping_charges)? num_format($order->shipping_charges) : 0.00}} @lang('lang.currency') </p>
                    <p><span>{{$order->tax}}%</span></p>
                    <p><span><?= ($order->has_payment == 2)?'COD':'Debit/Credit Card' ?></span></p>
                    <p><span>{{num_format($order->total)}} @lang('lang.currency')</span></p>
                    <?php
                      $status = '';
                      $class = '';
                      if ($order->status == 0) {
                        $status = 'Processing';
                      } else if ($order->status == 1) {
                        $status = 'Shipped';
                      } else if ($order->status == 2) {
                        $status = 'Delivered';
                      } else if ($order->status == 3) {
                        $status = 'Cancelled';
                      }
                    ?>
                    <p><?= $status; ?></p>
                </td>
            </tr>
            
          </tbody>
        </table>

        <table style="width: 100%;margin-top: 30px;">
          <thead>
            <tr>
              <th align="left" style="min-width:150px;width:200px;"><h4 style="text-transform: capitalize;">Billing address</h4></th>
              <th align="left" style="min-width:150px;width:200px;"><h4 style="text-transform: capitalize;">Shipping address</h4></th>
            </tr>
          </thead>
          <tbody style="border:1px solid #e5e5e5;">
            <tr>
              <td align="left" style="padding:5px 10px;border:1px solid #e1e1e1;vertical-align:top;">
                <p><b>Name:</b> {{$order->billing_name}}</p>
                <p><b>Email:</b> {{$order->billing_email}}</p>
                <p><b>Address:</b> {{$order->billing_address}}</p>
                <p><b>Phone:</b> {{$order->billing_phone}}</p>

              </td>
              <td align="left" style="padding:5px 10px;border:1px solid #e1e1e1;vertical-align:top;">
                
                  <?php
                    $has_address = $order->has_shipping_address;
                    if($has_address == 0){
                  ?>
                    <p><b>Name:</b> {{$order->billing_name}}</p>
                    <p><b>Email:</b> {{$order->billing_email}}</p>
                    <p><b>Address:</b> {{$order->billing_address}}</p>
                    <p><b>Phone:</b> {{$order->billing_phone}}</p>
                  <?php  
                  } else{
                  ?>
                    <p><b>Name:</b> {{$order->shipping_name}}</p>
                    <p><b>Email:</b> {{$order->shipping_email}}</p>
                    <p><b>Address:</b> {{$order->shipping_address}}</p>
                    <p><b>Phone:</b> {{$order->shipping_phone}}</p>
                    
                  <?php
                  }
                  ?>
              </td>
            </tr>
            
            <!--<tr>
              <td align="left" style="padding:2px 10px 0;border-right:1px solid #e1e1e1;border-left:1px solid #e1e1e1;">{{$order->billing_address}}</td>
              <td align="left" style="padding:2px 10px 10px;border-right:1px solid #e1e1e1;border-left:1px solid #e1e1e1;border-bottom:1px solid #e1e1e1; ">{{$order->shipping_address}}</td>
            </tr>
    
            <tr>
              <td align="left" style="padding:2px 10px 0;border-right:1px solid #e1e1e1;border-left:1px solid #e1e1e1;" >{{$order->billing_phone}}</td>
            </tr>
            <tr>
              <td align="left" style="padding:2px 10px 15px;border-right:1px solid #e1e1e1;border-left:1px solid #e1e1e1;border-bottom:1px solid #e1e1e1;" >{{$order->billing_email}}</td>
            </tr>-->
          </tbody>
        </table>
    </div>
    
      
    <table style="border: 2px solid #A9A9A9;background-color: #A9A9A9;width: 520px;margin: auto;">
    <tr>
        <td height="10px" align="center" style="width:388px;min-width:100px;padding: 5px;">
        <a href="{{@$setting->linkedin}}" target="_blank">
            <img src="{{ asset('public/website/assets/media/images/social-icons/linkedin.png') }}" alt="">
        </a>
        </td>
        <td height="10px" align="center" style="width:388px;min-width:100px;padding: 5px;">
        <a href="{{@$setting->instagram}}" target="_blank">
            <img src="{{ asset('public/website/assets/media/images/social-icons/instagram.png') }}" alt="">
        </a>
        </td>
        <td height="10px" align="center" style="width:388px;min-width:100px;padding: 5px;"><a href="{{@$setting->facebook}}" target="_blank">
            <img src="{{ asset('public/website/assets/media/images/social-icons/facebook.png') }}" alt="">

        </a>
    </td>
    </tr>
    </table>
    
    <div class="div-controls" style="text-align: center;padding: 0px 0px;width: 520px;margin: auto;" >
        <p style="text-align: center;color: #000;margin-bottom:0;font-size: 14px;">QUESTIONS ABOUT YOUR ORDER ?</p>
        <p style="text-align: center;color: #000;margin:0;font-size: 14px;">Our Customer Care team are available</p>
        <p style="text-align: center;color: #000;margin-top:0;font-size: 14px;">five days a week from 9am-7pm.</p>
        <p style="text-align: center;color: #000;margin-top:0;font-size: 14px;">For International Shipments, Import Fees may apply.</p>
        <div style="display:inline-block;margin:0 3px;">
            <a href="https://web.whatsapp.com/send?phone=+97145139747" target="_blank">
                <img src="{{ asset('public/website/assets/media/images/social-icons/whatsapp.png') }}" alt="" style="width:80px;height:100%;">
            </a>
        </div>
        <div style="display:inline-block;margin:0 3px;">
            <a href="mailto:orders@richhouse.me" target="_blank">
                <img src="{{ asset('public/website/assets/media/images/social-icons/email.png') }}" alt="" style="width:80px;height:100%;">
            </a> 
        </div>
    </div>
</body>
</html>