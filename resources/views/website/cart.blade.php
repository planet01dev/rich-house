@extends('layouts.web')
@section('content')
<!--=========================-->
      <!--=        Breadcrumb         =-->
      <!--=========================-->

      <section class="breadcrumb-area">
         <div class="container-fluid custom-container">
            <div class="row">
               <div class="col-xl-12">
                  <div class="bc-inner">
                     <p><a href="{{route('web.home')}}">Rich House /</a> <a href="{{route('cart')}}">@lang('lang.cart')</a></p>
                  </div>
               </div>
               <!-- /.col-xl-12 -->
            </div>
            <!-- /.row -->
         </div>
         <!-- /.container -->
      </section>

      <!--=========================-->
      <!--=        Breadcrumb         =-->
      <!--=========================-->

      <section class="cart-area">
         <div class="container-fluid custom-container">
            <div class="row">
               <div class="col-xl-9">
                  <div class="cart-table">
                     <table class="tables">
                        <thead>
                           <tr>
                              <th></th>
                              <th>@lang('lang.image')</th>
                              <th>@lang('lang.product') @lang('lang.name')</th>
                              <th>@lang('lang.quantity')</th>
                              <th>@lang('lang.price')</th>
                              <th>@lang('lang.total')</th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($product as $product)
                             <tr>
                                 <td class="product-remove"><a onclick="delete_data({!!$product['id']!!})" href="javascript:void(0)"><i class="fa fa-times"></i></a></td>
                                 <td class="product-thumbnail"><a href="javascript:void(0)">
                                     <img src="{{env('PRODUCT_IMAGES')}}{{$product['thumbnail']}}" alt="" class="cart-prod-img" /></a>
                                 </td>
                                 <td class="product-name"><a href="javascript:void(0)">{!!$product['title']!!}</a></td>
                                 <td class="product-quantity">
                                       <!--  <input type="number" class="qty" min="1" max="{{$product['inventory']}}" onclick="update_data({!!$product['id']!!})" value="{{$product['quantity']}}"/> -->
                                     <div class="quantity buttons_added">
                                        <input type="button" value="-" onclick="update_data({!!$product['id']!!})" class="quantity-left-minus">
                                        <input type="number" id="quantity" class="input-text qty_{!!$product['id']!!}  quantity text" min="1" max="{{$product['inventory']}}"  readonly="readonly"  value="{{$product['quantity']}}" >
                                        <input type="button" value="+" onclick="update_data({!!$product['id']!!})" class="quantity-right-plus">
                                      </div>
                                 </td>
                                 <td class="product-price"><span class="amount">
                                     @lang('lang.currency')
                                     {{$product['price']/$product['quantity']}} x <span class="pro_quantity">{{$product['quantity']}}</span>
                                 </span></td>
                                 <td class="product-subtotal">
                                     @lang('lang.currency')
                                     <span id="item_amount{{$product['id']}}">
                                      {{num_format($product['price'])}}    
                                     </span>
                                 </td>
                             </tr>
                           @endforeach
                         
                           <!-- /.single product  -->
                        </tbody>
                     </table>

                  </div>
                  <!-- /.cart-table -->
<!--                   <div class="row cart-btn-section">
                     <div class="col-12 col-sm-8 col-lg-6">
                        <div class="cart-btn-left">
                           <a class="coupon-code" href="#">Coupon Code</a>
                           <a href="#">Apply Coupon</a>
                        </div>
                     </div>
                     <div class="col-12 col-sm-4 col-lg-6">
                        <div class="cart-btn-right">
                           <a href="#">Update Cart</a>
                        </div>
                     </div>
                  </div> -->
               </div>
               <!-- /.col-xl-9 -->
 
               <div class="col-xl-3">
                  <div class="cart-subtotal">
                     <p>@lang('lang.subtotal')</p>
                     <ul>
                        <li>
                          <span class="cart-label">@lang('lang.subtotal'):</span>
                          <span class="cart-desc">@lang('lang.currency')<span class="product_total_sub_amount product_total_amount1"> {{$total_amount}}</span></span> 
                        </li style="<?= (@tax <= 0)?'display:none':'' ?>">
                         <li>
                          <span class="cart-label">@lang('lang.tax') :</span>
                          <span class="cart-desc">{{$tax}}%</span> 
                        </li>
                         <!-- <li><span class="cart-label">@lang('lang.shipping_method') :</span>{{@$shipping_method}} </li> -->
                        <li>
                          <span class="cart-label">@lang('lang.shipping') @lang('lang.charges') :</span> 
                          <span class="cart-desc"><span class="currency">@lang('lang.currency')</span> {{@$shipping_charges}} </span>
                        </li>
                        <!-- <li><span>Tax (-4.00):</span>$11.00</li>
                        <li><span>Shipping Cost:</span>$00.00</li> -->
                        <li>
                          <span class="cart-label">@lang('lang.total'):</span>
                          <span class="cart-desc">@lang('lang.currency')<span class="product_total_amount product_total_amount1"> {{$total_amount}}</span></span>
                        </li>
                     </ul>
                    <!--  <div class="note">
                        <span>Order Note :</span>
                        <textarea></textarea>
                     </div> -->
                      <input type="hidden" id="shipping" value="{{@$shipping_charges}}">
                     <input type="hidden" id="tax" value="{{$tax}}">
                     
                     <a href="{{route('web.checkout')}}" class="checkout-btn btn">@lang('lang.proceed_to_checkout')</a>
                  </div>
                  <!-- /.cart-subtotal -->


               </div>
               <!-- /.col-xl-3 -->
            </div>
         </div>
      </section>
      <!-- /.cart-area -->
@endsection
