@extends('layouts.web')
@section('content')
<!--=========================-->
<!--=        Breadcrumb     =-->
<!--=========================-->

<section class="breadcrumb-area">
	<div class="container-fluid custom-container">
		<div class="row">
			<div class="col-xl-12">
				<div class="bc-inner">
					<p><a href="{{route('web.home')}}">Rich House /</a> <a href="#">{{ @$data->page_text->title }}</a></p>
				</div>
			</div>
			<!-- /.col-xl-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>

<!--=========================-->
<!--=        Breadcrumb         =-->
<!--=========================-->



<!--Contact area
============================================= -->
<section class="contact-area">
	<div class="container-fluid custom-container">
		<div class="section-heading pb-30">
			<h3>{{ @$data->page_text->title }}</h3>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-8 col-lg-8 col-xl-6">
				@include('website.includes.alert')
				{!! @$data->page_text->content !!}
			</div>
		</div>
		<!-- /.row end -->
	</div>
	<!-- /.container-fluid end -->
</section>
<!-- /.contact-area end -->


@endsection
@section('footer')
<script>

   
</script>

@endsection
