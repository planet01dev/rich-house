@extends('layouts.web')
@section('content')
<div class="container custom-container">
    <div id="thanks">
        <!-- <div class="thanks-bg">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div> -->
        <div class="thanks">
            <div class="thank-you">
                <h1>@lang('lang.thank_you')</h1>
            </div>
            <h2>@lang('lang.for_more_shopping')</h2>
            <a href="{{route('web.home')}}">@lang('lang.continue_shopping')</a>
        </div>
    </div>
</div>
@endsection