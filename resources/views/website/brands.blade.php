@extends('layouts.web')
@section('content')
<section class="breadcrumb-area">
  <div class="container-fluid custom-container">
    <div class="row p-0">
      <div class="col-md-12 col-sm-12 col-xs-12 p-0 sec1-img">
        <div class="sin-banner align-items-center two-columns">
          <div class="mi-banner-top-text marquee">
          <?php
              $setting = get_settings();
            ?>
              <p>
                <?= $setting['ticker_1']?>
                <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
                <?= $setting['ticker_2']?>
                <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
                <?= $setting['ticker_3']?>
                <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
                <?= $setting['ticker_4']?>
              </p>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-6 col-sm-12 col-xs-12 p-0 sec1-img">
        <div class="sin-banner align-items-center two-columns">
          <div class="mi-banner-top-text marquee">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </div>
        </div>
      </div> -->
    </div>
    <div class="row">
      <div class="col-xl-12">
        <div class="bc-inner">
          <p><a href="{{route('web.home')}}">Rich House / </a> <a href="{{route('brands')}}"> @lang('lang.brands')  </a> </p>
        </div>
      </div>
      <!-- /.col-xl-12 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</section>

<!--=========================-->
<!--=        Shop area          =-->
<!--=========================-->

<section class="shop-area">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="shop-sorting-area row scents-section">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          	<h1 class="brand-title">@lang('lang.brands')</h1>
            <ul class="nav nav-tabs navi_tabs" role="tablist">
              <li class="active">
                <a href="#designers" class="tab-title" role="tab" data-toggle="tab">
                  @lang('lang.A_Z_Brands')
                </a>
              </li>
              {{-- <li>
                <a href="#categories" class="tab-title" role="tab" data-toggle="tab">
                  <i class="fa fa-list"></i> CATEGORIES
                </a>
              </li> --}}
              <li>
                <a href="#searchbyname" class="tab-title" role="tab" data-toggle="tab">
                    <i class="fa fa-search"></i> @lang('lang.search_by_name')
                </a>
              </li>
            </ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane fade active in show" id="designers">
                <div class="Brands-aToZ">
                  @foreach ($alphabets as $item)
                  <span class="Brands-keyContainer"><a class="Brands-scrollLink" href="#{{$item}}">{{$item}}</a></span>
                  @endforeach
                </div>
              </div>
              <div class="tab-pane fade" id="searchbyname">
                <div class="Brands-search">
                  <span class="Brands-searchIcon">
                    <i class="fa fa-search"></i>
                  </span>
                  <input placeholder="Search From Brands" id="brandSearch" value=""></div>
              </div>
            </div>
            <div class="Brands-content">
              @foreach ($brandsAlpha as $key => $item)
                  <div id="{{$key}}" class="Brands-group">
                    <h2 class="Brands-groupKey">{{$key}}</h2>
                    <div class="Brands-names">
                        @foreach ($item as $brand)
                          <p><a href="{{route('products.search',['tag_id'=>0,'brand_id'=>$brand->brand_text->brand_id])}}">{{$brand->brand_text->title}}</a></p>
                        @endforeach
                        
                    </div>
                  </div>
              @endforeach
          </div>
          </div>
        </div>
        <!-- /.shop-sorting-area -->
        
        <!-- /.shop-content -->
      </div>
      <!-- /.col- -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>
<!-- /.shop-area -->
@endsection
@section('footer')
<script>
  $('ul.navi_tabs li').click(function () {
    var $this = $(this);
    $(this).closest('li') // get current LI
    .addClass('active')
        .siblings() // get adjacent LIs
    .removeClass('active');
  });
</script>
@endsection
@push('custom-scripts')
    <script>
      $(document).ready(function(){
        $("#brandSearch").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $(".Brands-group").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
    </script>
@endpush