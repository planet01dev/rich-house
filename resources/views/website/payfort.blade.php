<?php
// How to calculate request signature
$shaString  = '';
$ref = 'richhouse'.time();
$ref = "TEST81002";
// array request
$arrData    = array(
'command'            =>'AUTHORIZATION',
'access_code'        =>'Bw9tgwnzRBjXWY3Dqhpu',
'merchant_identifier'=>'e1cf8f90',
'merchant_reference' => $ref,
'amount'             =>'10000',
'currency'           =>'AED',
'language'           =>'en',
'customer_email'     =>'test@richouse.com',
'order_description'  =>'iPhone 6-S',
);
// sort an array by key
ksort($arrData);
foreach ($arrData as $key => $value) {
    $shaString .= "$key=$value";
}
// make sure to fill your sha request pass phrase
$shaString = "63agQY..0LJZDQRvq6rmUZ@)" . $shaString . "63agQY..0LJZDQRvq6rmUZ@)";
$signature = hash("sha256", $shaString);
// your request signature
// echo $signature;
$requestParams = array(
    'command' => 'AUTHORIZATION',
    'access_code' => 'Bw9tgwnzRBjXWY3Dqhpu',
    'merchant_identifier' => 'e1cf8f90',
    'merchant_reference' => $ref,
    'amount' => '10000',
    'currency' => 'AED',
    'language' => 'en',
    'customer_email' => 'test@richouse.com',
    'signature' => $signature,
    'order_description' => 'iPhone 6-S',
);


$redirectUrl = 'https://sbcheckout.payfort.com/FortAPI/paymentPage';
echo "<html xmlns='https://www.w3.org/1999/xhtml'>\n<head></head>\n<body>\n";
echo "<form action='$redirectUrl' method='post' name='frm'>\n";
foreach ($requestParams as $a => $b) {
    echo "\t<input type='hidden' name='".htmlentities($a)."' value='".htmlentities($b)."'>\n";
}
echo "\t<script type='text/javascript'>\n";
echo "\t\tdocument.frm.submit();\n";
echo "\t</script>\n";
echo "</form>\n</body>\n</html>";
?>