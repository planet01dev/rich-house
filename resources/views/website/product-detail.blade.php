@php($page = "home")
@extends('layouts.web')
@section('content')
<style>
.carousel-control-next,
.carousel-control-prev {
    filter: invert(100%);
}
</style>
<section class="breadcrumb-area">
  <div class="container-fluid custom-container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="bc-inner viewCart-div" style="display: none;">
          <p class="view-cart-p pl-3 pr-3">
            <i class="fa fa-check pr-2"></i>"<span class="cart-prod-title"></span>" @lang('lang.has been added to your cart').
            <a class="view-cart-btn pull-right " href="{{route('cart')}}">View Cart</a> 
          </p>
        </div>
      </div>
      <div class="col-xl-12">
        <div class="bc-inner">
          <p>
            <a href="{{route('web.home')}}">Rich House / </a> 
            <a href="{{route('products')}}"> @lang('lang.product') / </a> 
            <a href="{{route('products.detail', $data->permalink)}}"> {!! @$data->product_text->title !!}  </a> 
          </p>
        </div>
      </div>
      <!-- /.col-xl-12 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</section>

<section class="shop-area style-two">
  <div class="container container-two">
    <div class="row">
      <div class="col-xl-12">
        <div class="row">
          <div class="col-lg-6 col-xl-6">
            <!-- Product View Slider -->
            <!-- <img src="{{ asset('public/website/assets/media/images/product-inner-img.png') }}" alt="" width=" 100%" height="100%"> -->
            
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">

              <?php
              $count = 0;
                  foreach(@$data->images as $v)
                  {
                    ?>
                    <div class="carousel-item <?= ($count == 0)?'active':''?> ">
                      <img class="d-block inner-prod-img" src="{{env('PRODUCT_IMAGES')}}{{@$v->image}}" alt="<?= $count?> slide">
                    </div>
                    <?php
                    $count++;
                  }
                ?>
                
              </div>
              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            <!-- /.quickview-slider -->
          </div>
          <!-- /.col-xl-6 -->

          <div class="col-lg-6 col-xl-6">
            <div class="product-details">
              <h5 class="pro-title mb-3">
                <a href="{{route('products.detail', $data->permalink)}}">{!! @$data->product_text->title !!}</a>
                <span class="price text-right">@lang('lang.currency') 
                    <?= (@$data->discount > 0)?'<s>'.@$data->front_price->price.'</s>':@$data->front_price->price ?>
                    <?= (@$data->discount > 0)?discounted_price(@$data->front_price->price, @$data->discount,@$data->discount_type ):'' ?>
                </span>
                <input type="hidden" id="discounted_price" value="{{discounted_price(@$data->front_price->price, @$data->discount,@$data->discount_type )}}" placeholder="">
              </h5>

              <h6 class="product-cat">{!! @$data->product_text->short_description !!}</h6>
              <!--Accordion wrapper-->
              <div class="mi-accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                <!-- Accordion card -->
              <div class="mi-card">

                  <!-- Card header -->
                  <div class="mi-card-header" role="tab" id="headingOne1">
                    
                      <!-- <h5 class="mb-0 accordion-text"> -->
                        <select name="variant" id="variant" class="form-control variant-dropdown mg-minus5">
                          <option selected value= "0" data-inventory="{{ @$data->inventory }}" data-price="{{ @$data->front_price->price }}" >{{@$data->weight_value}} {{@$data->weight->title}}</option>
                          <?php if (isset($data->productvariant)) {?>
                          @foreach(@$data->productvariant as $k => $v)
                          <?php
                          if (isset($data->price)) {
                            foreach ($data->price as $v2) {
                              if ($v2['variant_id'] == $v['id']) {
                                $price = $v2['price'];
                              }
                            }
                          }
                          ?>
                          <option value ="{{ $v['id'] }}" data-inventory="{{ $v['inventory'] }}" data-price="{{ @$price }}">{{ @$v->product_variant_text->title }} {{@$data->weight->title}}</option>
                          @endforeach
                        <?php } ?>
                        </select>
                        <!-- <span>{{@$data->weight_value}} {{@$data->weight->title}}</span>  -->
                        <!-- <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1" class="collapsed-width toggle_btn">
                          <i class="fas fa-angle-down rotate-icon float-rght"></i>
                        </a> -->
                      <!-- </h5> -->
                      <br>
                    <div class="cart-quantity">
                      <!-- <span>@lang('lang.stock') :</span><label id="stock_label">{{ @$data->inventory }}</label> -->
                      <?php
                            if(@$data->inventory <= 0)
                            {
                                ?>
                                <label><b>Out Of Stock</b></label>
                                <br>
                                <?php
                            }
                        ?>
                      <span>@lang('lang.quantity_p')</span>
                      <!-- <input type="number" id="quantity" style="width:50px" class="qty quantity prod-quantity" min="1" max="{{ @$data->inventory }}"  value="1"/> -->
                      <div class="quantity buttons_added">
                        <input type="button" value="-" class="minus">
                        <input type="number" id="quantity" readonly="readonly" step="1" min="1" max="{{ @$data->inventory }}" name="quantity" value="1" title="Qty" class="input-text qty text quantity prod-quantity" size="4" >
                        <input type="button" value="+" class="plus">
                      </div>
                      <!--<input type='button' value='-' class='qtyminus quantity-left-minus' field='' />-->
                      <!--<input type="text" readonly name="updates[]" id="quantity" style="width:50px" class="quantity text-center" value="1" />-->
                      <!--<input type='button' value='+' class='qtyplus quantity-right-plus' field='' />-->
                    </div>
                  </div>

                  <!-- Card body -->
                  <div id="collapseOne1" class="collapse show " role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
                    <div class="mi-card-body">
                      <a id="saa" href="javascript:void(0)" role="button" class="btn mi-bag-btn text-center scrollup" onclick="addtocart({{@$data->id}},  '{!!@$data->product_text->title!!}', '0');"  >@lang('lang.add_to_bag')</a>

                        <p>
                         {!! @$data->product_text->brief_description !!}
                        </p>
                        <div class="panel-group" id="accordion" data-toggle="collapse">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                            <a class="collapsed plus-accordion1-controls toggle_btn" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                <span class="plus-accordion-span">
                                @lang('lang.notes')
                                </span>
                                <i class="fas fa-angle-down plus-accordion-icon"></i> 
                            </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                             {!! @$data->product_text->notes !!}
                            </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                            <a class="plus-accordion2-controls toggle_btn" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                <span class="plus-accordion-span">
                                @lang('lang.ingredients')
                                </span>
                                <i class="fas fa-angle-down plus-accordion-icon"></i> 
                            </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in">
                            <div class="panel-body">
                             {!! @$data->product_text->ingredients !!}
                            </div>
                        </div>
                      </div>
                    </div>
                      </div>
                  </div>
              </div>
                <!-- Accordion card -->

              </div>
              <!-- Accordion wrapper -->
            </div>
            <!-- /.product-details -->
          </div>
          <!-- /.col-xl-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.col-xl-9 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>

<section class="main-product padding-50">
  @include('website.includes.relavant_product')
  <!-- Container  -->

  <!-- Modal  -->
  <div class="modal fade proceed_to_checkout-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog add-to-basket_modal pb-30" role="document">
      <div class="modal-content">
        <div class="modal-header">
        </div> -->
          <div class="modal-body">
            <h4 id="" class="addtoBasket-heading text-center padding-50">@lang('lang.item_added_to_shopping_basket')</h4>
            <div class="d-flex justify-content-between">
                <a href="javascript:void(0);" onclick="window.location.reload();">
                    <button type="button" class="btn btn-primary">@lang('lang.continue_shopping')</button>
                </a>
                <a href="{{route('cart')}}">
                    <button type="button" class="btn btn-success">@lang('lang.proceed_to_checkout')</button>
                </a>
            </div>
          </div>
        <div class="modal-footer">
        </div>
      </div> 
    </div>
  </div>
</section>

@endsection
@section('footer')
<script>
    $(function(){
    $(".toggle_btn").on("click", function () {
    $(this).find('i').toggleClass("fa-angle-up fa-angle-down");
    });
    });

    $( "#variant" ).change(function() {
      var price = $(this).find(":selected").data('price');
      var inventory = $(this).find(":selected").data('inventory');
      $('#stock_label').html(inventory);
      $('#quantity').attr('max', inventory);
      var discounted_price = ''; 
      var discount = "<?= $data->discount?>";

      var html = '';
      html = '@lang('lang.currency') ';
      if(discount > 0)
      {
        html += '<s>'+price+'</s>';
      }else
      {
        html += price;
      }

      if(discount > 0)
      {
        discount = (discount/100) * price;
        price = price - discount;
        price.toFixed(2);
        html += price.toFixed(2);
      }else
      {
        html += '';
      }
      
      $('.price').html(html);
      $('#discounted_price').val(parseFloat(price));
    });

    function wcqib_refresh_quantity_increments() {
        jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function(a, b) {
            var c = jQuery(b);
            c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
        })
    }
    String.prototype.getDecimals || (String.prototype.getDecimals = function() {
        var a = this,
            b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
    }), jQuery(document).ready(function() {
        wcqib_refresh_quantity_increments()
    }), jQuery(document).on("updated_wc_div", function() {
        wcqib_refresh_quantity_increments()
    }), jQuery(document).on("click", ".plus, .minus", function() {
      var max_limit = jQuery(this).closest(".quantity").find(".qty").attr('max');
      if(max_limit == 0)
      {
        return false;
      }
      var a = jQuery(this).closest(".quantity").find(".qty"),
            b = parseFloat(a.val()),
            c = parseFloat(a.attr("max")),
            d = parseFloat(a.attr("min")),
            e = a.attr("step");
        b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
    }); 
</script>
@endsection
