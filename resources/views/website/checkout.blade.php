@extends('layouts.web')
@section('content')
<!--=========================-->
<!--=        Breadcrumb     =-->
<!--=========================-->

<section class="breadcrumb-area">
   <div class="container-fluid custom-container">
      <div class="row">
         <div class="col-xl-12">
            <div class="bc-inner">
               <p><a href="{{route('web.home')}}">Rich House /</a> <a href="{{route('web.checkout')}}"> @lang('lang.checkout')</a></p>
            </div>
         </div>
         <!-- /.col-xl-12 -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container -->
</section>

<!--=========================-->
<!--=        Breadcrumb         =-->
<!--=========================-->

<style>
  .required-float-right
  {
    float:right;
  }
  .direction-ar .required-float-right
  {
    float:left;
  }
  .coupon_amount
  {
    display:none;
  }
  .iti 
  {
    width:100%
  }
</style>

<!--Contact area
============================================= -->
@include('website.includes.alert')
<section class="contact-area">
   <div class="container-fluid container-two">   
        <form class="form-horizontal ajaxForm_checkout validate" method="post" action="{{route('post.checkout')}}">
          @csrf
         <div class="row">
               <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12" style="background-color: #f5f5f5;">
                   <!--SHIPPING METHOD-->
                   <?php
                    if($user_data != null || $user_data != '')
                    {
                      // dd($user_data->user_address);
                      ?>
                      @foreach(@$user_data->user_address as $udk => $udv)
                        <div class="cust_profile_addresses d-inline-block">
                          <label>
                            <input type="radio" value="1" name="user_address_billing" class="user_address_billing" 
                            data-name="{{ @$udv['name']}}" data-email="{{ @$user_data->email}}" data-phone="{{ @$user_data->phone}}" data-country="{{ @$udv['country']}}"
                            data-state="{{ @$udv['state']}}" data-city="{{ @$udv['city']}}" data-zip-code="{{ @$udv['zip_code']}}" data-address="{{ @$udv['address']}}" >
                            {{ @$udv['title'] }}
                          </label>
                        </div>
                      @endforeach 
                        
                      <?php
                    }
                   ?>
                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">  
                      <div class="panel panel-info">
                         <div class="panel-heading ar-panel-heading pl-10 pr-10">@lang('lang.address')</div>
                            <div class="panel-body ar-panel-body">
                                <div class="form-group">
                                    <div class="col-md-12 mb-30">
                                        <h3><b>@lang('lang.contact') @lang('lang.information')</b></h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <strong>@lang('lang.fullname')*</strong>
                                        <label class="required-float-right">* @lang('lang.required')</label>
                                        <input type="text" name="billing[name]" class="form-control" value="" />
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                      <strong>@lang('lang.email') @lang('lang.address')*</strong>
                                      <label class="required-float-right"></label></div>
                                    <div class="col-md-12"><input type="email" name="billing[email]" class="form-control" value="" /></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <strong>@lang('lang.phone') @lang('lang.number')*</strong>
                                      <label class="required-float-right"></label></div>
                                    <div class="col-md-12">
                                      <input type="tel" name="billing[phone]" id="billing_phone" class="form-control">
                                      <!-- <input type="text" name="billing[phone]" class="form-control" value="" /> -->
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <div class="col-md-12 mb-30">
                                        <h3><b>@lang('lang.billing') @lang('lang.address')</b></h3>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                      <strong>@lang('lang.country')*</strong>
                                      <label class="required-float-right">* @lang('lang.required')</label></div>
                                    <div class="col-md-12">
                                      <select class="form-control shipping_country" name="billing[country]">
                                        <option selected disabled>@lang('lang.select') @lang('lang.country')</option>
                                        @for($i = 0; $i< count(@$shipping_data->shipping_text_all); $i++)
                                          <option value="{{ $shipping_data->shipping_text_all[$i]['id'] }}" data-cod="{{ $shipping_data->shipping_text_all[$i]['cod'] }}" data-rate="{{ $shipping_data->shipping_text_all[$i]['rate'] }}"> <?= (session()->has('lang') && session()->get('lang') == 'ar')?$shipping_data->shipping_text_all[$i+1]['country']:$shipping_data->shipping_text_all[$i]['country'] ?></option>
                                        @php($i ++)
                                        @endfor
                                      </select>
                                      <input type="hidden" id="shipping_rate" name="shipping_rate" value="0">
                                        <!-- <input type="text" class="form-control" name="billing[country]" value="" /> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <strong>@lang('lang.state')*</strong>
                                      <label class="required-float-right"></label></div>
                                    <div class="col-md-12">
                                        <input type="text" name="billing[state]" class="form-control" value="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <strong>@lang('lang.city')*</strong>
                                      <label class="required-float-right"></label></div>
                                    <div class="col-md-12">
                                        <input type="text" name="billing[city]" class="form-control" value="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <strong>@lang('lang.zip')  / @lang('lang.postal_code')</strong>
                                      <label class="required-float-right"></label></div>
                                    <div class="col-md-12">
                                        <input type="text" name="billing[zip]" class="form-control" value="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <strong>@lang('lang.address')</strong>
                                      <label class="required-float-right"></label></div>
                                    <div class="col-md-12">
                                        <input type="text" name="billing[address]" class="form-control" value="" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                      <input type="checkbox" class="shipping-check" name="has_shipping_address">
                                      <strong>@lang('lang.shipping_address_same_as_billing_address')</strong>
                                     </div>
                                </div>
                                <div class="shipping_address_section"> 
                                  <div class="form-group mt-30 mb-30">
                                      <div class="col-md-12">
                                         <h4><b>@lang('lang.shipping') @lang('lang.address')</b></h4>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <div class="col-md-12 col-xs-12">
                                          <strong>@lang('lang.fullname')*</strong>
                                          <label class="float-right">* @lang('lang.required')</label>
                                          <input type="text" name="shipping[name]" class="form-control" value="" />
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                        <strong>@lang('lang.email') @lang('lang.address')*</strong>
                                        <label class="required-float-right"></label></div>
                                      <div class="col-md-12"><input type="email" name="shipping[email]" class="form-control" value="" /></div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                        <strong>@lang('lang.phone') @lang('lang.number')*</strong>
                                        <label class="required-float-right"></label></div>
                                      <div class="col-md-12">
                                        <input type="tel" name="shipping[phone]" id="shipping_phone" class="form-control">
                                        <!-- <input type="text" name="shipping[phone]" class="form-control" value="" /> -->
                                      </div>
                                  </div>
                                 
                                  <div class="form-group">
                                      <div class="col-md-12">
                                        <strong>@lang('lang.country')*</strong>
                                        <label class="required-float-right"></label></div>
                                      <div class="col-md-12">
                                      <select class="form-control" name="shipping[country]">
                                          <option selected disabled>@lang('lang.select') @lang('lang.country')</option>
                                          @for($i = 0; $i< count(@$shipping_data->shipping_text_all); $i++)
                                            <option value="{{ $shipping_data->shipping_text_all[$i]['id'] }}" data-rate="{{ $shipping_data->shipping_text_all[$i]['rate'] }}"> <?= (session()->has('lang') && session()->get('lang') == 'ar')?$shipping_data->shipping_text_all[$i+1]['country']:$shipping_data->shipping_text_all[$i]['country'] ?></option>
                                          @php($i ++)
                                          @endfor
                                        </select>
                                          <!-- <input type="text" class="form-control" name="shipping[country]" value="" /> -->
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                        <strong>@lang('lang.state')*</strong>
                                        <label class="required-float-right"></label></div>
                                      <div class="col-md-12">
                                          <input type="text" name="shipping[state]" class="form-control" value="" />
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                        <strong>@lang('lang.city')*</strong>
                                        <label class="required-float-right"></label></div>
                                      <div class="col-md-12">
                                          <input type="text" name="shipping[city]" class="form-control" value="" />
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                        <strong>@lang('lang.zip')  / @lang('lang.postal_code')*</strong>
                                        <label class="required-float-right"></label></div>
                                      <div class="col-md-12">
                                          <input type="text" name="shipping[zip]" class="form-control" value="" />
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                        <strong>@lang('lang.address')*</strong>
                                        <label class="required-float-right"></label></div>
                                      <div class="col-md-12">
                                          <input type="text" name="shipping[address]" class="form-control" value="" />
                                      </div>
                                  </div>
                              </div>
                            
                                
                               
                          </div>
                      </div>
                    </div>
                  </div>
                  <!--SHIPPING METHOD END-->
                  <!--CREDIT CART PAYMENT-->
                  <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="cart-subtotal">
                      <p>@lang('lang.subtotal')</p>
                      <ul>
                        <li>
                          <span class="cart-label">@lang('lang.subtotal'):</span>
                          <span class="cart-desc">@lang('lang.currency')<span class="product_total_sub_amount product_total_amount1"> {{$total_amount}}</span></span> 
                        </li>
                        <li style="<?= (@tax <= 0)?'display:none':'' ?>">
                          <span class="cart-label">@lang('lang.tax') :</span>
                          <span class="cart-desc">{{@$tax}}% </span>
                        </li>
                        <!-- <li><span class="cart-label">@lang('lang.shipping_method') :</span>{{@$shipping_method}} </li> -->
                        <li>
                          <span class="cart-label">@lang('lang.shipping') @lang('lang.charges') :</span> 
                          <span class="cart-desc">@lang('lang.currency') <label class="shipping_rate_label">{{@$shipping_charges}}</label></span> 
                        </li>
                          <!-- <li><span>Tax (-4.00):</span>$11.00</li>
                          <li><span>Shipping Cost:</span>$00.00</li> -->
                        <li class="coupon_amount">
                          <span class="cart-label">@lang('lang.coupon'):</span>
                          <span class="cart-desc">@lang('lang.currency') <span class="coupon_amount_span"> </span></span>
                        </li>
                        <li>
                          <span class="cart-label">@lang('lang.total'):</span>
                          <span class="cart-desc">@lang('lang.currency')<span class="product_total_amount product_total_amount1"> {{$total_amount}}</span></span>
                        </li>
                      </ul>
                      
                      <div class="form-group">
                        <div class="col-md-12 mt-10">
                          <label><strong>Enter Coupon</strong></label>
                        </div>

                        <div class="row">
                          <div class="col-lg-8 col-md-12">
                            <input type="text" class="form-control float-right coupon_code" name="coupon_code" plcaeholder="Enter Coupon">
                            <span class="error coupon_error"></span>
                          </div>
                          <div class="col-lg-3 col-md-12 ">
                          <button type="button" class="btn coupon_redeem">Redeem</button>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 txt-float">
                                <label data-toggle="collapse" data-target="#giftNote">
                                    <i class="fas fa-gift"></i>&nbsp;
                                    <strong>Gift Note </strong>
                                    <i class="fa fa-plus"></i>
                                </label>
                                
                            </div>
                            <div class="col-md-12 ">
                                <div id="giftNote" class="collapse">
                                    <textarea style="resize:none" name="billing[message]" class="form-control"></textarea> 
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 txt-float">
                                <label>
                                    <div class="checkbox">
                                      <strong> 
                                        <input type="checkbox" name="billing[wrapping]" >&nbsp; Gift Wrapping &nbsp;
                                      </strong>
                                    </div>
                                </label>
                            </div>
                        </div>
                      </div>
                      @if(@$setting->is_cod)
                      <div class="form-group">
                        <div class="col-md-12 mt-10">
                          <label><strong>Payment Method</strong></label>
                        </div>
                        <div class="col-md-12 ">  
                            <label class="cod">
                              <input type="radio" value="2" checked name="billing[has_payment]">
                              Cash On Delivery
                            </label>
                            
                            <label>
                              <input type="radio" value="1" checked name="billing[has_payment]">
                              Credit/Debit Card
                            </label>
                            
                          <!--<input class="cod-checkbox" type="radio" checked value="2" name="billing[has_payment]" />-->
                          <!--<label class="cod-checkboxlabel"><strong>COD</strong></label>-->

                          <!--<input class="cod-checkbox" type="radio" checked value="1" name="billing[has_payment]" />-->
                          <!--<label class="cod-checkboxlabel"><strong>PAYFORT</strong></label>-->

                        </div>
                      </div>
                      @else
                        <input type="hidden" value="0" name="billing[has_payment]" />
                      @endif  

                      <input type="hidden" id="tax" value="{{@$tax}}">
                      <input type="hidden" id="shipping" value="{{@$shipping_charges}}">
                      <input type="hidden" name="coupon_id" id="coupon_id" value="0">
                      <input type="hidden" name="user_id" id="user_id" value="<?= (Auth::user() != null && Auth::user()->hasRole('customer'))?Auth::user()->id:0?>">
                      <input type="hidden" name="coupon_discount" id="coupon_discount" value="0">
                      <button type="button" class="checkout-btn ajaxFormSubmit_checkout">@lang('lang.proceed_to_checkout')</button>
                      <div class="loader" ></div>
                       <!-- <a href="{{route('web.checkout')}}">@lang('lang.proceed_to_checkout')</a> -->
                    </div>
                    <!-- /.cart-subtotal -->
                 </div>
                   <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                      <div class="panel panel-info">
                          <div class="panel-heading pl-10"><span><i class="glyphicon glyphicon-lock"></i></span> Secure Payment</div>
                          <div class="panel-body">
                              <div class="form-group">
                                  <div class="col-md-12"><strong>Card Type:</strong></div>
                                  <div class="col-md-12">
                                      <select id="CreditCardType" name="CreditCardType" class="form-control">
                                          <option value="5">Visa</option>
                                          <option value="6">MasterCard</option>
                                          <option value="7">American Express</option>
                                          <option value="8">Discover</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="col-md-12"><strong>Credit Card Number:</strong></div>
                                  <div class="col-md-12"><input type="text" class="form-control" name="car_number" value="" /></div>
                              </div>
                              <div class="form-group">
                                  <div class="col-md-12"><strong>Card CVV:</strong></div>
                                  <div class="col-md-12"><input type="text" class="form-control" name="car_code" value="" /></div>
                              </div>
                              <div class="form-group">
                                  <div class="col-md-12">
                                      <strong>Expiration Date</strong>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                      <select class="form-control" name="">
                                          <option value="">Month</option>
                                          <option value="01">01</option>
                                          <option value="02">02</option>
                                          <option value="03">03</option>
                                          <option value="04">04</option>
                                          <option value="05">05</option>
                                          <option value="06">06</option>
                                          <option value="07">07</option>
                                          <option value="08">08</option>
                                          <option value="09">09</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                  </select>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                      <select class="form-control" name="">
                                          <option value="">Year</option>
                                          <option value="2015">2015</option>
                                          <option value="2016">2016</option>
                                          <option value="2017">2017</option>
                                          <option value="2018">2018</option>
                                          <option value="2019">2019</option>
                                          <option value="2020">2020</option>
                                          <option value="2021">2021</option>
                                          <option value="2022">2022</option>
                                          <option value="2023">2023</option>
                                          <option value="2024">2024</option>
                                          <option value="2025">2025</option>
                                  </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="col-md-12">
                                      <span>Pay secure using your credit card.</span>
                                  </div>
                                  <div class="col-md-12">
                                      <ul class="cards">
                                          <li class="visa hand">Visa</li>
                                          <li class="mastercard hand">MasterCard</li>
                                          <li class="amex hand">Amex</li>
                                      </ul>
                                      <div class="clearfix"></div>
                                  </div>
                              </div>
                              @if(@$setting->is_cod)
                               <div class="form-group">

                                  <div class="col-md-12">
                                       <strong>COD</strong>
                                      <input type="radio" checked value="2" name="billing[has_payment]" />
                                  </div>
                              </div>
                              @else
                                <input type="hidden" value="0" name="billing[has_payment]" />
                              @endif
                              <div class="form-group">
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <button type="submit" class="btn btn-primary btn-submit-fix">Place Order</button>
                                  </div>
                              </div>
                          </div>
                      </div> -->
               <!-- </div> -->
            </div>
         </form>
      </div>
   </div>
</section>
   <!-- /.container-fluid end -->
<!-- /.contact-area end -->

<!-- /#map -->

@endsection
@section('footer')
<script>


$(document).ready(function() {
  
    $('input:radio[name="user_address_billing"]').change(
    function(){
        if ($(this).is(':checked') && $(this).val() == '1') {
            // append goes here
            $('input[name="billing[name]"]').val($(this).data('name'));
            $('input[name="billing[email]"]').val($(this).data('email'));
            $('input[name="billing[phone]"]').val($(this).data('phone'));
            $('input[name="billing[state]"]').val($(this).data('state'));
            $('input[name="billing[city]"]').val($(this).data('city'));
            $('input[name="billing[zip]"]').val($(this).data('zip-code'));
            $('input[name="billing[address]"]').val($(this).data('address'));
            
            $('select[name="billing[country]"]').val($(this).data('country')).attr('selected','selected');
            $('select[name="billing[country]"]').trigger('change');
        }
    });

    $("form.validate").validate({
      rules: {
        'billing[country]':{
          required: true
        },
        'billing[name]':{
          required: true
        },
        'billing[address]':{
          required: true
        },
        'billing[city]':{
          required: true
        },
        'billing[state]':{
          required: true
        },
        'billing[phone]':{
          required: true
        },
        'billing[email]':{
          required: true
        },
      }, 
      messages: {
        
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        //error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type   
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          $('<span class="error"></span>').insertAfter(element).append(label);
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('success-control').addClass('error-control');  
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
      $(".shipping-check").change(function() {
          if(this.checked) {
            $(".shipping_address_section").hide();  // unchecked
          }else{
            $(".shipping_address_section").show();  // checked
          }
      });

      $(".shipping_country").change(function() {
          var shipping_rate = $(this).find(':selected').data('rate');
          var cod = $(this).find(':selected').data('cod');
          if(cod == 0)
          {
            $('.cod').hide();
            $( "input[name='billing[has_payment]'][value='1']" ).prop( "checked", true );
          }else
          {
            $('.cod').show();
          }
          $('.shipping_rate_label').text(shipping_rate);
          $('#shipping_rate').val(shipping_rate);
          
          var coupon_amount = (parseFloat(<?= $total_amount;?>) / 100)*parseFloat($('#coupon_discount').val());
          var tax_amount = (parseFloat(<?=$total_amount;?>) / 100)*parseFloat(<?=$tax?>);
          var total  = (parseFloat(<?=$total_amount;?>) + tax_amount + parseFloat(shipping_rate)) - coupon_amount;
          $('.product_total_amount').text(total.toFixed(2));
      });

      $(".coupon_redeem").click(function() {
        $('.coupon_code').val();
        if( $('.coupon_code').val().length === 0 ) {
          $('.coupon_error').html('This Field is Required.');
        }else
        {
          $('.coupon_error').html('');
          $('#coupon_id').val(0);
          $.ajax({
            type: "POST",
            
            url: "{{route('web.coupon.redeem')}}",
            cache: false,
            data: {
              "_token": "{{ csrf_token() }}",
                code:$('.coupon_code').val()
            },
            success: function(result){
              if(result['id'])
              {
                if(result['IsActive'] == 1)
                {
                  $('#coupon_id').val(result['id']);
                  $('#coupon_discount').val(result['DiscountPercentage']);
                  var shipping_rate = $('.shipping_country').find(':selected').data('rate');
                  if(shipping_rate == undefined)
                  {
                    shipping_rate = 0;
                  }
                  $('.shipping_rate_label').text(shipping_rate);
                  $('#shipping_rate').val(shipping_rate);
                  if(result['discount_type'] == 0)
                  {
                    var coupon_amount = (parseFloat(<?= $total_amount;?>) / 100)*parseFloat(result['DiscountPercentage']);
                  }
                  else
                  {
                    var coupon_amount = parseFloat(result['DiscountPercentage']);
                  }
                  var tax_amount = (parseFloat(<?= $total_amount;?>) / 100)*parseFloat(<?=$tax?>);
                  var total  = (parseFloat(<?=$total_amount;?>) + tax_amount + parseFloat(shipping_rate))-coupon_amount;
                  $('.product_total_amount').text(total.toFixed(2));
                  $('.product_total_amount').text(total.toFixed(2));
                  $('.coupon_amount_span').text(coupon_amount.toFixed(2));
                  $('.coupon_amount').show();
                  success('Coupon Added');
                }
                else
                {
                  error('Coupon Already Used.');
                }
              }
              else
              {
                error('Wrong Coupon No');
              }
            }
          });
        }
      });

    var input = document.querySelector("#billing_phone");
    window.intlTelInput(input, {
	    preferredCountries: ['sa', 'us'],
      separateDialCode: false,
      utilsScript: "{{asset('public/website/assets/dependencies/input-phone/js/utils.js')}}",
    });

    var input = document.querySelector("#shipping_phone");
    window.intlTelInput(input, {
	    preferredCountries: ['sa', 'us'],
      separateDialCode: false,
      utilsScript: "{{asset('public/website/assets/dependencies/input-phone/js/utils.js')}}",
    });
      

  });
</script>
@endsection
