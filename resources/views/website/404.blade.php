@extends('layouts.web')
@section('content')
<div class="container custom-container">
    <div id="notfound">
        <div class="notfound-bg">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div class="notfound">
            <div class="notfound-404">
                <h1>404</h1>
            </div>
            <h2>@lang('lang.page_not_found')</h2>
            <a href="{{route('web.home')}}">@lang('lang.go_to_homepage')</a>
        </div>
    </div>
</div>
@endsection