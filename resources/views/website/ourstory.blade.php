@php($page = "home")
@extends('layouts.web')
@section('content')


<?php
        $data= get_settings();
      ?>
<section class=" padding-top-120 width-100">
  <div class="container custom-container">
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12 p-0">
        <div class="sin-banner align-items-center two-columns">
          <img src="{{env('STORY_IMAGES')}}{{ @$page_data->image }}" alt="">
        </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12 p-0">
        <div class="aboutus-text sin-banner align-items-center two-columns">
          <h1>{{ @$page_data->ourStoryText->title }}</h1>
          	{!! @$page_data->ourStoryText->content !!}
        </div>
      </div>
    </div>
  </div>
</section>
@endsection