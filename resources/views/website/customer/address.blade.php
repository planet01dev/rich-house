<div class="address_div d-none">
    <div class="row mg-bottom">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <h4 class="allheading">@lang('lang.address')</h4>
        </div>
        <div class="col-xl-5 col-lg-5 col-md-6 col-sm-6 col-xs-6">
            <button type="button" class="btn add-addressbtn text-uppercase" data-toggle="modal" data-target="#myModal">@lang('lang.add') @lang('lang.new') @lang('lang.address')</button>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-xs-12">
            <div class="addressbg">
                <div class="card">
                    <div class="card-header address-div" style="background-color: #F2F2FA !important;">
                    @lang('lang.primary') @lang('lang.addresses')
                    </div>
                    @if(isset($user_data->user_address) && $user_data->user_address != null)
                    @foreach($user_data->user_address as $dt)
                    @if($dt->primary_address == 1)
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 address-head">
                                {{ $dt['title']}} <i class="fas fa-map-marker-alt"></i>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-12 ">
                                <h5 class="customer_det-label">@lang('lang.name')</h5>
                                <p class="customer_det-input">{{ $dt['name']}}</p>
                            </div>
                            <div class="col-xl-4 col-lg-5 col-md-4 col-sm-12 col-xs-12 ">
                                <h5 class="customer_det-label">@lang('lang.address')</h5>
                                <p class="customer_det-input">{{ $dt['address']}}</p>
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12 ">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-3 col-md-4 col-sm-12 col-xs-12 ">
                                        <h5 class="customer_det-label">@lang('lang.phone') @lang('lang.number')</h5>
                                        <p class="customer_det-input">{{ $user_data['phone']}} 
                                        <!-- <i class="fa fa-check-circle" style="font-size:10px;color:#32aa0d"></i> -->
                                        </p>
                                    </div>
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1 edit_address" data-primary-address="<?= $dt['primary_address']?>" data-state="<?= $dt['state']?>" data-city="<?= $dt['city']?>" data-zip="<?= $dt['zip_code']?>" data-country="<?= $dt['country']?>" data-name="<?= $dt['name'];?>" data-title="<?= $dt['title']?>" data-address-id="<?= $dt['id'] ?>" data-address="<?= @$dt['address'] ?>" data-phone = "<?= $user_data['phone']?>" title="@lang('lang.edit')"> <i class="fa fa-pencil"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    @endif
                </div>
                <br>
                <div class="card">
                    <div class="card-header address-div" style="background-color: #F2F2FA !important;">
                    @lang('lang.other') @lang('lang.addresses')
                    </div>
                    @if(isset($user_data->user_address) && $user_data->user_address != null)
                    @foreach($user_data->user_address as $dt)
                    @if($dt->primary_address == 0)
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 address-head">
                                {{ $dt['title']}} <i class="fas fa-map-marker-alt"></i>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-12 col-xs-12 ">
                                <h5 class="customer_det-label">@lang('lang.name')</h5>
                                <p class="customer_det-input">{{ $dt['name']}}</p>
                            </div>
                            <div class="col-xl-4 col-lg-5 col-md-4 col-sm-12 col-xs-12 ">
                                <h5 class="customer_det-label">@lang('lang.address')</h5>
                                <p class="customer_det-input">{{ $dt['address']}}</p>
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12 ">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-9 col-sm-11 col-xs-11">
                                        <h5 class="customer_det-label">@lang('lang.phone') @lang('lang.number')</h5>
                                        <p class="customer_det-input">{{ $user_data['phone']}} 
                                            <!-- <i class="fa fa-check-circle" style="font-size:10px;color:#32aa0d"></i> -->
                                        </p>
                                    </div>
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1 edit_address" data-primary-address="<?= $dt['primary_address']?>" data-state="<?= $dt['state']?>" data-city="<?= $dt['city']?>" data-zip="<?= $dt['zip_code']?>" data-country="<?= $dt['country']?>" data-name="<?= $dt['name'];?>" data-title="<?= $dt['title']?>" data-address-id="<?= $dt['id'] ?>" data-address="<?= @$dt['address'] ?>" data-phone = "<?= $user_data['phone']?>" title="@lang('lang.edit')"> <i class="fa fa-pencil"></i></button>
                                    </div>
                                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                        <button type="button" class="btn-sm btn-dark waves-effect waves-light m-1" onclick="item_delete({{$dt->id}}, '{{route('customer.delete_address', $dt->id)}}')" title="@lang('lang.delete')"> <i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- The add Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">@lang('lang.add') @lang('lang.new') @lang('lang.address')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <form id="customer_address" class="ajaxForm_address_modal validate" method="post" action="{{route('customer.add_address')}}">
                    @csrf
                <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>@lang('lang.address') @lang('lang.title')</strong>
                                <input type="text" name="title" class="form-control"  required>
                            </div>
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>@lang('lang.name')</strong>
                                <input type="text" name="name" class="form-control"  required>
                            </div>
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>@lang('lang.address')</strong>
                                <input type="text" name="address" class="form-control"  required>
                            </div>
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>@lang('lang.country')</strong>
                                <select name="country" id="inputCountry" class="form-control" required>
                                <option selected disabled>@lang('lang.select') @lang('lang.country')</option>
                                      @for($i = 0; $i< count(@$shipping_data->shipping_text_all); $i++)
                                        <option value="{{ $shipping_data->shipping_text_all[$i]['id'] }}" data-rate="{{ $shipping_data->shipping_text_all[$i]['rate'] }}"> <?= (session()->has('lang') && session()->get('lang') == 'ar')?$shipping_data->shipping_text_all[$i+1]['country']:$shipping_data->shipping_text_all[$i]['country'] ?></option>
                                      @php($i ++)
                                      @endfor
                                </select>
                            </div>
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                <strong>@lang('lang.state')</strong>
                                <input type="text" name="state" class="form-control"  required>
                            </div>
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>@lang('lang.city')</strong>
                                <input type="text" name="city" class="form-control" id="inputCity" required>
                            </div>
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>@lang('lang.zip')</strong>
                                <input type="text" name="zip_code" class="form-control" id="inputZip">
                            </div>
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="checkbox" id="basic_checkbox_2" name="primary_address" />
                                <label for="basic_checkbox_2">@lang('lang.primary') @lang('lang.address')</label>
                            </div>
                        </div>
                    
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <input type="hidden" name="address_id">
                    <button type="button" class="btn btn-warning ajaxFormSubmit_address_modal" >@lang('lang.submit')</button>
                </div>
                </form>
            </div>
        </div>
    </div>

</div>


