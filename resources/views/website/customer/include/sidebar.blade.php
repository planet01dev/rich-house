
            
                <div class="w-100">
	                <h1 class="custprof-name"> {{ $user_data['f_name'] }}</h1>
	                <a class="custprof-signout" href="{{ route('customer-logout') }}">@lang('lang.sign_out')</a>
	                 <ul class="prof-menu">
	                    <li>
	                    	<a href="javascript:void(0)" data-div-id='1' class="btn customer_nav_chnage text-uppercase">@lang('lang.orders')</a>
	                    </li>
	                    <li>
	                    	<a href="javascript:void(0)" data-div-id='2' class="btn customer_nav_chnage text-uppercase">@lang('lang.addresses')</a>
	                    </li>
	                    <li>
	                    	<a href="javascript:void(0)" data-div-id='3' class="btn customer_nav_chnage text-uppercase">@lang('lang.profile')</a>
	                    </li>
	                    <li>
                            <a href="javascript:void(0)" data-div-id='4' class="btn customer_nav_chnage text-uppercase">@lang('lang.wishlist')</a>
                        </li>
	                </ul>
                   									
                    <!-- <a href="javascript:void(0)" data-div-id='1' class="btn customer_nav_chnage">ORDERS</a>
                    <a href="javascript:void(0)" data-div-id='2' class="btn customer_nav_chnage">ADDRESSES</a>
                    <a href="javascript:void(0)" data-div-id='3' class="btn customer_nav_chnage">PROFILE</a> -->
                </div>
            
