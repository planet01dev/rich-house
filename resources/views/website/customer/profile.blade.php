@extends('layouts.web')
@section('content')
<!-- <section class="banner padding-top-120 width-100 mb-30">
  <div class="container custom-container">  
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 p-0 sec1-img">
        <div class="sin-banner align-items-center">
          <div class="mi-banner-top-text marquee">
            <?php
            $setting = get_settings();
            ?>
              <p>
                <?= $setting['ticker_1'] ?>
                <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
                <?= $setting['ticker_2'] ?>
                <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
                <?= $setting['ticker_3'] ?>
                <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
                <?= $setting['ticker_4'] ?>
              </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->

<!-- <section class="featured-img-area padding-top-120"> -->
<section class="padding-top-120">
  <div class="container-fluid custom-container">
    <div class="row mt-5">

      <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
        @include('website.customer.include.sidebar')
      </div>

      <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
        @include('website.customer.orders')
        @include('website.customer.address')
        @include('website.customer.customer-profile')
        @include('website.customer.wishlist')
      </div>

    </div>

  </div>
</section>
@endsection
@section('footer')
<script>
  function cancel_order(id, this_url) {
    swal({
        title: "Do you really want to cancel it?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
            url: this_url,
            type: "post",
            data: {
              id: id
            },
            success: function(data) {
              if (data == 'success') {
                $('#success').show();
                $('#success-text').html('<strong>Success!</strong> Record has been deleted.');
                $('.back-to-top').click();
                setTimeout(
                  function() {
                    location.reload(true);
                  }, 1000);
              }else if(data == 'timeout'){
                swal("Cancellation Failed", "Cancellation must be within 1 hour of order", "error")
              }
            },
            error: function() {
              alert('There is error while deleting record');
            }
          });
        } else {

          return false;
        }
      });
  }
  $(document).ready(function() {

    $(document).on("click", ".edit_address", function(event) {

      
      $('input[name="title"]').val($(this).data('title'));
      $('input[name="name"]').val($(this).data('name'));
      $('input[name="address"]').val($(this).data('address'));
      $('#inputCountry').val($(this).data('country'));
      $('input[name="state"]').val($(this).data('state'));
      $('input[name="city"]').val($(this).data('city'));
      $('input[name="zip_code"]').val($(this).data('zip'));
      if($(this).data('primary-address') == 1)
      {
        $('input[name="primary_address"]').prop('checked', true);  
      }
      $('input[name="address_id"]').val($(this).data('address-id'));
      

      $('#myModal').modal('show');
    });

    $('#myModal').on('hidden.bs.modal', function (e) {
      $(this)
        .find("input,textarea,select")
          .val('')
          .end()
        .find("input[type=checkbox], input[type=radio]")
          .prop("checked", "")
          .end();
    });

    $(".customer_nav_chnage").click(function() {
      var div_id = $(this).data('div-id');
      if (div_id == 1) {
        $('.order_div').removeClass('d-none');
        $('.profile_div').addClass('d-none');
        $('.address_div').addClass('d-none');
        $('.wishlist_div').addClass('d-none');
      } else if (div_id == 2) {
        $('.address_div').removeClass('d-none');
        $('.profile_div').addClass('d-none');
        $('.order_div').addClass('d-none');
        $('.wishlist_div').addClass('d-none');
      } else if (div_id == 3) {
        $('.profile_div').removeClass('d-none');
        $('.address_div').addClass('d-none');
        $('.order_div').addClass('d-none');
        $('.wishlist_div').addClass('d-none');
      } else if (div_id == 4) {
        //$('.address_div').removeClass('d-none');
        $('.profile_div').addClass('d-none');
        $('.address_div').addClass('d-none');
        $('.order_div').addClass('d-none');
        $('.wishlist_div').removeClass('d-none');
        //$('.wishlist_div').addClass('d-none');
      }
    });

    $(".prof-menu li").click(function() {
      $(".prof-menu li").removeClass("menu_active");
      $(this).addClass("menu_active");
    });

    $('.ajaxFormSubmit_address_modal').on('click', function(e) {
      e.preventDefault();
      var form = $('form.ajaxForm_address_modal');
      var check = form.valid();
      if (!check) {
        $("#loader").hide();
        return false;
      }
      $("button[type=button]").attr("disabled", 'disabled');
      $("button[type=submit]").attr("disabled", 'disabled');
      form.submit();

    });

    $("form.ajaxForm_address_modal").ajaxForm({
      dataType: "json",
      beforeSubmit: function() {
        faction = $("form.login").attr("action");
        $("button[type=button]").attr("disabled", 'disabled');
        $("button[type=submit]").attr("disabled", 'disabled');

      },
      error: function(data) {
        $("button[type=button]").removeAttr("disabled");
        $("button[type=submit]").removeAttr("disabled");

        if (typeof data === 'object') {
          error("Please fill all mandatory fields.");
        } else {
          error("Error Occured.Invalid File Format.");
        }

      },
      success: function(data) {
        $("button[type=button]").removeAttr("disabled");
        $("button[type=submit]").removeAttr("disabled");
        if (data == null || data == "") {
          window.location.reload(true);
        } else if (data['error'] !== undefined) {
          error(data['error']);

        } else if (data['success'] !== undefined) {
          success(data['success']);
        }
        if (data['redirect'] !== undefined) {
          window.setTimeout(function() {
            window.location = data['redirect'];
          }, 2000);

        }

        if (data['reload'] !== undefined) {
          window.location.reload(true);
        }
        if (data['fieldsEmpty'] == 'yes') {
          resetForm();

        }
      }
    });

    $('.ajaxFormSubmit_name_change').on('click', function(e) {
      e.preventDefault();
      var form = $('form.ajaxForm_name_change');
      var check = form.valid();
      if (!check) {
        $("#loader").hide();
        return false;
      }
      $("button[type=button]").attr("disabled", 'disabled');
      $("button[type=submit]").attr("disabled", 'disabled');
      form.submit();

    });

    $("form.ajaxForm_name_change").ajaxForm({
      dataType: "json",
      beforeSubmit: function() {
        faction = $("form.login").attr("action");
        $("button[type=button]").attr("disabled", 'disabled');
        $("button[type=submit]").attr("disabled", 'disabled');

      },
      error: function(data) {
        $("button[type=button]").removeAttr("disabled");
        $("button[type=submit]").removeAttr("disabled");

        if (typeof data === 'object') {
          error("Please fill all mandatory fields.");
        } else {
          error("Error Occured.Invalid File Format.");
        }

      },
      success: function(data) {
        $("button[type=button]").removeAttr("disabled");
        $("button[type=submit]").removeAttr("disabled");
        if (data == null || data == "") {
          window.location.reload(true);
        } else if (data['error'] !== undefined) {
          error(data['error']);

        } else if (data['success'] !== undefined) {
          success(data['success']);
        }
        if (data['redirect'] !== undefined) {
          window.setTimeout(function() {
            window.location = data['redirect'];
          }, 2000);

        }

        if (data['reload'] !== undefined) {
          window.location.reload(true);
        }
        if (data['fieldsEmpty'] == 'yes') {
          resetForm();

        }
      }
    });

    $('.ajaxFormSubmit_detail_change').on('click', function(e) {
      e.preventDefault();
      var form = $('form.ajaxForm_detail_change');
      var check = form.valid();
      if (!check) {
        $("#loader").hide();
        return false;
      }
      $("button[type=button]").attr("disabled", 'disabled');
      $("button[type=submit]").attr("disabled", 'disabled');
      form.submit();

    });

    $("form.ajaxForm_detail_change").ajaxForm({
      dataType: "json",
      beforeSubmit: function() {
        faction = $("form.login").attr("action");
        $("button[type=button]").attr("disabled", 'disabled');
        $("button[type=submit]").attr("disabled", 'disabled');

      },
      error: function(data) {
        $("button[type=button]").removeAttr("disabled");
        $("button[type=submit]").removeAttr("disabled");

        if (typeof data === 'object') {
          error("Please fill all mandatory fields.");
        } else {
          error("Error Occured.Invalid File Format.");
        }

      },
      success: function(data) {
        $("button[type=button]").removeAttr("disabled");
        $("button[type=submit]").removeAttr("disabled");
        if (data == null || data == "") {
          window.location.reload(true);
        } else if (data['error'] !== undefined) {
          error(data['error']);

        } else if (data['success'] !== undefined) {
          success(data['success']);
        }
        if (data['redirect'] !== undefined) {
          window.setTimeout(function() {
            window.location = data['redirect'];
          }, 2000);

        }

        if (data['reload'] !== undefined) {
          window.location.reload(true);
        }
        if (data['fieldsEmpty'] == 'yes') {
          resetForm();

        }
      }
    });
  });
</script>

@endsection