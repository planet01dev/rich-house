

   
        <div class="profile_div d-none">
            <div class="row">
                  <div class="col-lg-11 col-md-12">
                    <h4 class="allheading">@lang('lang.profile')</h4>
                 </div>
            </div>
            <div class="row">
                  <div class="col-lg-11 col-md-12">
                    <div class="profilebg">
                        <div class="card mb-10 card_border">
                            <div class="card-header profile-div">
                            @lang('lang.general') @lang('lang.information')
                            </div>
                            <div class="card-body">
                            <form id="customer_name_change" class="ajaxForm_name_change validate" method="post" action="{{route('customer.customer_name_change')}}">
                                @csrf    
                                <div class="form-row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="prof_lbl" for="inputfirstname">@lang('lang.first') @lang('lang.name')</label>
                                            <input type="text" class="form-control cust-prof-firstname" name="f_name" value="{{ $user_data->f_name }}">
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="prof_lbl" for="inputlastname">@lang('lang.last') @lang('lang.name')</label>
                                            <input type="text" class="form-control cust-prof-lastname" name="l_name" value="{{ $user_data->l_name }}">
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <label class="prof_lbl" for="inputState">@lang('lang.preferred') @lang('lang.language')</label>
                                            <select class="cust-prof-lang form-control" name="preferred_language">
                                                <option <?= ($user_data['preferred_language'] == 1)?'selected':''?> value="1">English</option>
                                                <option <?= ($user_data['preferred_language'] == 2)?'selected':''?> value="2">Arabic</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="button" class="savebtn btn ajaxFormSubmit_name_change">@lang('lang.save')</button>
                                </form>  
                            </div>
                        </div>
                        <div class="card card_border">
                            <div class="card-header profile-div">
                            @lang('lang.security')
                            </div>
                            <div class="card-body">
                            <form id="customer_detail_change" class="ajaxForm_detail_change validate" method="post" action="{{route('customer.customer_detail_change')}}">
                            @csrf    
                                <div class="form-row">
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <label class="prof_lbl" for="inputEmail4">@lang('lang.email')</label>
                                            <input type="email" class="form-control cust-prof-email" readonly value="{{ $user_data->email }}">
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <label class="prof_lbl" for="inputPassword4">@lang('lang.password')</label>
                                            <input type="password" name="password" class="form-control cust-prof-pwd" required>
                                        </div>
                                    </div>
                                    <button type="button" class="changpwdbtn btn ajaxFormSubmit_detail_change">@lang('lang.change') @lang('lang.password')</button>
                                    <!-- <button type="submit" class="btn btn-light float-right">Change Password</button>    -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>

   
