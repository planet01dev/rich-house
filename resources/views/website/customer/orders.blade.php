<div class="order_div">
    <div class="row">
        <div class="col-xl-11 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4 class="allheading">Orders</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-11 col-md-12">
            <div class="orderbg">
                <div class="panel-group">
                    @if(isset($user_data->user_order) && $user_data->user_order != null)

                    <?php
                        $numOrder = count($user_data->user_order);
                        if($numOrder != 0)
                        {
                            ?>
                                @foreach($user_data->user_order as $dt)
                                    <div class="mi-panel panel <?= ($dt->customer_view == 1)?'notify_highlight':'';?>" style="border: 1px solid rgba(0,0,0,.125);    margin-bottom: 12px;" >
                                        <div class=" mi-panel-heading" style="background-color: #F2F2FA ">
                                            <div class="row">
                                                <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                                    <h6 class="orderhead">ORDER N0 {{ $dt['id'] }}
                                                        <span class="placeddate">
                                                            &nbsp;@lang('lang.placed_at') {{date('d-M-Y', strtotime($dt['created_at']))}}
                                                        </span>
                                                    </h6>
                                                </div>
                                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                <?php if($dt->status == 0){?>
                                                <button type="button" onclick="cancel_order({{$dt['id']}}, '{{route('customer.cancel_order', $dt['id'])}}')"  class="btn-sm btn-dark waves-effect waves-light m-1 float-right cancel-btn" > Cancel Order</button>
                                                <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                            $numItems = count($dt->orderDetail);
                                            $i = 0;
                                        ?>
                                        @foreach($dt->orderDetail as $ddt)
                                        <div class="panel-body mi-panelbody">
                                            <div class="row">
                                                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-3 text-center ">
                                                    <img class="customer-order-img" src="{{env('PRODUCT_IMAGES')}}{{@$ddt->orderProduct->images[0]->image}}" alt="...">
                                                </div>
                                                <div class="col-xl-11 col-lg-11 col-md-10 col-sm-10 col-xs-9">
                                                    <p class="customer-order-details">{{ @$ddt->orderProduct->product_text['title'] }}<br>
                                                        <?= limit_string(@$ddt->orderProduct->product_text->brief_description, 30) ?></p>


                                                </div>
                                            </div>
                                            <?php
                                                if(++$i === $numItems) {
                                                    
                                                }
                                                else
                                                {
                                                    echo '<hr>';
                                                }
                                            ?>
                                            
                                        </div>

                                        @endforeach
                                        <?php
                                        $status = '';
                                        $class='';
                                        if($dt->status == 0)
                                        {
                                            $status = trans('lang.processing');
                                            $class='label-warning';
                                        }
                                        else if($dt->status == 1)
                                        {
                                            $status = trans('lang.shipping');
                                            $class='label-primary';
                                        }else if($dt->status == 2)
                                        {
                                            $status = trans('lang.delivered');
                                            $class='label-success';
                                        }else if($dt->status == 3)
                                        {
                                            $status = trans('lang.cancelled');
                                            $class='label-danger cancel-process';
                                        }
                                        ?>
                                        <div class="panel-body mi-panelbody">
                                            <div class="row">
                                                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-3 text-center ">
                                                <p class="deliveredhead <?= $class ?>" style="margin-bottom: 0px">{{ $status }}</p>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                @endforeach
                            <?php
                        }else
                        {
                            ?>
                                <p>No Order Placed</p>
                            <?php
                        }
                    ?>
                    
                    
                    @else
                    
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
