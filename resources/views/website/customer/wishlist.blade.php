<div class="wishlist_div d-none">
    <div class="row">
        <div class="col-xl-11 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4 class="allheading">Wishlist</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-11 col-md-12">
            <div class="">
                <div class="panel-group">
                    <div class="row">
                        @if(isset($wishlist) && !empty($wishlist != null))
                        <?php
                            $numOrder = count($wishlist);
                            if($numOrder != 0)
                            {
                        ?>
                        @foreach($wishlist as $dt)
                           <div class="col-sm-6 col-xl-4">
                              <div class="sin-product style-two">
                                <div class="" style="height:320px" >
                                   <?= (@$dt->product->discount > 0)?'<label  class="offer_label">OFFER</label>':''?>
                                   <a href="#" onclick="wishlist({{$dt->product->id}}, '{{route('web.wishlist')}}')">
                                      <span class="wishlist-heart" ><i class="far fa-heart"></i></span>
                                   </a>
                                <a href="{{route('products.detail', $dt->product->permalink)}}">
                                 <img src="{{env('PRODUCT_IMAGES')}}{{@$dt->product->images[0]->image}}" alt="" class="prod-img">
                                </a>
                                </div>
                                <div class="shop-products-caption">
                                  <h5 class="pro-title  mb-0-7"><a href="{{route('products.detail', $dt->product->permalink)}}"><?= limit_string(@$dt->product->product_text->title,30) ?></a></h5>
                                  <a style="color:#000;" href="{{route('products.detail', $dt->product->permalink)}}">
                                  <div class="short_text mb-0-7" style='font-family: "AvenirLTStd-Light" !important;color:black !important'>
                                      <?= limit_string(@$dt->product->product_text->brief_description,30) ?>
                                  </div>
                                  <span class="product-thumbnail-price short_text"><b>@lang('lang.currency')
                                  <?= (@$dt->product->discount > 0)?'<s>'.@$dt->product->front_price->price.'</s>':@$dt->product->front_price->price ?>
                                  <?= (@$dt->product->discount > 0)?discounted_price(@$dt->product->front_price->price, @$dt->product->discount,@$dt->product->discount_type ):'' ?></b>
                                  </span>
                                  </a>
                                </div>
                              </div>
                              <!-- /.sin-product -->
                            </div>
                        @endforeach
                        <?php
                        }else
                        {
                            ?>
                                <p>No Item Added</p>
                            <?php
                        }
                    ?>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>