@extends('layouts.web')
@section('content')
<style>
  .iti 
  {
    width:100%
  }
</style>
<section class="breadcrumb-area">
	<div class="container-fluid custom-container">
		<div class="row">
			<div class="col-xl-12">
				<div class="bc-inner">
					<p><a href="{{route('web.home')}}">Rich House /</a> <a href="{{route('registration')}}"> @lang('lang.signup')</a></p>
				</div>
			</div>
			<!-- /.col-xl-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>

<div class="body-content">
   <div class="container margin-bottom">
      <div class="sign-in-page">
         <div class="row">
            <!-- create a new account -->
            <div class="offset-sm-3 col-md-6 col-sm-6 create-new-account">
               <h4 class="checkout-subtitle">Create a new account</h4>
               @include('includes.alert')
               <form class="validate ajaxForm" method="post" action="{{ route('registration-post')}}" id="signup">
                  @csrf
                  <div class="form-group">
                     <label class="info-title" >@lang('lang.email')<span>*</span></label>
                     <input name="email" type="email" class="form-control unicase-form-control text-input" >
                  </div>
                  <div class="form-group">
                     <label class="info-title" >@lang('lang.f_name') <span>*</span></label>
                     <input name="f_name" type="text" class="form-control unicase-form-control text-input" >
                  </div>
                  <div class="form-group">
                     <label class="info-title" >@lang('lang.l_name') <span>*</span></label>
                     <input name="l_name" type="text" class="form-control unicase-form-control text-input" >
                  </div>
                  <div class="form-group">
                     <label class="info-title" >@lang('lang.phone')<span>*</span></label>
                     <input type="tel" name="phone" id="phone" class="form-control">
                     <!-- <input name="phone" type="text" class="form-control unicase-form-control  only_num" autocomplete="off" > -->
                  </div>
                  
                  <div class="form-group">
                     <label class="info-title" >@lang('lang.password') <span>*</span></label>
                     <input name="password" type="password" class="form-control unicase-form-control text-input" >
                  </div>
                  <button type="button" class="btn signup-btn ajaxFormSubmit create btn-lg btn-block waves-effect waves-light m-1">@lang('lang.signup')</button>
               </form>
            </div>
            <!-- create a new account -->
         </div>
         <!-- /.row -->
      </div>
    </div>
  </div>
      <!-- /.sigin-in-->
      @endsection
@section('footer')
<script>
  $(document).ready(function() {

    var input = document.querySelector("#phone");
    var iti = window.intlTelInput(input, {
	    preferredCountries: ['sa', 'us'],
      separateDialCode: false,
      utilsScript: "{{asset('public/website/assets/dependencies/input-phone/js/utils.js')}}",
    });
    $.validator.addMethod(
      "check_number",
      function(value, element) {
        
        var status =true;
        if (input.value.trim()) {
            if (!iti.isValidNumber()) {
                status =false;
            }
        }
        return status;
      },

    );
    $("form.validate").validate({
      rules: {
         name:{
           required: true
         },
         phone:{
           required: true,
           check_number: true
           
         },
         email:{
           required: true,
           email: true
           
         },
         password:
         {
           required: true,
         }
      },
      messages: {
        name: "This field is required.",
        email: "This field is required.",
        password: "This field is required.",
        phone: {
          required: "This field is required.",
          check_number: "Invalid Number."
        },
      },
      invalidHandler: function(event, validator) {
        //display error alert on form submit 
        error("Please input all the mandatory values marked as red");

      },
      errorPlacement: function(label, element) { // render error placement for each input type   
        if(element[0].name=='phone')
        {
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
            $('<span class="error"></span>').insertAfter(element.parent()).append(label);
            var parent = $(element).parent('.input-with-icon');
            parent.removeClass('success-control').addClass('error-control');  
        }else
        {
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
            $('<span class="error"></span>').insertAfter(element).append(label);
            var parent = $(element).parent('.input-with-icon');
            parent.removeClass('success-control').addClass('error-control');  
        }   
      },
      highlight: function(element) { // hightlight error inputs
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
        var parent = $(element).parent();
        parent.removeClass('success-control').addClass('error-control');
      },
      unhighlight: function(element) { // revert the change done by hightlight
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent();
        parent.removeClass('error-control').addClass('success-control');
      },
      success: function(label, element) {
        var icon = $(element).parent('.input-with-icon').children('i');
        icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
        var parent = $(element).parent('.input-with-icon');
        parent.removeClass('error-control').addClass('success-control');

      }

    });

    

  });
  
  
  var max_fields = 6;
  var add_button = $("#customFields .addCF");
  var x = 1;
  
  
  $(document).on('keypress', ".only_num", function(e) {
    var x = e.which || e.keycode;
    if ((x >= 48 && x <= 57)) {
      return true;
    } else {
      e.preventDefault();
      return false;
    }
  });

</script>
@endsection