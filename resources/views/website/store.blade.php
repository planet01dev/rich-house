@extends('layouts.web')
@section('content')
<!--=========================-->
<!--=        Breadcrumb     =-->
<!--=========================-->

<section class="breadcrumb-area">
	<div class="container-fluid custom-container">
		<div class="row">
			<div class="col-xl-12">
				<div class="bc-inner">
					<p><a href="{{route('web.home')}}">Rich House /</a> <a href="{{route('stores')}}"> @lang('lang.store')</a></p>
				</div>
			</div>
			<!-- /.col-xl-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>

<!--=========================-->
<!--=      Breadcrumb     	=-->
<!--=========================-->



<!--Map area
============================================= -->

<section class="container container-two google-map pb-30">
	<div class="row no-gutters">
		
		<!-- /.col-xl-6 -->
<?php
	if(isset($store_data) && $store_data != null)
	{
		foreach($store_data as $v)
		{
			?>
<!-- <section class="google-map pb-30">
	<div class="row no-gutters"> -->
		<div class="col-md-6 col-lg-6">
			<div class="contact-info">
				<h5><?= $v['name']?></h5>
				<span><?= $v['city']?></span>
				<p><?= $v['address']?></p>
				<p><?= $v['days']?><span> <?= $v['timing']?></span> </p>
				<?php if($v['phone'] != ''){?>
                <p>@lang('lang.phone'):<span> <?= $v['phone']?></span></p>
                <?php } ?>

                <?php if($v['whatsapp'] != ''){?>
			    <p>Whatsapp:<span> <?= $v['whatsapp']?></span></p>
                <?php } ?>
			</div>
			<img  class="d-block w-100" src="{{env('STORE_IMAGES')}}{{ (file_exists(storage_path('store/').@$v['image']) && @$v['image'] != '') ? @$v['image'] : 'dummy.jpeg' }}" alt="">
			<!-- /.google-map -->
		</div>
		<!-- /.col-xl-6 -->
		<!-- <div class="col-md-6 col-lg-6">
			
		</div> -->
		<!-- /.col-xl-6 -->
	<!-- </div> -->
	<!-- /.row -->
<!-- </section> -->
			<?php
		}
	}
?>
		<!-- <div class="col-md-6 col-lg-6">
			
		</div> -->
		<!-- /.col-xl-6 -->
	</div>
	<!-- /.row -->
</section>

@endsection
@section('footer')
<script>
 $(document).ready(function() {
    
  });
</script>

@endsection
