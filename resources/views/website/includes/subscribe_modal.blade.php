<div class="modal popup-1" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-body popup-banner">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 mi-subscribe-img resp-order2">
                    <img src="{{env('SECTION_IMAGES')}}{{$subscribe->image}}" alt="">
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12 resp-order1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="mi-new-arrivals">
                        <h2>{!!$subscribe->subscribe_setting_text->title!!}</h2>
                        {!!$subscribe->subscribe_setting_text->description!!}
                        @include('website.includes.alert')
                        <div class="popup-subscribe">
                            <div class="subscribe-wrapper">
                                <form id="sub_form">
                                    <input placeholder="@lang('lang.email') @lang('lang.address')" id='subscribe_email' type="email" required class="">
                                </form>
                            </div>
                            <div class="mi-div-subscribe">
                                <button type="submit" id='subscribe_button' class="saa">@lang('lang.subscribe')</button>
                                <!-- <p> @lang('lang.no_spam') :)</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('footer')
<script>

    $(document).ready(function(){
        var visited = $.cookie("subscribe")

        if (visited == null) {
            $('#exampleModalCenter').modal('show');
        }

        // set cookie
        $.cookie('subscribe', 'yes', { expires: 1, path: '/' });

        $(document).on('click', "#subscribe_button", function() {
            var check = $('#sub_form').valid();
            if(!check)
            {
                return false;
            }
            var subscribe_email = $('#subscribe_email').val();
            $.ajax({
            url: "{{route('web.subscribe')}}",
            dataType: "json",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                email: subscribe_email
            },
            cache: false,
            success: function(result) {
                if(result['status'] == 'success')
                {
                    $('#success').show();
                    $('#success-text').html('Subscription Done');
                    $.cookie('subscribe', '1');
                }else if(result['status'] == 'error')
                {
                    $('#fail').show();
                    $('#fail-text').html(result['message']);
                    
                }
            }
            });
        });
    });
</script>
@endsection