<div id="mobilemenu" class="accordion">
        <ul>
        <?php
          $count = 0;
        ?>
          <!-- <li class="mob-logo"><a href="{{route('web.home')}}">
              <img src="{{ asset('public/website/assets/media/images/logo.png') }}" alt="">
            </a></li> -->
          <!-- <li><a href="#" class="closeme"><i class="flaticon-close"></i></a></li> -->
          @foreach($sidebars as $sidebar)
          <?php
          $count++;
        ?>
             @if(isset($sidebar->sidebar_sub_menu[0]->sidebar_sub_menu_text))
              <li>
                <a href="{{route('products.category.sidebar',[ $sidebar->permalink])}}" class="link drop_link">{{@$sidebar->sidebar_menu_text->title}}<i class="fa fa-chevron-down"></i></a>
                <ul class="submenu">
                  @foreach($sidebar->sidebar_sub_menu as $sub_menu)
                  <li><a href="{{route('products.category.sidebar.submenu',[ $sidebar->permalink, $sub_menu->permalink])}}">{{@$sub_menu->sidebar_sub_menu_text->title}}</a></li>
                  @endforeach
                </ul>
              </li>
            @else
            <li>
                <a href="{{route('products.category.sidebar',[ $sidebar->permalink])}}" class="link">{{@$sidebar->sidebar_menu_text->title}}</a>
            </li>
            @endif
            <?php
          if($count == 2)
          {
            ?>
        <li>
          <a href="{{route('brands')}}" class="link">@lang('lang.brands') <span role="button" id="dropdownAddClass" data-attr="0" class="drop_link"><i class="fa fa-chevron-down "></i></span></a>
          <ul class="submenu" >
            @foreach($all_brands as $brand)
              <li >
                <a href="{{route('products.brand',[ $brand->id])}}" >{{ $brand->brand_text->title }}</a>
              </li>
            @endforeach
          </ul>
        </li>
            <?php
          }
        ?>
          @endforeach

          <li>
            <a class="link" href="{{route('exclusive')}}">@lang('lang.exclusive')</a>
          </li>
          <li>
            <a class="link" href="{{route('best_seller')}}">@lang('lang.best_seller')</a>
          </li>
          <li>
            <a class="link" href="{{route('stores')}}">@lang('lang.store')</a>
          </li>
          <li>
          <a class="link" href="{{route('about')}}" >@lang('lang.about')</a>
        </li>
            <li >
              <a class="link" href="{{route('contact')}}">@lang('lang.contact')</a>
            </li>
            <li>
              <a class="link" href="{{route('our-story')}}">@lang('lang.our_story')</a>
            </li>
        </ul>
      <!--   <div class="mobile-login">
          <a href="#">Log in</a> |
          <a href="#">Create Account</a>
        </div> -->
        <form action="#" id="moble-search">
          <input placeholder="Search" type="text">
          <button type="submit"><i class="fa fa-search"></i></button>
        </form>
      </div>
