<?php
    $relavent_product = relavent_product(@$data->id);
    if (!empty($relavent_product)) {
        ?>
  <div class="container container-two">
    <div class="related-product-section-heading">
      <h3>@lang('lang.you_may_also_like')</h3>
    </div>
    <!-- /.section-heading-->
    <div class="row inner-wrapper">
      <!-- Single product -->
      
      <!-- Single product -->
      
      @foreach($relavent_product as $dt)
      <a href="{{route('products.detail', $dt->permalink)}}">
      <div class="col-sm-6 col-lg-4 col-xl-4">
        <div class="sin-product">
          <div class="pro-img">
            <img src="{{env('PRODUCT_IMAGES')}}{{@$dt->images[0]->image}}" alt="" class="related-prod prod-img">
          </div>
          <div class="shop-products-caption">
            <h5 class="pro-title"><a href="{{route('products.detail', $dt->permalink)}}">{!! @$dt->product_text->title !!}</a></h5>
            <span class="related-product-price">@lang('lang.currency')
            <?= (@$dt->discount > 0)?'<s>'.@$dt->front_price->price.'</s>':@$dt->front_price->price ?>
            <?= (@$dt->discount > 0)?discounted_price(@$dt->front_price->price, @$dt->discount,@$dt->discount_type ):'' ?>
            </span>
          </div>
          
        </div>
      </div>
      </a>
      @endforeach
    </div>
    <!-- Row End -->
  </div>
  <!-- Container  -->
  <?php
        }
      ?>