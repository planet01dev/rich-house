<script>
function addtocart( product_id, title, price = 0, quantity=1) {
	
	var inventory = $("#variant").find(":selected").data('inventory');
	if(inventory == 0)
	{
		error("Out Of Stock");
		return false;
	}
	if(price == 0){

      price = $("#discounted_price").val();
      /*if(price == '' || price == 0){
      	price = $.trim($(".span-pro-price").text());
      }*/
      if(Number.isNaN(price)){
      	price = 0;
      }
      quantity = $("#quantity").val();
	}
	price = price.replace(',', '');
	var variant_id = $('#variant').find(":selected").val();
    add_data(product_id, title, parseFloat(price), parseFloat(quantity),variant_id); 
}

set_data();
set_amount();

function add_data(product_id, title, price, quantity,variant_id){
	var data  = Array();
	data      = get_all_cookies();
	if(data.length){
		var total = 0;
		for(var i = 0; i<data.length; i++){
	      if(extract_product(data[i].val)[0] == product_id){
	      	total = parseFloat(extract_product(data[i].val)[1]);
	      }
		}

	 var product = product_format(product_id, total+quantity, title,price,variant_id);
	 $.cookie(product['key'], product['val'], { expires: 1, path: '/' });
	}else{
	   var product = product_format(product_id, quantity, title, price,variant_id);
	   $.cookie(product['key'], product['val'], { expires: 1, path: '/' });
	}
	//console.log($.cookie(product['key']));
	set_data();
	set_amount();
	cart_popup(title);
}

function set_amount(){
	var amount = count_amount();
	var with_tax_amount = count_amout_with_tax();
	if(amount > 0){
		$('.product_total_currency').css('display', 'inline');
        $(".product_total_sub_amount").html(amount);
        $(".product_total_amount").html(with_tax_amount);
	}else{
		$('.product_total_currency').css('display', 'none');
        $(".product_total_amount").html('');
	}
}

function count_amount(){
	var data  = Array();
	data      = get_all_cookies() ;
	var total = 0;
	for(var i = 0; i<data.length; i++){
	   total = parseFloat(total) + parseFloat(extract_product(data[i].val)[3]);
	}	
	

	return round_value(total);
}

function count_amout_with_tax(){
	var total = parseFloat(count_amount());
    if($('#tax').length){
		var tax = $('#tax').val();
		var tax_amount = (total/100) * parseFloat(tax);
		total = parseFloat(tax_amount) + parseFloat(total);
	}
	if($('#shipping').length){
		var tax = $('#shipping').val();
		total = parseFloat(total) + parseFloat(tax);
	}
    return round_value(total);
}

function count_item_amount(price, quantity){
    return round_value(quantity*price);
}

function set_data(){
	data = count_data();
	$("#product_count").html((data > 0)?data:'0');
}
function count_data(){
	var data  = Array();
	data      = get_all_cookies() ;
	var total = 0;
	for(var i = 0; i<data.length; i++){
	      total = parseFloat(total) + parseFloat(extract_product(data[i].val)[1]);
	}	
	return total;
}

function cart_popup(title){
	var title = title;
	// console.log(title + " ---");
	var total_amount = count_amount();
	var total_item   = count_data();
    $('#cart_product_title').html(title);
    $('#popup_total_item').html(total_item);
    $('#popup_total_amount').html(total_amount);
	$('#cart_popup').trigger("click");

    $('.cart-prod-title').html(title);
	$('.viewCart-div').show();
	setTimeout(function(){
    $(".viewCart-div").fadeOut("slow");
	},4500)
	}
	
$(document).ready(function () {
    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
});

function product_format(product_id, count, title, price,variant_id, update=0){
	var data     = Array(); 
	data['key']  = "product_"+product_id;
	if(update != 0){
    	data['val']  =  product_id+"__-__"+count+"__-__"+title+"__-__"+count_item_amount(price,count)+"__-__"+variant_id+"__-__"+update;
	}else{
    	data['val']  =  product_id+"__-__"+count+"__-__"+title+"__-__"+count_item_amount(price,count)+"__-__"+variant_id+"__-__"+add_variation(product_id);
	}
    return data;
}

function extract_product(item){
	var str = item;
	if(str.search("__-__") > -1){
	  return item.split("__-__");
	}else{
		var data = Array();
		data[0]  = '';
		data[1]  = 0;
		data[2]  = '';
		data[3]  = 0;
	}
}

function get_all_cookies(){
  var data = Array();
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    var row = c;
    var key = row.split('=')[0];
    var val = row.split('=')[1];
	var obj = {key : key, val: val};
    if(key.search("product_") > -1){
	    data.push(obj);
    }else{
    	//console.log(obj);
    }
  }
  return data;
}

function delete_data(product_id){
	 $.removeCookie('product_'+product_id, {path: '/' });
	 location.reload();
}

function update_data(product_id){
	setTimeout(function(){ 
		var data       = $.cookie("product_"+product_id);
		var quantity   = $(".qty_"+product_id).val();
		var row        = extract_product(data); 
		var item_price = row[3]/row[1];
		var price      = count_item_amount(quantity, item_price);
		var product    = "";
		if(row[4]){
			 product = product_format(row[0], quantity, row[2], item_price, row[4]);
		}else{
			 product = product_format(row[0], quantity, row[2], item_price);
		}
		//console.log(quantity);
		//console.log(product);
        $.cookie(product['key'], product['val'], { expires: 1, path: '/' });
		$("#item_amount"+product_id).html(price);
		set_data();
		set_amount();
		//cart_popup(row[2]);
		set_mycart_total();
	 }, 300);

}
function add_variation(product){
  var variation = $('#variation_option').val();  
  var vari  = Array();
  var itms_data  = Array();
  if(variation && variation != ''){
    var item = variation.split(",");
    for(var i = 0; i<item.length -1; i++){
       var val = $.trim($("#variation_item_"+item[i]+" option:selected").html());
       vari.push(item[i]+"="+val+",");
    }
  }
  return vari;
}

function set_mycart_total(){
	$('#my_cart_total').html(count_amount());
}
$('.variation_item_change').on('change', function(e) {
	if(this.value != '' && this.value != 0){
	   $("#pro_price").val(this.value);
	   $(".span-pro-price").html(this.value);
	}
});
if($(".variation_item_change option:selected")){
  var val = $(".variation_item_change option:selected").val();
  if(val != '' && val != 0){
  	 $("#pro_price").val(val);
	 $(".span-pro-price").html(val);
  }

}


var quantitiy = 1;
$('.quantity-right-plus').click(function (e) {
   e.preventDefault();
    var quantity = parseFloat($(this).siblings('.quantity').val());
   // increment
   if (Number.isNaN(quantity)) {
       quantity = 0;
    }
    if(parseFloat(quantity + 1) <=  $(this).siblings('.quantity').attr("max")){
	    $(this).siblings('.quantity').val(parseFloat(quantity)+ 1);
	    $(this).closest('td.product-quantity').next('td').find('.pro_quantity').html(parseFloat(quantity) + 1);

    }

});
$('.quantity-left-minus').click(function (e) {
    e.preventDefault();
    var quantity = parseFloat($(this).siblings('.quantity').val());
    //decrement
    if (quantity > 1 && parseFloat(quantity - 1) >= parseFloat($(this).siblings('.quantity').attr("min"))) {
        $(this).siblings('.quantity').val(parseFloat(quantity - 1));
        $(this).closest('td.product-quantity').next('td').find('.pro_quantity').html(parseFloat(quantity - 1));
    }
});
function round_value(num){
 // return Math.round(num).toFixed(2);
  return num.toFixed(2);
}
</script>
