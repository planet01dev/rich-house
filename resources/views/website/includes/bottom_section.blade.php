<section class="login-now">
	<div class="container-fluid custom-container">
		<div class="col-12">
			<span>@lang('lang.online_exclusive')</span>
			<a href="{{route('products')}}" class="btn-two">@lang('lang.view_products')</a>
		</div>
		<!-- /.col-12 -->
	</div>
	<!-- /.container-fluid -->
</section>