<div class="col-md-3 col-sm-3 col-xs-3">
  <div class="mi-mainmenu-haeding">
    <a class="mi-menu-heading" href="#menu">@lang('lang.menu')</a>
    <nav id="menu">
      <ul>
        <?php
          $count = 0;
        ?>
        @foreach($sidebars as $sidebar)
        <?php
          $count++;
        ?>
        @if(isset($sidebar->sidebar_sub_menu[0]->sidebar_sub_menu_text))
        <li class="Selected">
          <a href="{{route('products.category.sidebar',[ $sidebar->permalink])}}" class="" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="true">{{@$sidebar->sidebar_menu_text->title}} 
          <!-- <span class="caret"></span> -->
          </a>
          <ul class="">
            @foreach($sidebar->sidebar_sub_menu as $sub_menu)
            <li>
              <a href="{{route('products.category.sidebar.submenu',[ $sidebar->permalink, $sub_menu->permalink])}}">{{@$sub_menu->sidebar_sub_menu_text->title}}</a>
            </li>
            @endforeach
          </ul>
        </li>
        @else
        <li>
          <a href="{{route('products.category.sidebar',[ $sidebar->permalink])}}" class="">{{@$sidebar->sidebar_menu_text->title}}</a>
        </li>
        @endif
        <?php
          if($count == 2)
          {
            ?>
            <li class="Selected">
              <a class="" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="true" href="{{route('brands')}}">@lang('lang.brands')</a>
              <ul class="" >
                @foreach($all_brands as $brand)
                  <li>
                    <a href="{{route('products.brand',[ $brand->id])}}" >{{ $brand->brand_text->title }}</a>
                  </li>
                @endforeach
              </ul>
            </li>
            <?php
          }
        ?>
        @endforeach

        <!-- <li class="Selected"><a href="#">Home</a></li>
        <li><span>About us</span>
          <ul>
            <li><a href="#about/history">History</a></li>
            <li><span>The team</span>
              <ul>
                <li><a href="#about/team/management">Management</a></li>
                <li><a href="#about/team/sales">Sales</a></li>
                <li><a href="#about/team/development">Development</a></li>
              </ul>
            </li>
            <li><a href="#about/address">Our address</a></li>
          </ul>
        </li> -->
        <!-- <li><a href="#contact">Contact</a></li> -->
        <li>
          <a class="" href="{{route('exclusive')}}">@lang('lang.exclusive')</a>
        </li>
        <li>
          <a class="" href="{{route('best_seller')}}">@lang('lang.best_seller')</a>
        </li>
        <li>
          <a class="" href="{{route('stores')}}">@lang('lang.store')</a>
        </li>
        <li>
          <a class="" href="{{route('about')}}" >@lang('lang.about')</a>
        </li>
        <li>
          <a class="" href="{{route('contact')}}">@lang('lang.contact')</a>
        </li>
        <li>
          <a class="" href="{{route('our-story')}}">@lang('lang.our_story')</a>
        </li>
      </ul>
    </nav>
  </div>  
</div>

