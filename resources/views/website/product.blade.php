@php($page = "home")
@extends('layouts.web')
@section('content')

<!--=========================-->
<!--=        Breadcrumb         =-->
<!--=========================-->

<section class="breadcrumb-area">
  <div class="container-fluid custom-container">
    <div class="row p-0">
      <div class="col-md-12 col-sm-12 col-xs-12 p-0 sec1-img">
        <div class="sin-banner align-items-center two-columns">
          <div class="mi-banner-top-text marquee">
          <?php
              $setting = get_settings();
            ?>
              <p>
                <?= $setting['ticker_1']?>
                <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
                <?= $setting['ticker_2']?>
                <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
                <?= $setting['ticker_3']?>
                <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
                <?= $setting['ticker_4']?>
              </p>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-6 col-sm-12 col-xs-12 p-0 sec1-img">
        <div class="sin-banner align-items-center two-columns">
          <div class="mi-banner-top-text marquee">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </div>
        </div>
      </div> -->
    </div>
    <div class="row">
      <div class="col-xl-12">
        <div class="bc-inner">
          <p><a href="{{route('web.home')}}">Rich House / </a> <a href="{{route('products')}}"> @lang('lang.products')  </a> </p>
        </div>
      </div>
      <!-- /.col-xl-12 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</section>

<!--=========================-->
<!--=        Shop area          =-->
<!--=========================-->

<section class="shop-area">
  <div class="container-fluid custom-container">
    <div class="row">
      <div class="product_filter_button_div">
        <button class="product_filter_button btn toggle_btn">Filter <i class="fas fa-angle-down filter-arrow-down float-rght"></i></button>
      </div>
      <div class="order-1 order-lg-1 col-lg-3 col-xl-3 product_filter_div">
        <div class="shop-sidebar left-side">
          <div class="sidebar-widget category-widget">
            <h1>@lang('lang.filter_by')</h1>
            <ul>
              <?php
              $count = 0;
              ?>
              @foreach($tag as $dt)
              <li>
                <div class="custom-control custom-checkbox">
                  <input name="tag_filter" type="checkbox" <?= (@$tag_id == $dt->id)?"checked":""?> value="{{@$dt->id}}" class="custom-control-input product_filter" id="tagChecked<?= @$count ?>">
                  <label class="custom-control-label" for="tagChecked<?= @$count ?>">{!! @$dt->tag_text->title !!}<span></span></label>
                </div>
              </li>
              <?php
              $count++;
              ?>
              @endforeach

            </ul>
          </div>

          <div class="sidebar-widget range-widget">
            <h1>@lang('lang.brands')</h1>
            <div class="form-group has-search">
              <span class="fa fa-search form-control-feedback"></span>
              <input type="text" id='brand_search' class="form-control mi-search-controls" placeholder="Search Brands">
            </div>
            <div class="mi_list_overflow-y">
              <ul>
                <?php
                $count = 0;
                ?>
                @foreach($brand as $dt)
                <li>
                  <div class="custom-control custom-checkbox brands">
                    <input name='brand_filter' type="checkbox" <?= (@$brand_id == $dt->id)?"checked":""?> value="{{@$dt->id}}" class="custom-control-input product_filter" id="brandChecked<?= @$count ?>">
                    <label class="custom-control-label" for="brandChecked<?= @$count ?>">{{@$dt->brand_text->title}}<span></span></label>
                  </div>
                </li>
                <?php
                $count++;
                ?>
                @endforeach
              </ul>
            </div>
          </div>

          <div class="sidebar-widget color-widget">
            <h1>@lang('lang.price')</h1>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label class="label-min-amount">@lang('lang.minimum') @lang('lang.amount')</label>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-8 col-md-6 col-sm-6 col-xs-6">
                <input class="inputdefault product_price_filter txtboxToFilter" id="price-amount1" value='{{ @$min_amount }}' name="min_amount" type="text" placeholder="AED 50" >
                <small id="price-min-amount" class="form-text text-muted">@lang('lang.min') <br> @lang('lang.currency') <br> 50</small>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label class="label-max-amount">@lang('lang.maximum') @lang('lang.amount')</label>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-8 col-md-6 col-sm-6 col-xs-6">
                <input class="inputdefault product_price_filter txtboxToFilter" id="price-amount2" value='{{ @$max_amount }}' name="max_amount" type="text" placeholder="AED 5050" >
                <small id="price-max-amount" class="form-text text-muted">@lang('lang.min') <br> @lang('lang.currency') <br> 5050</small>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="order-2 order-lg-2 col-lg-9 col-xl-9">
        <div class="shop-sorting-area row scents-section">
          <h1><?= @$selected?></h1>
        </div>
        <!-- /.shop-sorting-area -->
        <div class="shop-content">
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <div class="row">
                  @foreach($data as $dt)
                  <div class="col-sm-6 col-xl-4">
                  <div class="sin-product style-two">
                    <div class="" style="height:320px">
                    <?php if(@$dt->exclusive == 1 || @$dt->new_in == 1){?>
                      <?php if(@$dt->exclusive == 1){?>
                    <label class="exclusive_label">@lang('lang.exclusive')</label>
                    <?php }?>
                    <?php if(@$dt->new_in == 1){?>
                    <label class="exclusive_label">@lang('lang.new_in')</label>
                    <?php }?>
                    <?php } 
                      else
                      {
                         (@$dt->discount > 0)?'<label class="offer_label">OFFER</label>':'';
                      }
                    ?>
                        <a href="#" onclick="wishlist({{$dt->id}}, '{{route('web.wishlist')}}')">
                           @if(!empty($wishlist) && in_array($dt->id, $wishlist))
                            <span class="wishlist-heart" ><i class="far fa-heart"></i></span>
                           @else
                           <label class="wishlist-heart" ><i class="far fa-heart"></i></label>
                           @endif
                         </a>
                    <a href="{{route('products.detail', $dt->permalink)}}">
                     <img src="{{env('PRODUCT_IMAGES')}}{{@$dt->images[0]->image}}" alt="" class="prod-img">
                    </a>
                    </div>
                    <div class="shop-products-caption">
                    
                      <h5 class="pro-title  mb-0-7"><a href="{{route('products.detail', $dt->permalink)}}"><?= limit_string(@$dt->product_text->title,30) ?></a></h5>
                      <a style="color:#000;" href="{{route('products.detail', $dt->permalink)}}">
                      <div class="short_text mb-0-7" style='font-family: "AvenirLTStd-Light" !important;color:black !important'>
                          <?= limit_string(@$dt->product_text->brief_description,30) ?>
                      </div>
                      <span class="product-thumbnail-price short_text"><b>@lang('lang.currency') 
                      <?= (@$dt->discount > 0)?'<s>'.@$dt->front_price->price.'</s>':@$dt->front_price->price ?>
                      <?= (@$dt->discount > 0)?discounted_price(@$dt->front_price->price, @$dt->discount,@$dt->discount_type ):'' ?></b>
                      </span>
                      </a>
                    </div>
                    <!-- <div class="icon-wrapper">
                      <div class="pro-icon product-thumbnail-icon">
                        <ul>
                          class="trigger"
                          <li><a href="{{route('products.detail', $dt->permalink)}}"><i class="flaticon-eye"></i></a></li>
                        </ul>
                      </div>
                      <div class="add-to-cart">
                        <a href="{{route('products.detail', $dt->permalink)}}">@lang('lang.view')</a>
                      </div>
                    </div> -->
                  </div>
                  <!-- /.sin-product -->
                </div>
                @endforeach
                <!-- /.col- -->


              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.tab-content -->
          <!--  <div class="load-more-wrapper">
                <a href="create-account.html" class="btn-two">Load More</a>
              </div> -->
          <!-- /.load-more-wrapper -->

        </div>
        <!-- /.shop-content -->
      </div>
      <!-- /.col- -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>
<!-- /.shop-area -->
<div style="display: flex; justify-content: center;">
<?php if (isset($data)) { ?>
  {!! $data->render() !!}
<?php } ?>
</div>
@endsection
@section('footer')

<script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!}
</script>

<script>
  $('.product_filter_button').click(function() {
    $('.product_filter_div').toggle();
  });
  $(function() {
    $("input:checkbox").on('click', function() {
      // in the handler, 'this' refers to the box clicked on
      var $box = $(this);
      if ($box.is(":checked")) {
        // the name of the box is retrieved using the .attr() method
        // as it is assumed and expected to be immutable
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        // the checked state of the group/box on the other hand will change
        // and the current value is retrieved using .prop() method
        $(group).prop("checked", false);
        $box.prop("checked", true);
      } else {
        $box.prop("checked", false);
      }
    });
  });

  $("#brand_search").keyup(function() {

    // Retrieve the input field text and reset the count to zero
    var filter = $(this).val(),
      count = 0;

    // Loop through the comment list
    $('.brands').each(function() {


      // If the list item does not contain the text phrase fade it out
      if ($(this).text().search(new RegExp(filter, "i")) < 0) {
        $(this).hide();

        // Show the list item if the phrase matches and increase the count by 1
      } else {
        $(this).show();
        count++;
      }

    });

  });
  $(document).on('click', ".product_filter", function() {
    var tag_id = $("[name='tag_filter']:checked").val();
    var brand_id = $("[name='brand_filter']:checked").val();

    if (tag_id == null){
      tag_id = 0;
    }
    if (brand_id == null){
      brand_id = 0;
    }

    var min_amount = $("[name='min_amount']").val();
    var max_amount = $("[name='max_amount']").val();

    
    
    window.location.href = APP_URL+'/products/search/'+tag_id+'/'+brand_id+'/'+min_amount+'/'+max_amount;
  });

  $(document).on('focusout', ".product_price_filter", function() {
    var tag_id = $("[name='tag_filter']:checked").val();
    var brand_id = $("[name='brand_filter']:checked").val();
    
    if (tag_id == null){
      tag_id = 0;
    }
    if (brand_id == null){
      brand_id = 0;
    }

    var min_amount = $("[name='min_amount']").val();
    var max_amount = $("[name='max_amount']").val();

    

    window.location.href = APP_URL+'/products/search/'+tag_id+'/'+brand_id+'/'+min_amount+'/'+max_amount;
    

  });
  
  
    $(function(){
      $(".toggle_btn").on("click", function () {
      $(this).find('i').toggleClass("fa-angle-up fa-angle-down");
      });
    }); 
</script>
@endsection