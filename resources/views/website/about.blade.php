@php($page = "home")
@extends('layouts.web')
@section('content')


<?php
        $data= get_settings();
      ?>
<section class=" padding-top-120 width-100">
  <div class="container custom-container">
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12 p-0">
        <div class="sin-banner align-items-center two-columns">
          <img src="{{env('SECTION_IMAGES')}}{{$data->about_image}}" alt="">
        </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12 p-0">
        <div class="aboutus-text sin-banner align-items-center two-columns">
          <h1>{{ @$page_data->page_text->title }}</h1>
          	{!! @$page_data->page_text->content !!}
        </div>
      </div>
    </div>
  </div>
</section>

<section class="">
  <div class="container custom-container">
    <div class="row justify-content-center">
      
        
            
      <div class="col-md-4 col-sm-12 col-xs-12 p-0">
        
        <div class="sin-product">
          <div class="h-auto" >
          <img src="{{env('SECTION_IMAGES')}}{{$data->section_1_image}}" alt="">
          </div>
          <?php if($data->setting_text->section_1_text == "" ){ ?>
            <div class="d-none">
              <h5 class="pro-title"><a href="{{$data->section_1_link}}">{{@$data->setting_text->section_1_text}}</a></h5>
            </div>
          <?php } else {?>
          <div class="mid-wrapper style-two">
            <h5 class="pro-title"><a href="{{$data->section_1_link}}">{{@$data->setting_text->section_1_text}}</a></h5>
          </div>
          <?php } ?>
        </div>
        
      </div>

      <div class="col-md-4 col-sm-12 col-xs-12 p-0">
        
        <div class="sin-product">
          <div class="h-auto" >
          <img src="{{env('SECTION_IMAGES')}}{{$data->section_2_image}}" alt="">
          </div>
          <?php if($data->setting_text->section_2_text == "" ){ ?>
            <div class="d-none">
              <h5 class="pro-title"><a href="{{$data->section_2_link}}">{{@$data->setting_text->section_2_text}}</a></h5>
            </div>
          <?php } else {?>
            <div class="mid-wrapper style-two">
              <h5 class="pro-title"><a href="{{$data->section_2_link}}">{{@$data->setting_text->section_2_text}}</a></h5>
            </div>
          <?php } ?>
        </div>
        
      </div>

      <div class="col-md-4 col-sm-12 col-xs-12 p-0">
        
        <div class="sin-product">
          <div class="h-auto" >
          <img src="{{env('SECTION_IMAGES')}}{{$data->section_3_image}}" alt="">
          </div>
          <?php if($data->setting_text->section_3_text == "" ){ ?>
            <div class="d-none">
              <h5 class="pro-title"><a href="{{$data->section_3_link}}">{{@$data->setting_text->section_3_text}}</a></h5>
            </div>
          <?php } else {?>  
            <div class="mid-wrapper style-two">
              <h5 class="pro-title"><a href="{{$data->section_3_link}}">{{@$data->setting_text->section_3_text}}</a></h5>
            </div>
          <?php } ?>
        </div>
        
      </div>

      
          
          
    </div>
    <!-- /.row -->
  </div>
  <!-- Container-two  -->
</section>
  <!-- /#site -->
@endsection