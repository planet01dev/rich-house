@php($page = "main")
@extends('layouts.web')
@section('content')

<section class="banner padding-top-120 width-100">
  <div class="container custom-container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 p-0 sec1-img">
        <div class="sin-banner align-items-center">
          <div class="mi-banner-top-text marquee">
            <?php
            $setting = get_settings();
            ?>
            <p>
              <?= $setting['ticker_1'] ?>
              <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
              <?= $setting['ticker_2'] ?>
              <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
              <?= $setting['ticker_3'] ?>
              <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
              <?= $setting['ticker_4'] ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="banner width-100">
  <div class="container custom-container">
    <div class="row">
      @foreach($data as $dt)
      @if($dt->section_no == 1 && $dt->is_active == 1)

      <div class="col-md-6 col-sm-12 col-xs-12 p-0 sec1-img">
        <div class="sin-banner align-items-center two-columns">
          <!-- <div class="mi-banner-top-text marquee">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div> -->
          <a class="index_anchor" href="{{route('products.category.section',[ $dt->permalink])}}">
            <img src="{{env('SECTION_IMAGES')}}{{$dt->image}}" alt="">
            <div class="row">
              <div class="col-md-6 banner1-text-left text-left">
                <p class="mi-img-text-left">{{@$dt->section_menu_text->title}}</p>
              </div>
              <div class="col-md-6 banner1-text-right text-right">
                <p class="mi-img-text-right">@lang('lang.more')...</p>
              </div>
            </div>
          </a>
        </div>
      </div>
      @endif
      @endforeach
      <!-- <div class="col-md-6 col-sm-12 col-xs-12 p-0 sec1-img">
            <div class="sin-banner align-items-center two-columns">
              <div class="mi-banner-top-text">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </div>
              <img src="{{ asset('public/website/assets/media/images/sec1-img2.png') }}" alt="">
              <div class="row">
                <div class="col-md-6 banner2-text-left text-left">
                  <p class="mi-img-text-left">Lorem epsum</p>
                </div>
                <div class="col-md-6 banner2-text-right text-right">
                  <p class="mi-img-text-right">More...</p>
                </div>
              </div>
            </div>
          </div> -->
      <!-- </a> -->
    </div>
  </div>
</section>

<section class="banner pb-30 width-100">
  <div class="container custom-container">
    <div class="row">
      @foreach($data as $dt)
      @if($dt->section_no == 2 && $dt->is_active == 1)

      <div class="col-md-4 col-sm-12 col-xs-12 p-0 sec1-img">
        <div class="sin-banner align-items-center three-columns">
          <!-- <div class="mi-banner-top-text marquee">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div> -->
          <a class="index_anchor" href="{{route('products.category.section',[ $dt->permalink])}}">
            <img src="{{env('SECTION_IMAGES')}}{{$dt->image}}" alt="">
            <div class="row">
              <div class="col-md-6 banner1-text-left text-left">
                <p class="mi-img-text-left">{{@$dt->section_menu_text->title}}</p>
              </div>
              <div class="col-md-6 banner1-text-right text-right">
                <p class="mi-img-text-right">@lang('lang.more')...</p>
              </div>
            </div>
          </a>
        </div>
      </div>

      <!--<div class="col-md-4 col-sm-12 col-xs-12 p-0 sec2-img">-->
      <!--  <div class="sin-banner align-items-center three-columns">-->
      <!--  <a class="index_anchor" href="{{route('products.category.section',[ $dt->permalink])}}">-->
      <!--    <img src="{{env('SECTION_IMAGES')}}{{$dt->image}}" alt="">-->
      <!--    <div class="row">-->
      <!--      <div class="col-md-6 banner1-text-left text-left">-->
      <!--        <p class="mi-img-text-left">{{@$dt->section_menu_text->title}}</p>-->
      <!--      </div>-->
      <!--      <div class="col-md-6 banner1-text-right text-right">-->
      <!--        <p class="mi-img-text-right">@lang('lang.more')...</p>-->
      <!--      </div>-->
      <!--    </div>-->
      <!--  </a>-->
      <!--  </div>-->
      <!--</div>-->
      @endif
      @endforeach

      <!--  <div class="col-md-4 col-sm-12 col-xs-12 p-0 sec2-img">
            <div class="sin-banner style-two three-columns">
              <img src="{{ asset('public/website/assets/media/images/sec2-img2.png') }}" alt="">
              <div class="row">
                <div class="col-md-6 banner2-text-left text-left">
                  <p class="mi-img-text-left">Lorem epsum</p>
                </div>
                <div class="col-md-6 banner2-text-right text-right">
                  <p class="mi-img-text-right">More...</p>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-sm-12 col-xs-12 p-0 sec2-img">
            <div class="sin-banner align-items-center three-columns">
              <img src="{{ asset('public/website/assets/media/images/sec2-img3.png') }}" alt="">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 banner1-text-left text-left">
                  <p class="mi-img-text-left">Lorem epsum</p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 banner1-text-right text-right">
                  <p class="mi-img-text-right">More...</p>
                </div>
              </div>
            </div>
          </div> -->
    </div>
  </div>
</section>

<section class="main-product">
  <div class="container container-two">
    <div class="section-heading text-center">
      <div class="text-center mi-heading-box">
        <h3>{{ @$setting->setting_text->below_section_heading }}</h3>
      </div>
      <p>{{ @$setting->setting_text->below_section_text }}</p>
    </div>
    <!-- <button class="text-center mi-heading-btn">
      <a class="mi-heading-link" href="#"> {{ @$setting->setting_text->below_section_button }} </a>
    </button> -->
    <div class="collection-content">
      <div class="row">
        @foreach($data as $dt)
        @if($dt->section_no == 3 && $dt->is_active == 1)
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="single-collection">
            <a class="index_anchor" href="{{route('products.category.section',[ $dt->permalink])}}">
              <img class="heart_gold-img" src="{{env('SECTION_IMAGES')}}{{$dt->image}}" alt="">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 banner1-text-left text-left">
                  <p class="heart_gold-img-text-left">{{@$dt->section_menu_text->title}}</p>
                </div>
              </div>
            </a>
          </div>
        </div>
        @endif
        @endforeach

        <!--  <div class="col-md-6 col-sm-12 col-xs-12">
              <div class="single-collection">
                <a href="#">
                  <img class="heart_gold-img" src="{{ asset('public/website/assets/media/images/sec3-img2.png') }}" alt="">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6 banner1-text-left text-left">
                      <p class="heart_gold-img-text-left">Lorem epsum</p>
                    </div>
                  </div>
                </a>
              </div>
            </div> -->
      </div>
    </div>
  </div>
</section>
@foreach($data as $dt)
@if($dt->section_no == 4 && $dt->is_active == 1)
<section class="featured-img-area">
  <div class="container-fluid custom-container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 p-0">
        <a class="index_anchor" href="{{route('products.category.section',[ $dt->permalink])}}">
          <div class="text-left">
            <p class="fullwidth-img-text-left">{{@$dt->section_menu_text->title}}</p>
          </div>
          <img src="{{env('SECTION_IMAGES')}}{{$dt->image}}" width="100%" height="100%" alt="">
        </a>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
</section>
@endif
@endforeach

<section class="banner-product">
  <div class="container container-two">
    <div class="row justify-content-center">
      @foreach($data as $dt)
      @if($dt->section_no == 5 && $dt->is_active == 1)
      <div class="col-md-4 col-sm-12 col-xs-12">
        <!-- Single Product-->
        <a class="pro-img-link" href="{{route('products.category.section',[ $dt->permalink])}}">
          <div class="sin-product">
            <div class="pro-img">
              <!-- <div class="icon-wrapper">
                      <div class="pro-icon">
                        <ul>
                          <li><a href="#"><i class="flaticon-valentines-heart"></i></a></li>
                          <li><a href="#"><i class="flaticon-compare"></i></a></li>
                          <li><a class="trigger" href="#"><i class="flaticon-eye"></i></a></li>
                        </ul>
                      </div>
                      <div class="add-to-cart">
                        <a href="#">add to cart</a>
                      </div>
                    </div> -->
              <img src="{{env('SECTION_IMAGES')}}{{$dt->image}}" width="100%" height="100%" alt="">
            </div>
            <div class="mid-wrapper style-two">
              <h5 class="pro-title">{{@$dt->section_menu_text->title}}</h5>
            </div>
          </div>
        </a>
        <!-- Single Product-->
      </div>
      @endif
      @endforeach

      <!--  <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="sin-product">
              <div class="pro-img">
               
                <img src="{{ asset('public/website/assets/media/images/sec4-img2.png') }}" width="100%" height="100%" alt="">
              </div>
              <div class="mid-wrapper style-two">
                <h5 class="pro-title"><a href="product.html">SHOP TRAVEL PERFUMES</a></h5>
              </div>
            </div>
          </div> -->

      <!--   <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="sin-product style-two">
              <div class="pro-img">
               
                <img src="{{ asset('public/website/assets/media/images/sec4-img3.png') }}" width="100%" height="100%" alt="">
              </div>
              <div class="mid-wrapper style-two">
                <h5 class="pro-title"><a href="product.html">DISCOVER NEW EDITIONS</a></h5>
              </div>
            </div>
          </div> -->
    </div>
    <!-- /.row -->
  </div>
  <!-- Container-two  -->
</section>
<!-- /#site -->
@endsection