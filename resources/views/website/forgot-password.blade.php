@extends('layouts.web')
@section('content')
<!--=========================-->
<!--=        Breadcrumb         =-->
<!--=========================-->

<section class="breadcrumb-area">
	<div class="container-fluid custom-container">
		<div class="row">
			<div class="col-xl-12">
				<div class="bc-inner">
					<p><a href="#">Forgot Password </a></p>
				</div>
			</div>
			<!-- /.col-xl-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>
<section class="contact-area">
	<div class="container-fluid custom-container">
		<div class="section-heading pb-30">
			<h3>Forgot Account Password</h3>
		</div>
		<div class="row justify-content-center">
			<div class="col-sm-9 col-md-8 col-lg-6 col-xl-4">
				@if (Session::has('message'))
				<div class="alert alert-success alert-icon-info alert-dismissible" id="success" role="alert">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<!-- <div class="alert-icon icon-part-info">
					<i class="icon-info"></i>
					</div> -->
					<div class="alert-message">
					  <span id="success-text">{{Session::get('message')}}</span>
					</div>
				</div>
				@endif
				<div class="contact-form">
					<form action="{{route('forgot-password.store')}}" method="post">
						@csrf
						<div class="row">
							<div class="col-xl-12 mb-4">
								<input type="text" <?= (Session::has('message'))?'readonly':''; ?> name="email" placeholder="Enter Email Address*" class="mb-0" value="{{old('email')}}">
								@if($errors->any())
								<p class="text-left text-danger ml-2">{{$errors->first()}}</p>
								@endif
								
							</div>
							<div class="col-xl-12">
								<input type="submit" value="Reset Password" >
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.row end -->
	</div>
	<!-- /.container-fluid end -->
</section>
<!-- /.contact-area end -->

<section class="login-now">
	<div class="container-fluid custom-container">
		<div class="col-12">
			<span>Dont have account</span>
			<a href="{{route('registration')}}" class="btn-two">Create Account</a>
		</div>
		<!-- /.col-12 -->
	</div>
	<!-- /.container-fluid -->
</section>
@endsection
