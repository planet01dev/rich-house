@extends('layouts.web')
@section('content')
<style>
.iti.iti--allow-dropdown
{
	width: 100%;
    margin-bottom: 30px;
}

</style>
<!--=========================-->
<!--=        Breadcrumb     =-->
<!--=========================-->

<section class="breadcrumb-area">
	<div class="container-fluid custom-container">
		<div class="row">
			<div class="col-xl-12">
				<div class="bc-inner">
					<p><a href="{{route('web.home')}}">Rich House /</a> <a href="{{route('contact')}}"> @lang('lang.contact')</a></p>
				</div>
			</div>
			<!-- /.col-xl-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>

<!--=========================-->
<!--=        Breadcrumb         =-->
<!--=========================-->



<!--Map area
============================================= -->

<section class="google-map pb-30">
	<div class="row no-gutters">
		<div class="col-md-6 col-lg-6">
		
		<img  class="d-block w-100" src="{{env('CONTACT_IMAGES')}}{{ (file_exists(storage_path('contact/').@$setting['contact_image']) && @$setting['contact_image'] != '') ? @$setting['contact_image'] : 'dummy.jpeg' }}" alt="">
			<!-- /.google-map -->
		</div>
		<!-- /.col-xl-6 -->
		<div class="col-md-6 col-lg-6">
			<div class="contact-info">
				<h5>Rich House</h5>
				<h3>Email: orders@richhouse.me</h3>
				<!-- <span>UAE</span>
				<p>{!! @$setting->address !!}</p> -->
				<p>Sunday - Thursday:<span> {{@$setting->timing}}</span> </p>
				<p>Phone:<span> {{@$setting->phone}}</span></p>
				<p>Whatsapp:<span> {{@$setting->whatsapp}}</span></p>
			</div>
		</div>
		<!-- /.col-xl-6 -->
	</div>
	<!-- /.row -->
</section>


<!--Contact area
============================================= -->
<section class="contact-area">
	<div class="container-fluid custom-container">
		<div class="section-heading pb-30">
			<h3>@lang('lang.connect_with_us')</h3>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-8 col-lg-8 col-xl-6">
				@include('website.includes.alert')
				<div class="contact-form">
					<form action="{{route('post.lead')}}" method="post" class="form validate ajaxForm">
						@csrf
						<div class="row">
							<div class="col-xl-12">
								<input type="text"  name="name" placeholder="@lang('lang.name')*" value="">
							</div>
							<div class="col-xl-6">
								<input type="email" name="email" placeholder="@lang('lang.email')*">
							</div>
							<div class="col-xl-6">
								<input type="tel" name="phone" id="phone" >
							</div>
							<div class="col-xl-12">
								<textarea name="message" placeholder="@lang('lang.message')"></textarea>
							</div>
							<div class="col-xl-12">
								<button type="button" class="ajaxFormSubmit contactSubmit-btn">@lang('lang.submit')</button>
								<!-- <input type="submit" value="@lang('lang.submit')"> -->
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.row end -->
	</div>
	<!-- /.container-fluid end -->
</section>
<!-- /.contact-area end -->

@endsection
@section('footer')
<script>
 $(document).ready(function() {
     
     var input = document.querySelector("#phone");     
     var iti = window.intlTelInput(input, {
	  preferredCountries: ['sa', 'us'],
      separateDialCode: false,
      utilsScript: "{{asset('public/website/assets/dependencies/input-phone/js/utils.js')}}",
    });

   
    $.validator.addMethod(
      "check_number",
      function(value, element) {
        
        var status =true;
        if (input.value.trim()) {
            if (!iti.isValidNumber()) {
                status =false;
            }
        }
        return status;
      },

    );
    $("form.validate").validate({
      rules: {
        name:{
          required: true
        },
        email:{
          required: true
        },
        phone:{
          required: true,
          check_number: true
            },
      }, 
      messages: {
        name: "This field is required.",
        email: "This field is required.",
        phone: {
          required: "This field is required.",
          check_number: "Invalid Number."
        },
      },
      invalidHandler: function (event, validator) {
        //display error alert on form submit 
        //error("Please input all the mandatory values marked as red");
   
        },
        errorPlacement: function (label, element) { // render error placement for each input type
        
        if(element[0].name=='phone')
        {
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
            $('<span class="error"></span>').insertAfter(element.parent()).append(label);
            var parent = $(element).parent('.input-with-icon');
            parent.removeClass('success-control').addClass('error-control');  
        }else
        {
            var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
            $('<span class="error"></span>').insertAfter(element).append(label);
            var parent = $(element).parent('.input-with-icon');
            parent.removeClass('success-control').addClass('error-control');  
        }   
          
        },
        highlight: function (element) { // hightlight error inputs
          var icon = $(element).parent('.input-with-icon').children('i');
            icon.removeClass('fa fa-check').addClass('fa fa-exclamation');  
          var parent = $(element).parent();
          parent.removeClass('success-control').addClass('error-control'); 
        },
        unhighlight: function (element) { // revert the change done by hightlight
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent();
          parent.removeClass('error-control').addClass('success-control'); 
        },
        success: function (label, element) {
          var icon = $(element).parent('.input-with-icon').children('i');
      icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
          var parent = $(element).parent('.input-with-icon');
          parent.removeClass('error-control').addClass('success-control');
          
        }
        // submitHandler: function (form) {
        // }
      });
  });
  
</script>
@endsection
