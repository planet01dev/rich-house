@extends('layouts.web')
@section('content')
<!--=========================-->
<!--=        Breadcrumb         =-->
<!--=========================-->

<section class="breadcrumb-area">
	<div class="container-fluid custom-container">
		<div class="row">
			<div class="col-xl-12">
				<div class="bc-inner">
					<!-- <p><a href="#">Login </a></p> -->
				</div>
			</div>
			<!-- /.col-xl-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>
<section class="contact-area">
	<div class="container-fluid custom-container">
		<div class="section-heading pb-30 mt-20">
			<h3>{{@title}}</h3>
		</div>
		<div class="row justify-content-center">
			<div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
				<form action="#">
					<!-- <div class="row">
						<div class="col-xl-12">
							<input type="text" placeholder="Email*">
						</div>
						<div class="col-xl-12">
							<input type="text" placeholder="Password*">
						</div>
						<div class="col-xl-12">
							<input type="submit" value="LOG IN">
						</div>
					</div> -->
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sit amet consectetur adipiscing. Tristique magna sit amet purus gravida quis blandit turpis. Eget est lorem ipsum dolor sit amet consectetur adipiscing elit. Donec adipiscing tristique risus nec feugiat in fermentum. Id leo in vitae turpis massa sed elementum tempus egestas. Vitae aliquet nec ullamcorper sit amet. Phasellus faucibus scelerisque eleifend donec pretium vulputate. Placerat in egestas erat imperdiet sed euismod. Tellus elementum sagittis vitae et leo. Pellentesque habitant morbi tristique senectus. Nibh tellus molestie nunc non. Porttitor leo a diam sollicitudin tempor id eu. Nulla pharetra diam sit amet nisl suscipit adipiscing bibendum est. Adipiscing tristique risus nec feugiat in fermentum. Et magnis dis parturient montes.

					Proin sed libero enim sed faucibus turpis in eu mi. Donec enim diam vulputate ut pharetra sit amet. Felis donec et odio pellentesque diam volutpat commodo sed egestas. Nunc lobortis mattis aliquam faucibus purus in massa tempor. Facilisis mauris sit amet massa vitae. Id porta nibh venenatis cras sed. Urna et pharetra pharetra massa massa ultricies mi quis. Est ullamcorper eget nulla facilisi etiam dignissim. Sed enim ut sem viverra aliquet eget. Et magnis dis parturient montes nascetur ridiculus mus mauris vitae. Cursus in hac habitasse platea dictumst quisque sagittis purus sit. Pellentesque habitant morbi tristique senectus et netus et malesuada. A scelerisque purus semper eget duis.

					Malesuada nunc vel risus commodo viverra maecenas. Scelerisque purus semper eget duis at tellus at. Nibh sit amet commodo nulla facilisi nullam. Etiam dignissim diam quis enim lobortis scelerisque fermentum dui faucibus. In fermentum et sollicitudin ac orci phasellus egestas tellus. Quam id leo in vitae turpis massa sed elementum. Purus viverra accumsan in nisl nisi scelerisque eu ultrices. Nunc mattis enim ut tellus elementum sagittis vitae. Sed faucibus turpis in eu mi bibendum neque. Eget nunc lobortis mattis aliquam faucibus purus in massa. Habitant morbi tristique senectus et netus et malesuada. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper. Venenatis a condimentum vitae sapien.

					Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. Consequat interdum varius sit amet mattis. Eget est lorem ipsum dolor sit. Congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Morbi tincidunt augue interdum velit euismod. Etiam sit amet nisl purus in mollis nunc. Elit duis tristique sollicitudin nibh sit amet. Facilisis gravida neque convallis a. Sit amet luctus venenatis lectus magna fringilla urna porttitor. Cras tincidunt lobortis feugiat vivamus. Sed viverra ipsum nunc aliquet. Nunc sed id semper risus in hendrerit. At lectus urna duis convallis convallis tellus. Mattis aliquam faucibus purus in massa tempor nec feugiat. Venenatis urna cursus eget nunc. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Vestibulum morbi blandit cursus risus. Mauris augue neque gravida in.

					Tristique senectus et netus et malesuada fames. Euismod nisi porta lorem mollis. Facilisis volutpat est velit egestas dui id ornare arcu odio. Tellus at urna condimentum mattis pellentesque id nibh tortor id. Massa id neque aliquam vestibulum. Amet consectetur adipiscing elit pellentesque habitant. Placerat duis ultricies lacus sed turpis tincidunt. Pretium nibh ipsum consequat nisl vel pretium lectus. Id aliquet risus feugiat in ante metus dictum. Fermentum iaculis eu non diam phasellus vestibulum. Ac ut consequat semper viverra nam libero. Mauris commodo quis imperdiet massa tincidunt. Nulla posuere sollicitudin aliquam ultrices sagittis.</p>
				</form>
			</div>
		</div>
		<!-- /.row end -->
	</div>
	<!-- /.container-fluid end -->
</section>
<!-- /.contact-area end -->

<section class="login-now">
	<div class="container-fluid custom-container">
		<div class="col-12">
			<span>Dont have account</span>
			<a href="create-account.html" class="btn-two">Create Account</a>
		</div>
		<!-- /.col-12 -->
	</div>
	<!-- /.container-fluid -->
</section>
@endsection
