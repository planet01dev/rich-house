<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{@$title}}</title>
  <!--favicon-->
  <link rel="icon" href="{{ asset('public/admin/images/favicon.png') }}" type="image/x-icon">
  <link href="{{ asset('public/admin/plugins/dropzone/css/dropzone.css') }}" rel="stylesheet" type="text/css">
  <!-- simplebar CSS-->
  <link href="{{ asset('public/admin/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
  <!-- Bootstrap core CSS-->
  <link href="{{ asset('public/admin/css/bootstrap.min.css') }}" rel="stylesheet" />
  <!-- animate CSS-->
  <link href="{{ asset('public/admin/css/animate.css') }}" rel="stylesheet" type="text/css" />
  <!-- Icons CSS-->
  <link href="{{ asset('public/admin/css/icons.css') }}" rel="stylesheet" type="text/css" />
  <!-- Sidebar CSS-->
  <link href="{{ asset('public/admin/css/sidebar-menu.css') }}" rel="stylesheet" />
  <!-- Custom Style-->
  <link href="{{ asset('public/admin/css/app-style.css') }}" rel="stylesheet" />
  <!-- Wordpad -->
  <link rel="stylesheet" href="{{ asset('public/admin/plugins/summernote/dist/summernote-bs4.css')}}" rel="stylesheet" />
  <!-- Tag input -->
  <link rel="stylesheet" href="{{ asset('public/admin/plugins/inputtags/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
  <!-- Date Picker -->
  <link href="{{ asset('public/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
  <!--switch button-->
  <link href="{{ asset('public/admin/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" />
  <link href="{{ asset('public/admin/plugins/bootstrap-switch/bootstrap-switch.min.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">

  <link href="{{ asset('public/admin/plugins/trees-elector/css/jquery.treeSelector.css')}}" rel="stylesheet" />
  <link href="{{ asset('public/admin/plugins/jquery-multi-select/multi-select.css')}}" rel="stylesheet" />
  <link href="{{ asset('public/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{ asset('public/css/toastr.css') }}" type="text/css">
  <link href="{{ asset('public/admin/css/custom.css') }}" rel="stylesheet" />

  <script src="{{ asset('public/admin/js/jquery.min.js') }}"></script>
  <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

  <!-- Dropzone JS  -->
  <script src="{{ asset('public/admin/plugins/dropzone/js/dropzone.js') }}"></script>
  <style>
    .search-link {
      cursor: pointer;
    }
  </style>
</head>

<body>

  <!-- Start wrapper-->
  <div id="wrapper">

    <!--Start sidebar-wrapper-->
    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
      <div class="brand-logo">
        <a href="{{route('admin.home')}}">
          <!-- <img src="{{asset('public/admin/images/favicon.png')}}" class="logo-icon" alt="logo icon"> -->
          <h5 class="logo-text"> @lang('lang.admin_panel')</h5>
        </a>
      </div>
      <ul class="sidebar-menu do-nicescrol">
        <li class="sidebar-header"> @lang('lang.main_navigation')</li>
        <li>
          <a href="{{route('admin.lead')}}" class="waves-effect">
            <i class="icon-home"></i> <span> @lang('lang.leads')</span>
          </a>
        </li>
        <li>
          <a href="{{route('admin.section.menu')}}" class="waves-effect">
            <i class="icon-layers"></i> <span> @lang('lang.home_page')</span>
          </a>
        </li>
        <li>
          <a href="#" class="waves-effect">
            <i class="icon-layers"></i> <span> @lang('lang.menu')</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="sidebar-submenu">
            <li><a href="{{route('admin.sidebar.menu')}}"><i class="fa fa-circle-o"></i>@lang('lang.sidebar_menu')</a></li>
            <li><a href="{{route('admin.footer.menu')}}"><i class="fa fa-circle-o"></i>@lang('lang.footer_menu')</a></li>
            <li><a href="{{route('admin.footer.menu.bottom')}}"><i class="fa fa-circle-o"></i>@lang('lang.footer_menu_bottom')</a></li>
          </ul>
        </li>
        <li>
          <a href="{{route('admin.product.manage')}}" class="waves-effect">
            <i class="icon-grid icons"></i> <span>@lang('lang.product')</span>
          </a>
        </li>
        <li>
          <a href="{{route('admin.tag.manage')}}" class="waves-effect">
            <i class="icon-tag"></i> <span> @lang('lang.tags')</span>
          </a>
        </li>
       <!--  <li>
          <a href="{{route('admin.variant.manage')}}" class="waves-effect">
            <i class="icon-social-dropbox"></i> <span>@lang('lang.variants')</span>
          </a>
        </li> -->
        <li>
          <a href="{{route('admin.brand.manage')}}" class="waves-effect">
            <i class="icon-anchor"></i> <span>@lang('lang.brands')</span>
          </a>
        </li>
        <li>
          <a href="{{route('admin.ourstory.add')}}" class="waves-effect">
            <i class="icon-tag"></i> <span>@lang('lang.our_story')</span>
          </a>
        </li>
        <li>
          <a href="{{route('admin.customer.manage')}}" class="waves-effect">
            <i class="icon-user-following icons"></i> <span>@lang('lang.users')</span>
          </a>
        </li>
        <li>
          <a href="{{route('admin.order.manage')}}" class="waves-effect">
            <i class="icon-basket-loaded icons"></i> <span>@lang('lang.orders')</span>
          </a>
        </li>
        <li>
          <a href="{{route('admin.coupon.manage')}}" class="waves-effect">
            <i class="icon-basket-loaded icons"></i> <span>@lang('lang.coupon')</span>
          </a>
        </li>
         <li>
          <a href="{{route('admin.subscribe.manage')}}" class="waves-effect">
            <i class="icon-paper-plane icons"></i> <span>@lang('lang.subscribe')</span>
          </a>
        </li>

        <li>
          <a href="{{route('admin.sign_up_customer.manage')}}" class="waves-effect">
            <i class="icon-user-following icons"></i> <span>@lang('lang.customer')</span>
          </a>
        </li>

        <li>
          <a href="#" class="waves-effect">
            <i class="icon-settings icons"></i> <span>@lang('lang.settings')</span>
           <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="sidebar-submenu">
          <li>
              <a href="{{route('admin.setting.edit_contact')}}"><i class="icon-user icons"></i>
                 @lang('lang.contact') 
              </a>
            </li>
            <li>
              <a href="{{route('admin.setting.edit')}}"><i class="icon-user icons"></i>
                @lang('lang.basic') @lang('lang.setting') 
              </a>
            </li>
            <li>
              <a href="{{route('admin.shipping.edit')}}"><i class="icon-plane icons"></i>
                @lang('lang.shipping') @lang('lang.setting') 
              </a>
            </li>
            <li>
              <a href="{{route('admin.setting.edit.email')}}"><i class="icon-globe icons"></i>
                @lang('lang.email') @lang('lang.setting') 
              </a>
            </li>
            <li>
              <a href="{{route('admin.subscribe.edit')}}"><i class="icon-envelope-open icons"></i>
                 @lang('lang.subscribe') @lang('lang.setting')
              </a>
            </li>
            <li>
              <a href="{{route('admin.store.manage')}}"><i class="icon-bag icons"></i>
                 @lang('lang.store') @lang('lang.setting')
              </a>
            </li>
            <li>
              <a href="{{route('admin.page.manage')}}"><i class="icon-screen-desktop icons"></i>
                 @lang('lang.pages') 
              </a>
            </li>
          </ul>
        </li>

      </ul>

    </div>
    <!--End sidebar-wrapper-->

    <!--Start topbar header-->
    <header class="topbar-nav">
      <nav class="navbar navbar-expand fixed-top bg-white">
      <ul class="navbar-nav mr-auto align-items-center">
          <li class="nav-item">
            <a class="nav-link toggle-menu" href="javascript:void();">
              <i class="iconl-menu menu-iconl icon-menu icons"></i>
            </a>
          </li>
          <li class="nav-item">
            <div class="search-bar">
              <select name="search_option" id="search_option" class="custom-select invt">
                <option <?= (@$search_type == 'product')?'selected':''?> value="product">@lang('lang.product')</option>
                <option <?= (@$search_type == 'brand')?'selected':''?> value="brand" >@lang('lang.brand')</option>
              </select>
            </div>
          </li>
          <li class="nav-item">
            <div class="search-bar">
              <input type="text" id="search" onkeypress="findbykey(event)" value='<?= @$seacrh_keyword; ?>' required="required" class="form-control" placeholder="@lang('lang.enter_keyword')">
              <a class="search-link" onclick="find()"><i class="icon-magnifier"></i></a>
            </div>
          </li>
        </ul>
        <ul class="navbar-nav align-items-center right-nav-link">


          <li class="nav-item">
            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <span class="user-profile"><i class="icon-power mr-2"></i>
                @lang('lang.logout')
              </span>
            </a>
          </li>
          <li class="nav-item language">
            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="#"><i class="flag-icon {{(session()->has('lang') && session()->get('lang') == 'ar')? 'flag-icon-ae' : 'flag-icon-gb'}}"></i></a>
            <ul class="dropdown-menu dropdown-menu-right">
              <li class="dropdown-item">
                <a class="" href="{{route('admin.lang', 'en')}}">
                  <i class="flag-icon flag-icon-gb mr-2"></i> @lang('lang.english')
                </a>
              </li>
              <li class="dropdown-item">
                <a class="" href="{{route('admin.lang', 'ar')}}">
                  <i class="flag-icon flag-icon-ae mr-2"></i> @lang('lang.arabic')
                </a>
              </li>

            </ul>
          </li>
        </ul>
      </nav>
    </header>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      {{ csrf_field() }}
    </form>
    <!--End topbar header-->


    @yield('content')

    <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

    <!--Start footer-->
    <footer class="footer">
      <div class="container">
        <div class="text-center">

        </div>
      </div>
    </footer>
    <!--End footer-->

  </div>
  <!--End wrapper-->


  <!-- Bootstrap core JavaScript-->

  <script src="{{ asset('public/admin/js/popper.min.js') }}"></script>
  <script src="{{ asset('public/admin/js/bootstrap.min.js') }}"></script>

  <!-- simplebar js -->
  <script src="{{ asset('public/admin/plugins/simplebar/js/simplebar.js') }}"></script>
  <!-- waves effect js -->
  <script src="{{ asset('public/admin/js/waves.js') }}"></script>
  <!-- sidebar-menu js -->
  <script src="{{ asset('public/admin/js/sidebar-menu.js') }}"></script>
  <!-- Custom scripts -->
  <script src="{{ asset('public/admin/js/app-script.js') }}"></script>
  <!-- Word pad-->
  <script src="{{ asset('public/admin/plugins/summernote/dist/summernote-bs4.min.js') }}"></script>
  <!-- Tag input-->
  <script src="{{ asset('public/admin/plugins/inputtags/js/bootstrap-tagsinput.js') }}"></script>
  <!-- Date picker-->
  <script src="{{ asset('public/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <!-- Switch Button-->
  <script src="{{ asset('public/admin/plugins/switchery/js/switchery.min.js') }}"></script>
  <script src="{{ asset('public/admin/plugins/alerts-boxes/js/sweetalert.min.js') }}"></script>
  <script src="{{ asset('public/admin/plugins/bootstrap-switch/bootstrap-switch.min.js') }}"></script>
  <!--Multi Select Js-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
  <script src="{{ asset('public/admin/plugins/trees-elector/js/jquery.treeSelector.js') }}"></script>
  <script src="{{ asset('public/admin/plugins/jquery-multi-select/jquery.multi-select.js') }}"></script>
  <script src="{{ asset('public/admin/plugins/select2/js/select2.min.js') }}"></script>
  <!-- Custom js-->
  <script src="{{ asset('public/js/toastr.js') }}"></script>
  <script src="{{ asset('public/admin/js/custom.js') }}"></script>
  @yield('footer')
  @include('includes.modal')
  <script>
    function find() {
      var keyword = $("#search").val();
      var type = $("#search_option").val();
      window.location.href = "{{route('admin.search')}}?keyword=" + keyword + "&type=" + type;
    }

    function findbykey(e) {
      if (e.keyCode == 13) {
        find();
      }
    }
    $(document).ready(function() {
      $(document).on('keydown keypress keyup change blur', ".txtboxToFilter", function() {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 39 || event.keyCode == 37) {
          // let it happen, don't do anything
          console.log("if---" + event.keyCode);
        } else {
          // console.log("else---"+event.keyCode);
          // Ensure that it is a number and stop the keypress
          if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
            console.log("else---if---" + event.keyCode);
            event.preventDefault();
          }
        }
      });

      $(document).on('keyup keydown', ".allow_decimal", function(event) {

        console.log(event.keyCode);
        if (event.shiftKey == true) {
          event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
          (event.keyCode >= 96 && event.keyCode <= 105) ||
          event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
          event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

        } else {
          event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
          event.preventDefault();
        //if a decimal has been added, disable the "."-button

      });

    });
  </script>

</body>

</html>