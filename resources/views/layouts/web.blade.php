<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Meta Data -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Richhouse | {{@$title}}</title>

  <!-- Fav Icon -->
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/website/assets/img/fav-icons/apple-touch-icon.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/website/assets/media/images/logo.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/website/assets/media/images/logo.png') }}">

  <!-- Dependency Styles -->
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/bootstrap/css/bootstrap.min.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/fontawesome/css/fontawesome-all.min.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/owl.carousel/css/owl.carousel.min.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/owl.carousel/css/owl.theme.default.min.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/flaticon/css/flaticon.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/wow/css/animate.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/jquery-ui/css/jquery-ui.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/venobox/css/venobox.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/slick-carousel/css/slick.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/admin/css/icons.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/sidebar/mmenu-light.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('public/css/toastr.css') }}" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Avenir:300,400,500,600,700'>
  <link href='"https://fonts.googleapis.com/css?family=Century+Gothic:300,400,500,600,700'>
  <link rel="stylesheet" href="{{ asset('public/website/assets/dependencies/input-phone/css/intlTelInput.css')}}">
  <!-- Site Stylesheet -->
  <link rel="stylesheet" href="{{ asset('public/website/assets/css/app.css') }}" type="text/css">
  <style>
    @font-face {
      font-family: 'AvenirLTStd-Light';
      src: url("http://localhost/rich-house/public/website/assets/dependencies/fonts/Avenir-Free/AvenirLTStd-Light.otf");
    }

    @font-face {
      font-family: AvenirLTStd-Book;
      src: url("http://localhost/rich-house/public/website/assets/dependencies/fonts/Avenir-Free/AvenirLTStd-Book.otf");
    }

    @font-face {
      font-family: AvenirLTStd-Roman;
      src: url("http://localhost/rich-house/public/website/assets/dependencies/fonts/Avenir-Free/AvenirLTStd-Roman.otf");
    }
  </style>

</head>

<body id="home-version-1" class="home-version-1" data-style="default">

  <div class="site-content">


    <!--=========================-->
    <!--=        Header         =-->
    <!--=========================-->




    <header id="header" class="header-area">
      <div class="container-fluid custom-container menu-rel-container">
        <div class="row">

          <!-- Main menu
          ============================================= -->

          @include('website.includes.sidebar_nav')


          <!-- Logo
          ============================================= -->

          <div class="col-md-6 col-sm-6 col-xs-6 order-lg-2 order-xl-2 menu-container">
            <div class="logo text-center">
              <a href="{{route('web.home')}}">
                <img class="header_logo" src="{{ asset('public/website/assets/media/images/logo.png') }}" alt="">
              </a>
            </div>
          </div>
          <!--Main menu end-->
          <div class="col-md-3 col-sm-3 col-xs-3 order-lg-3 order-xl-3">
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="header-right-two">
                  <ul>
                    <li class="language">
                      <!-- <select class="custom-select">
                        <option {{(session()->has('lang') && session()->get('lang') == 'ar')? '' : 'selected'}}>@lang('lang.english')</option>
                        <option {{(session()->has('lang') && session()->get('lang') == 'ar')? 'selected' : ''}}>@lang('lang.arabic')</option>
                      </select> -->
                      <a class="dropdown-toggle lang-switch" data-toggle="dropdown" href="#">
                        {{(session()->has('lang') && session()->get('lang') == 'ar')? 'Arabic' : 'English'}}
                        <!-- <i class="flag-icon {{(session()->has('lang') && session()->get('lang') == 'ar')? 'flag-icon-ae' : 'flag-icon-gb'}}"></i> -->
                      </a>
                      <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-item">
                          <a class="" href="{{route('admin.lang', 'en')}}">
                            <!-- <i class="flag-icon flag-icon-gb mr-2"></i> --> @lang('lang.english')
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="" href="{{route('admin.lang', 'ar')}}">
                            <!-- <i class="flag-icon flag-icon-ae mr-2"></i> --> @lang('lang.arabic')
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="header-right-one">
                  <ul>
                    <li class="top-search">
                      <a href="javascript:void(0)"><i class="fa fa-search" style="" aria-hidden="true"></i>
                      </a>
                      <input type="text" class="search-input" id="header_search" placeholder="Search">
                    </li>
                    <li class="wishlist">
                    @if(Auth::user() != null && Auth::user()->hasRole('customer'))
                    <a  href="{{ route('customer.profile') }}#wishlist_div" >
                    @else
                    <a href="#" class="add_to_wishlist">
                    @endif
                    
                      
                        <i class="far fa-heart"></i>
                        <div style="color: #000 !important;display: inline-block;">(</span>
                          <span id="" style="color: #000 !important;text-align: center;">{{wishlist_total_count()}}</span>
                          <span style="color: #000 !important;">)</span>
                        </div>
                      </a>
                    </li>
                    @if(Auth::user() != null && Auth::user()->hasRole('customer'))
                    <li>
                    <a  href="{{ route('customer.profile') }}" >
                        <i class="far fa-user" aria-hidden="true"></i>
                      </a>  
                    </li>
                    <li >
                      <a class="" href="{{ route('customer-logout') }}" >
                          @lang('lang.logout')
                      </a>
                    </li>
                    @else
                    <li class="user-login">

                      <a href="javascript:void(0)" class="open_login_dd" dropdown-toggle="" data-toggle="dropdown">login</a>
                      <ul class="dropdown-menu loginpanel">
                        <li class="login-customer">
                          <form action="{{route('customer-login')}}" method="post" class="ajaxForm_login_2 validate" id="loginForm_2">
                          @csrf
                            <h2 class="login-headng" >Login</h2>
                            <div class="row mg-auto">
                              <div class="col-sm-12 col-xs-12 col-md-12 text-left">
                                <label class="">Email</label>
                              </div>
                              <div class="col-sm-12 col-xs-12 col-md-12 mb-10">
                                <input type="email" name="email" placeholder="Enter Email Address" class="form-control required">
                              </div>
                              <div class="col-sm-12 col-xs-12 col-md-12 text-left">
                                <label class="">Password</label>
                              </div>
                              <div class="col-sm-12 col-xs-12 col-md-12 mb-10">
                                <input type="password" name="password" placeholder="Enter Password" class="form-control required">
                              </div>
                              <div class="col-sm-12 col-xs-12 col-md-12 text-left mb-3">
                                <a href="{{route('forgot-password')}}">Forgot Password ?</a>
                              </div>
                              <div class="col-sm-12 col-xs-12 col-md-12">
                                <button type="submit" class="btn login-btn ajaxFormSubmit_login_2 w-100">Login</button>
                              </div>
                            </div>
                          </form>
                        </li>
                      </ul>

                      <span>/</span>
                      <a class="signup-usr" href="{{route('registration')}}"><span> Sign Up </span></a>
                    </li>
                    @endif

                    <li class="top-cart">
                      <a href="{{route('cart')}}" class="cart-link">
                        <i class="flaticon-bag" style="" aria-hidden="true"></i>
                        <!-- </a> -->
                        <!-- <a href="{{route('cart')}}" style="color: #000 !important;">   -->
                        <div style="color: #000 !important;display: inline-block;">(</span>
                          <span id="product_count" style="color: #000 !important;text-align: center;"></span>
                          <span style="color: #000 !important;">)</span>
                        </div>
                        <!-- ( <span id="my_cart_total" class="product_total_amount" style="color: #000 !important;" >0</span> ) -->
                      </a>
                      <!-- <div class="cart-drop">
                        <div class="single-cart">
                          <div class="cart-img">
                            <img alt="" src="{{ asset('public/website/assets/media/images/product/car1.jpg') }}">
                          </div>
                          <div class="cart-title">
                            <p><a href="#">Aliquam Consequat</a></p>
                          </div>
                          <div class="cart-price">
                            <p>1 x $500</p>
                          </div>
                          <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                        <div class="single-cart">
                          <div class="cart-img">
                            <img alt="" src="{{ asset('public/website/assets/media/images/product/car2.jpg') }}">
                          </div>
                          <div class="cart-title">
                            <p><a href="#">Quisque In Arcuc</a></p>
                          </div>
                          <div class="cart-price">
                            <p>1 x $200</p>
                          </div>
                          <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                        <div class="cart-bottom">
                          <div class="cart-sub-total">
                            <p>Sub-Total <span>$700</span></p>
                          </div>
                          <div class="cart-sub-total">
                            <p>Eco Tax (-2.00)<span>$7.00</span></p>
                          </div>
                          <div class="cart-sub-total">
                            <p>VAT (20%) <span>$40.00</span></p>
                          </div>
                          <div class="cart-sub-total">
                            <p>Total <span>$244.00</span></p>
                          </div>
                          <div class="cart-checkout">
                            <a href="cart.html"><i class="fa fa-shopping-cart"></i>View Cart</a>
                          </div>
                          <div class="cart-share">
                            <a href="#"><i class="fa fa-share"></i>Checkout</a>
                          </div>
                        </div>
                      </div> -->
                    </li>
                  </ul>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!--Container-Fluid-->
    </header>
    <!--Main Header end-->




    <!--=========================-->
    <!--=        Mobile Header     =-->
    <!--=========================-->



    <header class="mobile-header" style="position: relative;">
      <div class="container-fluid custom-container">
        <div class="row">

          <!-- Mobile menu Opener
          ============================================= -->
          <div class="col-12">
            <div class="language d-inline-block mt-10 mr-2">
              <a class="dropdown-toggle lang-switch" data-toggle="dropdown" href="#" style="color:#000;">
                {{(session()->has('lang') && session()->get('lang') == 'ar')? 'Arabic' : 'English'}}
                <!-- <i class="flag-icon {{(session()->has('lang') && session()->get('lang') == 'ar')? 'flag-icon-ae' : 'flag-icon-gb'}}"></i> -->
              </a>
              <ul class="dropdown-menu dropdown-menu-right">
                <li class="dropdown-item">
                  <a class="" href="{{route('admin.lang', 'en')}}">
                    <!-- <i class="flag-icon flag-icon-gb mr-2"></i> --> @lang('lang.english')
                  </a>
                </li>
                <li class="dropdown-item">
                  <a class="" href="{{route('admin.lang', 'ar')}}">
                    <!-- <i class="flag-icon flag-icon-ae mr-2"></i> --> @lang('lang.arabic')
                  </a>
                </li>
              </ul>
            </div>
            <div class="top-cart">
              <!-- <a href="{{route('cart')}}"><i class="flaticon-bag" style="color:#565656;font-weight: 600;" aria-hidden="true"></i></a> -->
              <!-- <a href="{{route('cart')}}" style="color: #000 !important;">  
                <span id="product_total_currency" style="color: #000 !important;"> @lang('lang.currency')</span>  ( <span id="my_cart_total" class="product_total_amount" style="color: #000 !important;" >0</span> )
              </a> -->
              <!-- <a href="{{route('cart')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i>  -->
              <!-- <span id="product_total_currency">: @lang('lang.currency')</span>  (<span id="my_cart_total" class="product_total_amount">0</span>) -->
              <!-- </a> -->
              <a href="{{route('cart')}}" class="cart-link">
                <i class="flaticon-bag icon-header" aria-hidden="true"></i>
                <!-- </a> -->
                <!-- <a href="{{route('cart')}}" style="color: #000 !important;">   -->
                <!--<div style="color: #000 !important;display: inline-block;">(</span>-->
                <!--  <span id="product_count" style="color: #000 !important;text-align: center;"></span>-->
                <!--  <span style="color: #000 !important;">)</span>-->
                <!--</div>-->
                <!-- ( <span id="my_cart_total" class="product_total_amount" style="color: #000 !important;" >0</span> ) -->
              </a>
            </div>
            <div class="user-login">
            @if(Auth::user() != null && Auth::user()->hasRole('customer'))
                    <li class="cust-login_signup">
                      <a  href="{{ route('customer.profile') }}" >
                        <i class="far fa-user" aria-hidden="true"></i>
                      </a>  
                    </li>
                    <li class="cust-login_signup">
                      <a class="mobile-logout" href="{{ route('customer-logout') }}" >
                          @lang('lang.logout')
                      </a>
                    </li>
                    @else
                    <li class="cust-login_signup" >

                      <a href="javascript:void(0)" class="open_login_dd" dropdown-toggle="" data-toggle="dropdown">login</a>
                      <ul class="dropdown-menu loginpanel">
                        <li class="login-customer">
                          <form action="{{route('customer-login')}}" method="post" class="ajaxForm_login validate" id="loginForm">
                          @csrf
                            <h2 class="login-headng" >Login</h2>
                            <div class="row mg-auto">
                              <div class="col-md-12 text-left">
                                <label class="">Email</label>
                              </div>
                              <div class="col-md-12 mb-10">
                                <input type="email" name="email" placeholder="Enter Email Address" class="form-control required">
                              </div>
                              <div class="col-md-12 text-left">
                                <label class="">Password</label>
                              </div>
                              <div class="col-md-12 mb-10">
                                <input type="password" name="password" placeholder="Enter Password" class="form-control required">
                              </div>
                              <div class="col-md-12">
                                <button type="submit" class="btn login-btn ajaxFormSubmit_login w-100">Login</button>
                              </div>
                            </div>
                          </form>
                        </li>
                      </ul>

                      <span>/</span>
                      <a class="signup-usr" href="{{route('registration')}}"> Sign Up </a>
                    </li>
                    @endif
              <!-- <a href="{{route('login')}}"><i class="fa fa-user-o icon-header" aria-hidden="true"></i></a> -->
              <!--<a class="login-usr" href="{{route('login')}}"><span> Login </span></a>-->
              <!--<span>/</span>-->
              <!--<a class="signup-usr" href="{{route('login')}}"><span> Signup </span></a>-->
            </div>
            <div class="wishlist">
            @if(Auth::user() != null && Auth::user()->hasRole('customer'))
                    <a  href="{{ route('customer.profile') }}#wishlist_div" >
                    @else
                    <a href="#" class="add_to_wishlist">
                    @endif
              
                <i class="far fa-heart icon-header"></i>
                <div style="color: #000 !important;display: inline-block;">(</span>
                  <span id="" style="color: #000 !important;text-align: center;">{{wishlist_total_count()}}</span>
                  <span style="color: #000 !important;">)</span>
                </div>
              </a>
            </div>
            <!--<div class="top-search">-->
            <!--  <a href="javascript:void(0)"><i class="fa fa-search" style="color:#565656;" aria-hidden="true"></i>-->
            <!--  </a>-->
            <!--  <input type="text" class="search-input"  id="header_search" placeholder="Search">-->
            <!--</div>-->
            <!-- <div class="cart-drop">
                <div class="single-cart">
                  <div class="cart-img">
                    <img alt="" src="{{ asset('public/website/assets/media/images/product/car1.jpg') }}">
                  </div>
                  <div class="cart-title">
                    <p><a href="#">Aliquam Consequat</a></p>
                  </div>
                  <div class="cart-price">
                    <p>1 x $500</p>
                  </div>
                  <a href="#"><i class="fa fa-times"></i></a>
                </div>
                <div class="single-cart">
                  <div class="cart-img">
                    <img alt="" src="{{ asset('public/website/assets/media/images/product/car2.jpg') }}">
                  </div>
                  <div class="cart-title">
                    <p><a href="#">Quisque In Arcuc</a></p>
                  </div>
                  <div class="cart-price">
                    <p>1 x $200</p>
                  </div>
                  <a href="#"><i class="fa fa-times"></i></a>
                </div>
                <div class="cart-bottom">
                  <div class="cart-sub-total">
                    <p>Sub-Total <span>$700</span></p>
                  </div>
                  <div class="cart-sub-total">
                    <p>Eco Tax (-2.00)<span>$7.00</span></p>
                  </div>
                  <div class="cart-sub-total">
                    <p>VAT (20%) <span>$40.00</span></p>
                  </div>
                  <div class="cart-sub-total">
                    <p>Total <span>$244.00</span></p>
                  </div> -->
            <!--<div class="cart-checkout">-->
            <!--  <a href="#"><i class="fa fa-shopping-cart"></i>View Cart</a>-->
            <!--</div>-->
            <!--<div class="cart-share">-->
            <!--  <a href="#"><i class="fa fa-share"></i>Checkout</a>-->
            <!--</div>-->
          </div>
          <div class="col-4">
            <div class="accordion-wrapper">
              <a href="#" class="mobile-open">@lang('lang.menu')</a>
            </div>
          </div>
          <div class="col-4">
            <div class="logo">
              <a href="{{route('web.home')}}">
                <img class="header_logo" src="{{ asset('public/website/assets/media/images/logo.png') }}" alt="">
              </a>
            </div>
          </div>
        </div>
      </div>
  </div>
  <!-- /.row end -->
  </div>
  <!-- /.container end -->
  </header>

  <div class="accordion-wrapper">

    <!-- Mobile Menu Navigation
        ============================================= -->
    @include('website.includes.sidebar_mobile_nav')

  </div>


  @yield('content')
  <footer class="footer-widget-area">
    <div class="container-fluid custom-container">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
          <div class="footer-widget">
            <nav class="footer-menulinks">
              <ul class="footer-menu-lists">
                @foreach($footerbar as $footer)
                <li><a href="{{url($footer->url)}}" title="{{@$footer->footer_menu_text->title}}">{{@$footer->footer_menu_text->title}}</a></li>
                @endforeach
                

                <!--  <li><a href="#" title="">BRANDS</a></li>
                      <li><a href="#" title="">ABOUT RICH HOUSE</a></li>
                      <li><a href="#" title="">EXPLORE</a></li>
                      <li><a href="#" title="">STORES</a></li>
                      <li><a href="#" title="">CONTACT US</a></li>
                      <li><a href="#" title="">ONLINE STORES</a></li> -->
              </ul>
            </nav>
          </div>
          <div class="text-center mi-footer-logo">
            <a href="{{route('web.home')}}">
              <img class="footer_logo" src="{{ asset('public/website/assets/media/images/logo.png') }}" alt="">
            </a>
          </div>
          <div class="social text-center">
            <ul>
              <li><a href="{{@$setting->facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="{{@$setting->instagram}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
              <!-- <li><a href="{{@$setting->pinterest}}" target="_blank"><i class="fab fa-pinterest"></i></a></li>
              <li><a href="{{@$setting->twitter}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
              <li><a href="{{@$setting->youtube}}" target="_blank"><i class="fab fa-youtube"></i></a></li>
              <li><a href="{{@$setting->linkedin}}" target="_blank"><i class="fab fa-linkedin"></i></a></li> -->
            </ul>
          </div>
        </div>

      </div>
      <div class="footer-bottom">
        <div class="row">
          <div class="col-12">
            <p class="footer-copyrights">© Richhouse
              @foreach($footer_bootom_bar as $bottom)
              | <a href="{{route('page.detail',$bottom->url)}}">
                {{$bottom->footer_menu_bottom_text->title}}
              </a>
              @endforeach
              
            </p>
          </div>
          <!-- /.col-xl-6 -->

        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- container-fluid -->
  </footer>


  <!-- Back to top
  ============================================= -->

  <div class="backtotop">
    <i class="fa fa-angle-up backtotop_btn"></i>
  </div>


  <!--=========================-->
  <!--=   Popup area      =-->
  <!--=========================-->


  @if(isset($page) && $page == 'main' && $subscribe->is_active == 1)
  @include('website.includes.subscribe_modal')
  @endif

  <!--=========================-->
  <!--=   Product Quick view area    =-->
  <!--=========================-->

  <!-- Quick View -->
  <!-- <div class="modal quickview-wrapper">
      <div class="quickview">
        <div class="row">
          <div class="col-12">
            <span class="close-qv">
          <i class="flaticon-close"></i>
        </span>
          </div>
          <div class="col-md-6">
            <div class="quickview-slider">
              <div class="slider-for">
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b1.jpg') }}" alt="Thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b2.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b3.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b4.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b5.jpg') }}" alt="Thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b1.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b2.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b3.jpg') }}" alt="thumb">
                </div>
              </div>

              <div class="slider-nav">

                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b1.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b2.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b3.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <div class="">
                    <img src="{{ asset('public/website/assets/media/images/product/single/b4.jpg') }}" alt="Thumb">
                  </div>
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b5.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b1.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b2.jpg') }}" alt="thumb">
                </div>
                <div class="">
                  <img src="{{ asset('public/website/assets/media/images/product/single/b3.jpg') }}" alt="thumb">
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="product-details">
              <h5 class="pro-title"><a href="#">Woman fashion dress</a></h5>
              <span class="price">Price : $387</span>
              <div class="size-variation">
                <span>size :</span>
                <select name="size-value">
              <option value="">1</option>
              <option value="">2</option>
              <option value="">3</option>
              <option value="">4</option>
              <option value="">5</option>
            </select>
              </div>
              <div class="color-variation">
                <span>color :</span>
                <ul>
                  <li><i class="fas fa-circle"></i></li>
                  <li><i class="fas fa-circle"></i></li>
                  <li><i class="fas fa-circle"></i></li>
                  <li><i class="fas fa-circle"></i></li>
                </ul>
              </div>

              <div class="add-tocart-wrap">
                <div class="cart-plus-minus-button">
                  <input type="text" value="1" name="qtybutton" class="cart-plus-minus">
                </div>
                <a href="#" class="add-to-cart"><i class="flaticon-shopping-purse-icon"></i>Add to Cart</a>
              </div>

              <p>Tags <a href="#">boys,</a><a href="#"> dress,</a><a href="#">Rok-dress</a></p> 

              <p>But I must explain to you how all this mistaken idedenounc pleasure and praisi pain was born and I will give you a complete accosystem, and expound the actu teachings of the great explore tmaster-builder of human happiness. No one rejects, dislikes,
                or avoids.</p>

              <div class="product-social">
                <span>Share :</span>
                <ul>
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div> -->





  </div>
  <!-- Dependency Scripts -->
  <script src="{{ asset('public/website/assets/dependencies/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/popper.js/popper.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/owl.carousel/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/wow/js/wow.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/isotope-layout/js/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/imagesloaded/js/imagesloaded.pkgd.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/jquery.countdown/js/jquery.countdown.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/gmap3/js/gmap3.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/venobox/js/venobox.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/slick-carousel/js/slick.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/headroom/js/headroom.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/jquery-ui/js/jquery-ui.min.js') }}"></script>
  <!--Google map api -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsBrMPsyNtpwKXPPpG54XwJXnyobfMAIc"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
  <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>

  <!-- Site Scripts -->
  <script src="{{ asset('public/admin/plugins/alerts-boxes/js/sweetalert.min.js') }}"></script>
  <script src="{{ asset('public/website/assets/js/app.js') }}"></script>
  <script src="{{ asset('public/js/toastr.js') }}"></script>
  <script src="{{ asset('public/website/assets/js/custom.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/sidebar/mmenu-light.js') }}"></script>
  <script src="{{ asset('public/website/assets/dependencies/input-phone/js/intlTelInput.js')}}"></script>
  <script>
    $(document).ready(function() {

      var lang = <?= json_encode(session()->get('lang')); ?>;
      if (lang == 'ar') {
        console.log('ARABIC LANGUAGE: ' + lang);
        $('html').find('body , div.modal , header , footer').addClass('direction-ar');
      }
      // else if(lang == 'en')
      // {
      //     console.log('ENGLISH LANGUAGE: '+lang);
      //     $('html').find('body , div.modal , header , footer').addClass('direction-en');
      // }

      $(document).on('keydown keypress keyup change blur', ".txtboxToFilter", function() {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 39 || event.keyCode == 37) {
          // let it happen, don't do anything
          console.log("if---" + event.keyCode);
        } else {
          // console.log("else---"+event.keyCode);
          // Ensure that it is a number and stop the keypress
          if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
            console.log("else---if---" + event.keyCode);
            event.preventDefault();
          }
        }
      });

      $(document).on('keyup keydown', ".allow_decimal", function(event) {

        console.log(event.keyCode);
        if (event.shiftKey == true) {
          event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
          (event.keyCode >= 96 && event.keyCode <= 105) ||
          event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
          event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

        } else {
          event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
          event.preventDefault();
        //if a decimal has been added, disable the "."-button

      });

      $('#header_search').keypress(function(e) {
        var key = e.which;
        if (key == 13) // the enter key code
        {
          var keyword = $("#header_search").val();

          window.location.href = "{{route('header.search')}}?keyword=" + keyword;
        }
      });

    });

    var menu = new MmenuLight(
      document.querySelector('#menu'),
      'all'
    );

    var navigator = menu.navigation({
      // selectedClass: 'Selected',
      // slidingSubmenus: true,
      // theme: 'dark',
      // title: 'Menu'
    });

    var drawer = menu.offcanvas({
      // position: 'left'
    });

    //  Open the menu.
    document.querySelector('a[href="#menu"]')
      .addEventListener('click', evnt => {
        evnt.preventDefault();
        drawer.open();
      });
    $(document).on('click','#dropdownAddClass',function(){
      if($(this).attr('data-attr') == 0)
      {
        $(this).parents('li').addClass('open');
        $(this).parents('li').find('.submenu').slideDown(500);
        $(this).attr('data-attr',1)
      }
      else
      {
        $(this).parents('li').removeClass('open');
        $(this).attr('data-attr',0)
      }
    })
  </script>
  @stack('custom-scripts')
  @yield('footer')
  @include('website.includes.cart_js')
<script>
  $(document).ready(function(){
    @if(Session::has('reset'))
      success("{{Session::get('reset')}}");
    @endif
  })  
</script>
</body>
</html>