function item_delete(id, this_url){      
    swal({
          title: "Do you really want to delete it?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
             $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                        url: this_url,
                        type: "post",
                        data: {
                            id:id
                        },
                        success: function(data) {
                            if (data == 'success') {
                                $('#success').show();
                                $('#success-text').html('<strong>Success!</strong> Record has been deleted.');
                                $('.back-to-top').click();
                                setTimeout(
                                  function() 
                                  {
                                    location. reload(true);
                                  }, 1000);                       
                            };
                        },
                        error: function() {
                            alert('There is error while deleting record');
                        }
                    }); 
          } else {

            return false;
          }
        });
}

//update_status
function update_status(id, this_url) {
    if (window.confirm('Do you really want to change it?')){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
                url: this_url,
                type: "post",
                data: {
                    id:id
                },
                success: function(data) {
                    if (data == 'success') {
                        $('#success').show();
                        $('#success-text').html('<strong>Success!</strong> Status has been changed.');
                        $('.back-to-top').click();
                        setTimeout(
                          function() 
                          {
                            location. reload(true);
                          }, 1000);                       
                    };
                },
                error: function() {
                    alert('There is error while changing status');
                }
            }); 
    }
}
function initComp(){
    $('.ajaxFormSubmit').on('click',function(e){
        var form = $('form.ajaxForm');
        var check  = form.valid();
        if(!check){
            $("#loader").hide();
            return false;
        } 
        $("button[type=button]").attr("disabled",'disabled');
        $("button[type=submit]").attr("disabled",'disabled');
        swal({
          title: "Are you sure?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            form.submit();
          } else {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();

            return false;
          }
        });
    });

    $("form.ajaxForm").ajaxForm({
        dataType: "json",
        beforeSubmit: function() {
            $(".loading-wrapper").show();
            $("#loader").show();
            faction = $("form.login").attr("action");
            $("button[type=button]").attr("disabled",'disabled');
            $("button[type=submit]").attr("disabled",'disabled');
        
        },
        error: function(data)
        {
            // console.log('Error::');
            var response = JSON.parse(data.responseText);
            // console.log(response.errors.permalink[0]);
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();
            $(".loading-wrapper").hide();

            if(typeof data === 'object'){
                $('#fail').show();
                var text = "Please fill all mandatory fields.";
                if(response.errors.permalink[0] !== undefined)
                {
                  text = response.errors.permalink[0];
                }
                $('#fail-text').html(text);
                $('.back-to-top').click();
              // error("Please fill all mandatory fields.");   
            }else{
                $('#fail').show();
                $('#fail-text').html("Error Occured.Invalid File Format.");
                $('.back-to-top').click();
             //error("Error Occured.Invalid File Format.");   
            }

        },
        success: function(data) {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            if (data == null || data == "")
            {
                window.location.reload(true);
            }
            else if (data['error'] !== undefined)
            {
                //error(data['error']);
                $('#fail').show();
                $('#fail-text').html(data['error']);
                $('.back-to-top').click();
                $("#loader").hide();
                $(".loading-wrapper").hide();
            }
            else if (data['success'] !== undefined){
                $("#loader").hide();
                $(".loading-wrapper").hide();
                $('#success').show();
                $('#success-text').html(data['success']);
                //success(data['success']);
            }
            if (data['redirect'] !== undefined){
                window.setTimeout(function() { window.location = data['redirect']; }, 2000);
                
            }
            if (data['reload'] !== undefined){
                window.location.reload(true);
            }
            if (data['fieldsEmpty'] == 'yes'){

                resetForm();

            }
        }
    });
}
$(document).ready(function() {
    var form = $('.ajaxForm'),
    original = form.serialize()

    form.submit(function(){
      window.onbeforeunload = null
    })

    window.onbeforeunload = function(){
      if (form.serialize() != original)
        return 'Are you sure you want to leave?'
    }

    initComp();
});
function resetForm(){
    $("form input[type=text]").val("");

    $("form input[type=password]").val("");

    $("form input[type=email]").val("");

    $("form input[type=color]").val("");

    $("form input[type=date]").val("");

    $("form input[type=datetime-local]").val("");

    $("form input[type=file]").val("");

    $("form input[type=image]").val("");

    $("form input[type=month]").val("");

    $("form input[type=number]").val("");

    $("form input[type=range]").val("");

    $("form input[type=tel]").val("");

    $("form input[type=url]").val("");

    $("form input[type=week]").val("");

    $("form select").val("");

    $("form textarea").val("");

}

//switch button
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

elems.forEach(function(html) {
  var switchery = new Switchery(html);
});
//texteditor
$('.summernoteEditor').summernote({
  toolbar: [
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['AvenirLTStd-Light']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']]
  ],  
  height: 100,
    tabsize: 2
});
//multiple select
$('.multiple-select').select2();
function image_delete(remove_url, max_file){
     Dropzone.options.dropzone = {
          renameFile: function (file) {
            let newName = new Date().getTime() + '_' + file.name;
            return newName;
          },
          maxFiles: (max_file === undefined)? 4 : max_file,
          addRemoveLinks: true,
          acceptedFiles: "image/jpeg,image/png,image/gif",
          accept: function(file, done) {
            done();
          },

          removedfile: function(file) {
            var _ref;
            var name = file.upload.filename;
            var fileList = $('#fileName').val();
            fileList = fileList.replace(name+"~~", "");
            $('#fileName').val(fileList);
          $.ajax({
                type: "get",
                url: remove_url,
                data: { file: name },
                dataType: 'html'
            });
             return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
          },
          init: function() {
            thisDropzone = this;
        
            if(window.findImage != ''){
              console.log("images found"+window.findImage);
                $.each(window.images, function(key,value){
                    var mockFile = { name: 'Uploaded', size: 15, 'upload':{'filename' : value} };
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, image_path+value);
                    thisDropzone.emit("complete", mockFile);
                });
            }
            this.on("success", function (file, responseText) {
                if(responseText.length > 0){
                  document.getElementById('fileName').value += responseText+"~~";
                }
                file.name = responseText;
                console.log(file);
              });
            } 
        };

}
 /*$(".js-switch").on("change" , function(e) {
    if (window.confirm('Do you really want to delete it?')){
         window.swt = 1;
        alert(true);
    }else{
       $('.js-switch', this).prop('checked', true);

       // console.log("false");
         window.swt = 0;
        return false;
    }
    aler('fuck');
 });*/
 /*(function () {
    var previous;

    $(".js-switch").on('focus', function () {
        // Store the current value on focus and on change
        previous = this.value;
        console.log("old " + this.value);
    }).change(function() {
        // Do something with the previous value after the change
        alert(previous);

        // Make sure the previous value is updated
        previous = this.value;
        console.log("new " + this.value);

    });
})();*/
/*var changeCheckbox = $('.js-switch');

$(".js-switch").on('change', function () {
    
    var id = $(this).data('id'); 
    var url = $(this).data('url'); 
    var status = $(this).checked; 
    console.log(' id ' +$(this).data('id'));
    console.log(' url ' +$(this).data('url'));
      if (window.confirm('Do you really want to change it?')){
            item_delete(id, url);
      }else{
        var switchery = new Switchery(window.elem, { disabled: true });

       if(status){
        $('.js-switch', this).prop('checked', true);
       }else if(!status){
       $('.js-switch', this).prop('checked', false);
       }
      }
   console.log("statuys "+changeCheckbox.checked);
});
*/
$(document).on('keypress', ".num_field", function(e) {
        var x = e.which || e.keycode;
         // console.log(x);
      if((x>=48 && x<=57)){
        return true;
      }else if((x == 46)){
        return true;
      }else{
          e.preventDefault();
            //console.log(false);
        return false;
      }
});
