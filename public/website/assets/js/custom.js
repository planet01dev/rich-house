function item_delete(id, this_url){      
    swal({
          title: "Do you really want to delete it?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
             $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                        url: this_url,
                        type: "post",
                        data: {
                            id:id
                        },
                        success: function(data) {
                            if (data == 'success') {
                                // $('#success').show();
                                // $('#success-text').html('<strong>Success!</strong> Record has been deleted.');
                                // $('.back-to-top').click();
                                success('Record Deleted');
                                setTimeout(
                                  function() 
                                  {
                                    location. reload(true);
                                  }, 1000);                       
                            };
                        },
                        error: function() {
                            alert('There is error while deleting record');
                        }
                    }); 
          } else {

            return false;
          }
        });
}
function initComp(){

    
    $(document).on("click", ".ajaxFormSubmit_login", function(event){
    
        var form = $('form.ajaxForm_login');
        var check  = form.valid();
        if(!check){
            $("#loader").hide();
            return false;
        } 
        $("button[type=button]").attr("disabled",'disabled');
        $("button[type=submit]").attr("disabled",'disabled');
        form.submit();

       /* swal({
          title: "Are you sure?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            form.submit();
          } else {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();

            return false;
          }
        });*/
    });

    $("form.ajaxForm_login").ajaxForm({
        dataType: "json",
        beforeSubmit: function() {
            faction = $("form.login").attr("action");
            $("button[type=button]").attr("disabled",'disabled');
            $("button[type=submit]").attr("disabled",'disabled');
        
        },
        error: function(data)
        {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            
            if(typeof data === 'object'){
                $('#fail').show();
                $('#fail-text').html("Please fill all mandatory fields.");
                $('.back-to-top').click();
              // error("Please fill all mandatory fields.");   
            }else{
                $('#fail').show();
                $('#fail-text').html("Error Occured.Invalid File Format.");
                $('.back-to-top').click();
             //error("Error Occured.Invalid File Format.");   
            }

        },
        success: function(data) {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            if (data == null || data == "")
            {
                window.location.reload(true);
            }
            else if (data['error'] !== undefined)
            {
                error(data['error']);
                // $('#fail').show();
                // $('#fail-text').html(data['error']);
                // $('.back-to-top').click();
                
                
            }
            else if (data['success'] !== undefined){
                
                // $('#success').show();
                // $('#success-text').html(data['success']);
                success(data['success']);
            }
            if (data['redirect'] !== undefined){
                window.setTimeout(function() { window.location = data['redirect']; }, 2000);
                
            }
            
            if (data['reload'] !== undefined){
                window.location.reload(true);
            }
            if (data['fieldsEmpty'] == 'yes'){
                resetForm();

            }
        }
    });

    $(document).on("click", ".ajaxFormSubmit_login_2", function(event){
        e.preventDefault();
        var form = $('form.ajaxForm_login_2');
        var check  = form.valid();
        if(!check){
            $("#loader").hide();
            return false;
        } 
        $("button[type=button]").attr("disabled",'disabled');
        $("button[type=submit]").attr("disabled",'disabled');
        form.submit();

       /* swal({
          title: "Are you sure?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            form.submit();
          } else {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();

            return false;
          }
        });*/
    });

    $("form.ajaxForm_login_2").ajaxForm({
        dataType: "json",
        beforeSubmit: function() {
            faction = $("form.login").attr("action");
            $("button[type=button]").attr("disabled",'disabled');
            $("button[type=submit]").attr("disabled",'disabled');
        
        },
        error: function(data)
        {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            
            if(typeof data === 'object'){
                $('#fail').show();
                $('#fail-text').html("Please fill all mandatory fields.");
                $('.back-to-top').click();
              // error("Please fill all mandatory fields.");   
            }else{
                $('#fail').show();
                $('#fail-text').html("Error Occured.Invalid File Format.");
                $('.back-to-top').click();
             //error("Error Occured.Invalid File Format.");   
            }

        },
        success: function(data) {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            if (data == null || data == "")
            {
                window.location.reload(true);
            }
            else if (data['error'] !== undefined)
            {
                error(data['error']);
                // $('#fail').show();
                // $('#fail-text').html(data['error']);
                // $('.back-to-top').click();
                
                
            }
            else if (data['success'] !== undefined){
                
                // $('#success').show();
                // $('#success-text').html(data['success']);
                success(data['success']);
            }
            if (data['redirect'] !== undefined){
                window.setTimeout(function() { window.location = data['redirect']; }, 2000);
                
            }
            
            if (data['reload'] !== undefined){
                window.location.reload(true);
            }
            if (data['fieldsEmpty'] == 'yes'){
                resetForm();

            }
        }
    });



    $('.ajaxFormSubmit').on('click',function(e){
        var form = $('form.ajaxForm');
        var check  = form.valid();
        if(!check){
            $("#loader").hide();
            return false;
        } 
        $("button[type=button]").attr("disabled",'disabled');
        $("button[type=submit]").attr("disabled",'disabled');
        form.submit();

       /* swal({
          title: "Are you sure?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            form.submit();
          } else {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();

            return false;
          }
        });*/
    });

    $("form.ajaxForm").ajaxForm({
        dataType: "json",
        beforeSubmit: function() {
            faction = $("form.login").attr("action");
            $("button[type=button]").attr("disabled",'disabled');
            $("button[type=submit]").attr("disabled",'disabled');
        
        },
        error: function(data)
        {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            
            if(typeof data === 'object'){
                $('#fail').show();
                $('#fail-text').html("Please fill all mandatory fields.");
                $('.back-to-top').click();
              // error("Please fill all mandatory fields.");   
            }else{
                $('#fail').show();
                $('#fail-text').html("Error Occured.Invalid File Format.");
                $('.back-to-top').click();
             //error("Error Occured.Invalid File Format.");   
            }

        },
        success: function(data) {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            if (data == null || data == "")
            {
                window.location.reload(true);
            }
            else if (data['error'] !== undefined)
            {
                //error(data['error']);
                $('#fail').show();
                $('#fail-text').html(data['error']);
                $('.back-to-top').click();
                
                
            }
            else if (data['success'] !== undefined){
                
                $('#success').show();
                $('#success-text').html(data['success']);
                //success(data['success']);
            }
            if (data['redirect'] !== undefined){
                window.setTimeout(function() { window.location = data['redirect']; }, 2000);
                
            }
            
            if (data['reload'] !== undefined){
                window.location.reload(true);
            }
            if (data['fieldsEmpty'] == 'yes'){
                resetForm();

            }
        }
    });

    $('.ajaxFormSubmit_checkout').on('click',function(e){
        
        $(".loader").css('display','block ');
        var form = $('form.ajaxForm_checkout');
        var check  = form.valid();
        if(!check){
            $(".loader").css('display','none ');
            return false;
        } 
        $("button[type=button]").attr("disabled",'disabled');
        $("button[type=submit]").attr("disabled",'disabled');
        form.submit();

       /* swal({
          title: "Are you sure?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            form.submit();
          } else {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $("#loader").hide();

            return false;
          }
        });*/
    });

    $("form.ajaxForm_checkout").ajaxForm({
        dataType: "json",
        beforeSubmit: function() {
            $(".loader").css('display',' block ');
            faction = $("form.login").attr("action");
            $("button[type=button]").attr("disabled",'disabled');
            $("button[type=submit]").attr("disabled",'disabled');
        
        },
        error: function(data)
        {
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            $(".loader").css('display',' none');

            if(typeof data === 'object'){
              error("Please fill all mandatory fields.");   
            }else{
              error("Error Occured.Invalid File Format.");   
            }

        },
        success: function(data) {
            $(".loader").css('display',' none ');
            $("button[type=button]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
            if (data == null || data == "")
            {
                window.location.reload(true);
            }
            else if (data['error'] !== undefined)
            {
                error(data['error']);
            }
            else if (data['success'] !== undefined){
                success(data['success']);
            }
            else if (data['payfort'] !== undefined){
                $.cookie("billing",data['billing']);
                $.cookie("shipping",data['shipping']);
                var form = $("<form/>", 
                                { action: data['redirectUrl'],
                                    id: 'payfort',
                                    method:'post'
                                }
                            );
                
                jQuery.each( data['request_para'], function( i, val ) {
                    form.append( 
                        $("<input>", 
                            { 
                                type:'hidden', 
                                name:i,
                                value: val, 
                             }
                        )
                    );

                });
                $("body").append(form);
                $('form#payfort').submit();
            }
            if (data['redirect'] !== undefined){
                window.setTimeout(function() { window.location = data['redirect']; }, 2000);
                
            }
            if (data['reload'] !== undefined){
                window.location.reload(true);
            }
            if (data['fieldsEmpty'] == 'yes'){
                resetForm();

            }
        }
    });
}

$(document).ready(function() {
    var form = $('.ajaxForm'),
    original = form.serialize()

    form.submit(function(){
      window.onbeforeunload = null
    })

    /*window.onbeforeunload = function(){
      if (form.serialize() != original)
        return 'Are you sure you want to leave?'
    }*/

    initComp();
});
function resetForm(){
    $("form input[type=text]").val("");

    $("form input[type=password]").val("");

    $("form input[type=email]").val("");

    $("form input[type=color]").val("");

    $("form input[type=date]").val("");

    $("form input[type=datetime-local]").val("");

    $("form input[type=file]").val("");

    $("form input[type=image]").val("");

    $("form input[type=month]").val("");

    $("form input[type=number]").val("");

    $("form input[type=range]").val("");

    $("form input[type=tel]").val("");

    $("form input[type=url]").val("");

    $("form input[type=week]").val("");

    $("form select").val("");

    $("form textarea").val("");

}
function error(message)
{
    toastr.error(message, 'Error');
    
}
function success(message)
{
    toastr.success(message, 'Success');
    
}
function wishlist(id, this_url) {
   // if (window.confirm('Do you really want to change it?')){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
                url: this_url,
                type: "post",
                data: {
                    id:id
                },
                success: function(data) {
                    if (data.status == 'success') {
                        success(data.message);
                        setTimeout(
                          function()
                          {
                            location. reload(true);
                          }, 1000);
                    }else if(data.status == 'login_failed'){
                        $(".user-login").addClass("open");
                        $(".loginpanel").addClass("show");
                        //$(".open_login_dd").attr("aria-expanded", "true");
                        error(data.message);
                    }
                    else if(data.status == 'error'){
                        error(data.message);
                    }
                },
                error: function() {
                    alert('There is error while changing status');
                }
            });
    //}
}

// function proceedToCheckout() {
    
//     $.ajax({
//         type: "GET",
//         url: "{{ route('checkIfUserLoggedin') }}",
//         success: function (result) {
//             hideCustomLoader();
//             if (result == 0) {
//                 // not logged in
//                 // showMessage("You need to be logged in to proceed to checkout", 'danger');
//                 showMessage("Become a user & login to add items to your favourite list ", 'danger');
//                 $(".login_li").addClass("open");
//                 $(".open_login_dd").attr("aria-expanded", "true");
//             } else {
//                 // logged in
//                 window.location.href = "{{route('web.checkout')}}";
//             }
//         }
//     });
// }