<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class ProductVariantText extends Model {
	public function product_variant(){
        return $this->belongsTo('App\ProductVariant', 'product_variant_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

