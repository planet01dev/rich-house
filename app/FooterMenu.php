<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class FooterMenu extends Model {
	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function footer_menu_text(){
        return $this->hasOne('App\FooterMenuText', 'footer_id', 'id');
    }

    public function footer_menu_text_all(){
        return $this->hasMany('App\FooterMenuText', 'footer_id', 'id')->withoutGlobalScopes();
    }

   
}

