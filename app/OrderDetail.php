<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{

    public function orderDetail(){
        return $this->belongsTo('App\Order', 'id');
    }  

    public function order(){
        return $this->hasMany('App\Order', 'id', 'order_id');
    } 

    public function orderProduct(){
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

   /* public function OrderVariationDetail(){
        return $this->hasMany('App\OrderVariantDetail', 'detail_id', 'id');
    }  */

}
