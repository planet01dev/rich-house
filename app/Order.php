<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    public function coupon(){
        return $this->belongsTo('App\Coupon', 'coupon_id', 'id');
    }

    public function orderDetail(){
        return $this->hasMany('App\OrderDetail', 'order_id', 'id');
    }
    
    public function order(){
        return $this->belongsTo('App\OrderDetail', 'order_id');
    }
}
