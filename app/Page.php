<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Page extends Model {

    public function page_text(){
        return $this->hasOne('App\PageText', 'page_id', 'id');
    }

    public function page_text_all(){
        return $this->hasMany('App\PageText', 'page_id', 'id')->withoutGlobalScopes();
    }
    
}

