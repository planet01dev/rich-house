<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    public function product(){
        return $this->hasMany('App\Product', 'id', 'product_id');
    }

    public function sidebar(){
        return $this->hasMany('App\SidebarMenu', 'id', 'sidebar_id');
    }

    public function sidebar_sub(){
        return $this->hasMany('App\SidebarSubMenu', 'id', 'sidebar_child_id');
    }
    public function tag(){
        return $this->hasMany('App\Tag', 'id', 'tag_id');
    }
}

