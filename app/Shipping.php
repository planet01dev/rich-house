<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model {  

	public function shipping_text(){
        return $this->hasOne('App\ShippingText', 'shipping_id', 'id');
    }

    public function shipping_text_all(){
        return $this->hasMany('App\ShippingText', 'shipping_id', 'id')->withoutGlobalScopes();
    } 
}

