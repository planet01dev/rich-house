<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class LangScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $lang = (session()->has('lang') ? session()->get('lang') : 'en' );
        $lang_id = ($lang == 'ar') ? '2' : '1';  
        //$builder->where('lang', 2);
        $builder->where('lang', $lang_id);
    }
}