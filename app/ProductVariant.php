<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model {
    protected $appends = ['ProductVariantTexts','Variants'];
    public function variant()
    {
        return $this->belongsTo('App\Variant', 'variant_id', 'id');
    }
    public function variant_text()
    {
        return $this->hasOne('App\VariantText', 'variant_id', 'id');
    }
    public function product_variant_text(){
        return $this->hasOne('App\ProductVariantText', 'product_variant_id', 'id');
    }
    
    public function variant_and_text()
    {
        return $this->product_variant_text()->merge($this->variant()->variant_text());
    }

    public function product_variant_text_all(){
        return $this->hasMany('App\ProductVariantText', 'product_variant_id', 'id')->withoutGlobalScopes();
    }
}

