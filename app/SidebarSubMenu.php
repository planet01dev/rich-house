<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class SidebarSubMenu extends Model {

	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function sidebar_menu(){
        return $this->belongsTo('App\SidebarMenuText', 'sidebar_id', 'id');
    }

    public function sidebar_sub_menu_text(){
        return $this->hasOne('App\SidebarSubMenuText', 'sidebar_sub_menu_id', 'id');
    }

    public function sidebar_sub_menu_text_all(){
        return $this->hasMany('App\SidebarSubMenuText', 'sidebar_sub_menu_id', 'id')->withoutGlobalScopes();
    }
    public function category(){
        return $this->hasMany('App\Category', 'sidebar_child_id', 'id');
    }
  

   
}

