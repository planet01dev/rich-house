<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class FooterMenuBottomText extends Model {
	public function footer_menu(){
        return $this->belongsTo('App\FooterMenuBottom', 'footer_bottom_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

