<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class SectionMenu extends Model {
	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function section_menu_text(){
        return $this->hasOne('App\SectionMenuText', 'section_id', 'id');
    }

    public function section_menu_text_all(){
        return $this->hasMany('App\SectionMenuText', 'section_id', 'id')->withoutGlobalScopes();
    }
    public function category(){
        return $this->hasMany('App\Category', 'section_id', 'id');
    }
  

   
}

