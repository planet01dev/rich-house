<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class BrandText extends Model {
	public function brand(){
        return $this->belongsTo('App\Brand', 'brand_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

