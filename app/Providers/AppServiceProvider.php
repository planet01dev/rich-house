<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\SidebarMenu;
use App\SidebarMenuText;
use App\SidebarSubMenu;
use App\SidebarSubMenuText;
use App\FooterMenu;
use App\FooterMenuBottom;
use App\Setting;
use App\Brand;
use App\SubscribeSetting;
use App\Scopes\LangScope;
use Session;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view) {
            $sidebars = SidebarMenu::where('is_active', 1)->with(['sidebar_menu_text', 'sidebar_sub_menu' => function($query){
                    $query->where('is_active', 1)->orderBy('list_order', 'Asc')->with('sidebar_sub_menu_text');
                }])->orderBy('list_order', 'Asc')->get();
            $all_brands = Brand::with('brand_text')->orderBy('id', 'desc')->get();
            $view->with(['sidebars'=> $sidebars,'all_brands'=>$all_brands]);
        });

        View::composer('*', function($view){
            $footerbar = FooterMenu::where('is_active', 1)->with('footer_menu_text')->orderBy('list_order', 'Asc')->get();
            $view->with('footerbar', $footerbar);
        });

        View::composer('*', function($view) {
            $footer_bootom_bar = FooterMenuBottom::where('is_active', 1)->with('footer_menu_bottom_text')->orderBy('list_order', 'Asc')->get();
            $view->with('footer_bootom_bar', $footer_bootom_bar);
        });

        View::composer('*', function($view) {
            $setting = Setting::with('setting_text')->first();
            $view->with('setting', $setting);
            $subscribe = SubscribeSetting::with('subscribe_setting_text')->first();
            $view->with('subscribe', $subscribe);
        });
       
        
    }
}
