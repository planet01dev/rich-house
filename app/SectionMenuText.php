<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class SectionMenuText extends Model {
	public function footer_menu(){
        return $this->belongsTo('App\SectionMenu', 'section_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

