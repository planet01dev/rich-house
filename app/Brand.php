<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model {
	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


    public function brand_text(){
        return $this->hasOne('App\BrandText', 'brand_id', 'id');
    }

    public function brand_text_all(){
        return $this->hasMany('App\BrandText', 'brand_id', 'id')->withoutGlobalScopes();
    }  
}

