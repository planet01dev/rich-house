<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class ShippingText extends Model {
	public function shipping(){
        return $this->belongsTo('App\Shipping', 'shipping_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

