<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class FooterMenuBottom extends Model {
	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function footer_menu_bottom_text(){
        return $this->hasOne('App\FooterMenuBottomText', 'footer_bottom_id', 'id');
    }

    public function footer_menu_bottom_text_all(){
        return $this->hasMany('App\FooterMenuBottomText', 'footer_bottom_id', 'id')->withoutGlobalScopes();
    }

   
}

