<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model {
	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


    public function variant_text(){
        return $this->hasOne('App\VariantText', 'variant_id', 'id');
    }

    public function variant_text_all(){
        return $this->hasMany('App\VariantText', 'variant_id', 'id')->withoutGlobalScopes();
    }

	   

	   
}

