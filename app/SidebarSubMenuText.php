<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class SidebarSubMenuText extends Model {
	public function sidebar_sub_menu(){
        return $this->belongsTo('App\SidebarSubMenu', 'sidebar_sub_menu_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

