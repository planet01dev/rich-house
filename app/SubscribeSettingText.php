<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class SubscribeSettingText extends Model {
	public function subscribe_setting(){
        return $this->belongsTo('App\SubscribeSetting', 'subscribe_setting_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

