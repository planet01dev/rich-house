<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class SidebarMenu extends Model {
	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function sidebar_sub_menu(){
        return $this->hasMany('App\SidebarSubMenu', 'sidebar_id', 'id');
    }
    
    public function sidebar_menu_text(){
        return $this->hasOne('App\SidebarMenuText', 'sidebar_id', 'id');
    }
    
    public function sidebar_menu_text_all(){
        return $this->hasMany('App\SidebarMenuText', 'sidebar_id', 'id')->withoutGlobalScopes();
    }

    public function category(){
        return $this->hasMany('App\Category', 'sidebar_id', 'id');
    }


   
}

