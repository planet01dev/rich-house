<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class SubscribeSetting extends Model {
	
	public function subscribe_setting_text(){
        return $this->hasOne('App\SubscribeSettingText', 'subscribe_setting_id', 'id');
    }

    public function subscribe_setting_text_all(){
        return $this->hasMany('App\SubscribeSettingText', 'subscribe_setting_id', 'id')->withoutGlobalScopes();
    }
}

