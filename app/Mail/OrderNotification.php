<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *

     * @return void
     */
    public $order;
    public $isAdmin;
    public $subject;
    public function __construct($order,$isAdmin)
    {
       $this->order = $order;   
       $this->isAdmin = $isAdmin;
       if($this->isAdmin == 1)
       {
        $this->subject = "Rich House Order";
       }else
       {
        $this->subject = "Rich House Order Confirmation";
       }
       
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        return $this->from(['address' => 'donotreply@richhouse.me', 'name' => 'Rich House'])->subject($this->subject)
        ->view('website.notification.email')->with(['order'=>$this->order,'isadmin' => $this->isAdmin]);
        
    }
}
