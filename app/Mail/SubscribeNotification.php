<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscribeNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *

     * @return void
     */
    public $data;
    public function __construct($data)
    {
       $this->data = $data;   
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'donotreply@richhouse.me', 'name' => 'Rich House'])->subject('Rich House Sign Up')
        ->view('website.notification.subscribe_customer')->with('data', $this->data);
        
    }
}
