<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $token;
    public $username;
    public function __construct($token,$username)
    {
        $this->token = $token;
        $this->username = $username;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'donotreply@richhouse.me', 'name' => 'Rich House'])
            ->view('website.notification.forgot-password')->with(['token' =>$this->token,'username'=>$this->username]);
    }
}
 