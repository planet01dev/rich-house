<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $contactData;
    public function __construct($data)
    {
        $this->contactData = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'donotreply@richhouse.me', 'name' => 'Rich House'])->subject('Rich House Contact Request')
            ->view('website.notification.contact_us')->with(['contactData'=>$this->contactData]);
    }
}
