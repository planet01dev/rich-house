<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use App\ProductVariant;
use App\Image;
use App\Setting;
use App\Shipping;
use App\Order;
use App\OrderDetail;
use App\SettingEmail;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderNotification;
use App\Coupon;
use App\CouponText;
use App\User;
use Illuminate\Support\Facades\Auth;
class CartController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function __construct()
    {
        //$this->middleware('auth')
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function cart(){
      $cookie = $this->verify_cookie();
      $product = Array();
      $total_amount = 0;
      foreach ($cookie as $cookie){
        $list = explode("__-__", $_COOKIE[$cookie['data']]);
        $product_data   = Product::where('id', $list[0])->first();
        $inventory = $product_data->inventory;
        if($list[4] != '' && $list[4] != null && $list[4] != 0)
        {
          $variant_data = ProductVariant::where('id', $list[4])->first();
          $inventory = $variant_data->inventory;
        }
        $row  = Array('id' => $list[0], 'quantity' => $list[1], 'title' => $list[2], 'price' => $list[3], 'thumbnail' => $cookie['thumbnail'],'inventory' => $inventory);
        array_push($product, $row);
        $total_amount = $total_amount + $list[3];
      }
      $setting = Setting::where('id', '1')->first();
      $tax = $setting->tax;
      $shipping_charges = ($setting->shipping_rate > 0)? num_format($setting->shipping_rate) : 0.00;
      
      return view('website/cart')->with(['product' => $product, 'title' => trans('lang.cart'), 'total_amount' => $total_amount, 'tax' => $tax, 'shipping_charges' => $shipping_charges, 'shipping_method' => $setting->shipping_method]);
    }

    public function checkout(Request $request){
      // return view('website/payfort');
      $cookie = $this->verify_cookie();
      if(count($cookie) < 1){
        return redirect()->route('products');
      }
      $product = Array();
      $total_amount = 0;
      foreach ($cookie as $cookie){
        $list = explode("__-__", $_COOKIE[$cookie['data']]);
        $row  = Array('id' => $list[0], 'quantity' => $list[1], 'title' => $list[2], 'price' => $list[3], 'thumbnail' => $cookie['thumbnail']);
        array_push($product, $row);
        $total_amount = $total_amount + $list[3];
      }
      $setting = Setting::where('id', '1')->first();
      $tax = $setting->tax;
      $shipping_charges = ($setting->shipping_rate > 0)? num_format($setting->shipping_rate) : 0.00;
      $shipping_data = Shipping::with('shipping_text_all')->where('id', 1)->first();

      if(Auth::user() != null && Auth::user()->hasRole('customer')) 
      {
        $user_id = Auth::user()->id;
        $user_data = User::with(['user_address'])->where('id', $user_id)->first();
        // dd($user_data);
      }

      return view('website/checkout')->with('product', $product)->with(['user_data'=>@$user_data,'title' => trans('lang.checkout'), 'total_amount'=> $total_amount, 'tax' => $tax,'shipping_data'=>$shipping_data, 'shipping_charges' => $shipping_charges, 'shipping_method' => $setting->shipping_method]);
    }

    public function post_checkout(Request $request){
    
        
      $cookie      = $this->verify_cookie();
      $billing  = $request->billing;
      $shipping = $request->shipping;

      $error = array();
      $error_check = 0;

      if (!Auth::check())
      {
        if(User::where('email', $billing['email'])->count() > 0){
          echo json_encode(array("error" => 'User Email Already Exists, Login.'));
          exit();
        }
      }else
      {
        $user_data = User::where('id', Auth::user()->id)->first();
        if($user_data['email'] != $billing['email'])
        {
          echo json_encode(array("error" => 'Use logged in email.'));
          exit();
        }
      }
      

      $has_address = (isset($request->has_shipping_address))? 0 : 1 ;
      $customer_email = $billing['email'];
      if($has_address == 1)
      {
          if($shipping['name'] == null || $shipping['name'] == '')
          {
            echo json_encode(array("error" => 'Please Fill all shipping fields'));
            exit();
          }
      }

      foreach ($cookie as $cookies) {
        $list = explode("__-__", $_COOKIE[$cookies['data']]);
        $product_id = $list[0];
        $quantity = $list[1];
        $variant_id = $list[4];
        if(!check_inventory($product_id,$quantity,$variant_id))
        {
          $error_check = 1;
          $temp = Product::with(['product_text'])->where('id', $product_id)->first();
          array_push($error,@$temp->product_text->title.' is out of stock.');
        }
      }

      if($error_check == 1)
      {
          echo json_encode(array("error" => $error));
          exit();
      }

      
      if($billing['has_payment'] == 1)
      {
          $order_id_global = $this->add($request,1);
          session()->put('payment_order_id',$order_id_global);
          
        if($order_id_global != 0)
        {
            $total_amount = 0;
            foreach ($cookie as $cookies) {
                $list = explode("__-__", $_COOKIE[$cookies['data']]);
                $total_amount = $total_amount+$list[3];
            }
            $subtotal = $total_amount;
            $setting = Setting::where('id', '1')->first();
            $tax = $setting->tax;
            // $shipping_charges = ($setting->shipping_rate > 0)? num_format($setting->shipping_rate) : 0.00;
            $shipping_charges = ($request->shipping_rate > 0)? num_format($request->shipping_rate) : 0.00;
            $coupon_amount = ($subtotal / 100)*num_format($request->coupon_discount,2);
            $tax_amount = ($subtotal / 100)*$tax;
            $total = ($subtotal + $tax_amount + $shipping_charges)-$coupon_amount;
            $total = strval(number_format($total,2));
            $total = str_replace(',', '', $total); 
            $total = str_replace('.', '', $total);
            // $total = 100;
            // How to calculate request signature
            $shaString  = '';
            $ref = 'richhouse'.time();
            // $ref = "TEST81002";
            // array request
            $arrData    = array(
            'access_code'        =>'OVxOozLKKm7M9fBigTco',
            'amount'             => $total,
            'command'            =>'PURCHASE',
            'currency'           =>'AED',
            'customer_email'     => $customer_email,
            'language'           =>'en',
            'merchant_identifier'=>'hGHgAydq',
            'merchant_reference' => $ref,
            'order_description'  => 'Order ID '.$order_id_global,
            'merchant_extra1' => (isset($request->has_shipping_address))? 0 : 1 ,
            'merchant_extra2' => $order_id_global
            );
            // sort an array by key
            ksort($arrData);
            foreach ($arrData as $key => $value) {
                $shaString .= "$key=$value";
            }
            // make sure to fill your sha request pass phrase
            $shaString = "14oAsZCDzJQx/YI3dlZkAk{@" . $shaString . "14oAsZCDzJQx/YI3dlZkAk{@";
            $signature = hash("sha256", $shaString);
            // your request signature
            // echo $signature;
            $requestParams = array(
                'access_code' => 'OVxOozLKKm7M9fBigTco',
                'amount' => $total,
                'command' => 'PURCHASE',
                'currency' => 'AED',
                'customer_email' => $customer_email,
                'language' => 'en',
                'merchant_identifier' => 'hGHgAydq',
                'merchant_reference' => $ref,
                'order_description' => 'Order ID '.$order_id_global,
                'signature' => $signature,
                'merchant_extra1' => (isset($request->has_shipping_address))? 0 : 1 ,
                'merchant_extra2' => $order_id_global
            );


            $redirectUrl = 'https://checkout.payfort.com/FortAPI/paymentPage';
            
            return ["payfort" => "Successfully Added.", "request_para" => $requestParams,'redirectUrl' => $redirectUrl,'billing' => json_encode($billing),'shipping'=>json_encode($shipping)];
        }else
        {
            return ["error" => "Something Went Wrong."];
        }
      }
      else
      {
         $orderId =  $this->add($request);
         $this->send_email($orderId);
          return ["success" => "Successfully Added.", "redirect" => route('thankyou')];
      }
    }
    
    public function add(Request $request, $payment = 0)
    {
        
          $cookie      = $this->verify_cookie();
          $billing  = $request->billing;
          $shipping = $request->shipping;
          $has_address = (isset($request->has_shipping_address))? 0 : 1 ;
          
          $order = new Order();
          if (Auth::check())
          {
              $order->user_id         = Auth::user()->id;
          }
          else
          {
              $order->user_id         = 0;
          }
          $order->billing_name         = $billing['name'];
          $order->billing_email        = $billing['email'];
          $order->billing_phone        = $billing['phone'];
          $order->billing_country      = $billing['country'];
          $order->billing_address      = $billing['address'];
          $order->billing_country      = $billing['country'];
          $order->billing_city         = $billing['city'];
          $order->billing_state        = $billing['state'];
          $order->billing_zip          = $billing['zip'];
          $order->billing_message      = $billing['message'];
          $order->billing_wrapping     = (isset($billing['wrapping']))?'1':'0';
          $order->shipping_name        = @$shipping['name'];
          $order->shipping_email       = @$shipping['email'];
          $order->shipping_phone       = @$shipping['phone'];
          $order->shipping_country     = @$shipping['country'];
          $order->shipping_address     = @$shipping['address'];
          $order->shipping_country     = @$shipping['country'];
          $order->shipping_city        = @$shipping['city'];
          $order->shipping_state       = @$shipping['state'];
          $order->shipping_zip         = @$shipping['zip'];
          $order->has_shipping_address = $has_address;
          $order->has_payment          = $billing['has_payment'];
          if($billing['has_payment'] == 1)
          {
            $order->status = 4;
          }
          $order->coupon_id = $request->coupon_id;
          $order->payfort_id = (isset($request->fort_id)?$request->fort_id:'0');
          $order->save();
          if ($order->id) {
              $products = array();
              $total_amount = 0;
              foreach ($cookie as $cookies) {
                  $list = explode("__-__", $_COOKIE[$cookies['data']]);
                  $variation = array();
                  if (isset($list[5])) {
                      $vari = explode(",", $list[5]);
                      for ($j= 0; $j<sizeof($vari) -1; $j++) {
                          if (!empty($vari[$j])) {
                              $vari_exp = explode("=", $vari[$j]);
                              $var_key     = $vari_exp[0];
                              $var_val     = $vari_exp[1];
                              $variation[$var_key] = $var_val;
                          }
                      }
                  }
                  $row  = array('id' => $list[0], 'quantity' => $list[1], 'price' => $list[3], 'variation' => $variation,'variant_id' =>$list[4]);
                  array_push($products, $row);
                  $total_amount = $total_amount+$list[3];
              }
              $subtotal = $total_amount;
              $setting = Setting::where('id', '1')->first();
              $tax = $setting->tax;
              // $shipping_charges = ($setting->shipping_rate > 0)? num_format($setting->shipping_rate) : 0.00;
              $coupon_amount = ($subtotal / 100)*num_format($request->coupon_discount,2);
              $shipping_charges = ($request->shipping_rate > 0)? num_format($request->shipping_rate) : 0.00;
              $tax_amount = ($subtotal / 100)*$tax;
              $total = ($subtotal + $tax_amount + $shipping_charges)-$coupon_amount;
              Order::where('id', $order->id)->update(['tax' => $tax, 'shipping_charges' => $shipping_charges, 'shipping_method' => $setting->shipping_method, 'subtotal' => num_format($subtotal), 'total' => num_format($total)]);
              
              foreach ($products as $product) {
                  $order_detail = new OrderDetail();
                  $order_detail->order_id   = $order->id;
                  $order_detail->product_id = $product['id'];
                  $order_detail->quantity   = $product['quantity'];
                  $order_detail->amount     = $product['price'];
                  $order_detail->variant_id     = $product['variant_id'];
                  $order_detail->save();

                  $temp = Product::where('id', $product['id'])->first();

                  if(intval($product['variant_id']) != 0)
                  {
                    $data = ProductVariant::where('id', $product['variant_id'])->update([
                      'inventory' => $temp->inventory - $product['quantity'],
                  ]);
                  }else
                  {
                    $data = Product::where('id', $product['id'])->update([
                        'inventory' => $temp->inventory - $product['quantity'],
                    ]);
                  }

                  /*if(isset($product['variation']) && !empty($product['variation'])){
                      foreach($product['variation'] as $key =>$vari){
                          $order_variant_detail = new OrderVariantDetail();
                          $order_variant_detail->order_id   = $order_id;
                          $order_variant_detail->detail_id  = $order_detail->id;
                          $order_variant_detail->var_key    = $key;
                          $order_variant_detail->var_value  = $vari;
                          $order_variant_detail->save();
                      }
                  }     */
              }
              //empty cookie
              foreach ($cookie as $cook) {
                  $list = explode("__-__", $_COOKIE[$cook['data']]);
                  $row  = array('id' => $list[0], 'quantity' => $list[1], 'price' => $list[3]);
                  $key = "product_".$row['id'];
                  setcookie($key, "", time() - 3600, "/");
              }
              setcookie("billing", "", time() - 3600);
              setcookie("shipping", "", time() - 3600);

              $coupon_data = Coupon::where('id', $request->coupon_id)->first();
              Coupon::where('id', $request->coupon_id)->update([
                'UsageCount' => num_format($coupon_data['UsageCount']) + 1,
                // 'IsActive' => 0,
              ]);

              
          }
          return $order->id;
    }

    public function payment_response(Request $request)
    {
        
        $convert_request_payfort_id = substr($request, strpos($request, 'fort_id=') );
        $convert_request_status = substr($request, strpos($request, 'status=') );    
        $convert_request_merchant_extra_1 = substr($request, strpos($request, 'merchant_extra1=') );  
        $convert_request_merchant_extra_2 = substr($request, strpos($request, 'merchant_extra2=') );   //order_id
        
        $fort_id =  '';
        for($i=0;$i<18;$i++)
        {
            $fort_id .= $convert_request_payfort_id[8+$i];
        }
        $status =  $convert_request_status[7].$convert_request_status[8];
        $merchant_extra_1 = $convert_request_merchant_extra_1[16];
        
        $data = new Request();
        $data->status = $status;
        $data->fort_id = $fort_id;
        $data->merchant_extra1 = $merchant_extra_1;
        
        
        if($data->status == '14')
        {
            // $order = Order::where('payfort_id','=','0')->where('has_payment',1)->max('id');
            $order = $convert_request_merchant_extra_2;
            $order = session()->get('payment_order_id');
            $data = Order::where('id', $order)->update([
                'payfort_id' => $fort_id,
                'status' => 0
              ]);

              $this->send_email($order);
              session()->forget('payment_order_id');
            return redirect(route('thankyou')); 
        }else
        {
            return redirect(route('web.checkout'));
        }
        
      
    }

    public function send_email($id){
      $order = Order::with(['orderDetail', 'orderDetail.orderProduct.product_text','orderDetail.orderProduct.productvariant'])->where('id', $id)->first();
        //  $order = Order::with(['orderDetail', 'orderDetail.orderProduct.product_text','orderProduct.productvariant'])->where('id', $id)->first();
      $setting_email = SettingEmail::get();
      foreach ($setting_email as $recipient) {
          Mail::to($recipient->email)->send(new OrderNotification($order,1));
      }
      Mail::to($order->billing_email)->send(new OrderNotification($order,0));
   }

    public function verify_cookie(){
      $cookie = $_COOKIE;
      $data   = Array();
      foreach ($cookie as $cookie){
        if (strpos($cookie, '__-__') !== false) {
            $list    = explode("__-__", $cookie);
            $product = Product::with('images')->where('id', $list[0])->first();
            if(!$product){
              $c_name = 'product_'.$list[0];
              setcookie($c_name, "", time() - 3600);
            }else{
              $amount  = $list[3] / $list[1];
              $total   = $list[3];
              array_push($data, Array('data' => 'product_'.$product->id, 'thumbnail' => $product->images[0]->image));
            }
        }
      }
      return $data;
    }

    public function update_payment_status(Request $request){
      if(isset($request->id)){
        Order::where('id', $request->id)->update(['has_payment' => 1]);
      }
       return redirect(route('thankyou'));
    }

    public function redeem(Request $request)
    {
      $data = Coupon::with(['coupon_text'])->where('CouponCode', $request->code)->first();
      return $data;
    }

    

   
}