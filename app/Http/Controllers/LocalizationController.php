<?php
namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
class LocalizationController extends Controller {
	public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index($lang){
        App::setlocale($lang);
        session()->put('lang', $lang);
        return redirect()->back();
    }
}