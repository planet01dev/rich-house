<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\OurStory;
use App\OurStoryText;
use Auth;
use Illuminate\Support\Facades\File;

class OurStoryController extends Controller
{
    public function create()
    {
        $ourstory = OurStory::first();
        $title = 'Our Story';
        return view('admin.ourstory.create',compact('ourstory','title'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'title1'=>'required',
            'title2'=>'required',
            'content1'=>'required',
            'content2'=>'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
            $file = explode("~~",$request->fileName);
            $data = OurStory::where('id', $request->id)->update([
                'user_id' => Auth::user()->id,
                'image' => @$file[0]
            ]);
            OurStoryText::withoutGlobalScopes()->where('our_story_id', $request->id)->delete();
            $data = OurStoryText::insert([
                ['our_story_id' => $request->id, 'title' => $request->title1,'content' => $request->content1, 'lang' => 1],
                ['our_story_id' => $request->id, 'title' => $request->title2,'content' => $request->content2, 'lang' => 2],
            ]);
        }
        else
        {
            $request->validate([
                'image' => 'required|image'
            ]);
            $dataOne = new OurStory;
            $dataOne->user_id = Auth::user()->id;
            $dataOne->image = '1234';
            $dataOne->save();
            $data = OurStoryText::insert([
                ['our_story_id' => $dataOne->id, 'title' => $request->title1,'content' => $request->content1, 'lang' => 1],
                ['our_story_id' => $dataOne->id, 'title' => $request->title2,'content' => $request->content2, 'lang' => 2],
            ]);
        }
        return redirect()->route('admin.ourstory.add');
    }

    public function ImageUpload(Request $request)
    {
      $file = $request->file('file');
        $target_dir = env('STORY_IMAGES');
        $target_file = $target_dir . basename($_FILES["file"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $filename  = basename($_FILES['file']['name']);
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $new       = $filename;
        move_uploaded_file($_FILES["file"]["tmp_name"], storage_path('our_story')."/{$new}");
        return $new;
    }
  
    public function removeImage(Request $request)
    {
      $file = $request->file;
      $delete = File::delete(storage_path('our_story/' . $file));
      return 'true';
    }
}
