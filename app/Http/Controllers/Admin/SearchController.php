<?php
namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use App\ItemType;
use App\Product;
use App\Image;
use App\Lead;
use App\Review;
use App\Order;
use App\ProductText;
use App\Brand;
use App\BrandText;


class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->paginate = 25;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function adminSearch(Request $request)
    {  
        $type    = $request->type;
        $keyword = $request->keyword;
        if($type != '' && $keyword !=''){
            if($type == 'product'){
                $data = ProductText::where('title', 'Like', '%'.$keyword.'%')->get();
                if (isset($data)) {
                    $products = array_column($data->toArray(), 'product_id');
                    $data = Product::with(['product_text', 'front_price', 'images', 'weight'])->whereIn('id', $products)->orderBy('id', 'desc')->paginate($this->paginate);
                }
                else{
                    $data = [];
                }
                return view('admin/product/manage')->with(['title' => trans('lang.product'), 'data' => $data,'search_type'=>$type,'seacrh_keyword'=>$keyword]);
            }else if($type == 'brand'){
                $data = BrandText::where('title', 'Like', '%'.$keyword.'%')->get();
                if (isset($data)) {
                    $products = array_column($data->toArray(), 'brand_id');
                    $data = Brand::whereIn('id', $products)->orderBy('id', 'desc')->paginate($this->paginate);
                }
                else{
                    $data = [];
                }
                return view('admin/brand/manage')->with(['title' => trans('lang.brand'), 'data' => $data,'search_type'=>$type,'seacrh_keyword'=>$keyword]); 
            }
        }else{
            return redirect('admin/lead');
        }
    }

}
