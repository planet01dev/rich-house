<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use DB;
use PDF;
use Carbon;
use App\User;
use Excel;
use App\Exports\UsersExport;

class SignUpCustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $data = User::where('id', '!=' ,'1')->orderBy('id', 'desc')->paginate(25);
        // dd($data);
        return view('admin/sign_up_customer/manage')->with(['title' => trans('lang.customer'), 'data' => $data]);
        
    }
    public function export()
    {
        
        return Excel::download(new UsersExport, 'customers.xlsx');
        
    }
}
