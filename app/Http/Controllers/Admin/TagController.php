<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\Tag;
use App\TagText;
use App;
class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* public function index()
    {   
       
        return view('admin/dashboard');
        
    }*/

    public function index(){  
        $data = Tag::with('tag_text')->orderBy('id', 'desc')->paginate(25);
        return view('admin/tag/manage')->with(['title' => trans('lang.tag'), 'data' => $data]); 
    }
    public function add(Request $request){
        return view('admin/tag/edit')->with(['title' => trans('lang.create')]);   
    }

    public function post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = Tag::where('id', $request->id)->update([
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
            ]);
            TagText::withoutGlobalScopes()->where('tag_id', $request->id)->delete();
            $data = TagText::insert([
                ['tag_id' => $request->id, 'title' => $request->title1, 'lang' => 1],
                ['tag_id' => $request->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.tag.manage')];
        }else{
            $data = new Tag();
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->save();
            $text = TagText::insert([
                ['tag_id' => $data->id, 'title' => $request->title1, 'lang' => 1],
                ['tag_id' => $data->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.tag.manage')];
        }
    }

    public function edit(Request $request){
        $data = Tag::with('tag_text_all')->where('id', $request->id)->first();
        return view('admin/tag/edit')->with(['title' => trans('lang.edit'), 'data' => $data]);   
    }

    public function delete(Request $request){
      $id = $request->id;
      Tag::where('id', $id)->delete();
      TagText::withoutGlobalScopes()->where('tag_id', $id)->delete();
      return 'success';
    }
  }