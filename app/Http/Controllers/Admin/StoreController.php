<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Http\Request;
use App\Coupon;
use App\CouponText;
use App\Store;
use App\Image;
use Illuminate\Support\Facades\File;
//use App\Review;



class StoreController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $data = Store::orderBy('id', 'desc')->paginate(25);
    // dd($data);
    return view('admin/store/manage')->with(['title' => trans('lang.store'), 'data' => $data]);
  }
  public function add()
  {
    return view('admin.store.edit')->with('title', trans('lang.create'));
  }

  public function post(Request $request)
  {


    if (isset($request->id) && !empty($request->id)) {
      //Update
      $request->validate([
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'address' => 'required',
        'lat' => 'required',
        'long' => 'required',
      ]);
      $file = explode("~~",$request->fileName);
        if(!$file[0]){
            return ["error" => trans('lang.image_required')];
        }
      // dd($request);
      $data = Store::where('id', $request->id)->update([
        'image' => $file[0],
        'name' => $request->name,
        'phone' => $request->phone,
        'email' => $request->email,
        'address' => $request->address,
        'days' => $request->days,
        'timing' => $request->timing,
        'city' => $request->city,
        'whatsapp' => $request->whatsapp,
        'lat' => $request->lat,
        'long' => $request->long,
      ]);
      
      return ["success" => "Successfully Updated.", "redirect" => route('admin.store.manage')];
    } else {
      //Insert
      $request->validate([

        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'address' => 'required',
        'lat' => 'required',
        'long' => 'required',
      ]);
      $file = explode("~~",$request->fileName);
        if(!$file[0]){
            return ["error" => trans('lang.image_required')];
        }
      $store                     = new Store;
      $store->image = $file[0];
      $store->name          = $request->name;
      $store->phone          = $request->phone;
      $store->email          = $request->email;
      $store->address          = $request->address;
      $store->days          = $request->days;
      $store->timing          = $request->timing;
      $store->city          = $request->city;
      $store->whatsapp          = $request->whatsapp;
      $store->lat          = $request->lat;
      $store->long          = $request->long;
      $store->save();


      
      return ["success" => "Successfully Added.", "redirect" => route('admin.store.manage')];
    }
  }

  
  public function details(Request $request)
  {
    $data = Store::where('id', $request->id)->first();
    return View('admin.store.detail', compact('data'))->with(['title' => 'Store Details']);
  }

  public function edit(Request $request)
  {
    $data = Store::where('id', $request->id)->first();
    return view('admin.store.edit')->with('title', trans('lang.edit'))->with(['data' => $data,'edit' => 1]);
  }



  public function delete(Request $request)
  {
    $coupon_id = $request->id;
    Store::where('id', $coupon_id)->delete();
    return 'success';
  }

  public function ImageUpload(Request $request)
  {
    $file = $request->file('file');
      $target_dir = env('STORE_IMAGES');
      $target_file = $target_dir . basename($_FILES["file"]["name"]);
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
      $filename  = basename($_FILES['file']['name']);
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      $new       = $filename;
      move_uploaded_file($_FILES["file"]["tmp_name"], storage_path('store')."/{$new}");
      return $new;
  }

  public function removeImage(Request $request)
  {
    $file = $request->file;
    $delete = File::delete(storage_path('store/' . $file));
    Image::where('image', $file)->delete();
    return 'true';
  }
 

}
