<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Http\Request;
use App\Product;
use App\ProductText;
use App\ProductVariant;
use App\ProductVariantText;
use App\Price;
use App\Category;
use App\SidebarMenu;
use App\SidebarSubMenu;
use App\Image;
use App\Variant;
use App\SectionMenu;
use App\Tag;
use App\Brand;
use App\WeightType;
use App\Coupon;
use App\CouponText;
use Illuminate\Support\Facades\File;
//use App\Review;



class CouponController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $data = Coupon::with(['coupon_text'])->orderBy('id', 'desc')->paginate(25);
    // dd($data);
    return view('admin/coupon/manage')->with(['title' => trans('lang.coupon'), 'data' => $data]);
  }
  public function add()
  {
    return view('admin.coupon.edit')->with('title', trans('lang.create'));
  }

  public function post(Request $request)
  {


    if (isset($request->id) && !empty($request->id)) {
      //Update
      $request->validate([
        'CouponCode' => 'required',
        'DiscountPercentage' => 'required',
        'title1' => 'required',
        'title2' => 'required',
      ]);
      if (Coupon::where('CouponCode', $request->CouponCode)->where('id', '!=', $request->id)->count() > 0) {
        return ["error" => "Coupon Code alredy in used."];
      }
      // dd($request);
      $data = Coupon::where('id', $request->id)->update([
        'CouponCode' => $request->CouponCode,
        'discount_type' => $request->discount_type,
        'DiscountPercentage' => $request->DiscountPercentage,
        'IsActive' => (isset($request->IsActive) && $request->IsActive == 'on' )? 1 : 0,
      ]);
      CouponText::withoutGlobalScopes()->where('coupon_id', $request->id)->delete();
      $text = CouponText::insert([
        ['coupon_id' => $request->id, 'Title' => $request->title1, 'Description' => $request->Description1, 'lang' => 1],
        ['coupon_id' => $request->id, 'Title' => $request->title2, 'Description' => $request->Description2, 'lang' => 2],

      ]);

      return ["success" => "Successfully Updated.", "redirect" => route('admin.coupon.manage')];
    } else {
      //Insert
      $request->validate([

        'CouponCode' => 'required|unique:coupons|max:255',
        'DiscountPercentage' => 'required',
        'title1' => 'required',
        'title2' => 'required',
      ]);
      
      $coupon                     = new Coupon;
      $coupon->CouponCode          = $request->CouponCode;
      $coupon->DiscountPercentage          = $request->DiscountPercentage;
      $coupon->discount_type          = $request->discount_type;
      $coupon->IsActive = (isset($request->IsActive) && $request->IsActive == 'on' )? 1 : 0;
      $coupon->save();

      $text = CouponText::insert([
        ['coupon_id' => $request->id, 'Title' => $request->title1, 'Description' => $request->Description1, 'lang' => 1],
        ['coupon_id' => $request->id, 'Title' => $request->title2, 'Description' => $request->Description2, 'lang' => 2],

      ]);

      
      return ["success" => "Successfully Added.", "redirect" => route('admin.coupon.manage')];
    }
  }

  
  public function details(Request $request)
  {
    $data = Coupon::with(['coupon_text'])->where('id', $request->id)->first();
    return View('admin.coupon.detail', compact('data'))->with(['title' => 'Coupon Details']);
  }

  public function edit(Request $request)
  {
    $data = Coupon::with(['coupon_text_all'])->where('id', $request->id)->first();
    return view('admin.coupon.edit')->with('title', trans('lang.edit'))->with(['data' => $data,'edit' => 1]);
  }



  public function delete(Request $request)
  {
    $coupon_id = $request->id;
    Coupon::where('id', $coupon_id)->delete();
    CouponText::withoutGlobalScopes()->where('coupon_id', $coupon_id)->delete();
    return 'success';
  }

 

}
