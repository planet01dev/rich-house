<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\HeaderMenu;
use App\HeaderSubMenu;
use App\HeaderSubChildMenu;
use App\FooterMenu;
use App\FooterSubMenu;
use App\FooterMenuText;
use App\FooterMenuBottom;
use App\FooterMenuBottomText;
use App\SidebarMenu;
use App\SidebarMenuText;
use App\SidebarSubMenu;
use App\SidebarSubMenuText;
use App\SectionMenu;
use App\SectionMenuText;
use Illuminate\Support\Facades\File;
class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* public function index()
    {   
       
        return view('admin/dashboard');
        
    }*/

   
    public function footer_menu(){   
        $data = FooterMenu::with('footer_menu_text')->orderBy('id', 'desc')->paginate(25);
        return view('admin/menu/footer_menu/manage')->with(['title' => trans('lang.footer_menu'), 'data' => $data]); 
    }
    public function footer_menu_add(Request $request){
        return view('admin/menu/footer_menu/edit')->with(['title' => trans('lang.create')]);   
    }

    public function footer_menu_post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
            'url' => 'required',
            'list_order' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
            $exists = FooterMenu::where('id', '!=', $request->id)->where('url',$request->url)->count();
            if($exists) {
              return ["error" => trans('lang.url_taken')];
            }
             $data = FooterMenu::where('id', $request->id)->update([
                //'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'url' => $request->url,
                'list_order' => $request->list_order,
                'user_id' => Auth::user()->id,
            ]);
            FooterMenuText::withoutGlobalScopes()->where('footer_id', $request->id)->delete();
            $data = FooterMenuText::insert([
                ['footer_id' => $request->id, 'title' => $request->title1, 'lang' => 1],
                ['footer_id' => $request->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.footer.menu')];
        }else{
            $exists = FooterMenu::where('url',$request->url)->count();
            if($exists) {
              return ["error" => trans('lang.url_taken')];
            }
            $data = new FooterMenu();
            //$data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->url = $request->url;
            $data->list_order = $request->list_order;
            $data->save();
            $text = FooterMenuText::insert([
                ['footer_id' => $data->id, 'title' => $request->title1, 'lang' => 1],
                ['footer_id' => $data->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.footer.menu')];
        }
    }

    public function footer_menu_edit(Request $request){
        $data = FooterMenu::with('footer_menu_text_all')->where('id', $request->id)->first();
        return view('admin/menu/footer_menu/edit')->with(['title' => trans('lang.edit'), 'data' => $data]);   
    }

    public function footer_menu_delete(Request $request){
      $id = $request->id;
      FooterMenu::where('id', $id)->delete();
      //FooterSubMenu::where('footer_id', $id)->delete();
      FooterMenuText::withoutGlobalScopes()->where('footer_id', $id)->delete();

      return 'success';
    }


    public function footer_menu_bottom(){   
        $data = FooterMenuBottom::with('footer_menu_bottom_text')->orderBy('id', 'desc')->paginate(25);
        return view('admin/menu/footer_menu_bottom/manage')->with(['title' => trans('lang.footer_menu_bottom'), 'data' => $data]); 
    }
    public function footer_menu_bottom_add(Request $request){
        return view('admin/menu/footer_menu_bottom/edit')->with(['title' => trans('lang.create')]);   
    }

    public function footer_menu_bottom_post(Request $request){
        $request->validate([
            'title1' => 'required', 
            'title2' => 'required',
            'url' => 'required',
            'list_order' => 'required',
        ]);

        if(isset($request->id) && !empty($request->id)){
            $exists = FooterMenuBottom::where('id', '!=', $request->id)->where('url',$request->url)->count();
            if($exists) {
              return ["error" => trans('lang.url_taken')];
            }
             $data = FooterMenuBottom::where('id', $request->id)->update([
                //'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
                'url' => $request->url,
                'list_order' => $request->list_order,

            ]);
            FooterMenuBottomText::withoutGlobalScopes()->where('footer_bottom_id', $request->id)->delete();
            $data = FooterMenuBottomText::insert([
                ['footer_bottom_id' => $request->id, 'title' => $request->title1, 'lang' => 1],
                ['footer_bottom_id' => $request->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.footer.menu.bottom')];
        }else{
             $exists = FooterMenuBottom::where('id', '!=', $request->id)->where('url',$request->url)->count();
            if($exists) {
              return ["error" => trans('lang.url_taken')];
            }
            $data = new FooterMenuBottom();
            //$data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->url = $request->url;
            $data->list_order = $request->list_order;            
            $data->user_id = Auth::user()->id;
            $data->save();
            $text = FooterMenuBottomText::insert([
                ['footer_bottom_id' => $data->id, 'title' => $request->title1, 'lang' => 1],
                ['footer_bottom_id' => $data->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.footer.menu.bottom')];
        }
    }

    public function footer_menu_bottom_edit(Request $request){
        $data = FooterMenuBottom::with('footer_menu_bottom_text_all')->where('id', $request->id)->first();
        return view('admin/menu/footer_menu_bottom/edit')->with(['title' => trans('lang.edit'), 'data' => $data]);   
    }

    public function footer_menu_bottom_delete(Request $request){
      $id = $request->id;
      FooterMenuBottom::where('id', $id)->delete();
      //FooterSubMenu::where('footer_id', $id)->delete();
      FooterMenuBottomText::withoutGlobalScopes()->where('footer_bottom_id', $id)->delete();

      return 'success';
    }

    public function sidebar_menu(){   
        $data = SidebarMenu::with('sidebar_menu_text')->orderBy('id', 'desc')->paginate(25);
        return view('admin/menu/sidebar_menu/manage')->with(['title' => trans('lang.sidebar_menu'), 'data' => $data]); 
    }
    public function sidebar_menu_add(Request $request){
        return view('admin/menu/sidebar_menu/edit')->with(['title' => trans('lang.create'),'add' => 1]);   
    }

    public function sidebar_menu_post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
            'permalink' => 'required',
            'list_order' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
            $exists = SidebarMenu::where('id', '!=', $request->id)->where('permalink',$request->permalink)->count();
            if($exists) {
              return ["error" => trans('lang.permalink_taken')];
            }
             $data = SidebarMenu::where('id', $request->id)->update([
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'permalink' => $request->permalink,
                'list_order' => $request->list_order,
                'user_id' => Auth::user()->id,
            ]);
            SidebarMenuText::withoutGlobalScopes()->where('sidebar_id', $request->id)->delete();
            $data = SidebarMenuText::insert([
                ['sidebar_id' => $request->id, 'title' => $request->title1, 'lang' => 1],
                ['sidebar_id' => $request->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.sidebar.menu')];
        }else{
            $exists = SidebarMenu::where('permalink',$request->permalink)->count();
            if($exists) {
              return ["error" => trans('lang.permalink_taken')];
            }
            $data = new SidebarMenu();
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->permalink = $request->permalink;
            $data->list_order = $request->list_order;
            $data->save();
            $text = SidebarMenuText::insert([
                ['sidebar_id' => $data->id, 'title' => $request->title1, 'lang' => 1],
                ['sidebar_id' => $data->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.sidebar.menu')];
        }
    }

    public function sidebar_menu_edit(Request $request){
        $data = SidebarMenu::with('sidebar_menu_text_all')->where('id', $request->id)->first();
        return view('admin/menu/sidebar_menu/edit')->with(['title' => trans('lang.edit'), 'data' => $data,'add' => 0]);   
    }

    public function sidebar_menu_delete(Request $request){
      $id = $request->id;
      SidebarMenu::where('id', $id)->delete();
      SidebarSubMenu::where('sidebar_id', $id)->delete();
      SidebarMenuText::withoutGlobalScopes()->where('sidebar_menu', $id)->delete();
      return 'success';
    }


    public function sidebar_sub_menu($id){   
        $data = SidebarSubMenu::with(['sidebar_menu', 'sidebar_sub_menu_text'])->where('sidebar_id', $id)->orderBy('id', 'desc')->paginate(25);
            return view('admin/menu/sidebar_menu/sub_sidebar/manage')->with(['title' => trans('lang.sidebar_sub_menu'), 'data' => $data, 'sidebar_id' => $id, ]); 
    }
    public function sidebar_sub_menu_add(Request $request){
        $sidebar_menu = SidebarMenu::with('sidebar_menu_text')->get();
        return view('admin/menu/sidebar_menu/sub_sidebar/edit')->with(['title' => trans('lang.create'), 'sidebar_menu' => $sidebar_menu, 'sidebar_id' => $request->id,'add' => 1]);   
    }

    public function sidebar_sub_menu_post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
            'sidebar_id' => 'required',
            'permalink' => 'required',
            'list_order' => 'required',

        ]);
        if(isset($request->id) && !empty($request->id)){
            $exists = SidebarSubMenu::where('id', '!=', $request->id)->where('permalink',$request->permalink)->count();
            if($exists) {
              return ["error" => trans('lang.permalink_taken')];
            }
             $data = SidebarSubMenu::where('id', $request->id)->update([
                //'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
                'sidebar_id' => $request->sidebar_id,
                'permalink' => $request->permalink,
                'list_order' => $request->list_order,

            ]);
            SidebarSubMenuText::withoutGlobalScopes()->where('sidebar_sub_menu_id', $request->id)->delete();
            $data = SidebarSubMenuText::insert([
                ['sidebar_sub_menu_id' => $request->id, 'sidebar_id' => $request->sidebar_id, 'title' => $request->title1, 'lang' => 1],
                ['sidebar_sub_menu_id' => $request->id, 'sidebar_id' => $request->sidebar_id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.sidebar.sub.menu',$request->sidebar_id )];
        }else{
            $exists = SidebarSubMenu::where('permalink',$request->permalink)->count();
            if($exists) {
              return ["error" => trans('lang.permalink_taken')];
            }
            $data = new SidebarSubMenu();
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->permalink = $request->permalink;
            $data->user_id = Auth::user()->id;
            $data->sidebar_id = $request->sidebar_id;
            $data->list_order = $request->list_order;

            $data->save();
            $text = SidebarSubMenuText::insert([
                ['sidebar_sub_menu_id' => $data->id, 'sidebar_id' => $request->sidebar_id, 'title' => $request->title1, 'lang' => 1],
                ['sidebar_sub_menu_id' => $data->id, 'sidebar_id' => $request->sidebar_id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.sidebar.sub.menu', $request->sidebar_id)];
        }
    }

    public function sidebar_sub_menu_edit(Request $request){
        $data = SidebarSubMenu::with(['sidebar_menu', 'sidebar_sub_menu_text_all'])->where('id', $request->id)->first();
        $sidebar_menu = SidebarMenu::with('sidebar_menu_text')->get();
        return view('admin/menu/sidebar_menu/sub_sidebar/edit')->with(['title' => trans('lang.edit'), 'data' => $data, 'sidebar_menu' => $sidebar_menu, 'sidebar_id'=> $request->sidebar_id,'add' => 0]);   
    }

    public function sidebar_sub_menu_delete(Request $request){
      $id = $request->id;
      SidebarSubMenu::where('id', $id)->delete();
      SidebarSubMenuText::withoutGlobalScopes()->where('sidebar_sub_menu_id', $id)->delete();
      return 'success';
    }


    public function section_menu(){   
        $data = SectionMenu::with('section_menu_text')->orderBy('id', 'desc')->paginate(25);
        return view('admin/menu/section_menu/manage')->with(['title' => trans('lang.section_menu'), 'data' => $data]); 
    }
    public function section_menu_add(Request $request){
        return view('admin/menu/section_menu/edit')->with(['title' => trans('lang.create')]);   
    }

    public function section_menu_post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
            'section_no' => 'required',
            'fileName' => 'required',
            'permalink' => 'required',
        ]);
        $file = explode("~~",$request->fileName);
        if(!$file[0]){
            return ["error" => trans('lang.image_required')];
        }
        if(isset($request->id) && !empty($request->id)){
            $exists = SectionMenu::where('id', '!=', $request->id)->where('permalink',$request->permalink)->count();
            if($exists) {
              return ["error" => trans('lang.permalink_taken')];
            }
             $data = SectionMenu::where('id', $request->id)->update([
                'section_no' => $request->section_no,
                'image' => @$file[0],
                'permalink' => $request->permalink,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'other' => (isset($request->other) && $request->other == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
            ]);
            SectionMenuText::withoutGlobalScopes()->where('section_id', $request->id)->delete();
            $data = SectionMenuText::insert([
                ['section_id' => $request->id, 'title' => $request->title1, 'lang' => 1],
                ['section_id' => $request->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.section.menu')];
        }else{
            $exists = SectionMenu::where('permalink',$request->permalink)->count();
            if($exists) {
              return ["error" => trans('lang.permalink_taken')];
            }
            $data = new SectionMenu();
            $data->image = $file[0];
            $data->section_no = $request->section_no;
            $data->permalink = $request->permalink;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->other = (isset($request->other) && $request->other == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->save();
            $text = SectionMenuText::insert([
                ['section_id' => $data->id, 'title' => $request->title1, 'lang' => 1],
                ['section_id' => $data->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.section.menu')];
        }
    }

    public function section_menu_edit(Request $request){
        $data = SectionMenu::with('section_menu_text_all')->where('id', $request->id)->first();
        return view('admin/menu/section_menu/edit')->with(['title' => trans('lang.edit'), 'data' => $data]);   
    }

    public function section_menu_delete(Request $request){
      $id = $request->id;
      SectionMenu::where('id', $id)->delete();
      SectionMenuText::withoutGlobalScopes()->where('section_id', $id)->delete();

      return 'success';
    }

    public function sectionImageUpload(Request $request){
      $file = $request->file('file');
      $target_dir = env('SECTION_IMAGES');
      $target_file = $target_dir . basename($_FILES["file"]["name"]);
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
      $filename  = basename($_FILES['file']['name']);
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      $new       = $filename;
      move_uploaded_file($_FILES["file"]["tmp_name"], storage_path('section')."/{$new}");
      return $new;
    }

    public function removeImage(Request $request){
      $file = $request->file;
      $delete = File::delete(storage_path('section/'.$file));
      SectionMenu::where('image', $file)->update([
                'image' => null,
            ]);
      return 'true';
    }


}
