<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use PDF;
use App\SettingEmail;
use App\Shipping;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderNotification;
use Carbon;

class OrderController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */

  public function index(Request $request)
  {
    $from = "";
    $to   = "";
    if (isset($request->from) && isset($request->to)) {
      $from = $request->from;
      $to   = $request->to;
      $this_to = date("Y-m-d", strtotime("$to +1 day"));
      $data = Order::with(['orderDetail'])->whereBetween('created_at', [date("Y-m-d", strtotime($from)), $this_to])->orderBy('id', 'desc')->paginate(25);
    } else {
      $data = Order::with(['orderDetail'])->orderBy('id', 'desc')->paginate(25);
    }
    return View('admin.order.manage')->with(['title' => trans('lang.manage'), 'data' => $data, 'from' => $from, 'to' => $to]);
  }

  public function details(Request $request)
  {
    $order = Order::with(['coupon'])->where('id', $request->id)->first();
    $data = OrderDetail::with(['order', 'orderProduct.product_text','orderProduct.productvariant'])->orderBy('id', 'desc')->where('order_id', $request->id)->get();
    $shipping_data = Shipping::with('shipping_text_all')->where('id', 1)->first();
    return View('admin.order.orderDetail')->with(['title' => trans('lang.order_details'), 'data' => $data,'shipping_data' =>$shipping_data,'order'=>$order]);
  }


  public function invoice(Request $request)
  {
    $order = OrderDetail::with('order')->orderBy('id', 'desc')->with('orderProduct')->where('order_id', $request->id)->get();
    $pdf = PDF::loadview('admin.invoice', ['order' => $order])->setPaper('a4', 'portrait');
    return $pdf->stream('admin/invoice.pdf');
  }

  public function shipping(Request $request)
  {
    $order = Order::where('id', $request->id)->update([
      'shipping_charges' => $request->shipping_charges
    ]);
    return ["success" => trans('lang.successfully_added'), "reload" => 'yes'];
  }

  public function delete(Request $request)
  {
    $id = $request->id;
    $model = Order::find($id);
    $model->delete();
    return 'success';
  }

  public function report(Request $request)
  {
    $from = "";
    $to   = "";
    if (isset($request->from) && isset($request->to)) {
      $from = $request->from;
      $to   = $request->to;
      $this_to = date("Y-m-d", strtotime("$to +1 day"));
      $data = Order::with(['orderDetail'])->whereBetween('created_at', [date("Y-m-d", strtotime($from)), $this_to])->orderBy('id', 'desc')->paginate(25);
    } else {
      $data = Order::with(['orderDetail'])->orderBy('id', 'desc')->paginate(25);
    }
    $pdf = PDF::loadview('admin.order.report', ['data' => $data, 'from' => $from, 'to' => $to])->setPaper('a4', 'portrait');
    return $pdf->stream('admin/order.pdf');
  }
  public function statusChange(Request $request)
  {

    if (isset($request->id) && !empty($request->id)) {
      $data = Order::where('id', $request->id)->update([
        'status' => $request->status,
      ]);

      $order = Order::with(['orderDetail', 'orderDetail.orderProduct.product_text','orderDetail.orderProduct.productvariant'])->where('id', $request->id)->first();
      $setting_email = SettingEmail::get();
      foreach ($setting_email as $recipient) {
          Mail::to($recipient->email)->send(new OrderNotification($order));
      }
      Mail::to($order->billing_email)->send(new OrderNotification($order));
    }

    return redirect('/admin/order/');
  }
}
