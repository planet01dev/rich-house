<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use App\Subscribe;
use App\SubscribeSetting;
use App\SubscribeSettingText;
use Redirect;
class SubscribeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $data = Subscribe::orderBy('id','desc')->paginate(25);;
        return View('admin.subscribe.manage')->with(['title' => trans('lang.subscribe'), 'data' => $data]);      

    }

    public function edit_subscribe(Request $request)
    {
        $data = Subscribe::where('id', $request->id)->first();
        return view('admin.subscribe.edit_subscribe')->with('title', trans('lang.edit'))->with(['data' => $data,'edit' => 1]);
    }

    public function post_subscribe(Request $request){
        $request->validate([
            'email' => 'required',
            
        ]);
        if(Subscribe::where('email', $request->email)->where('id', '!=', $request->id)->count() > 0){
            return ["error" => "Email alredy in used."];
        }
        if(isset($request->id) && !empty($request->id)){
            $data = Subscribe::where('id', $request->id)->update([
                
                'email' => $request->email,
                
            ]);
            
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.subscribe.manage')];
        }else{
           
        }
    }


    public function edit(){
        $data = SubscribeSetting::with('subscribe_setting_text_all')->where('id', 1)->first();
        return view('admin/subscribe/edit')->with(['title' => trans('lang.edit'), 'data' => $data]);   
    }

    public function post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
            'description1' => 'required',
            'description2' => 'required',

        ]);
        $file = explode("~~",$request->fileName);
        if(!$file[0]){
            return ["error" => trans('lang.image_required')];
        }
        if(isset($request->id) && !empty($request->id)){
            $data = SubscribeSetting::where('id', $request->id)->update([
                'image' => @$file[0],
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
            ]);
            SubscribeSettingText::withoutGlobalScopes()->where('subscribe_setting_id', $request->id)->delete();
           $text = SubscribeSettingText::insert([
                ['subscribe_setting_id' => $request->id, 'title' => $request->title1, 'description' => $request->description1, 'lang' => 1],
                ['subscribe_setting_id' => $request->id, 'title' => $request->title2, 'description' => $request->description2,'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.subscribe.edit')];
        }else{
            $data = new SubscribeSetting();
            $data->image = $file[0];
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->save();
            $text = SubscribeSettingText::insert([
                ['subscribe_setting_id' => $data->id, 'title' => $request->title1, 'description' => $request->description1, 'lang' => 1],
                ['subscribe_setting_id' => $data->id, 'title' => $request->title2, 'description' => $request->description2,'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.subscribe.edit')];
        }
    }

    public function imageUpload(Request $request){
      $file = $request->file('file');
      $target_dir = env('SECTION_IMAGES');
      $target_file = $target_dir . basename($_FILES["file"]["name"]);
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
      $filename  = basename($_FILES['file']['name']);
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      $new       = $filename;
      move_uploaded_file($_FILES["file"]["tmp_name"], storage_path('section')."/{$new}");
      return $new;
    }

    public function removeImage(Request $request){
      $file = $request->file;
      $delete = File::delete(storage_path('section/'.$file));
      SectionMenu::where('image', $file)->update([
                'image' => null,
            ]);
      return 'true';
    }

    public function delete(Request $request)
    {
        $product_id = $request->id;
        Subscribe::where('id', $product_id)->delete();
        return 'success';
    }
}
