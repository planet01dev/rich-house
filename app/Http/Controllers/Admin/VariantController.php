<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\Variant;
use App\VariantText;
class VariantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* public function index()
    {   
       
        return view('admin/dashboard');
        
    }*/

    public function index(){   
        $data = Variant::with('variant_text')->orderBy('id', 'desc')->paginate(25);
        return view('admin/variant/manage')->with(['title' => trans('lang.variant'), 'data' => $data]); 
    }
    public function add(Request $request){
        return view('admin/variant/edit')->with(['title' => trans('lang.create')]);   
    }

    public function post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = Variant::where('id', $request->id)->update([
                //'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
            ]);
            VariantText::withoutGlobalScopes()->where('variant_id', $request->id)->delete();
            $data = VariantText::insert([
                ['variant_id' => $request->id, 'title' => $request->title1, 'lang' => 1],
                ['variant_id' => $request->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.variant.manage')];
        }else{
            $data = new Variant();
            //$data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->save();
            $text = VariantText::insert([
                ['variant_id' => $data->id, 'title' => $request->title1, 'lang' => 1],
                ['variant_id' => $data->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.variant.manage')];
        }
    }

    public function edit(Request $request){
        $data = Variant::with('variant_text_all')->where('id', $request->id)->first();
        return view('admin/variant/edit')->with(['title' => trans('lang.edit'), 'data' => $data]);   
    }

    public function delete(Request $request){
      $id = $request->id;
      Variant::where('id', $id)->delete();
      VariantText::withoutGlobalScopes()->where('variant_id', $id)->delete();
     
      return 'success';
    }
  }