<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\Brand;
use App\BrandText;
class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* public function index()
    {   
       
        return view('admin/dashboard');
        
    }*/

    public function index(){   
        $data = Brand::with('brand_text')->orderBy('id', 'desc')->paginate(25);
        return view('admin/brand/manage')->with(['title' => trans('lang.brand'), 'data' => $data]); 
    }
    public function add(Request $request){
        return view('admin/brand/edit')->with(['title' => trans('lang.create')]);   
    }

    public function post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = Brand::where('id', $request->id)->update([
                //'title' => $request->title,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
            ]);
            BrandText::withoutGlobalScopes()->where('brand_id', $request->id)->delete();
            $data = BrandText::insert([
                ['brand_id' => $request->id, 'title' => $request->title1, 'lang' => 1],
                ['brand_id' => $request->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.brand.manage')];
        }else{
            $data = new Brand();
            //$data->title = $request->title;
            $data->is_active = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->user_id = Auth::user()->id;
            $data->save();
            $text = BrandText::insert([
                ['brand_id' => $data->id, 'title' => $request->title1, 'lang' => 1],
                ['brand_id' => $data->id, 'title' => $request->title2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.brand.manage')];
        }
    }

    public function edit(Request $request){
        $data = Brand::with('brand_text_all')->where('id', $request->id)->first();
        return view('admin/brand/edit')->with(['title' => trans('lang.edit'), 'data' => $data]);   
    }

    public function delete(Request $request){
      $id = $request->id;
      Brand::where('id', $id)->delete();
      BrandText::withoutGlobalScopes()->where('brand_id', $id)->delete();
     
      return 'success';
    }
  }