<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\Page;
use App\PageText;
use App;
class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
       
        $data = Page::with('page_text')->orderBy('id', 'desc')->paginate(25);
        return view('admin/page/manage')->with(['title' => trans('lang.pages'), 'data' => $data]); 
        
    }

    public function post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = Page::where('id', $request->id)->update([
                'page_name' => $request->page_name,
            ]);
            PageText::withoutGlobalScopes()->where('page_id', $request->id)->delete();
            $data = PageText::insert([
                ['page_id' => $request->id, 'title' => $request->title1,'content' => $request->content1, 'lang' => 1],
                ['page_id' => $request->id, 'title' => $request->title2,'content' => $request->content2, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.page.manage')];
        }else{
            // $data = new Setting();
            // $data->email = $request->email;
            // $data->website = $request->website;
            // $data->phone = $request->phone;
            // $data->website = $request->website;
            // $data->whatsapp = $request->whatsapp;
            // $data->facebook = $request->facebook;
            // $data->instagram = $request->instagram;
            // $data->pinterest = $request->pinterest;
            // $data->twitter = $request->twitter;
            // $data->youtube = $request->youtube;
            // $data->linkedin = $request->linkedin;
            // $data->tax      = $request->tax;
            // $data->ticker_1      = $request->ticker_1;
            // $data->ticker_2      = $request->ticker_2;
            // $data->ticker_3      = $request->ticker_3;
            // $data->ticker_4      = $request->ticker_4;
            // $data->shipping_method = $request->shipping_method;
            // $data->shipping_rate = $request->shipping_rate;
            // $data->address = $request->address;
            // $data->is_cod  = (isset($request->is_cod) ? $request->is_cod : 0);
            // $data->save();
            // $text = SettingText::insert([
            //     ['setting_id' => $data->id, 'title' => $request->title1, 'lang' => 1],
            //     ['setting_id' => $data->id, 'title' => $request->title2, 'lang' => 2],
            // ]);
            // return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.setting.edit')];
        }
    }

    public function edit(Request $request){
        $data = Page::with('page_text_all')->where('id', $request->id)->first();
        return view('admin/page/edit')->with(['title' => trans('lang.pages'), 'data' => $data]);   
    }

    


  
  }