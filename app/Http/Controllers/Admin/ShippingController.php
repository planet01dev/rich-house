<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\Setting;
use App\SettingText;
use App\Shipping;
use App\ShippingText;
use App\SettingEmail;
use App;
class ShippingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* public function index()
    {   
       
        return view('admin/dashboard');
        
    }*/

    public function post(Request $request){
        $request->validate([
            'method' => 'required',
            'country1' => 'required',
            'country2' => 'required'
        ]);
        if(isset($request->id) && !empty($request->id)){
             $data = Shipping::where('id', $request->id)->update([
                'method' => $request->method,
            ]);
            ShippingText::withoutGlobalScopes()->where('shipping_id', $request->id)->delete();

            $country = $request->country1;
            for($k = 0; $k< sizeof($country); $k++)
            {
                $cod = (isset($request->cod[$k]) && $request->cod[$k] == 'on' )? 1 : 0;
                $data = ShippingText::insert([
                    ['shipping_id' => $request->id, 'country' => $request->country1[$k], 'lang' => 1,'rate' => $request->rate[$k],'cod' => $cod],
                    ['shipping_id' => $request->id, 'country' => $request->country2[$k], 'lang' => 2,'rate' => $request->rate[$k],'cod'=> $cod],
                ]);
            }
            
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.shipping.edit')];
        }else{
            
        }
    }

    public function edit(){
        $data = Shipping::with('shipping_text_all')->where('id', 1)->first();
        return view('admin/shipping/edit')->with(['title' => trans('lang.shipping'), 'data' => $data]);   
    }


  
  }