<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\Setting;
use App\SettingText;
use App\SettingEmail;
use App;
use Illuminate\Support\Facades\File;
class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   /* public function index()
    {   
       
        return view('admin/dashboard');
        
    }*/

    public function post(Request $request){
        $request->validate([
            'title1' => 'required',
            'title2' => 'required',
        ]);

        $file = explode("~~",$request->fileName);
        if(!$file[3]){
            return ["error" => '4 '.trans('lang.image_required')];
        }
        if(count($file)>4)
        {
            if(@$file[5] != '')
            {
                return ["error" => '4 image allowed only'];   
            }
        }
        if(isset($request->id) && !empty($request->id)){
             $data = Setting::where('id', $request->id)->update([
                'email' => $request->email,
                'website' => $request->website,
                'phone' => $request->phone,
                'whatsapp' => $request->whatsapp,
                'facebook' => $request->facebook,
                'instagram' => $request->instagram,
                'pinterest' => $request->pinterest,
                'twitter' => $request->twitter,
                'youtube' => $request->youtube,
                'linkedin' => $request->linkedin,
                'tax' => $request->tax,
                'ticker_1' => $request->ticker_1,
                'ticker_2' => $request->ticker_2,
                'ticker_3' => $request->ticker_3,
                'ticker_4' => $request->ticker_4,
                'section_1_link' => $request->section_1_link,
                'section_2_link' => $request->section_2_link,
                'section_3_link' => $request->section_3_link,
                'about_image' => @$file[0],
                'section_1_image' => @$file[1],
                'section_2_image' => @$file[2],
                'section_3_image' => @$file[3],
                'timing' => $request->timing,
                'is_cod' => (isset($request->is_cod) ? $request->is_cod : 0),
                'address' => $request->address,
                // 'product_label' => $request->product_label,
            ]);
            SettingText::withoutGlobalScopes()->where('setting_id', $request->id)->delete();
            $data = SettingText::insert([
                ['setting_id' => $request->id, 'title' => $request->title1, 'below_section_heading' => $request->below_section_heading1, 'below_section_text' => $request->below_section_text1, 'below_section_button' => $request->below_section_button1, 'section_1_text'=>$request->section_1_text1,'section_2_text'=>$request->section_2_text1,'section_3_text'=>$request->section_3_text1, 'lang' => 1],
                ['setting_id' => $request->id, 'title' => $request->title2, 'below_section_heading' => $request->below_section_heading2, 'below_section_text' => $request->below_section_text2, 'below_section_button' => $request->below_section_button2, 'section_1_text'=>$request->section_1_text2,'section_2_text'=>$request->section_2_text2,'section_3_text'=>$request->section_3_text1, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.setting.edit')];
        }else{
            $data = new Setting();
            $data->email = $request->email;
            $data->website = $request->website;
            $data->phone = $request->phone;
            $data->whatsapp = $request->whatsapp;
            $data->facebook = $request->facebook;
            $data->instagram = $request->instagram;
            $data->pinterest = $request->pinterest;
            $data->twitter = $request->twitter;
            $data->youtube = $request->youtube;
            $data->linkedin = $request->linkedin;
            $data->tax      = $request->tax;
            $data->ticker_1      = $request->ticker_1;
            $data->ticker_2      = $request->ticker_2;
            $data->ticker_3      = $request->ticker_3;
            $data->ticker_4      = $request->ticker_4;
            $data->section_1_link      = $request->section_1_link;
            $data->section_2_link      = $request->section_2_link;
            $data->section_3_link      = $request->section_3_link;
            $data->address = $request->address;
            $data->is_cod  = (isset($request->is_cod) ? $request->is_cod : 0);
            $data->save();
            $text = SettingText::insert([
                ['setting_id' => $data->id, 'title' => $request->title1, 'below_section_heading' => $request->below_section_heading1, 'below_section_text' => $request->below_section_text1, 'below_section_button' => $request->below_section_button1, 'section_1_text'=>$request->section_1_text1,'section_2_text'=>$request->section_2_text1,'section_3_text'=>$request->section_3_text1, 'lang' => 1],
                ['setting_id' => $data->id, 'title' => $request->title2, 'below_section_heading' => $request->below_section_heading2, 'below_section_text' => $request->below_section_text2, 'below_section_button' => $request->below_section_button2, 'section_1_text'=>$request->section_1_text2,'section_2_text'=>$request->section_2_text2,'section_3_text'=>$request->section_3_text1, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.setting.edit')];
        }
    }

    public function edit(){
        $data = Setting::with('setting_text_all')->where('id', 1)->first();
        return view('admin/setting/edit')->with(['title' => trans('lang.setting'), 'data' => $data]);   
    }

    public function edit_contact(){
        $data = Setting::with('setting_text_all')->where('id', 1)->first();
        return view('admin/setting/contact_us')->with(['title' => trans('lang.setting'), 'data' => $data]);   
    }
    public function post_contact(Request $request){
        $request->validate([
            'phone' => 'required',
        ]);
        $file = explode("~~",$request->fileName);

        if(!$file[0]){
            return ["error" => trans('lang.image_required')];
        }
        if(isset($request->id) && !empty($request->id)){
             $data = Setting::where('id', $request->id)->update([
                
                'phone' => $request->phone,
                'whatsapp' => $request->whatsapp,
                'contact_image' => @$file[0],
                'timing' => $request->timing,
            ]);
            SettingText::withoutGlobalScopes()->where('setting_id', $request->id)->delete();
            $data = SettingText::insert([
                ['setting_id' => $request->id, 'title' => $request->title1, 'below_section_heading' => $request->below_section_heading1, 'below_section_text' => $request->below_section_text1, 'below_section_button' => $request->below_section_button1, 'section_1_text'=>$request->section_1_text1,'section_2_text'=>$request->section_2_text1,'section_3_text'=>$request->section_3_text1, 'lang' => 1],
                ['setting_id' => $request->id, 'title' => $request->title2, 'below_section_heading' => $request->below_section_heading2, 'below_section_text' => $request->below_section_text2, 'below_section_button' => $request->below_section_button2, 'section_1_text'=>$request->section_1_text2,'section_2_text'=>$request->section_2_text2,'section_3_text'=>$request->section_3_text1, 'lang' => 2],
            ]);
            return ["success" => trans('lang.successfully_updated'), "redirect" => route('admin.setting.edit_contact')];
        }else{
            
        }
    }

    public function edit_email(){
        $data = SettingEmail::get();
        return view('admin/setting/email')->with(['title' => trans('lang.email'), 'data' => $data]);   
    }

     public function post_email(Request $request){
        $request->validate([
            'email' => 'required|array',
        ]);
        SettingEmail::where('id', '>', '0')->delete();
        $email = $request->email;
        for($i= 0; $i<sizeof($email); $i++){
            $data = new SettingEmail();
            $data->email = $email[$i];
            $data->save();
        }    
        return ["success" => trans('lang.successfully_added'), "redirect" => route('admin.setting.edit.email')];
    }

    public function ImageUpload(Request $request)
  {
    $file = $request->file('file');
      $target_dir = env('CONTACT_IMAGES');
      $target_file = $target_dir . basename($_FILES["file"]["name"]);
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
      $filename  = basename($_FILES['file']['name']);
      $extension = pathinfo($filename, PATHINFO_EXTENSION);
      $new       = $filename;
      move_uploaded_file($_FILES["file"]["tmp_name"], storage_path('contact')."/{$new}");
      return $new;
  }

  public function removeImage(Request $request)
  {
    $file = $request->file;
    $delete = File::delete(storage_path('contact/' . $file));
    return 'true';
  }


  
  }