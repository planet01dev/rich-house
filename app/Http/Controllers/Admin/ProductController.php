<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Http\Request;
use App\Product;
use App\ProductText;
use App\ProductVariant;
use App\ProductVariantText;
use App\Price;
use App\Category;
use App\SidebarMenu;
use App\SidebarSubMenu;
use App\Image;
use App\Variant;
use App\SectionMenu;
use App\Tag;
use App\Brand;
use App\WeightType;
use Illuminate\Support\Facades\File;
//use App\Review;



class ProductController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $data = Product::with(['images','weight', 'product_text','productvariant.product_variant_text'])->orderBy('id', 'desc')->paginate(15);
    // dd($data);
    return view('admin/product/manage')->with(['title' => trans('lang.product'), 'data' => $data]);
  }
  public function add()
  {

    $sidebar_menu           = SidebarMenu::with('sidebar_sub_menu')->get();
    $weight_type            = WeightType::get();
    $variant                = Variant::with('variant_text')->get();
    $tag                    = Tag::get();
    $brand                  = Brand::with('Brand_text')->get();
    $section                = SectionMenu::with('section_menu_text')->get();
    // dd($variant);
    return view('admin.product.edit')->with('title', trans('lang.create'))->with(['section' => $section, 'brand' => $brand, 'sidebar_menu' => $sidebar_menu, 'weight_type' => $weight_type, 'variant' => $variant, 'tag' => $tag, 'add' => 1]);
  }

  public function post(Request $request)
  {


    if (isset($request->id) && !empty($request->id)) {
      //Update
      $request->validate([
        'title1' => 'required',
        'title2' => 'required',
      ]);
      if (Product::where('permalink', $request->permalink)->where('id', '!=', $request->id)->count() > 0) {
        return ["error" => "Permalink is alredy in used."];
      }
      // dd($request);
      $data = Product::where('id', $request->id)->update([
        'brand_id' => $request->brand,
        'permalink' => $request->permalink,
        'weight_value' => $request->weight_value,
        'weight_type_id' => $request->weight_type_id,
        'discount_type' => $request->discount_type,
        'discount' => ($request->discount > 0) ? $request->discount : 0,
        'best_seller' => (isset($request->best_seller) && $request->best_seller == 'on' )? 1 : 0,
        'exclusive' => (isset($request->exclusive) && $request->exclusive == 'on' )? 1 : 0,
        'new_in' => (isset($request->new_in) && $request->new_in == 'on' )? 1 : 0,
      ]);
      ProductText::withoutGlobalScopes()->where('product_id', $request->id)->delete();
      $text = ProductText::insert([
        ['product_id' => $request->id, 'title' => $request->title1, 'short_description' => $request->short_description1, 'brief_description' => $request->brief_description1, 'ingredients' => $request->ingredients1, 'notes' => $request->notes1, 'lang' => 1],
        ['product_id' => $request->id, 'title' => $request->title2, 'short_description' => $request->short_description2, 'brief_description' => $request->brief_description2, 'ingredients' => $request->ingredients2, 'notes' => $request->notes2, 'lang' => 2],

      ]);

      Price::withoutGlobalScopes()->where('product_id', $request->id)->delete();
      $product_price                       = new Price;
      $product_price->price                = $request->price;
      $product_price->product_id           = $request->id;
      $product_price->save();

      Category::where('product_id', $request->id)->delete();

      foreach (@$request->sidebar_menu as $k => $v) {
        $menu = explode('_', $request->sidebar_menu[$k]);
        $productTag = new Category();
        $productTag->product_id = $request->id;
        $productTag->sidebar_id = $menu[0];
        if (isset($menu[1]) && $menu[1] != '' && $menu[1] != NULL) {
          $productTag->sidebar_child_id = $menu[1];
        }
        $productTag->save();
      }

      foreach (@$request->section as $k => $v) {
        $productTag = new Category();
        $productTag->product_id = $request->id;
        $productTag->section_id = $request->section[$k];
        $productTag->save();
      }
      if (isset($request->tag) && $request->tag!= null) {
          foreach (@$request->tag as $k => $v) {
              $productTag = new Category();
              $productTag->product_id = $request->id;
              $productTag->tag_id = $request->tag[$k];
              $productTag->save();
          }
      }

      //Variant
      ProductVariant::withoutGlobalScopes()->where('product_id', $request->id)->delete();
      for ($j = 1; $j <= 1; $j++) {

        if (isset($request->{"variant_$j"}) && $request->{"variant_$j"} != '0' && isset($request->{"value_$j"}) || isset($request->{"value_ar_$j"})) {
          $value = explode(",", $request->{"value_$j"});
          $value_ar = explode(",", $request->{"value_ar_$j"});
          $value_prc = explode(",", $request->variant_price);

          for ($i = 0; $i < count($value); $i++) {
            $ProductVariant = new ProductVariant();
            $ProductVariant->product_id = $request->id;
            $ProductVariant->variant_id = $request->{"variant_$j"};
            $ProductVariant->save();

            ProductVariantText::insert([
              ['product_variant_id' => $ProductVariant->id, 'title' => $value[$i], 'lang' => 1],
              ['product_variant_id' => $ProductVariant->id, 'title' => $value_ar[$i], 'lang' => 2],
            ]);

            $product_price                       = new Price;
            $product_price->price                = $value_prc[$i];
            $product_price->product_id           = $request->id;
            $product_price->variant_id           = $ProductVariant->id;
            $product_price->save();

          }
        }
      }

      $today_stamp                 = date("Y-m-d H:i:s");
      $file                        = explode("~~", $request->fileName);
      
      for ($i = 0; $i < count($file) - 1; $i++) {
        $upload = Image::insert(['image' => $file[$i], 'product_id' => $request->id, 'created_at' => $today_stamp, 'updated_at' => $today_stamp]);
      }

      return ["success" => "Successfully Updated.", "redirect" => route('admin.product.manage')];
    } else {
      //Insert
      $request->validate([

        'permalink' => 'required|unique:products|max:255',
        'fileName' => 'required',
        'title1' => 'required',
        'title2' => 'required',
      ]);
      // dd($request);
      $today_stamp                 = date("Y-m-d H:i:s");
      $file                        = explode("~~", $request->fileName);


      $product                     = new Product;
      $product->brand_id          = $request->brand;
      $product->permalink          = $request->permalink;
      $product->weight_value             = $request->weight_value;
      $product->weight_type_id        = $request->weight_type_id;
      $product->discount_type           = $request->discount_type;
      $product->discount           = ($request->discount > 0) ? $request->discount : 0;
      $product->variant_has_price = $request->variant_has_price;
      $product->best_seller = (isset($request->best_seller) && $request->best_seller == 'on' )? 1 : 0;
      $product->exclusive = (isset($request->exclusive) && $request->exclusive == 'on' )? 1 : 0;
      $product->new_in = (isset($request->new_in) && $request->new_in == 'on' )? 1 : 0;
      
      $product->save();

      $text = ProductText::insert([
        ['product_id' => $product->id, 'title' => $request->title1, 'short_description' => $request->short_description1, 'brief_description' => $request->brief_description1, 'ingredients' => $request->ingredients1, 'notes' => $request->notes1, 'lang' => 1],
        ['product_id' => $product->id, 'title' => $request->title2, 'short_description' => $request->short_description2, 'brief_description' => $request->brief_description2, 'ingredients' => $request->ingredients2, 'notes' => $request->notes2, 'lang' => 2],

      ]);

      $product_id = $product->id;

      $product_price                       = new Price;
      $product_price->price                = $request->price;
      $product_price->product_id           = $product_id;
      $product_price->save();

      foreach (@$request->sidebar_menu as $k => $v) {
        $menu = explode('_', $request->sidebar_menu[$k]);
        $productTag = new Category();
        $productTag->product_id = $product->id;
        $productTag->sidebar_id = $menu[0];
        if (isset($menu[1]) && $menu[1] != '' && $menu[1] != NULL) {
          $productTag->sidebar_child_id = $menu[1];
        }
        $productTag->save();
      }

      foreach (@$request->section as $k => $v) {
        $productTag = new Category();
        $productTag->product_id = $product->id;
        $productTag->section_id = $request->section[$k];
        $productTag->save();
      }

      if(isset($request->tag) && $request->tag!= null)
      {
        foreach (@$request->tag as $k => $v) {
          $productTag = new Category();
          $productTag->product_id = $product->id;
          $productTag->tag_id = $request->tag[$k];
          $productTag->save();
        }
      }
      
      
      //Variant
      for ($j = 1; $j <= 1; $j++) {

        if (isset($request->{"variant_$j"}) && $request->{"variant_$j"} != '0' && isset($request->{"value_$j"}) || isset($request->{"value_ar_$j"})) {
          $value = explode(",", $request->{"value_$j"});
          $value_ar = explode(",", $request->{"value_ar_$j"});
          $value_prc = explode(",", $request->variant_price);


          for ($i = 0; $i < count($value); $i++) {
            $ProductVariant = new ProductVariant();
            $ProductVariant->product_id = $product->id;
            $ProductVariant->variant_id = $request->{"variant_$j"};
            $ProductVariant->save();

            ProductVariantText::insert([
              ['product_variant_id' => $ProductVariant->id, 'title' => $value[$i], 'lang' => 1],
              ['product_variant_id' => $ProductVariant->id, 'title' => $value_ar[$i], 'lang' => 2],
            ]);

            $product_price                       = new Price;
            $product_price->price                = $value_prc[$i];
            $product_price->product_id           = $product_id;
            $product_price->variant_id           = $ProductVariant->id;
            $product_price->save();
            
          }
        }
      }

      $file = explode("~~", $request->fileName);
      for ($i = 0; $i < count($file) - 1; $i++) {
        $upload = Image::insert(['image' => $file[$i], 'product_id' => $product_id, 'created_at' => $today_stamp, 'updated_at' => $today_stamp]);
      }
      return ["success" => "Successfully Added.", "redirect" => route('admin.product.manage')];
    }
  }

  public function productImageUpload(Request $request)
  {
    $file = $request->file('file');
    $target_dir = env('PRODUCT_IMAGES');
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

    $filename  = basename($_FILES['file']['name']);
    $extension = pathinfo($filename, PATHINFO_EXTENSION);
    $new       = pathinfo($filename)['filename'] . "_" . time() . '.' . $extension;

    move_uploaded_file($_FILES["file"]["tmp_name"], storage_path('products') . "/{$new}");

    return $new;
  }

  public function variant_insert(array $val)
  {
    for ($i = 0; $i < sizeof($val['value']); $i++) {
      $variant                      = new ProductVariant;
      $variant->title               = $val['value'][$i];
      $variant->product_id          = $val['product_id'];
      $variant->variant_category_id = $val['category_id'];
      $variant->save();
      $variant_price                       = new Price;
      $variant_price->price                = ($val['is_price'] == 1) ? (isset($val['vari_price'][$i]) ? (int)$val['vari_price'][$i] : 0) : 0;
      $variant_price->country_id           = $val['country_id'];
      $variant_price->product_id           = $val['product_id'];
      $variant_price->variant_id           = $variant->id;
      $variant_price->save();
    }
  }

  protected function format_header_menu_data()
  {
    // $menu = HeaderMenu::with('sub_header_menu.header_sub_child_menu')->get();
    $menu = [];
    $data = [];
    foreach ($menu as $header) {
      if (isset($header->sub_header_menu) && isset($header->sub_header_menu[0]->header_sub_child_menu) && !empty($header->sub_header_menu) && !empty($header->sub_header_menu[0]->header_sub_child_menu)) {
        $row = [];
        $row['id']    =  $header->id;
        $row['title'] =  $header->title;
        $row['value'] =  $header->id;
        foreach ($header->sub_header_menu as $sub_header) {
          $sub_row = [];
          $sub_row['id']    =  $sub_header->id;
          $sub_row['title'] =  $sub_header->title;
          $sub_row['value'] =  $header->id . '_' . $sub_header->id;
          foreach ($sub_header->header_sub_child_menu as $header_child) {
            $child = [];
            $child['id']    =  $header_child->id;
            $child['title'] =  $header_child->title;
            $child['value'] =  $header->id . '_' . $sub_header->id . "_" . $header_child->id;
            $sub_row['children'][] = $child;
          }
          $row['children'][] = $sub_row;
        }
        $data[] = $row;
      }
    }
    return json_encode($data);
  }

  protected function format_sidebar_menu_data()
  {
    $menu = SidebarMenu::with('sidebar_sub_menu')->get();
    $data = [];
    foreach ($menu as $sidebar) {
      if (isset($sidebar->sidebar_sub_menu) && !empty($sidebar->sidebar_sub_menu)) {
        $row = [];
        $row['id']    =  $sidebar->id;
        $row['title'] =  $sidebar->title;
        $row['value'] =  $sidebar->id;
        foreach ($sidebar->sidebar_sub_menu as $sub_sidebar) {
          $sub_row = [];
          $sub_row['id']    =  $sub_sidebar->id;
          $sub_row['title'] =  $sub_sidebar->title;
          $sub_row['value'] =  $sidebar->id . '_' . $sub_sidebar->id;
          $row['children'][] = $sub_row;
        }
        $data[] = $row;
      }
    }
    return json_encode($data);
  }

  public function productImageDuplicate($files)
  {
    $files = $files;
    $new_files = array();
    foreach ($files as $file) {
      if ($file != "") {
        $old = storage_path('products/' . $file);
        $fileExtension = \File::extension($old);
        $new = time() . "_duplicate." . $fileExtension;
        $newPathWithName =  storage_path('products/' . $new);
        \File::copy($old, $newPathWithName);
        $new_files[] = $new;
      }
    }
    $new_files[] = "";
    return $new_files;
  }

  public function removeImage(Request $request)
  {
    $file = $request->file;
    $delete = File::delete(storage_path('products/' . $file));
    Image::where('image', $file)->delete();
    return 'true';
  }



  public function details(Request $request)
  {
    $sidebar_menu           = SidebarMenu::with('sidebar_sub_menu')->get();
    $weight_type            = WeightType::get();
    $section                = SectionMenu::with('section_menu_text')->get();
    $data = Product::with('images', 'price', 'brand.brand_text', 'category.tag.tag_text', 'category.sidebar.sidebar_menu_text', 'category.sidebar.sidebar_sub_menu.sidebar_sub_menu_text', 'productvariant.product_variant_text', 'productvariant.variant.variant_text')->where('id', $request->id)->first();
    return View('admin.product.detail', compact('data', 'weight_type', 'section', 'sidebar_menu'))->with(['title' => 'Product Details']);
  }

  public function edit(Request $request)
  {
    $sidebar_menu           = SidebarMenu::with('sidebar_sub_menu')->get();
    $weight_type            = WeightType::get();
    $variant                = Variant::with('variant_text')->get();
    $tag                    = Tag::get();
    $brand                  = Brand::with('Brand_text')->get();
    $section                = SectionMenu::with('section_menu_text')->get();
    $data   = Product::with('images', 'price', 'category', 'category.sidebar.sidebar_menu_text', 'category.sidebar.sidebar_sub_menu.sidebar_sub_menu_text', 'productvariant.product_variant_text', 'productvariant.variant.variant_text')->where('id', $request->id)->first();
    // dd($data->productvariant[0]->product_variant_text_all[0]->title);
    return view('admin.product.edit')->with('title', trans('lang.edit'))->with(['data' => $data, 'section' => $section, 'brand' => $brand, 'sidebar_menu' => $sidebar_menu, 'weight_type' => $weight_type, 'variant' => $variant, 'tag' => $tag, 'add' => 0, 'edit' => 1]);
  }



  public function delete(Request $request)
  {
    $product_id = $request->id;
    Product::where('id', $product_id)->delete();
    ProductText::withoutGlobalScopes()->where('product_id', $product_id)->delete();
    Category::where('product_id', $product_id)->delete();
    Price::where('product_id', $product_id)->delete();
    Image::where('product_id', $product_id)->delete();
    return 'success';
  }

  public function add_inventory(Request $request)
  {
    if($request->variant == 0)
    {
      $data = Product::where('id', $request->product_id)->update([
        'inventory' => $request->inventory,
      ]);
    }else
    {
      $data = ProductVariant::where(['id'=> $request->variant,'product_id'=>$request->product_id])->update([
        'inventory' => $request->inventory,
      ]);
    }
    return ["success" => "Successfully Added.", "redirect" => route('admin.product.manage')];
  }
}
