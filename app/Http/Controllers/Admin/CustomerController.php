<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use DB;
use PDF;
use Carbon;
class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $from = "";
        $to   = "";
        if(isset($request->from) && isset($request->to)){
          $from = $request->from;
          $to   = $request->to;
          $this_to = date("Y-m-d", strtotime("$to +1 day"));
          $data = Order::groupBy('billing_email')->whereBetween('created_at', [date("Y-m-d", strtotime($from)), $this_to])->orderBy('id', 'desc')->paginate(20);
        }else{
          $data = Order::groupBy('billing_email')->paginate(20);
        }
        return view('admin.customer.manage')->with(['title' => trans('lang.customer'), 'data'=> $data, 'from' => $from, 'to' => $to]);
        
    }
    public function report(Request $request){
        $from = "";
        $to   = "";
        if(isset($request->from) && isset($request->to)){
          $from = $request->from;
          $to   = $request->to;
          $this_to = date("Y-m-d", strtotime("$to +1 day"));
          $data = Order::groupBy('billing_email')->whereBetween('created_at', [date("Y-m-d", strtotime($from)), $this_to])->orderBy('id', 'desc')->paginate(25);
        }else{
          $data = Order::groupBy('billing_email')->orderBy('id', 'desc')->paginate(25);
        }
        $pdf= PDF::loadview('admin.customer.report',['data' => $data, 'from' => $from, 'to' => $to])->setPaper('a4', 'portrait');
        return $pdf->stream('admin/customer.pdf');
    }
}
