<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SectionMenu;
use App\SectionMenuText;
use App\FooterMenuBottom;
use App\Tag;
use App\Brand;
use App\Page;
use App\Product;
use App\Store;
use App\ProductText;
use App\FooterMenuBottomText;
use App\OurStory;
use App\OurStoryText;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->paginate = 25;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = SectionMenu::with('section_menu_text')->orderBy('id', 'desc')->get();
        return view('website.index')->with('data', $data)->with('title', trans('lang.home'));
    }
    public function page_not_found()
    {   
        return view('website.404')->with('title', '404');
    }
    public function thankyou()
    {   
        return view('website.thankyou')->with('title', trans('lang.thank_you'));
    }
    public function brands()
    {   
        $brands = Brand::with('brand_text')->where('is_active',1)->get()->sortBy('brand_text.title',SORT_REGULAR,false);
        
        $brandsAlpha = $brands->groupBy(function ($item, $key) {
            return is_numeric(substr($item->brand_text->title, 0, 1))?'0-9':ucfirst(substr($item->brand_text->title, 0, 1));
        });
        $alphabets = array_keys($brandsAlpha->toArray());
        // dd($brandsAlpha);
        return view('website.brands',compact('alphabets','brandsAlpha'))->with('title', trans('lang.brands'));
    }
    public function privacy_policy()
    {   /*$data = FooterMenuBottom::where('url', 'privacy-policy')->with('footer_menu_bottom_text')->first();*/
        //return view('website.privacy')->with('title', @$data->footer_menu_bottom_text->title);
        return view('website.privacy')->with('title', trans('lang.privacy_policy'));
        
    }
    public function faqs()
    {   
        return view('website.faq')->with('title', trans('lang.faqs'));
    }
    public function terms_and_condition()
    {   
        return view('website.faq')->with('title', trans('lang.terms_and_condition'));
    }
    public function about()
    {   
        // $data = SectionMenu::with('section_menu_text')->orderBy('id', 'desc')->get();
        $page_data = Page::with(['page_text'])->where('id', 4)->first();
        return view('website.about')->with(['page_data' => $page_data])->with('title', trans('lang.about'));
    }
    public function contact()
    {   
        $store_data = Store::orderBy('id', 'desc')->get();
        return view('website.contact-us')->with(['title'=> trans('lang.contact'),'store_data' =>$store_data,'selected'=> trans('lang.contact')]);
    }
    public function stores()
    {   
        $store_data = Store::orderBy('id', 'desc')->get();
        return view('website.store')->with(['title'=> trans('lang.store'),'selected'=> trans('lang.store'),'store_data' => $store_data]);
    }

    public function header_search(Request $request)
    {  
        $keyword = $request->keyword;
        if ($keyword !='') {
            $data = ProductText::where('title', 'Like', '%'.$keyword.'%')->get();
            if (isset($data)) {
                $products = array_column($data->toArray(), 'product_id');
                $data = Product::with(['product_text', 'front_price', 'images', 'weight'])->whereIn('id', $products)->orderBy('id', 'desc')->paginate(25);
            } else {
                $data = [];
            }
                
            $tag = Tag::with('tag_text')->get();
            $brand = Brand::with('brand_text')->get();
            return view('website.product')->with(['data'=> $data,'tag'=>$tag,'brand'=>$brand]);  
        }
    }
    public function ourStory()
    {
        $page_data = OurStory::with(['ourStoryText'])->first();
        return view('website.ourstory')->with(['page_data' => $page_data])->with('title', trans('lang.our_story'));
    }

    public function best_seller()
    {   
        $selected = trans('lang.best_seller');
        $data = Product::with(['product_text', 'front_price', 'images', 'weight'])->where('best_seller','1')->paginate($this->paginate);
        $tag = Tag::with('tag_text')->get();
        $brand = Brand::with('brand_text')->get();
        $wishlist = [];
        if(Auth::check() && Auth::user()->id && Auth::user()->hasRole('customer')){
            $wishlist = WishList::where('user_id', Auth::user()->id)->pluck('product_id')->toArray();
        }
        return view('website.product')->with(['data'=> $data,'tag'=>$tag, 'wishlist'=>$wishlist,'brand'=>$brand, 'title' => trans('lang.best_seller'),'selected'=>$selected]);
    }

    public function exclusive()
    {   
        $selected = trans('lang.exclusive');
        $data = Product::with(['product_text', 'front_price', 'images', 'weight'])->where('exclusive','1')->paginate($this->paginate);
        $tag = Tag::with('tag_text')->get();
        $brand = Brand::with('brand_text')->get();
        $wishlist = [];
        if(Auth::check() && Auth::user()->id && Auth::user()->hasRole('customer')){
            $wishlist = WishList::where('user_id', Auth::user()->id)->pluck('product_id')->toArray();
        }
        return view('website.product')->with(['data'=> $data,'tag'=>$tag, 'wishlist'=>$wishlist,'brand'=>$brand, 'title' => trans('lang.exclusive'),'selected'=>$selected]);
    }
}
