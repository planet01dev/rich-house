<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App;
use App\SettingEmail;
use Spatie\Permission\Models\Role;
use App\Mail\CustomerSignUp;
use Illuminate\Support\Facades\Mail;


class UserController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    use AuthenticatesUsers;
    
    protected $redirectTo = '/home';
    public function __construct()
    {
        // $this->middleware(['auth']);
        
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       return view('website.customer.signin')->with('title', trans('lang.signup'));
    } 
    
    public function post(Request $request){
        
        // dd($request);
        $request->validate([
            'f_name' => 'required',
            'l_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required',

        ]);
        
        if(isset($request->id) && !empty($request->id)){
        	if(User::where('email', $request->email)->where('id', '!=', $request->id)->count() > 0){
	        	return ["error" => "Email already in use."];
            }
            
	        $data = User::where('id', $request->id)->update([
                'f_name' => $request->f_name,
                'l_name' => $request->l_name,
                'email' => $request->email,
                'phone' => $request->phone,
                
                
	        ]);


        }else{
	        if(User::where('email', $request->email)->count() > 0){
	        	return ["error" => "User Email already in use."];
	        }
            $password = bcrypt($request->password);
	        $data = new User();
            $data->f_name                    = $request->f_name;
            $data->l_name                    = $request->l_name;
	        $data->email             = @$request->email;
            $data->phone             = @$request->phone;
            $data->password        = @$password;
            $data->save();

            $user = User::where('id', $data->id)->first();
            
            // id 2 is for customer
            $role_r = Role::where('id', '=', '2')->firstOrFail();  
            $user->assignRole($role_r);

            $text = "NEW CUSTOMER REGISTERED ".$request->f_name." ".$request->l_name.". Email: ".@$request->email;
            $setting_email = SettingEmail::get();
            foreach ($setting_email as $recipient) {
                Mail::to($recipient->email)->send(new CustomerSignUp($text));
            }
            $text = "Thank you for joining Rich House.";
            Mail::to($request->email)->send(new CustomerSignUp($text));
        
        }
        return ["success" => "Successfully Registered.", "redirect" => route('web.home')];
    }

    function checklogin(Request $request)
    {
     $this->validate($request, [
      'email'   => 'required|email',
      'password'  => 'required|alphaNum'
     ]);

     $user_data = array(
      'email'  => $request->get('email'),
      'password' => $request->get('password')
     );

     if(Auth::attempt($user_data))
     {
        $user_data = User::where('id', Auth::user()->id);
        if(Auth::user()->preferred_language == 1)
        {
            $lang= 'en';
        }else
        {
            $lang = 'ar';
        }
        App::setlocale($lang);
        session()->put('lang', $lang);
      return ["success" => 'Successfully Logged In', "redirect" => route('web.home')];
     }
     else
     {
        return ["error" => 'Wrong User Email/Password'];
    //   return back()->with('error', 'Wrong Login Details');
     }

    }

    // public function checkIfUserLoggedin()
    // {
    //     if(Auth::user() != null && Auth::user()->hasRole('customer')) {
    //         $loggedIN = 1;
    //     } else {
    //         $loggedIN = 0;
    //     }
    //     echo $loggedIN;
    //     exit();
    // }


    function logout()
    {
     Auth::logout();
     return redirect('/');
    }

}

