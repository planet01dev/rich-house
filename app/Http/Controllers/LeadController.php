<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lead;
use App\Mail\ContactUsMail;
use Illuminate\Support\Facades\Mail;

class LeadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function post_lead(Request $request){
        
        if(empty($request->email) || empty($request->name))
        {
                echo json_encode(array("error" => 'Please Fill all required fields'));
                exit();
        }
        $data = new Lead();
        $data->name       = $request->name;
        $data->email      = $request->email;
        $data->phone      = $request->phone;
        $data->message    = $request->message;
        $data->save();
        Mail::to('orders@richhouse.me')->send(new ContactUsMail($data));
        return ['success' => trans('lang.message_submitted_successfully'), 'fieldsEmpty' => 'yes'];
    }
   
}
