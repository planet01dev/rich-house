<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Subscribe;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubscribeNotification;

class SubscribtionController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function __construct()
    {
        //$this->middleware('auth')
    }

    public function add_subscribe(Request $request)
    {
        if(Subscribe::where('email', $request->email)->count() > 0){
            return response()->json(['status' => 'error','message'=>'Already Exists']);
         }
         $data = new Subscribe();
         $data->email      = $request->email;
         $data->save();
         Mail::to($request->email)->send(new SubscribeNotification($data->email));
         return response()->json(['status' => 'success']);
    }

    
    

   
}