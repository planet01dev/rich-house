<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PasswordReset;
use Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    public function add()
    {
        return view('website/forgot-password');
    }
    public function store(Request $request)
    {
        $request->validate([
            'email'=> 'email:rfc,dns|required'
        ]);
        $user = User::where('email', $request->email)->first();
        if(!$user){
            return back()->withErrors(["Email is not Exist. Please Sign Up"])->withInput();
        }
        $passwordReset =  PasswordReset::where('email' ,$request->email)->first();
        if($passwordReset)
        {
            PasswordReset::where('email',$request->email)->delete();
        }
        $str_token = Str::random(30);
        $reset = new PasswordReset();
        $reset->email = $request->email;
        $reset->token = $str_token;
        $reset->created_at = NOW();
        $reset->save();
        Mail::to($request->email)->send(new ForgotPasswordMail($str_token,$user->f_name.''.$user->l_name));
        return back()->withMessage("Please Check Your Email for Password reset.");
    }
    public function reset(Request $request, $token)
    {
        $passwordReset = PasswordReset::where('token' ,$token)->first();
        $mutable = Carbon::now();
        if(!$passwordReset)
        {
            return abort(404);
        }
        // dd($mutable->diffInHours($passwordReset->created_at));
        if($mutable->diffInHours($passwordReset->created_at) > 24)
        {
            return abort(404);
        }
        session()->put('token',$passwordReset->token);
        session()->put('email',$passwordReset->email);
        return view('website/reset-password');
    }
    public function resetStore(Request $request)
    {
        $request->validate([
            'password'=> 'required|min:6|confirmed'
        ]);
        $token = session()->get('token');
        $email = session()->get('email');
        // dd($email);
        $user = User::where('email',$email)->first();
        $user->password = Hash::make($request->password);
        $user->save();
        PasswordReset::where('email',$email)->delete();
        return redirect()->route('web.home')->with(["reset"=>"Password changed successfully. Please Login with new password"]);
    }
}
