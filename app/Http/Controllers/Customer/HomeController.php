<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\User;
use App\UserAddress;
use App\Shipping;
use App\Order;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\WishList;
use Carbon\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $user_data = User::with(['user_order.orderDetail.orderProduct.product_text','user_order.orderDetail.orderProduct.images','user_address'])->where('id', $user_id)->first();
        // dd($user_data);
        $wishlist = WishList::with('product.product_text', 'product.front_price','product.price', 'product.images', 'product.weight','product.productvariant.product_variant_text')->where('user_id', $user_id)->get();
        $shipping_data = Shipping::with('shipping_text_all')->where('id', 1)->first();
        Order::where('user_id', $user_id)->update([
            'customer_view' => 0,
          ]);
        return view('website/customer/profile')->with(['title'=>'Profile','shipping_data' => $shipping_data,'user_data' => $user_data, 'wishlist' => $wishlist]);
    }

    public function add_address(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
        ]);
        if (isset($request->address_id) && !empty($request->address_id)) 
        {
            $data = UserAddress::where('id', $request->address_id)->update([
                'title' => $request->title,
                'name' => $request->name,
                'address' => $request->address,
                'country' => $request->country,
                'state' => $request->state,
                'city' => $request->city,
                'zip_code' => $request->zip_code,
                'primary_address' => (isset($request->primary_address) && $request->primary_address == 'on' )? 1 : 0,
                'user_id' => Auth::user()->id,
              ]);
        }else
        {
                if(isset($request->primary_address) && $request->primary_address == 'on' )
                {
                    UserAddress::where('user_id', '>', Auth::user()->id)->update(['primary_address' => 0]);
                    // UserAddress::query()->update('primary_address' => '0');
                }
                
                $data = new UserAddress();
                $data->title = $request->title;
                $data->name = $request->name;
                $data->address = $request->address;
                $data->country = $request->country;
                $data->state = $request->state;
                $data->city = $request->city;
                $data->zip_code = $request->zip_code;
                $data->primary_address = (isset($request->primary_address) && $request->primary_address == 'on' )? 1 : 0;
                $data->user_id = Auth::user()->id;
                $data->save();
        }
        
        return ["success" => trans('lang.successfully_updated'), "redirect" => route('customer.profile')];
    }
    public function customer_name_change(Request $request)
    {
        $request->validate([
            'f_name' => 'required',
            'l_name' => 'required',
        ]);
        $data = User::where('id', Auth::user()->id)->update([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'preferred_language' => $request->preferred_language

        ]);
        return ["success" => trans('lang.successfully_updated')];
    }

    public function customer_detail_change(Request $request)
    {
        $request->validate([
            // 'email' => 'required',
            'password' => 'required',
        ]);
        
        $password = bcrypt($request->password);
        $data = User::where('id', Auth::user()->id)->update([
            // 'email' => $request->email,
            'password' => $password,

        ]);
        return ["success" => trans('lang.successfully_updated')];
    }

    public function delete_customer_address(Request $request){
        $id = $request->id;
        UserAddress::where('id', $id)->delete();
        return 'success';
    }

    public function cancel_order(Request $request)
    {
        $order_id = $request->id;
        $order = Order::find($order_id);
        $mutable = Carbon::now();
        // dd($mutable);
        if($mutable->diffInHours($order->created_at) >= 1)
        {
            return 'timeout';
        }
        else{
            $order->status = 3;
            $order->save();
            return 'success';
        }
    }

    public function customer_order_viewed(Request $request)
    {

    }

}
