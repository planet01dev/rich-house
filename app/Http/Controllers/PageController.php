<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SidebarMenu;
use App\SidebarSubMenu;
use App\SectionMenu;
use App\Product;
use App\Tag;
use App\Brand;
use App\Category;
use App\Price;
use App\Page;
class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $paginate;
    public function __construct()
    {
        
        //$this->middleware('auth');
        $this->paginate = 25;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    

    public function detail($page_name = null)
    {
        if(isset($page_name)){
            // 'productvariant.product_variant_text', 'productvariant.variant_text'
            $data = Page::with(['page_text'])->where('page_name', $page_name)->first();
       }else{
            return redirect()->route('web.404');
        }
        if(empty($data)){
            return redirect()->route('web.404');
        }   
        return view('website.page')->with(['data'=> $data, 'title' => trans('lang.pages')]);  
        
    }
    
}
