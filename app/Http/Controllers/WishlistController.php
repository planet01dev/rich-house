<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\WishList;
use Illuminate\Support\Facades\Auth;
class WishlistController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function post(Request $request)
    {
        if(Auth::check() && Auth::user()->id && Auth::user()->hasRole('customer')){
            if(WishList::where('product_id', $request->id)->where('user_id', Auth::user()->id)->count() > 0){
                WishList::where('product_id', $request->id)->where('user_id', Auth::user()->id)->delete();
                return response()->json(['status' => 'success','message'=>'Removed from wishlist']);
             }
             $data = new WishList();
             $data->product_id      = $request->id;
             $data->user_id         = Auth::user()->id;
             $data->save();
             return response()->json(['status' => 'success', 'message'=> 'Item added to your wishlist']);
        }else{
            return response()->json(['status' => 'login_failed','message'=>'Please login first for adding item in wishlist']);
        }
    }
}