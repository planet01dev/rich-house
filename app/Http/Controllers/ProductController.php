<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SidebarMenu;
use App\SidebarSubMenu;
use App\SectionMenu;
use App\Product;
use App\Tag;
use App\Brand;
use App\Category;
use App\Price;
use App\WishList;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\URL;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $paginate;
    public function __construct()
    {
        
        //$this->middleware('auth');
        $this->paginate = 25;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function sidebar(Request $request)
    { 
        $selected = trans('lang.products');
        if(isset($request->sidebar) && isset($request->submenu)){
           /* $data = SidebarSubMenu::with('category.product.product_text')->where('permalink', $request->submenu)->orderBy('id', 'desc')->paginate($this->paginate);*/
            $data = SidebarSubMenu::with(['category','sidebar_sub_menu_text'])->where('permalink', $request->submenu)->get();
            $selected = $data[0]->sidebar_sub_menu_text->title;
            if(isset($data[0]->category)){
                $products = array_column($data[0]->category->toArray(), 'product_id');
                $data = Product::whereIn('id', $products)->orderBy('id', 'desc')->paginate($this->paginate);
            }else{
                $data = [];
            }
        }else if(isset($request->sidebar)){
            /*$data = SidebarMenu::with('category.product.product_text')->where('permalink', $request->sidebar)->orderBy('id', 'desc')->paginate($this->paginate);*/
            $data = SidebarMenu::with(['category','sidebar_menu_text'])->where('permalink', $request->sidebar)->get();
            $selected = $data[0]->sidebar_menu_text->title;
            if(isset($data[0]->category)){
                $products = array_column($data[0]->category->toArray(), 'product_id');
                $data = Product::whereIn('id', $products)->orderBy('id', 'desc')->paginate($this->paginate);
            }else{
                $data = [];
            }
        }else{
            $data = [];
        }
        if(empty($data)){
            //return redirect('404');
            return redirect()->route('web.404');
        }        
        $tag = Tag::with('tag_text')->get();
        $brand = Brand::with('brand_text')->get();   
        return view('website.product')->with(['data'=> $data,'tag'=>$tag,'brand'=>$brand, 'title' => trans('lang.products'),'selected'=>$selected]); 
        
    }

    public function section_menu($section)
    {
        
        if(isset($section)){
            $data = SectionMenu::with('category')->where('permalink', $section)->get();
            if(isset($data[0]->category)){
                if($data[0]->other == 1)
                {
                    $URL = url('/'.$section);
                    return redirect()->away($URL);
                }
                $products = array_column($data[0]->category->toArray(), 'product_id');
                $data = Product::whereIn('id', $products)->orderBy('id', 'desc')->paginate($this->paginate);
            }else{
                $data = [];
            }
        }else{
            $data = [];
        }
        if(empty($data)){
            return redirect()->route('web.404');
        }
        $tag = Tag::with('tag_text')->get();
        $brand = Brand::with('brand_text')->get();   
        return view('website.product')->with(['data'=> $data,'tag'=>$tag,'brand'=>$brand, 'title' => trans('lang.products')]);  
        
    }

    public function detail($permalink = null)
    {
        if(isset($permalink)){
            // 'productvariant.product_variant_text', 'productvariant.variant_text'
            $data = Product::with(['product_text', 'front_price','price', 'images', 'weight','productvariant.product_variant_text'])->where('permalink', $permalink)->first();
       }else{
            return redirect()->route('products');
        }
        if(empty($data)){
            return redirect()->route('web.404');
        }   
        return view('website.product-detail')->with(['data'=> $data, 'title' => trans('lang.products')]);  
        
    }
    public function products(){
        $selected = trans('lang.products');
        $data = Product::with(['product_text', 'front_price', 'images', 'weight'])->paginate($this->paginate);
        $tag = Tag::with('tag_text')->get();
        $brand = Brand::with('brand_text')->get();
        $wishlist = [];
        if(Auth::check() && Auth::user()->id && Auth::user()->hasRole('customer')){
            $wishlist = WishList::where('user_id', Auth::user()->id)->pluck('product_id')->toArray();
        }
        return view('website.product')->with(['data'=> $data,'tag'=>$tag, 'wishlist'=>$wishlist,'brand'=>$brand, 'title' => trans('lang.products'),'selected'=>$selected]);
        // return view('website.product')->with(['data'=> $data,'tag'=>$tag,'brand'=>$brand, 'title' => trans('lang.products')]);  

    }
    public function search(Request $request)
    {
        $selected = trans('lang.search');
        $tag_id = $request->tag_id;
        $brand_id = $request->brand_id;
        $min_amount = $request->min_amount;
        $max_amount = $request->max_amount;
        
        $min_amount = number_format($min_amount, 2, '.', '');
        $max_amount = number_format($max_amount, 2, '.', '');
        
        
        
        if($tag_id == '0')
        {   
            if($brand_id == '0')
            {
                $data = Product::with(['product_text', 'front_price', 'images', 'weight'])->orderBy('id', 'desc')->paginate($this->paginate);
                if(isset($min_amount) && number_format($min_amount, 2, '.', '') > 0)
                {
                    foreach(@$data as $k => $v) 
                    {
                        if(number_format($v->front_price->price,2) < number_format($min_amount, 2, '.', ''))
                        {
                            unset($data[$k]);
                        }
                    }
                }
                if(isset($max_amount) && number_format($max_amount, 2, '.', '') > 0)
                {
                    foreach(@$data as $k => $v) 
                    {
                        if(number_format($v->front_price->price,2) > number_format($max_amount, 2, '.', ''))
                        {
                            unset($data[$k]);
                        }
                    }
                }
            }
            else
            {
                $data = Product::with(['product_text', 'front_price', 'images', 'weight'])->where('brand_id',$brand_id)->orderBy('id', 'desc')->paginate($this->paginate);
                if(isset($min_amount) && number_format($min_amount, 2, '.', '') > 0)
                {
                    foreach(@$data as $k => $v) 
                    {
                        if(number_format($v->front_price->price,2) < number_format($min_amount, 2, '.', ''))
                        {
                            unset($data[$k]);
                        }
                    }
                }
                if(isset($max_amount) && number_format($max_amount, 2, '.', '') > 0)
                {
                    foreach(@$data as $k => $v) 
                    {
                        if(number_format($v->front_price->price,2) > number_format($max_amount, 2, '.', ''))
                        {
                            unset($data[$k]);
                        }
                    }
                }
            }
        }else
        {
            $data = Category::where('tag_id',$tag_id)->get();
            if(isset($data)){
                $products = array_column($data->toArray(), 'product_id');
                if($brand_id == '0')
                {
                    $data = Product::with(['product_text', 'front_price', 'images', 'weight'])->whereIn('id', $products)->orderBy('id', 'desc')->paginate($this->paginate);
                    if(isset($min_amount) && number_format($min_amount, 2, '.', '') > 0)
                    {
                        foreach(@$data as $k => $v) 
                        {
                            if(number_format($v->front_price->price,2) < number_format($min_amount, 2, '.', ''))
                            {
                                unset($data[$k]);
                            }
                        }
                    }
                    if(isset($max_amount) && number_format($max_amount, 2, '.', '') > 0)
                    {
                        foreach(@$data as $k => $v) 
                        {
                            if(number_format($v->front_price->price,2) > number_format($max_amount, 2, '.', ''))
                            {
                                unset($data[$k]);
                            }
                        }
                    }
                }
                else
                {
                    $data = Product::with(['product_text', 'front_price', 'images', 'weight'])->whereIn('id', $products)->where('brand_id',$brand_id)->orderBy('id', 'desc')->paginate($this->paginate);
                    if(isset($min_amount) && number_format($min_amount, 2, '.', '') > 0)
                    {
                        foreach(@$data as $k => $v) 
                        {
                            if(number_format($v->front_price->price,2) < number_format($min_amount, 2, '.', ''))
                            {
                                unset($data[$k]);
                            }
                        }
                    }
                    if(isset($max_amount) && number_format($max_amount, 2, '.', '') > 0)
                    {
                        foreach(@$data as $k => $v) 
                        {
                            if(number_format($v->front_price->price,2) > number_format($max_amount, 2, '.', ''))
                            {
                                unset($data[$k]);
                            }
                        }
                    }
                }
                
            }else{
                $data = [];
            }
        }
            
        $tag = Tag::with('tag_text')->get();
        $brand = Brand::with('brand_text')->get();
        return view('website.product')->with(['data'=> $data,'tag'=>$tag,'brand'=>$brand,'tag_id'=> @$tag_id, 'brand_id'=>@$brand_id,'min_amount'=>$min_amount,'max_amount'=>$max_amount,'selected'=>$selected]);
       
    }

    public function brand(Request $request)
    {
        if($request->brand == 0)
        {
            $data = Product::with(['product_text', 'front_price', 'images', 'weight','brand.brand_text'])->orderBy('id', 'desc')->paginate($this->paginate);
            $selected = trans('lang.brands');
        }else
        {
            $data = Product::with(['product_text', 'front_price', 'images', 'weight','brand.brand_text'])->where('brand_id',$request->brand)->orderBy('id', 'desc')->paginate($this->paginate);
            $temp = Brand::with('brand_text')->where('id',$request->brand)->get();
            $selected = $temp[0]->brand_text->title;
        }
        $tag = Tag::with('tag_text')->get();
        $brand = Brand::with('brand_text')->get();   
        return view('website.product')->with(['data'=> $data,'tag'=>$tag,'brand'=>$brand, 'title' => trans('lang.products'),'selected'=>$selected]); 
    }   
}
