<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model {  

	public function coupon_text(){
        return $this->hasOne('App\CouponText', 'coupon_id', 'id');
    }

    public function coupon_text_all(){
        return $this->hasMany('App\CouponText', 'coupon_id', 'id')->withoutGlobalScopes();
    } 
}

