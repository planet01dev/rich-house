<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OurStory extends Model
{
    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function ourStoryText(){
        return $this->hasOne('App\OurStoryText', 'our_story_id', 'id');
    }
    public function our_story_text_all(){
        return $this->hasMany('App\OurStoryText', 'our_story_id', 'id')->withoutGlobalScopes();
    } 
}
