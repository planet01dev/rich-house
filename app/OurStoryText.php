<?php

namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class OurStoryText extends Model
{
    public function ourStory(){
        return $this->belongsTo('App\OurStory', 'our_story_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }
}
