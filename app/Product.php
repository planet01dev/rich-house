<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    public function images(){
        return $this->hasMany('App\Image', 'product_id', 'id');
    }
    
    public function brand(){
        return $this->hasOne('App\Brand', 'id', 'brand_id');
    }

    public function price(){
        return $this->hasMany('App\Price', 'product_id', 'id');
    }

    public function front_price(){
        return $this->hasOne('App\Price', 'product_id', 'id')->whereNull('variant_id');
    }

    public function weight(){
        return $this->hasOne('App\WeightType', 'id', 'weight_type_id');
    }

    public function category(){
        return $this->hasMany('App\Category', 'product_id', 'id');
    }

    public function productvariant(){
        return $this->hasMany('App\ProductVariant', 'product_id', 'id');
    }

    public function product_text(){
        return $this->hasOne('App\ProductText', 'product_id', 'id');
    }

    public function product_text_all(){
        return $this->hasMany('App\ProductText', 'product_id', 'id')->withoutGlobalScopes();
    }
}

