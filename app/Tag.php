<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model {
	public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function tag_text(){
        return $this->hasOne('App\TagText', 'tag_id', 'id');
    }

    public function tag_text_all(){
        return $this->hasMany('App\TagText', 'tag_id', 'id')->withoutGlobalScopes();
    }

	   
}

