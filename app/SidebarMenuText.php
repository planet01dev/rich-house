<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class SidebarMenuText extends Model {
	public function sidebar_menu(){
        return $this->belongsTo('App\SidebarMenu', 'sidebar_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

