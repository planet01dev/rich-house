<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class CouponText extends Model {
	public function coupon(){
        return $this->belongsTo('App\Coupon', 'coupon_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

