<?php
use App\Product;
use App\ProductVariant;
use App\SidebarMenu;
use App\SidebarSubMenu;
use App\Setting;
use App\WishList;
function dt_format($date) {
   return date("M-d-Y", strtotime($date));
}

function dt_time_format($date) {
    $dt   = date("M-d-Y", strtotime($date));
    $time = date("H:i:s", strtotime($date));
   return [$dt, $time];
}
function discounted_price($price, $discount,$discount_type = 0){
   if($discount_type == 0)
   {
      $discount = ($discount/100) * $price;
      $price = $price - $discount;
      return number_format($price,2);   
   }
   else
   {
      $price = $price - $discount;
      return number_format($price,2); 
   }
   
}
function num_format($number){
   return number_format((float)$number, 2, '.', '');
}
function relavent_product($product_id)
{
   $sidebar_id = array();
   $section_id = array();
   $temp = Product::where('id', $product_id)->with('category')->get();
   foreach(@$temp as $k => $v) 
   {
      foreach(@$v->category as $k2 => $v2) 
      {
         if($v2['sidebar_id'] != NULL)
         {
            array_push($sidebar_id,$v2['sidebar_id']);
         }
         if($v2['section_id'] != NULL)
         {
            array_push($section_id,$v2['section_id']);
         }
         
      }
      
   }
   if($section_id){
        $data = Product::where('id','!=', $product_id)->with('product_text')->with('front_price')->with('category')->whereHas('category', function($q) use($sidebar_id,$section_id){
          $q->where('sidebar_id', $sidebar_id)->orwhere('section_id', $section_id);
       })->orderBy('id', 'desc')->take(3)->get();
    
       if(empty($data[0]))
       {
          $data = [];
       }
       return $data;
    }
   
}
function payment_method($num){
   $status=["", "Payfort", "COD"];
   return @$status[$num];
}
function count_total_amount($tax, $shipping, $total){
   $tx = ($total/100)*$tax;
   $tot = $tx+$shipping+$total;
   return $tot;
}

function get_settings()
{
   $data = Setting::with(['setting_text_all','setting_text'])->where('id', 1)->first();
   return $data;
}
function check_inventory($product_id,$quantity,$variant_id = '')
{
   if($variant_id == '' || $variant_id == null || $variant_id == 0)
   {
      $temp = Product::where('id', $product_id)->first();
   }
   else
   {
      $temp = ProductVariant::where('id', $variant_id)->first();
   }
   $stock = $temp->inventory;
   if($quantity <= $stock)
   {
      return true;
   }
   else
   {
      return false;
   }
}

function limit_string($x, $length){
   if(strlen($x)<=$length)
   {
     echo $x;
   }
   else
   {
     $y=mb_substr($x,0,$length) . '...';
     echo $y;
   }
}

 function permalink_product()
 {
    $id = Product::max('id');
    $id += 1;
    $id = 'Product-'.$id;
    return $id;
 }

 function permalink_menu()
 {
    $id = SidebarMenu::max('id');
    $id += 1;
    $id = 'Menu-'.$id;
    return $id;
 }

 function permalink_sub_menu()
 {
    $id = SidebarSubMenu::max('id');
    $id += 1;
    $id = 'Sub-Menu-'.$id;
    return $id;
 }
 function wishlist_total_count(){
    $tot = 0;
    if(isset(Auth::user()->id) && Auth::user()->hasRole('customer')){
      $tot = WishList::where('user_id', Auth::user()->id)->count();
    }
    return $tot;
 }