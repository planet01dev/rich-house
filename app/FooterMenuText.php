<?php
namespace App;
use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class FooterMenuText extends Model {
	public function footer_menu(){
        return $this->belongsTo('App\FooterMenu', 'footer_id', 'id');
    }

    protected static function booted()
    {
        static::addGlobalScope(new LangScope);
    }

	   
}

