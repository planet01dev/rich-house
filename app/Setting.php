<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model {  

	public function setting_text(){
        return $this->hasOne('App\SettingText', 'setting_id', 'id');
    }

    public function setting_text_all(){
        return $this->hasMany('App\SettingText', 'setting_id', 'id')->withoutGlobalScopes();
    } 
}

