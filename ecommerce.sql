-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2021 at 09:03 AM
-- Server version: 8.0.18
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>active 0 => deactivate',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `is_active`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 1, 1, '2020-11-24 16:11:30', '2020-11-24 16:11:30', NULL),
(4, 1, 1, '2020-11-24 16:12:02', '2020-12-03 17:27:43', NULL),
(5, 1, 1, '2020-11-24 16:12:32', '2020-11-24 16:12:32', NULL),
(6, 1, 1, '2020-11-24 16:13:17', '2020-11-24 16:13:17', NULL),
(7, 1, 1, '2020-11-24 16:14:52', '2020-12-09 17:35:38', NULL),
(8, 1, 1, '2020-11-24 16:15:23', '2020-11-24 16:15:23', NULL),
(9, 1, 1, '2020-12-03 17:26:10', '2020-12-03 17:26:10', NULL),
(10, 1, 1, '2020-12-03 17:26:39', '2020-12-03 17:26:39', NULL),
(11, 1, 1, '2020-12-03 17:27:05', '2020-12-03 17:27:05', NULL),
(12, 1, 1, '2020-12-03 17:27:57', '2020-12-14 17:39:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brand_texts`
--

CREATE TABLE `brand_texts` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand_texts`
--

INSERT INTO `brand_texts` (`id`, `brand_id`, `title`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(13, 3, 'JACQUES ZOLTY', 1, NULL, NULL, NULL),
(14, 3, 'جاك زولتي', 2, NULL, NULL, NULL),
(17, 5, 'MORESQUE', 1, NULL, NULL, NULL),
(18, 5, 'موريسك', 2, NULL, NULL, NULL),
(19, 6, 'NASAMAT', 1, NULL, NULL, NULL),
(20, 6, 'ناسامات', 2, NULL, NULL, NULL),
(23, 8, 'BLEND OUD', 1, NULL, NULL, NULL),
(24, 8, 'مزيج العود', 2, NULL, NULL, NULL),
(25, 9, 'THE HOUSE OF OUD', 1, NULL, NULL, NULL),
(26, 9, 'THE HOUSE OF OUD', 2, NULL, NULL, NULL),
(27, 10, 'CUPID', 1, NULL, NULL, NULL),
(28, 10, 'CUPID', 2, NULL, NULL, NULL),
(29, 11, 'LA PERLA HOME', 1, NULL, NULL, NULL),
(30, 11, 'LA PERLA HOME', 2, NULL, NULL, NULL),
(31, 4, 'MOOD', 1, NULL, NULL, NULL),
(32, 4, 'مزاج', 2, NULL, NULL, NULL),
(39, 7, 'AMORINO', 1, NULL, NULL, NULL),
(40, 7, 'امورينو', 2, NULL, NULL, NULL),
(41, 12, 'MUSIC DE PARFUM', 1, NULL, NULL, NULL),
(42, 12, 'MUSIC DE PARFUM.', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sidebar_id` int(11) DEFAULT NULL,
  `sidebar_child_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `product_id`, `sidebar_id`, `sidebar_child_id`, `section_id`, `tag_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(386, 23, 1, 4, NULL, NULL, '2020-12-14 20:05:50', '2020-12-14 20:05:50', NULL),
(387, 23, NULL, NULL, 9, NULL, '2020-12-14 20:05:50', '2020-12-14 20:05:50', NULL),
(388, 24, 1, 4, NULL, NULL, '2020-12-15 10:46:09', '2020-12-15 10:46:09', NULL),
(389, 24, NULL, NULL, 6, NULL, '2020-12-15 10:46:09', '2020-12-15 10:46:09', NULL),
(390, 25, 1, 4, NULL, NULL, '2020-12-15 10:48:48', '2020-12-15 10:48:48', NULL),
(391, 25, NULL, NULL, 6, NULL, '2020-12-15 10:48:48', '2020-12-15 10:48:48', NULL),
(403, 27, 1, 4, NULL, NULL, '2020-12-15 11:01:20', '2020-12-15 11:01:20', NULL),
(404, 27, NULL, NULL, 6, NULL, '2020-12-15 11:01:20', '2020-12-15 11:01:20', NULL),
(406, 26, 1, 4, NULL, NULL, '2020-12-15 11:04:11', '2020-12-15 11:04:11', NULL),
(407, 26, NULL, NULL, 6, NULL, '2020-12-15 11:04:11', '2020-12-15 11:04:11', NULL),
(410, 31, 1, 4, NULL, NULL, '2020-12-15 11:09:00', '2020-12-15 11:09:00', NULL),
(411, 31, NULL, NULL, 6, NULL, '2020-12-15 11:09:00', '2020-12-15 11:09:00', NULL),
(415, 33, 1, 4, NULL, NULL, '2020-12-15 11:34:29', '2020-12-15 11:34:29', NULL),
(416, 33, NULL, NULL, 6, NULL, '2020-12-15 11:34:29', '2020-12-15 11:34:29', NULL),
(419, 36, 1, 4, NULL, NULL, '2020-12-15 14:09:57', '2020-12-15 14:09:57', NULL),
(420, 36, NULL, NULL, 12, NULL, '2020-12-15 14:09:57', '2020-12-15 14:09:57', NULL),
(421, 37, 1, 4, NULL, NULL, '2020-12-15 14:11:46', '2020-12-15 14:11:46', NULL),
(422, 37, NULL, NULL, 12, NULL, '2020-12-15 14:11:46', '2020-12-15 14:11:46', NULL),
(423, 38, 1, 4, NULL, NULL, '2020-12-15 14:13:16', '2020-12-15 14:13:16', NULL),
(424, 38, NULL, NULL, 12, NULL, '2020-12-15 14:13:16', '2020-12-15 14:13:16', NULL),
(425, 39, 1, 4, NULL, NULL, '2020-12-15 14:14:25', '2020-12-15 14:14:25', NULL),
(426, 39, NULL, NULL, 12, NULL, '2020-12-15 14:14:25', '2020-12-15 14:14:25', NULL),
(427, 40, 1, 4, NULL, NULL, '2020-12-15 14:15:35', '2020-12-15 14:15:35', NULL),
(428, 40, NULL, NULL, 12, NULL, '2020-12-15 14:15:35', '2020-12-15 14:15:35', NULL),
(429, 41, 1, 4, NULL, NULL, '2020-12-15 14:17:04', '2020-12-15 14:17:04', NULL),
(430, 41, NULL, NULL, 12, NULL, '2020-12-15 14:17:04', '2020-12-15 14:17:04', NULL),
(431, 42, 1, 4, NULL, NULL, '2020-12-15 14:18:33', '2020-12-15 14:18:33', NULL),
(432, 42, NULL, NULL, 12, NULL, '2020-12-15 14:18:33', '2020-12-15 14:18:33', NULL),
(433, 43, 1, 4, NULL, NULL, '2020-12-15 14:19:44', '2020-12-15 14:19:44', NULL),
(434, 43, NULL, NULL, 12, NULL, '2020-12-15 14:19:44', '2020-12-15 14:19:44', NULL),
(435, 44, 1, 4, NULL, NULL, '2020-12-15 14:21:04', '2020-12-15 14:21:04', NULL),
(436, 44, NULL, NULL, 12, NULL, '2020-12-15 14:21:04', '2020-12-15 14:21:04', NULL),
(437, 45, 1, 4, NULL, NULL, '2020-12-15 14:45:27', '2020-12-15 14:45:27', NULL),
(438, 45, NULL, NULL, 12, NULL, '2020-12-15 14:45:27', '2020-12-15 14:45:27', NULL),
(439, 46, 1, 4, NULL, NULL, '2020-12-15 14:46:24', '2020-12-15 14:46:24', NULL),
(440, 46, NULL, NULL, 12, NULL, '2020-12-15 14:46:24', '2020-12-15 14:46:24', NULL),
(441, 47, 1, 4, NULL, NULL, '2020-12-15 15:56:06', '2020-12-15 15:56:06', NULL),
(442, 47, NULL, NULL, 7, NULL, '2020-12-15 15:56:06', '2020-12-15 15:56:06', NULL),
(449, 48, 1, 4, NULL, NULL, '2021-01-07 00:53:59', '2021-01-07 00:53:59', NULL),
(450, 48, NULL, NULL, 7, NULL, '2021-01-07 00:53:59', '2021-01-07 00:53:59', NULL),
(455, 22, 1, 4, NULL, NULL, '2021-01-28 09:32:05', '2021-01-28 09:32:05', NULL),
(456, 22, NULL, NULL, 9, NULL, '2021-01-28 09:32:05', '2021-01-28 09:32:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `CouponCode` varchar(255) NOT NULL,
  `UsageCount` int(11) NOT NULL,
  `discount_type` int(11) DEFAULT '0' COMMENT '0 => "Percentage", 1=> "Amount"',
  `DiscountPercentage` decimal(10,2) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `CouponCode`, `UsageCount`, `discount_type`, `DiscountPercentage`, `IsActive`, `created_at`) VALUES
(6, 'ASD!', 0, 0, '10.00', 1, '2020-11-30 03:45:49');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_texts`
--

CREATE TABLE `coupon_texts` (
  `id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `lang` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupon_texts`
--

INSERT INTO `coupon_texts` (`id`, `coupon_id`, `Title`, `Description`, `lang`) VALUES
(16, 6, 'ASD', 'AD', 2),
(15, 6, 'ASD', 'AD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `footer_menus`
--

CREATE TABLE `footer_menus` (
  `id` int(11) NOT NULL,
  `url` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>active 0 => deactivate',
  `list_order` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_menus`
--

INSERT INTO `footer_menus` (`id`, `url`, `is_active`, `list_order`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'products', 1, 2, 1, '2020-08-19 07:35:36', '2020-10-19 04:08:28', NULL),
(3, 'contact-us', 1, 8, 1, '2020-10-16 02:43:14', '2020-12-11 20:23:31', NULL),
(7, 'http://richhouse.me/demo/product/news-in', 1, 1, 1, '2020-12-04 16:22:16', '2020-12-04 16:22:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `footer_menu_bottoms`
--

CREATE TABLE `footer_menu_bottoms` (
  `id` int(11) NOT NULL,
  `url` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>active 0 => deactivate',
  `list_order` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_menu_bottoms`
--

INSERT INTO `footer_menu_bottoms` (`id`, `url`, `is_active`, `list_order`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'terms_and_condition', 1, 2, 1, '2020-08-19 07:35:36', '2020-10-19 04:08:51', NULL),
(2, 'privacy_policy', 1, 3, 1, '2020-10-08 04:55:45', '2020-10-19 04:09:01', NULL),
(3, 'faqs', 1, 1, 1, '2020-10-08 04:56:37', '2020-10-19 04:08:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `footer_menu_bottom_texts`
--

CREATE TABLE `footer_menu_bottom_texts` (
  `id` int(11) NOT NULL,
  `footer_bottom_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_menu_bottom_texts`
--

INSERT INTO `footer_menu_bottom_texts` (`id`, `footer_bottom_id`, `title`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(29, 3, 'FAQs', 1, NULL, NULL, NULL),
(30, 3, 'أسئلة وأجوبة', 2, NULL, NULL, NULL),
(31, 1, 'Terms and conditions', 1, NULL, NULL, NULL),
(32, 1, 'الأحكام والشروط', 2, NULL, NULL, NULL),
(33, 2, 'Privacy policy', 1, NULL, NULL, NULL),
(34, 2, 'سياسة خاصة', 2, NULL, NULL, NULL),
(35, 4, 'About Us', 1, NULL, NULL, NULL),
(36, 4, 'سياسة خاصة', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `footer_menu_texts`
--

CREATE TABLE `footer_menu_texts` (
  `id` int(11) NOT NULL,
  `footer_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_menu_texts`
--

INSERT INTO `footer_menu_texts` (`id`, `footer_id`, `title`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(33, 2, 'About Rich House', 1, NULL, NULL, NULL),
(34, 2, 'حول Rich House', 2, NULL, NULL, NULL),
(35, 1, 'Explore', 1, NULL, NULL, NULL),
(36, 1, 'يكتشف', 2, NULL, NULL, NULL),
(47, 7, 'New In', 1, NULL, NULL, NULL),
(48, 7, 'الجديد في', 2, NULL, NULL, NULL),
(51, 3, 'Contact Us', 1, NULL, NULL, NULL),
(52, 3, 'اتصل بنا', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `product_id`, `image`, `updated_at`, `deleted_at`) VALUES
(38, 22, '1607951450776_Black Essence-01_1607951463.jpg', '2020-12-14 18:11:20', NULL),
(41, 23, '1607958329812_Black Essence-02_1607958343.jpg', '2020-12-14 20:05:50', NULL),
(42, 24, '1608010877495_Black Essence-03_1608010890.jpg', '2020-12-15 10:46:09', NULL),
(43, 25, '1608011228451_Black Essence-04_1608011241.jpg', '2020-12-15 10:48:48', NULL),
(44, 26, '1608011593030_Black Essence-05_1608011606.jpg', '2020-12-15 10:54:46', NULL),
(45, 27, '1608011959491_Black Essence-07_1608011972.jpg', '2020-12-15 11:01:20', NULL),
(46, 31, '1608012454136_Black Essence-06_1608012467.jpg', '2020-12-15 11:09:00', NULL),
(47, 33, '1608012565365_Black Essence-08_1608012578.jpg', '2020-12-15 11:12:11', NULL),
(48, 36, '1608023083623_cupidsqr-01_1608023097.jpg', '2020-12-15 14:09:57', NULL),
(49, 37, '1608023420748_cupidsqr-02_1608023434.jpg', '2020-12-15 14:11:46', NULL),
(50, 38, '1608023504319_cupidsqr-03_1608023517.jpg', '2020-12-15 14:13:16', NULL),
(51, 39, '1608023595830_cupidsqr-04_1608023609.jpg', '2020-12-15 14:14:25', NULL),
(52, 40, '1608023663069_cupidsqr-05_1608023676.jpg', '2020-12-15 14:15:35', NULL),
(53, 41, '1608023733910_cupidsqr-06_1608023747.jpg', '2020-12-15 14:17:04', NULL),
(54, 42, '1608023820507_cupidsqr-07_1608023833.jpg', '2020-12-15 14:18:33', NULL),
(55, 43, '1608023911884_cupidsqr-08_1608023925.jpg', '2020-12-15 14:19:44', NULL),
(56, 44, '1608023983602_cupidsqr-09_1608023997.jpg', '2020-12-15 14:21:04', NULL),
(57, 45, '1608025462702_cupidsqr-10_1608025476.jpg', '2020-12-15 14:45:27', NULL),
(58, 46, '1608025523915_cupidsqr-11_1608025537.jpg', '2020-12-15 14:46:24', NULL),
(59, 47, '1608029598830_b_o-01_1608029612.jpg', '2020-12-15 15:56:06', NULL),
(60, 48, '1608029772571_b_o-02_1608029786.jpg', '2020-12-15 15:58:29', NULL),
(61, 48, '1609998587524_1608120767270_LAPERLA_1609998587.jpg', '2021-01-07 00:49:55', NULL),
(62, 48, '1609998669474_1608120673410_jz-01_1609998669.jpg', '2021-01-07 00:51:16', NULL),
(63, 48, '', '2021-01-07 00:51:16', NULL),
(64, 48, '1609998836379_1608120159367_bnw-05_1609998836.jpg', '2021-01-07 00:53:59', NULL),
(65, 48, '', '2021-01-07 00:53:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `name`, `email`, `phone`, `message`, `updated_at`, `deleted_at`) VALUES
(1, 'Test Lead', 'test@gmail.com', '123456789', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2020-10-16 03:23:45', NULL),
(2, 'waqar', 'waqaryounus277@gmail.com', '345313853', 'dasfasdff', '2020-12-02 12:08:32', NULL),
(3, 'accc', 'waqaryounus277@gmail.com', '345313853', 'abc testing', '2020-12-02 12:09:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_06_22_124838_create_permission_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 3),
(2, 'App\\User', 4);

-- --------------------------------------------------------

--
-- Table structure for table `notification_emails`
--

CREATE TABLE `notification_emails` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `uppdated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `billing_name` varchar(255) NOT NULL,
  `billing_email` varchar(255) NOT NULL,
  `billing_phone` varchar(255) NOT NULL,
  `billing_country` varchar(255) DEFAULT NULL,
  `billing_address` text,
  `billing_city` varchar(255) DEFAULT NULL,
  `billing_state` varchar(255) DEFAULT NULL,
  `billing_zip` varchar(255) DEFAULT NULL,
  `billing_message` text,
  `billing_wrapping` int(11) DEFAULT NULL,
  `shipping_name` varchar(255) DEFAULT NULL,
  `shipping_email` varchar(255) DEFAULT NULL,
  `shipping_phone` varchar(255) DEFAULT NULL,
  `shipping_country` varchar(255) DEFAULT NULL,
  `shipping_address` text,
  `shipping_city` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_zip` varchar(255) DEFAULT NULL,
  `has_shipping_address` tinyint(1) NOT NULL DEFAULT '0',
  `tax` int(11) DEFAULT '0',
  `subtotal` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `has_payment` int(11) DEFAULT '0' COMMENT '0=>non ,1=>payfort, 2=>cod',
  `shipping_charges` double DEFAULT NULL,
  `shipping_method` varchar(255) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0' COMMENT '0 => "Processing",  1=>"Shipping", 2 => "Delivered", 3=> "Cancelled"',
  `payfort_id` text,
  `customer_view` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `billing_name`, `billing_email`, `billing_phone`, `billing_country`, `billing_address`, `billing_city`, `billing_state`, `billing_zip`, `billing_message`, `billing_wrapping`, `shipping_name`, `shipping_email`, `shipping_phone`, `shipping_country`, `shipping_address`, `shipping_city`, `shipping_state`, `shipping_zip`, `has_shipping_address`, `tax`, `subtotal`, `total`, `has_payment`, `shipping_charges`, `shipping_method`, `coupon_id`, `user_id`, `status`, `payfort_id`, `customer_view`, `updated_at`, `deleted_at`) VALUES
(1, 'Shaikh Abbas', 'abbas@gmail.com', '0314564565', '59', 'ABCD . block, Ab Street, House No 1', 'Karachi', 'Sindh', '75400', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5, 0, 25, 2, 25, NULL, 0, 2, 3, '0', 0, '2021-02-01 03:53:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `variant_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `amount` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `our_stories`
--

CREATE TABLE `our_stories` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `our_stories`
--

INSERT INTO `our_stories` (`id`, `user_id`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 1, '1611838182183_1608120767270_LAPERLA.jpg', '2021-01-28 06:46:48', '2021-01-28 07:49:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `our_story_texts`
--

CREATE TABLE `our_story_texts` (
  `id` int(11) NOT NULL,
  `our_story_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `lang` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `our_story_texts`
--

INSERT INTO `our_story_texts` (`id`, `our_story_id`, `title`, `content`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(15, 7, '01 penty', 'is a leading fragrance company in the Middle East & Africa, with a portfolio of more than 24 luxury niche brands.\r\n\r\nOur website www.richhouse.me is owned and managed by the company RICH HOUSE PERFUMES & COSMETICS TRADING LLC.', 1, NULL, NULL, NULL),
(16, 7, 'بنت', 'ريتش هاوس هي شركة عطور رائدة في الشرق الأوسط وأفريقيا،\r\n\r\nمع مجموعة تضم أكثر من 24 علامة تجارية متخصصة فاخرة. إن موقعنا الإلكتروني www.richhouse.me مملوك ومدار من قبل شركة RICH HOUSE PERFUMES & COSMETICS TRADING LLC.', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `page_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_name`, `created_at`, `deleted_at`) VALUES
(1, 'faqs', '2020-11-12 05:18:04', NULL),
(2, 'terms_and_condition', '2020-11-12 05:18:23', NULL),
(3, 'privacy_policy', '2020-11-12 05:18:39', NULL),
(4, 'about_us', '2020-11-12 05:18:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_texts`
--

CREATE TABLE `page_texts` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lang` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page_texts`
--

INSERT INTO `page_texts` (`id`, `page_id`, `title`, `content`, `lang`, `created_at`, `deleted_at`) VALUES
(33, 4, 'About Us', '<p style=\"text-align: center;\">                  RichHouse is a leading fragrance company in the Middle East &amp; Africa, with a portfolio of more than 24 luxury niche brands.\r\n</p><p style=\"text-align: center;\">\r\nOur website www.richhouse.me is owned and managed by the company RICH HOUSE PERFUMES &amp; COSMETICS TRADING LLC.\r\n                  </p>', 1, '2020-11-12 06:43:20', NULL),
(34, 4, 'معلومات عنا', '<p style=\"text-align: center; \">                  ريتش هاوس هي شركة عطور رائدة في الشرق الأوسط وأفريقيا،</p><p style=\"text-align: center;\"> مع مجموعة تضم أكثر من 24 علامة تجارية متخصصة فاخرة.\r\n\r\nإن موقعنا الإلكتروني www.richhouse.me مملوك ومدار من قبل شركة RICH HOUSE PERFUMES &amp; COSMETICS TRADING LLC.\r\n                  </p>', 2, '2020-11-12 06:43:20', NULL),
(41, 1, 'FAQ', '', 1, '2020-11-12 06:49:38', NULL),
(42, 1, 'التعليمات', '', 2, '2020-11-12 06:49:38', NULL),
(47, 2, 'Terms And Condition', '<div>The general Terms and Conditions of Sale detailed below govern the contractual relationship between the ‘User’ and www.richhouse.me, belonging to the company <b>RICH HOUSE PERFUMES &amp; COSMETICS TRADING LL</b>C, Both parties accept these Conditions unreservedly. These general Conditions of Sale are the only conditions that are applicable and replace all other conditions, except in the case of express. written, prior dispensation. We maintain that, by confirming your order, you have read and do unreservedly accept our general Conditions of Sale. These Terms and Conditions of Sale are important to you and RICH HOUSE PERFUMES &amp; COSMETICS TRADING LLC, as they are used to protect your rights as a valued customer and our rights as a business.</div><div>Changes to the Terms of Service and the Website</div><div><br></div><div><b>RICH HOUSE PERFUMES &amp; COSMETICS TRADING LLC</b> website’s terms of service is constantly updated. We reserve the right to update the Website and these Terms of Service from time to time, at our discretion and without any notice. It is solely your responsibility to keep up-to-date with the latest Terms and Conditions by checking back regularly. Your continued use of the Website following the publishing of updated Terms of Service will be taken to mean that you have read and agree to the changes.</div><div><br></div><div><b>Access to the Website</b></div><div>We work hard to ensure the Website is always up and available, but we can\'t guarantee that the Website will not have downtime for any reason. We reserve the right to close the Website for short periods of time for general maintenance, but will attempt to keep this to a minimum. We will not be liable if for any reason all or any part of the Website is unavailable at any time, for any length of time.</div><div><br></div><div>Parts of the Website require you to input a password to access certain features. This is to be able to give you details relevant to your order &amp; order tracking whilst still keeping them private to you. In order to take advantage of personalized feature, we recommend you to register an account with our website, by entering your email and choosing a secure password. You can also utilize the website’s “Guest Check-out” option. We highly recommend that you choose a strong password, and you log out from your account at the end of every session. There is a password reset procedure in case you forget your password. You must treat your account log-in information as confidential, not disclosing it to any third party. If you think there may have been any breach of security then it is your responsibility to notify us immediately and if log-in is possible, to change your password.</div><div><br></div><div>It is a condition of your use of the Website that all the information you provide on the Website is correct, current and complete. We reserve the right to disable any user account in our sole discretion, at any time for any or no reason, including if, in our opinion, you have failed to comply with any provision of these Terms of Service.</div><div><br></div><div>We do not guarantee that the Website or any content provided on the Website is error free. We manage your personal data according to our Privacy Policy.</div><div>Product information Richhouse.me, takes great care when putting product information, descriptions and images on-line but will not be held responsible for any mistakes or omissions to any information given.</div><div><br></div><div><b>Prices</b></div><div>The prices indicated on richhouse.me, are shown in AED and do not include delivery. The delivery costs are clearly shown and invoiced at the end of the order in addition to the price of the products. We reserve the right to modify our prices at any moment but this will be indicated to you on the order at the time the order is placed. If an obviously incorrect price appears on our website and is not corrected on your total at the time of order completion, for whatever reason (human error, technical error etc.), your order will be cancelled and the payment refunded to you, even if it has been initially validated.</div><div><br></div><div><b>Order</b></div><div>When you place an Order with us, then subject to your rights to cancel or return the items, you commit to buy the items described in that Order, at the price indicated including any delivery fees, taxes and duties where applicable.</div><div><br></div><div>Orders are subject to our acceptance, which we may withhold in our sole discretion including for reasons such as ineligibility, inability to confirm payment authorization, suspected fraud, shipping restrictions and stock availability. Items in your Website basket are not reserved until your Order is paid for.</div><div>Receipt of your Order will be acknowledged by email. However, the Order is only confirmed when you receive a notification from us confirming dispatch of the relevant item(s). No party other than us has the authority to confirm acceptance of the Order.</div><div><br></div><div>Order processing time is 24 to 48 hours from the time your order is paid and confirmed.&nbsp;</div><div><br></div><div><b>Availability</b></div><div>We will always endeavor to fulfil your order once completed and paid for as per our order regulations. If a product should become unavailable after your order has been confirmed and paid for, we will refund the price you paid for the product within 7 days following the date of payment. If your order includes products that are temporarily unavailable, we will send the available products out first and will follow with the outstanding items once they are available again.&nbsp;</div><div><br></div><div><b>Complimentary Gift with Purchase</b></div><div>We hope you enjoy your free gift, complimentary with purchases made www.richhouse.me. Please be aware that items purchased on the site, will be non-returnable if the free gift has been opened or if the packaging has been tampered with.</div><div><br></div><div><b>Secure payment</b></div><div>Richhouse, currently offers multiple ways to pay for your order, Visa, Master Card, Apple Pay and PayPal. We may request additional personal documents, such as a passport copy or ID card with photo, in order to confirm your order. These payment options use advanced SSL encryption to keep your transaction secure and do not cost you anything to use Richhouse, is charged as the seller, you are not charged as the buyer so you will not pay any extra for your order.</div><div>We also offer Cash On Delivery within the United Arab Emirates.&nbsp;</div><div><br></div><div>•<span style=\"white-space:pre\">	</span>Any dispute or claim arising out of or in connection with this website shall be governed and construed in accordance with the laws of UAE.</div><div>•<span style=\"white-space:pre\">	</span>United Arab of Emirates is our country of domicile.</div><div>•<span style=\"white-space:pre\">	</span>Minors under the age of 18 shall are prohibited to register as a User of this website and are not allowed to transact or use the website.</div><div>•<span style=\"white-space:pre\">	</span>If you make a payment for our products or services on our website, the details you are asked to submit will be provided directly to our payment provider via a secured connection.</div><div>•<span style=\"white-space:pre\">	</span>The cardholder must retain a copy of transaction records and Merchant policies and rules.</div><div>•<span style=\"white-space:pre\">	</span>We accept payments online using Visa and MasterCard credit/debit cards.</div><div>•<span style=\"white-space:pre\">	</span>www.richhouse.me&nbsp;will&nbsp;NOT deal or provide any services or products to any of OFAC (Ofﬁce of Foreign Assets Control) sanctions countries in accordance with the law of UAE.</div><div>•<span style=\"white-space:pre\">	</span>Multiple transactions may result in multiple postings to the cardholder’s monthly statement.</div><div><br></div><div><b>Delivery</b></div><div>Your order will be delivered to the address you indicated when your order was placed. The products bought on richhouse.me, will be delivered to specified delivery address confirmed while placing the order.</div><div><br></div><div>Richhouse.me, does everything in its power to respect the delivery times indicated on the website. We cannot, however, be held responsible for the consequences of a late delivery or the loss of a package caused by a third-party contracted to make delivery, or by you, or because of some unforeseen force majeure. In the event where you do not receive your package, an investigation will be conducted with the carrier and may take several days upon receipt of your claim. During this period of investigation, no reimbursement or re-delivery will take place.</div><div><br></div><div><b>Delivery Charges</b></div><div>Delivery charges are 30 AED for all deliveries within U.A.E</div><div>International shipping charges are calculated by DHL on order checkout.&nbsp;</div>', 1, '2020-12-07 08:44:19', NULL),
(48, 2, 'أحكام وشروط', '<div><div>المدفوع ، سنعيد لك السعر الذي دفعته مقابل المنتج في غضون 7 أيام من تاريخ الدفع. إذا كان طلبك يتضمن منتجات غير متوفرة مؤقتًا ، فسنرسل المنتجات المتاحة أولاً وسنتابع مع العناصر المعلقة بمجرد توفرها مرة أخرى.</div><div>هدية مجانية عند الشراء</div><div>نأمل أن تستمتع بهديتك المجانية ، مجانًا مع عمليات الشراء التي تتم على www.richhouse.me. يرجى العلم أن العناصر التي تم شراؤها على الموقع ، لن يتم إرجاعها إذا تم فتح الهدية المجانية أو إذا تم العبث بالعبوة.</div><div>دفع امن</div><div>تقدم Richhouse حاليًا طرقًا متعددة للدفع مقابل طلبك ، Visa و Master Card و Apple Pay و PayPal. قد نطلب مستندات شخصية إضافية ، مثل نسخة من جواز السفر أو بطاقة الهوية مع صورة لتأكيد طلبك. تستخدم خيارات الدفع هذه تشفير SSL متقدمًا للحفاظ على أمان معاملتك ولا تكلفك أي شيء لاستخدام Richhouse ، ويتم تحصيل رسوم منك كبائع ، ولا يتم تحصيل رسوم منك كمشتري لذلك لن تدفع أي مبلغ إضافي مقابل طلبك.</div><div>كما نقدم خدمة الدفع عند الاستلام داخل دولة الإمارات العربية المتحدة.</div><div><br></div><div>• يخضع أي نزاع أو مطالبة تنشأ عن أو فيما يتعلق بهذا الموقع الإلكتروني ويتم تفسيره وفقًا لقوانين دولة الإمارات العربية المتحدة.</div><div>• الإمارات العربية المتحدة هي بلد إقامتنا.</div><div>• يحظر على القاصرين الذين تقل أعمارهم عن 18 عامًا التسجيل كمستخدم لهذا الموقع ولا يُسمح لهم بالتعامل أو استخدام الموقع.</div><div>• إذا قمت بالدفع مقابل منتجاتنا أو خدماتنا على موقعنا الإلكتروني ، فسيتم تقديم التفاصيل التي يُطلب منك إرسالها مباشرةً إلى مزود الدفع الخاص بنا عبر اتصال آمن.</div><div>• يجب على حامل البطاقة الاحتفاظ بنسخة من سجلات المعاملات وسياسات التاجر وقواعده.</div><div>• نقبل الدفع عبر الإنترنت باستخدام بطاقات الائتمان / الخصم Visa و MasterCard.</div><div>• لن يتعامل موقع www.richhouse.com أو يقدم أي خدمات أو منتجات إلى أي من الدول التي تخضع لعقوبات من مكتب مراقبة الأصول الأجنبية وفقًا لقانون دولة الإمارات العربية المتحدة.</div><div>• قد تؤدي المعاملات المتعددة إلى عمليات ترحيل متعددة في كشف الحساب الشهري لحامل البطاقة.</div><div><br></div><div>توصيل</div><div>سيتم توصيل طلبك إلى العنوان الذي أشرت إليه عند تقديم طلبك. سيتم تسليم المنتجات التي تم شراؤها على richhouse.me إلى عنوان التسليم المحدد الذي تم تأكيده أثناء تقديم الطلب.</div><div>Richhouse.me يبذل كل ما في وسعه لاحترام مواعيد التسليم الموضحة على الموقع. ومع ذلك ، لا يمكننا أن نتحمل المسؤولية عن عواقب التسليم المتأخر أو فقدان الطرد بسبب تعاقد طرف ثالث للتسليم ، أو من قبلك ، أو بسبب بعض القوة القاهرة غير المتوقعة. في حالة عدم استلام الطرد الخاص بك ، سيتم إجراء تحقيق مع الناقل وقد يستغرق عدة أيام عند استلام مطالبتك. خلال فترة التحقيق هذه ، لن يتم سداد التكاليف أو إعادة التسليم.</div><div>رسوم التوصيل:</div><div>رسوم التوصيل 30 درهمًا إماراتيًا لجميع عمليات التوصيل داخل دولة الإمارات العربية المتحدة</div><div>يتم احتساب رسوم الشحن الدولي بواسطة DHL عند الخروج من الطلب.</div></div>', 2, '2020-12-07 08:44:19', NULL),
(49, 3, 'Privacy Policy', '<div>This Privacy and Cookie Policy applies to information collected and processed via www.richhouse.me website. We take your privacy very seriously and therefore want you to know how your data is collected, used, shared and stored.&nbsp;</div><div><br></div><div>This policy has been compiled to better serve those who are concerned with how their \'Personally Identifiable Information\' (PII) is being used online. PII, as described in privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</div><div><br></div><div>By visiting the Website or supplying information through its associated customer service channels, you are consenting to the practices described in this Privacy and Cookie Policy and our Website Terms and Conditions. Please note that our Privacy &amp; Cookie Policy is updated from time to time and that you will be bound by the most recent update of the policy appearing on our Website. It is therefore your responsibility to continue to check our Privacy and Cookie Policy for updates.</div><div><br></div><div><br></div><div>What personal information do we collect from the people that visit our blog, website or app?</div><div><br></div><div>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number, credit card information or other details to help you with your experience.</div><div><br></div><div>When do we collect information?</div><div><br></div><div>We collect information from you when you register on our site, place an order or enter information on our site.</div><div><br></div><div>How do we use your information?</div><div><br></div><div>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</div><div>&nbsp; &nbsp; &nbsp; • To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</div><div>&nbsp; &nbsp; &nbsp; • To allow us to better service you in responding to your customer service requests.</div><div>&nbsp; &nbsp; &nbsp; • To administer a contest, promotion, survey or other site feature.</div><div>&nbsp; &nbsp; &nbsp; • To quickly process your transactions.</div><div>&nbsp; &nbsp; &nbsp; • To ask for ratings and reviews of services or products</div><div><br></div><div>How do we protect your information?</div><div><br></div><div>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.</div><div>We use regular Malware Scanning.</div><div>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</div><div><br></div><div>We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.</div><div><br></div><div>All transactions are processed through a gateway provider and are not stored or processed on our servers.</div><div><br></div><div>•<span style=\"white-space:pre\">	</span>All credit/debit cards detail and personally identiﬁable information will NOT be stored, sold, shared, rented or leased to any third parties.</div><div>•<span style=\"white-space:pre\">	</span>The Website Policies and Terms &amp; Conditions may be changed or updated occasionally to meet the requirements and standards. Therefore, the Customers’ are encouraged to frequently visit these sections in order to be updated about the changes on the website. Modiﬁcations will be effective on the day they are posted.</div><div>•<span style=\"white-space:pre\">	</span>Some of the advertisements you see on the Site are selected and delivered about you and your online activities, either on the Site or on other websites, through cookies, web beacons, and other technologies in an effort to understand your interests and deliver to your advertisements that are tailored to your interests. Please remember that we do not have access to, or control over, the information these third parties may collect. The information practices of these third parties are not covered by this privacy policy. Kindly remove any other statements that contradicts with the above statements.</div><div><br></div><div>Do we use \'cookies\'?</div><div><br></div><div>Yes, Our Website uses first party cookies (very small files that are sent by us to your computer or other access device) and other similar technologies which we can access when you visit our Website in the future. We may also use third party cookies.</div><div>For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</div><div><br></div><div>We use cookies to:</div><div><br></div><div>&nbsp; &nbsp; &nbsp; • Help remember and process the items in the shopping cart.</div><div>&nbsp; &nbsp; &nbsp; • Understand and save user\'s preferences for future visits.</div><div>&nbsp; &nbsp; &nbsp; • Compile data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.</div><div><br></div><div>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since each browser is different, look at your browser\'s Help Menu to learn the correct way to modify your cookies.</div><div><br></div><div>If you turn cookies off, certain features that make your site experience more efficient may not function properly. It won\'t affect the user\'s experience that make your site experience more efficient and may not function properly.</div><div><br></div><div>Who May We Share Information With</div><div>We may share your information with any of the following:</div><div>•<span style=\"white-space:pre\">	</span>Our group companies</div><div>•<span style=\"white-space:pre\">	</span>Service providers who perform services on our behalf, such as: payment processing, information technology, Website maintenance, surveys and market research, warehousing and logistics, couriers, advertising and marketing or any service required to process your Order</div><div>•<span style=\"white-space:pre\">	</span>Our professional services providers such as: legal, audit, financial and insurance advisers</div><div>•<span style=\"white-space:pre\">	</span>Any assignee, successor operator or prospective purchaser of the Website or of us</div><div>•<span style=\"white-space:pre\">	</span>Any legal, regulatory, law enforcement, government or municipal bodies</div><div>•<span style=\"white-space:pre\">	</span>Any gift recipient or provider of any warranty in relation to your Order</div><div>Third-party disclosure</div><div><br></div><div>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information.</div><div><br></div><div>Third-party links</div><div><br></div><div>We do not include or offer third-party products or services on our website.</div><div><br></div><div><div><b>Refund &amp; Cancellation Policy&nbsp;</b></div><div><br></div><div>Right of Return:</div><div>We don’t accept returns for perfumes. Unless the product has manufacturer, damage or has been damaged during shipping and retains its original packaging.</div><div>Right of refund and exchange:</div><div>If the product is returned according to the regulations above, a refund or exchange will be processed upon receiving the damaged product. A refund is issued via the same payment method used during purchase within 7 days.&nbsp;</div><div>Refunds will be done only through the Original Mode of Payment.</div><div>Cash on Delivery payments are refunded as store credit.</div></div><div><br></div><div><br></div>', 1, '2020-12-14 06:01:28', NULL),
(50, 3, 'سياسة خاصة', '<div><div>تحكم الشروط والأحكام العامة للبيع المفصلة أدناه العلاقة التعاقدية بين \"المستخدم\" و www.richhouse.me ، التابعة لشركة RICH HOUSE PERFUMES &amp; COSMETICS TRADING LLC ، يقبل كلا الطرفين هذه الشروط دون تحفظ. شروط البيع العامة هذه هي الشروط الوحيدة القابلة للتطبيق وتحل محل جميع الشروط الأخرى ، باستثناء حالة صريحة. إعفاء مكتوب مسبق. نحن نؤكد أنه بتأكيد طلبك ، تكون قد قرأت ووافقت دون تحفظ على شروط البيع العامة الخاصة بنا. تعتبر شروط وأحكام البيع هذه مهمة بالنسبة لك ولـ RICH HOUSE PERFUMES &amp; COSMETICS TRADING LLC ، حيث يتم استخدامها لحماية حقوقك كعميل مهم وحقوقنا كشركة.</div><div>التغييرات على شروط الخدمة والموقع الإلكتروني</div><div>يتم تحديث شروط خدمة موقع RICH HOUSE PERFUMES &amp; COSMETICS TRADING LLC باستمرار. نحتفظ بالحق في تحديث الموقع الإلكتروني وشروط الخدمة هذه من وقت لآخر ، وفقًا لتقديرنا وبدون أي إشعار. تقع على عاتقك وحدك مسؤولية البقاء على اطلاع دائم بأحدث الشروط والأحكام عن طريق التحقق مرة أخرى بانتظام. إن استمرارك في استخدام الموقع بعد نشر شروط الخدمة المحدثة سيعني أنك قرأت التغييرات ووافقت عليها.</div><div>الوصول إلى الموقع</div><div>نحن نعمل بجد للتأكد من أن موقع الويب دائمًا متاح ومتاح ، لكن لا يمكننا ضمان عدم توقف الموقع الإلكتروني لأي سبب من الأسباب. نحتفظ بالحق في إغلاق الموقع الإلكتروني لفترات قصيرة لإجراء الصيانة العامة ، ولكننا سنحاول تقليل ذلك إلى الحد الأدنى. لن نكون مسؤولين إذا كان لأي سبب من الأسباب عدم توفر كل أو جزء من الموقع في أي وقت ولأي فترة زمنية.</div><div>تتطلب أجزاء من الموقع إدخال كلمة مرور للوصول إلى ميزات معينة. هذا لكي تكون قادرًا على إعطائك التفاصيل ذات الصلة بطلبك وتتبع الطلبات مع الاحتفاظ بها خاصة بك. للاستفادة من الميزة المخصصة ، نوصيك بتسجيل حساب على موقعنا الإلكتروني ، عن طريق إدخال بريدك الإلكتروني واختيار كلمة مرور آمنة. يمكنك أيضًا استخدام خيار \"تسجيل خروج الضيف\" في موقع الويب. نوصي بشدة أن تختار كلمة مرور قوية ، وأن تقوم بتسجيل الخروج من حسابك في نهاية كل جلسة. هناك إجراء لإعادة تعيين كلمة المرور في حالة نسيان كلمة المرور الخاصة بك. يجب أن تعامل معلومات تسجيل الدخول إلى حسابك على أنها سرية ، ولا تفصح عنها لأي طرف ثالث. إذا كنت تعتقد أنه قد يكون هناك أي خرق للأمان ، فمن مسؤوليتك إخطارنا على الفور وإذا كان تسجيل الدخول ممكنًا ، لتغيير كلمة المرور الخاصة بك.</div><div>من شروط استخدامك للموقع أن تكون جميع المعلومات التي تقدمها على الموقع صحيحة وحديثة وكاملة. نحتفظ بالحق في تعطيل أي حساب مستخدم وفقًا لتقديرنا الخاص ، في أي وقت ولأي سبب أو بدون سبب ، بما في ذلك ، في رأينا ، إذا أخفقت في الامتثال لأي شرط من شروط الخدمة هذه.</div><div>نحن لا نضمن خلو موقع الويب أو أي محتوى متوفر على الموقع من الأخطاء. ندير بياناتك الشخصية وفقًا لسياسة الخصوصية الخاصة بنا.</div><div>معلومات المنتج</div><div>إن موقع Richhouse.me يهتم كثيرًا عند وضع معلومات المنتج وأوصافه وصورته على الإنترنت ، ولكنه لن يتحمل المسؤولية عن أي أخطاء أو سهو في أي معلومات يتم تقديمها.</div><div>الأسعار</div><div>الأسعار الموضحة على موقع richhouse.me معروضة بالدرهم الإماراتي ولا تشمل التوصيل. يتم عرض تكاليف التسليم بشكل واضح وفواتير في نهاية الطلب بالإضافة إلى سعر المنتجات. نحتفظ بالحق في تعديل أسعارنا في أي وقت ، ولكن سيتم توضيح ذلك لك في الطلب في وقت تقديم الطلب. إذا ظهر سعر غير صحيح بشكل واضح على موقعنا الإلكتروني ولم يتم تصحيحه على إجمالي المبلغ في وقت إكمال الطلب ، لأي سبب من الأسباب (خطأ بشري أو خطأ فني وما إلى ذلك) ، فسيتم إلغاء طلبك ورد المبلغ المدفوع إليك ، حتى لو تم التحقق من صحتها في البداية.</div><div>طلب</div><div>عند تقديم طلب معنا ، ثم يخضع لحقوقك في إلغاء أو إرجاع العناصر ، فإنك تلتزم بشراء العناصر الموضحة في هذا الطلب ، بالسعر المحدد بما في ذلك أي رسوم توصيل وضرائب ورسوم عند الاقتضاء.</div><div>تخضع الطلبات لقبولنا ، والتي قد نحجبها وفقًا لتقديرنا الخاص بما في ذلك لأسباب مثل عدم الأهلية وعدم القدرة على تأكيد تفويض الدفع والاحتيال المشتبه به وقيود الشحن وتوافر المخزون. لا يتم حجز العناصر الموجودة في سلة موقع الويب الخاص بك حتى يتم دفع ثمن طلبك.</div><div>سيتم الاعتراف باستلام طلبك عبر البريد الإلكتروني. ومع ذلك ، يتم تأكيد الطلب فقط عندما تتلقى إشعارًا منا يؤكد إرسال العنصر (العناصر) ذات الصلة. لا يوجد طرف آخر غيرنا لديه السلطة لتأكيد قبول الطلب.</div><div><br></div><div>تستغرق معالجة الطلب من 24 إلى 48 ساعة من وقت دفع وتأكيد طلبك.</div><div><br></div><div>&nbsp;التوفر</div><div>سنسعى دائمًا إلى تلبية طلبك بمجرد اكتماله ودفع ثمنه وفقًا للوائح الطلبات الخاصة بنا. إذا أصبح المنتج غير متاح بعد تأكيد طلبك و</div><div><br></div><div><b>سياسة الاسترداد والإلغاء</b></div><div><br></div><div>حق العودة:</div><div>نحن لا نقبل إرجاع العطور. ما لم يكن للمنتج مُصنِّع ، يتعرض المنتج للتلف أو التلف أثناء الشحن ويحتفظ بعبوته الأصلية.</div><div>حق الاسترداد والاستبدال:</div><div>إذا تم إرجاع المنتج وفقًا للوائح المذكورة أعلاه ، فستتم معالجة استرداد أو استبدال المنتج عند استلام المنتج التالف. يتم إصدار رد الأموال بنفس طريقة الدفع المستخدمة أثناء الشراء في غضون 7 أيام.</div><div>سيتم استرداد المبالغ فقط من خلال طريقة الدفع الأصلية.</div><div>يتم رد مدفوعات الدفع عند الاستلام كرصيد في المتجر.</div></div>', 2, '2020-12-14 06:01:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`, `created_at`) VALUES
(1, 'abbas@gmail.com', 'vR2yrgVmQpPG1eT41Xj02pDAZHB0XW', '2021-01-29 04:41:17');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `variant_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `price`, `product_id`, `variant_id`, `updated_at`, `deleted_at`) VALUES
(114, '10.00', 14, NULL, '2020-12-14 15:47:51', NULL),
(159, '1073.00', 23, NULL, '2020-12-14 20:05:50', NULL),
(160, '1073.00', 24, NULL, '2020-12-15 10:46:09', NULL),
(161, '1073.00', 25, NULL, '2020-12-15 10:48:48', NULL),
(169, '750.00', 27, NULL, '2020-12-15 11:01:20', NULL),
(171, '750.00', 26, NULL, '2020-12-15 11:04:11', NULL),
(174, '750.00', 31, NULL, '2020-12-15 11:09:00', NULL),
(177, '790.00', 33, NULL, '2020-12-15 11:34:29', NULL),
(180, '850.00', 36, NULL, '2020-12-15 14:09:57', NULL),
(181, '850.00', 37, NULL, '2020-12-15 14:11:46', NULL),
(182, '850.00', 38, NULL, '2020-12-15 14:13:16', NULL),
(183, '850.00', 39, NULL, '2020-12-15 14:14:25', NULL),
(184, '850.00', 40, NULL, '2020-12-15 14:15:35', NULL),
(185, '750.00', 41, NULL, '2020-12-15 14:17:04', NULL),
(186, '750.00', 42, NULL, '2020-12-15 14:18:33', NULL),
(187, '750.00', 43, NULL, '2020-12-15 14:19:44', NULL),
(188, '750.00', 44, NULL, '2020-12-15 14:21:04', NULL),
(189, '750.00', 45, NULL, '2020-12-15 14:45:27', NULL),
(190, '750.00', 46, NULL, '2020-12-15 14:46:24', NULL),
(191, '650.00', 47, NULL, '2020-12-15 15:56:06', NULL),
(195, '650.00', 48, NULL, '2021-01-07 00:53:59', NULL),
(198, '1156.00', 22, NULL, '2021-01-28 09:32:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `permalink` text NOT NULL,
  `weight_value` text,
  `weight_type_id` int(20) NOT NULL,
  `discount_type` int(11) DEFAULT '0' COMMENT '0 => "Percentage", 1=> "Amount"',
  `discount` int(11) NOT NULL DEFAULT '0',
  `variant_has_price` int(11) DEFAULT NULL COMMENT 'variant_id',
  `best_seller` int(11) DEFAULT '0',
  `exclusive` int(11) DEFAULT '0',
  `new_in` int(11) DEFAULT '0',
  `inventory` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `brand_id`, `permalink`, `weight_value`, `weight_type_id`, `discount_type`, `discount`, `variant_has_price`, `best_seller`, `exclusive`, `new_in`, `inventory`, `updated_at`, `deleted_at`) VALUES
(22, 7, 'Amorino_Black_Essence', '100', 1, 0, 0, 1, 1, 1, 1, 0, '2021-01-28 09:32:05', NULL),
(23, 7, 'Product-23', '100', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-14 20:05:50', NULL),
(24, 7, 'Product-24', '100', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 10:46:09', NULL),
(25, 7, 'Product-25', '100', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 10:48:48', NULL),
(26, 7, 'Amorino_Gold_Feel_Me', '50', 1, 0, 0, 1, 0, 0, 0, 1, '2020-12-15 11:04:10', NULL),
(27, 7, 'Product-27', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 11:01:20', NULL),
(31, 7, 'Product-31', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 11:09:00', NULL),
(33, 7, 'Amorino_Imperial_Oud', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 11:34:29', NULL),
(36, 10, 'cupid_1177', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:09:57', NULL),
(37, 10, 'Product-37', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:11:46', NULL),
(38, 10, 'Product-38', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:13:16', NULL),
(39, 10, 'Product-39', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:14:25', NULL),
(40, 10, 'Product-40', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:15:35', NULL),
(41, 10, 'Product-41', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:17:04', NULL),
(42, 10, 'Product-42', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:18:33', NULL),
(43, 10, 'Product-43', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:19:44', NULL),
(44, 10, 'Product-44', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:21:04', NULL),
(45, 10, 'Product-45', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:45:27', NULL),
(46, 10, 'Product-46', '50', 1, 0, 0, 1, 0, 0, 0, 0, '2020-12-15 14:46:24', NULL),
(47, 8, 'Product-47', '60', 1, 0, 0, 1, 0, 0, 0, 10, '2021-01-04 03:31:23', NULL),
(48, 8, 'Product-48', '60', 1, 0, 0, 1, 0, 0, 0, 19, '2021-01-20 08:06:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_texts`
--

CREATE TABLE `product_texts` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `short_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `brief_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ingredients` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `notes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `lang` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_texts`
--

INSERT INTO `product_texts` (`id`, `product_id`, `title`, `short_description`, `brief_description`, `ingredients`, `notes`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(233, 23, 'Black Secret', '-', '“\r\nA fragrance of great warmth and sensuality Without being overly\r\nsweet, without being either overly resinous, a continuous sparkle of\r\nsweetness lives all the time', '-', 'Top Notes\r\nFruity & Honey notes\r\n\r\n\r\nMiddle Notes\r\nAmber & Floral notes\r\n\r\n\r\nBase Notes\r\nVanilla, Musk', 1, NULL, NULL, NULL),
(234, 23, 'Black Secret', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(235, 24, 'One More Love', 'Eau De Parfum', '“Savour the sensual sweetness, love, passion and sentimentality.\r\nA wonderful concoction that will stimulate your sences.\r\nFeel the love over and over again.”', '-', 'Top Notes\r\nOrange, Orange Blossom, Bergamot\r\n\r\nMiddle Notes\r\nMimosa, Jasmine, Turkish Rose, Ylang ylang\r\n\r\nBase Notes\r\nTonka beans, Patchouli, Opoponax,\r\nVanilla, Vetiver, White Musk', 1, NULL, NULL, NULL),
(236, 24, 'One More Love', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(237, 25, 'Black Oud', 'Eau De Parfum', '“A captivating marriage of the raw with the cultivated, the\r\nfamiliar and the far away creates a provocative feeling of deja vu..”', '-', 'Top Notes\r\nLemon, Orange, Bergamot\r\n\r\nMiddle Notes\r\nStrawberry, Rose and Chocolate notes\r\n\r\nBase Notes\r\nPatchouli, Amber, Musk', 1, NULL, NULL, NULL),
(238, 25, 'Black Oud', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(253, 27, 'Gold Touch Me', 'Eau De Parfum', 'The fragrance is like a tender whisper, subtle weightless but with a noticeable presence. A bottle illed with gentle caresses and love, like a soft white chiffon fabric floating in the air. The woody-musky ornamentation renders the effect to be more sophisticated and refined. Its sensual, yet innocent radiance serves as a perfect prelude to the woody dry down.', '-', 'Top Notes\r\nPink Pepper, Saffron, Leathery Notes\r\n\r\nMiddle Notes\r\nIris, Violet, Suede Notes\r\n\r\nBase Notes\r\nVanilla, Cedar wood, Patchouli, Incense', 1, NULL, NULL, NULL),
(254, 27, 'Gold Touch Me', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(257, 26, 'Gold Feel Me', 'Eau De Parfum', 'The fragrance is made with the most precious natural ingredients, a modern interpretation of an oriental fragrance with a twist of citrus. It entwines the elegance of iris with the strength of patchouli and the sweetness of a vanilla blend; For an incredible scent with depth and complexity.', '-', 'Top Notes\r\nLemon, Orange, Black Currant\r\n\r\nMiddle Notes\r\nIris, Orange Flower, Jasmine\r\n\r\nBase Notes\r\nPatchouli, Tonka Bean, Vanilla, Musk', 1, NULL, NULL, NULL),
(258, 26, 'Gold Feel Me', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(263, 31, 'Gold More than Love', 'Eau De Parfum', 'This is a truly seductive fragrance, classic with a very great lasting power and silage. Fresh floral fragrance with warm musky base notes.', '-', 'Top Notes\r\nCassis, Bergamot, Apple, Pineapple, Aromatic Notes\r\nMiddle Notes\r\nPink Pepper, Jasmine, Birch, Patchouli\r\nBase Notes\r\nAmber, Vanilla, Oakmoss, Musk', 1, NULL, NULL, NULL),
(264, 31, 'Gold More than Love', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(269, 33, 'Imperial Oud', 'Eau De Parfum', 'The citrus freshness of bergamot, lemon and orange deliciously blends with Mediterranean fruits, along with warm, cuddly and sensual amber, vanilla and musk in the base makes a fragrance for hot & rainy weather.', '-', 'Top Notes\r\nSweet Fruits\r\n\r\nMiddle Notes\r\nVanilla\r\n\r\nBase Notes\r\nAmber, Sweet Musks', 1, NULL, NULL, NULL),
(270, 33, 'Imperial Oud', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(275, 36, 'Cupid 1177', 'Eau De Parfum', 'The Fragrance is inspired by the story of Lancelot and Guinevere\r\n\r\nOne of the best-known tragic love stories of Arthurian Legend. Lovers overpowered by love and passion.', '-', 'Top Notes: Lemon, Mandarin Orange, Sea Notes\r\n\r\nMiddle Notes: Rosemary, thyme, tonka bean and white flowers\r\n\r\nBase Notes: Vanilla, Patchouli and Amber', 1, NULL, NULL, NULL),
(276, 36, 'Cupid 1177', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(277, 37, 'Cupid 1240', 'Eau De Parfum', 'Tells the story of Tristan & Isolde\r\nThe tragic love story that has been told and retold through various stories and manuscripts', '-', 'Top notes: Lemon, Mandarin orange, pineapples, apricot, magnolia, rose and ozonic notes\r\n\r\nMiddle notes: Cashmeran Lily-of-the-valley and geranium\r\n\r\nBase Notes: White musk, heliotrope, sandalwood, vanilla and tonka bean', 1, NULL, NULL, NULL),
(278, 37, 'Cupid 1240', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(279, 38, 'Cupid 1260', 'Eau De Parfum', 'The Fragrance is about Paris & Helena,’\r\n\r\nThe first name in romance, the most ancient and the most enduring.', '-', 'Top Notes: Cedar & Amber\r\n\r\nMiddle Notes: Cypriol oil or nagarmotha and Gurjan balsam\r\n\r\nBase Notes: Agarwood (Oud) And woody notes', 1, NULL, NULL, NULL),
(280, 38, 'Cupid 1260', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(281, 39, 'Cupid 1597', 'Eau De Parfum', 'Inspired by the tale of Romeo & Juliet\r\n\r\n“The most famous lovers ever. This couple has become a synonym for love itself. Their love story is very tragic. Risked it all for their love”', '-', 'Top Notes: Lemon, Grapefruit, apple and mint\r\n\r\nMiddle Notes: Cashmere wood, tonka bean, geranium and cedar\r\n\r\nBase Notes: Sea Notes, Moss, Madagascar vanilla, amber and musk', 1, NULL, NULL, NULL),
(282, 39, 'Cupid 1597', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(283, 40, 'Cupid 1623', 'Eau De Parfum', 'Is about the love between Cleopatra & Mark Anthony\r\n\r\n“One of the most memorable, intriguing and moving love story of all times. Their relationship is a rue test of Love.”', '-', 'Top Notes: Lemon, Mandarin Orange, Coconut and Peach\r\n\r\nMiddle Notes: Ylang-Ylang, tuberose and Jasmin\r\n\r\nBase notes: Sandalwood, vanilla, musk and amber', 1, NULL, NULL, NULL),
(284, 40, 'Cupid 1623', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(285, 41, 'Cupid n5', 'Eau De Parfum', 'Accompanied with citrus spicy notes in its top, orange blossom and blends with Jasmine, Rose and Iris in its floral heart. It finally reveals sweet and musky facets of Vanilla and Caramel, among others.', '-', 'Top Note: bergamot, neroli, pink pepper and coriander\r\n\r\nMiddle Note: , honeysuckle, orange blossom, Egyptian jasmine, Bulgarian rose and iris\r\n\r\nBase Notes: caramelized sugar, vanilla, labdanum and white musk', 1, NULL, NULL, NULL),
(286, 41, 'Cupid n5', '-', 'Accompanied with citrus spicy notes in its top, orange blossom and blends with Jasmine, Rose and Iris in its floral heart. It finally reveals sweet and musky facets of Vanilla and Caramel, among others.', '-', 'Top Note: bergamot, neroli, pink pepper and coriander\r\n\r\nMiddle Note: , honeysuckle, orange blossom, Egyptian jasmine, Bulgarian rose and iris\r\n\r\nBase Notes: caramelized sugar, vanilla, labdanum and white musk', 2, NULL, NULL, NULL),
(287, 42, 'Cupid n1', 'Eau De Parfum', 'The high freshness is brought by the presence of aromatic and citrus notes in its top. Floral facets blend cassis, apple and pineapple. The spicy woody heart is followed by a base full of noble raw materials as amber, vanilla, oak moss and musk.', '-', 'Top Note: Cassis, Bergamot, Apple, Pineapple & Aromatic Notes\r\n\r\nMiddle Note: Pink Pepper, Jasmine, Birch & Patchouli\r\n\r\nBase Notes: Amber, Vanilla, Oakmoss & Musk', 1, NULL, NULL, NULL),
(288, 42, 'Cupid n1', '-', 'The high freshness is brought by the presence of aromatic and citrus notes in its top. Floral facets blend cassis, apple and pineapple. The spicy woody heart is followed by a base full of noble raw materials as amber, vanilla, oak moss and musk.', '-', 'Top Note: Cassis, Bergamot, Apple, Pineapple & Aromatic Notes\r\n\r\nMiddle Note: Pink Pepper, Jasmine, Birch & Patchouli\r\n\r\nBase Notes: Amber, Vanilla, Oakmoss & Musk', 2, NULL, NULL, NULL),
(289, 43, 'Cupid n3', 'Eau De Parfum', 'Opens with fruity notes brought by blackberry. Its sweet side develops into a heart full of caramel and a vanilla bottom. Musky notes cover the fragrance.', '-', 'Top Note: Blackberry \r\n\r\nMiddle Note: white rose and caramel\r\n\r\nBase Notes: vanilla and musk', 1, NULL, NULL, NULL),
(290, 43, 'Cupid n3', '-', 'Opens with fruity notes brought by blackberry. Its sweet side develops into a heart full of caramel and a vanilla bottom. Musky notes cover the fragrance.', '-', 'Top Note: Blackberry \r\n\r\nMiddle Note: white rose and caramel\r\n\r\nBase Notes: vanilla and musk', 2, NULL, NULL, NULL),
(291, 44, 'Cupid n7 Limited Edition', 'Eau De Parfum', 'Strong Perfume with plenty of woods. Smoky notes of oud are mixed with a milky sandal. The warmth of amber is lightened by powdery roles of violet and musk', '-', 'Top Note: Oud Syn & Honey\r\n\r\nMiddle Note: Violet & Sandal\r\n\r\nBase Note: Amber, Woody & Musk', 1, NULL, NULL, NULL),
(292, 44, 'Cupid n7 Limited Edition', '-', 'Strong Perfume with plenty of woods. Smoky notes of oud are mixed with a milky sandal. The warmth of amber is lightened by powdery roles of violet and musk', '-', 'Top Note: Oud Syn & Honey\r\n\r\nMiddle Note: Violet & Sandal\r\n\r\nBase Note: Amber, Woody & Musk', 2, NULL, NULL, NULL),
(293, 45, 'Cupid n8', 'Eau De Parfum', '-', '-', 'Top Notes\r\nBergamot, Pink pepper\r\n\r\nHeart Notes\r\nYlang-Ylang\r\n\r\nBase Notes\r\nAmber, Labdanum, Benzoin', 1, NULL, NULL, NULL),
(294, 45, 'Cupid n8', '-', '-', '-', 'Top Notes\r\nBergamot, Pink pepper\r\n\r\nHeart Notes\r\nYlang-Ylang\r\n\r\nBase Notes\r\nAmber, Labdanum, Benzoin', 2, NULL, NULL, NULL),
(295, 46, 'Cupid n9', 'Eau De Parfum', '-', '-', 'Top Notes\r\nFloral notes\r\n\r\nHeart Notes\r\nCedar\r\n\r\nBase Notes\r\nHoney', 1, NULL, NULL, NULL),
(296, 46, 'Cupid n9', '-', '-', '-', 'Top Notes\r\nFloral notes\r\n\r\nHeart Notes\r\nCedar\r\n\r\nBase Notes\r\nHoney', 2, NULL, NULL, NULL),
(297, 47, '7 Moons', 'Eau De Parfum', 'A magical fragrance inspired by two symbols of perfection: the number “seven” that expresses balance, and the moon which represents constant change and renaissance. Seven, the number of weeks needed for an embryo to become a fetus. Seven, the number of new moons that usually occur before childbirth. The precious ingredients that make up this bouquet celebrate the perfection of nature, the importance of its cycles, as well as its continuous evolution. Our senses are captivated by a game of notes that leads to the discovery of a lost harmony, made of a woody and floral soul.', '-', 'TOP NOTES:\r\nLABDANUM\r\n\r\nHEART NOTES:\r\nPATCHOULI\r\n\r\nBASE NOTES:\r\nSANDALWOOD, ROSE, OUD', 1, NULL, NULL, NULL),
(298, 47, '7 Moons', '-', NULL, '-', 'TOP NOTES:\r\nLABDANUM\r\n\r\nHEART NOTES:\r\nPATCHOULI\r\n\r\nBASE NOTES:\r\nSANDALWOOD, ROSE, OUD', 2, NULL, NULL, NULL),
(305, 48, 'Sana', 'Eau De Parfum', 'SANA is a fragrance dedicated to a warrior of our times: an independent and combative woman, able to use both her sweetness and character as weapons to defeat the enemies of our society: intolerance, prejudice, hypocrisy. SANA is therefore a bouquet of great charisma inspired by a new\r\nconcept of femininity: gone is the past model of a damsel in distress. Instead, it crowns the strongest personalities forged by our present world. The sweet base notes remind us of the kindness and delicacy that can still be found even in the most warlike woman.', '-', 'TOP NOTES:\r\nBERGAMOT, BLOOD ORANGE AND LAVENDER\r\n\r\nHEART NOTES:\r\nJASMINE, CINNAMON AND LICORICE\r\n\r\nBASE NOTES:\r\nMUSK, VANILLE AND CARAMEL', 1, NULL, NULL, NULL),
(306, 48, 'Sana', '-', '-', '-', '-', 2, NULL, NULL, NULL),
(311, 22, 'Black Essence', 'Eau De Parfum', '“This is sweeping the globe with it\'s pheromonic effect. Subtly\r\nsmoldering with a mesmeric appeal, you\'ll be totally irresistible\r\nin the time it takes for a quick spritz of Black Essence..”', '-', 'Top Notes\r\nBergamot, Lemon, Mandarin\r\n\r\nMiddle Notes\r\nIris, Sage, Carot See, Ginger, Geranium\r\n\r\nBase Notes\r\nMusk, Amber, Vetiver', 1, NULL, NULL, NULL),
(312, 22, 'Black Essence', '-', '-', '-', '-', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `variant_id` int(11) NOT NULL,
  `inventory` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `product_id`, `variant_id`, `inventory`, `created_at`, `updated_at`, `deleted_at`) VALUES
(14, 10, 1, 0, '2020-12-02 12:02:10', '2020-12-02 12:02:10', NULL),
(15, 10, 1, 0, '2020-12-02 12:02:10', '2020-12-02 12:02:10', NULL),
(20, 11, 1, 0, '2020-12-03 17:42:34', '2020-12-03 17:42:34', NULL),
(21, 11, 1, 0, '2020-12-03 17:42:34', '2020-12-03 17:42:34', NULL),
(33, 13, 1, 0, '2020-12-14 15:47:12', '2020-12-14 15:47:12', NULL),
(34, 13, 1, 0, '2020-12-14 15:47:12', '2020-12-14 15:47:12', NULL),
(35, 12, 1, 0, '2020-12-14 15:47:36', '2020-12-14 15:47:36', NULL),
(36, 12, 1, 0, '2020-12-14 15:47:36', '2020-12-14 15:47:36', NULL),
(39, 15, 1, 0, '2020-12-14 16:14:52', '2020-12-14 16:14:52', NULL),
(40, 15, 1, 0, '2020-12-14 16:14:52', '2020-12-14 16:14:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_variant_texts`
--

CREATE TABLE `product_variant_texts` (
  `id` int(11) NOT NULL,
  `product_variant_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lang` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_variant_texts`
--

INSERT INTO `product_variant_texts` (`id`, `product_variant_id`, `title`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '100', 1, NULL, NULL, NULL),
(2, 1, '100', 2, NULL, NULL, NULL),
(3, 2, '150', 1, NULL, NULL, NULL),
(4, 2, '150', 2, NULL, NULL, NULL),
(5, 3, '100', 1, NULL, NULL, NULL),
(6, 3, '100', 2, NULL, NULL, NULL),
(7, 4, '100', 1, NULL, NULL, NULL),
(8, 4, '100', 2, NULL, NULL, NULL),
(9, 5, '100', 1, NULL, NULL, NULL),
(10, 5, '100', 2, NULL, NULL, NULL),
(11, 6, '100', 1, NULL, NULL, NULL),
(12, 6, '100', 2, NULL, NULL, NULL),
(13, 7, '150', 1, NULL, NULL, NULL),
(14, 7, '150', 2, NULL, NULL, NULL),
(15, 8, '12', 1, NULL, NULL, NULL),
(16, 8, '12', 2, NULL, NULL, NULL),
(17, 9, '13', 1, NULL, NULL, NULL),
(18, 9, '13', 2, NULL, NULL, NULL),
(19, 10, '20', 1, NULL, NULL, NULL),
(20, 10, '20', 2, NULL, NULL, NULL),
(21, 11, '50', 1, NULL, NULL, NULL),
(22, 11, '50', 2, NULL, NULL, NULL),
(23, 12, '12', 1, NULL, NULL, NULL),
(24, 12, '12', 2, NULL, NULL, NULL),
(25, 13, '13', 1, NULL, NULL, NULL),
(26, 13, '13', 2, NULL, NULL, NULL),
(27, 14, '100', 1, NULL, NULL, NULL),
(28, 14, '100', 2, NULL, NULL, NULL),
(29, 15, '150', 1, NULL, NULL, NULL),
(30, 15, '150', 2, NULL, NULL, NULL),
(31, 16, '12', 1, NULL, NULL, NULL),
(32, 16, '12', 2, NULL, NULL, NULL),
(33, 17, '13', 1, NULL, NULL, NULL),
(34, 17, '13', 2, NULL, NULL, NULL),
(35, 18, '12', 1, NULL, NULL, NULL),
(36, 18, '12', 2, NULL, NULL, NULL),
(37, 19, '13', 1, NULL, NULL, NULL),
(38, 19, '13', 2, NULL, NULL, NULL),
(39, 20, '12', 1, NULL, NULL, NULL),
(40, 20, '12', 2, NULL, NULL, NULL),
(41, 21, '13', 1, NULL, NULL, NULL),
(42, 21, '13', 2, NULL, NULL, NULL),
(43, 22, '20', 1, NULL, NULL, NULL),
(44, 22, '20', 2, NULL, NULL, NULL),
(45, 23, '50', 1, NULL, NULL, NULL),
(46, 23, '50', 2, NULL, NULL, NULL),
(47, 24, '20', 1, NULL, NULL, NULL),
(48, 24, '20', 2, NULL, NULL, NULL),
(49, 25, '50', 1, NULL, NULL, NULL),
(50, 25, '50', 2, NULL, NULL, NULL),
(51, 26, '150', 1, NULL, NULL, NULL),
(52, 26, '150', 2, NULL, NULL, NULL),
(53, 27, '200', 1, NULL, NULL, NULL),
(54, 27, '200', 2, NULL, NULL, NULL),
(55, 28, '200', 1, NULL, NULL, NULL),
(56, 28, '200', 2, NULL, NULL, NULL),
(57, 29, '200', 1, NULL, NULL, NULL),
(58, 29, '200', 2, NULL, NULL, NULL),
(59, 30, '250', 1, NULL, NULL, NULL),
(60, 30, '250', 2, NULL, NULL, NULL),
(61, 31, '200', 1, NULL, NULL, NULL),
(62, 31, '200', 2, NULL, NULL, NULL),
(63, 32, '250', 1, NULL, NULL, NULL),
(64, 32, '250', 2, NULL, NULL, NULL),
(65, 33, '200', 1, NULL, NULL, NULL),
(66, 33, '200', 2, NULL, NULL, NULL),
(67, 34, '250', 1, NULL, NULL, NULL),
(68, 34, '250', 2, NULL, NULL, NULL),
(69, 35, '20', 1, NULL, NULL, NULL),
(70, 35, '20', 2, NULL, NULL, NULL),
(71, 36, '50', 1, NULL, NULL, NULL),
(72, 36, '50', 2, NULL, NULL, NULL),
(73, 37, '15', 1, NULL, NULL, NULL),
(74, 37, '15', 2, NULL, NULL, NULL),
(75, 38, '16', 1, NULL, NULL, NULL),
(76, 38, '16', 2, NULL, NULL, NULL),
(77, 39, '15', 1, NULL, NULL, NULL),
(78, 39, '15', 2, NULL, NULL, NULL),
(79, 40, '16', 1, NULL, NULL, NULL),
(80, 40, '16', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-08-18 03:28:18', '2020-08-18 03:28:18'),
(2, 'customer', 'web', '2020-12-07 11:36:30', '2020-12-07 11:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `section_menus`
--

CREATE TABLE `section_menus` (
  `id` int(11) NOT NULL,
  `section_no` int(11) NOT NULL,
  `image` text,
  `permalink` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>active 0 => deactivate',
  `other` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section_menus`
--

INSERT INTO `section_menus` (`id`, `section_no`, `image`, `permalink`, `is_active`, `other`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 1, NULL, '/sospiro', 1, 0, 1, '2020-10-08 07:31:35', '2020-12-22 03:29:10', NULL),
(5, 1, '1608102217912_THoO-niche-perfume_7dic_20 copia.jpg', '/thoo', 1, 0, 1, '2020-10-08 07:32:22', '2020-12-16 12:04:47', NULL),
(6, 2, '1608120159367_bnw-05.jpg', '//blendoud', 1, 0, 1, '2020-10-08 07:33:11', '2020-12-16 17:02:54', NULL),
(7, 2, NULL, '/amorino', 1, 0, 1, '2020-10-08 07:33:47', '2020-12-22 03:29:10', NULL),
(8, 2, '1608120124968_bnw-03.jpg', '//moresque', 1, 0, 1, '2020-10-08 07:34:42', '2020-12-16 17:02:20', NULL),
(9, 3, '1608120673410_jz-01.jpg', 'section-3-1', 1, 0, 1, '2020-10-08 07:35:58', '2020-12-16 17:11:34', NULL),
(10, 3, '1608120767270_LAPERLA.jpg', 'section-3-2', 1, 0, 1, '2020-10-08 07:37:14', '2020-12-16 17:13:04', NULL),
(11, 4, '1607958399948_NASAMAT GOLD_WEBSITE.jpg', 'section-4-1', 1, 0, 1, '2020-10-08 07:37:56', '2020-12-14 20:07:10', NULL),
(12, 5, '1602160716917_sec4-img1.png', 'section-5-1', 1, 0, 1, '2020-10-08 07:38:52', '2020-11-24 16:41:31', NULL),
(13, 5, '1602160740834_sec4-img2.png', 'section-5-2', 1, 0, 1, '2020-10-08 07:39:21', '2020-11-24 16:41:59', NULL),
(14, 5, '1602160836641_sec4-img3.png', 'section-5-3', 1, 0, 1, '2020-10-08 07:41:05', '2020-11-24 16:42:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `section_menu_texts`
--

CREATE TABLE `section_menu_texts` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section_menu_texts`
--

INSERT INTO `section_menu_texts` (`id`, `section_id`, `title`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(127, 12, 'Section 12 Dummy', 1, NULL, NULL, NULL),
(128, 12, 'القسم المحتوى الوهمي', 2, NULL, NULL, NULL),
(129, 13, 'Section 11 Dummy', 1, NULL, NULL, NULL),
(130, 13, 'القسم المحتوى الوهمي', 2, NULL, NULL, NULL),
(131, 14, 'Section 10 Dummy', 1, NULL, NULL, NULL),
(132, 14, 'القسم المحتوى الوهمي', 2, NULL, NULL, NULL),
(147, 11, 'Nasamat Gold', 1, NULL, NULL, NULL),
(148, 11, 'القسم المحتوى الوهمي', 2, NULL, NULL, NULL),
(165, 5, 'The House of Oud', 1, NULL, NULL, NULL),
(166, 5, '-', 2, NULL, NULL, NULL),
(167, 4, 'Sospiro', 1, NULL, NULL, NULL),
(168, 4, '-', 2, NULL, NULL, NULL),
(181, 8, 'Moresque', 1, NULL, NULL, NULL),
(182, 8, '-', 2, NULL, NULL, NULL),
(183, 7, 'Amorino', 1, NULL, NULL, NULL),
(184, 7, '-', 2, NULL, NULL, NULL),
(185, 6, 'Blend Oud', 1, NULL, NULL, NULL),
(186, 6, '-', 2, NULL, NULL, NULL),
(187, 9, 'Jacquez Zolty', 1, NULL, NULL, NULL),
(188, 9, 'القسم المحتوى الوهمي', 2, NULL, NULL, NULL),
(191, 10, 'La Perla', 1, NULL, NULL, NULL),
(192, 10, 'القسم المحتوى الوهمي', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` text,
  `phone` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(255) DEFAULT NULL,
  `facebook` text,
  `instagram` text,
  `pinterest` text,
  `twitter` text,
  `youtube` text,
  `linkedin` text,
  `tax` int(11) NOT NULL DEFAULT '0',
  `is_cod` tinyint(1) NOT NULL DEFAULT '0',
  `address` text,
  `ticker_1` text NOT NULL,
  `ticker_2` text NOT NULL,
  `ticker_3` text NOT NULL,
  `ticker_4` text NOT NULL,
  `section_1_link` text NOT NULL,
  `section_2_link` text NOT NULL,
  `section_3_link` text NOT NULL,
  `section_1_image` text NOT NULL,
  `section_2_image` text NOT NULL,
  `section_3_image` text NOT NULL,
  `about_image` text NOT NULL,
  `contact_image` text,
  `timing` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `email`, `website`, `phone`, `whatsapp`, `facebook`, `instagram`, `pinterest`, `twitter`, `youtube`, `linkedin`, `tax`, `is_cod`, `address`, `ticker_1`, `ticker_2`, `ticker_3`, `ticker_4`, `section_1_link`, `section_2_link`, `section_3_link`, `section_1_image`, `section_2_image`, `section_3_image`, `about_image`, `contact_image`, `timing`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'info@richhouse.com', 'www.richhouse.com', '+971 451 39747', '+971 451 39747', 'facebook.com', 'instagram.com', 'pinterest.com', 'twitter.com', 'youtube.com', 'linkedin.com', 5, 1, '<p class=\"p1\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 16px; line-height: normal; font-family: \" helvetica=\"\" neue\";=\"\" letter-spacing:=\"\" normal;\"=\"\">Office No: 2212, Prime Tower Business Bay, Dubai P.O.Box: 89289</p>', '1 Enjoy Complimentary Gift with every purchase above AED 500', '2 Enjoy Complimentary Gift with every purchase above AED 500', '3 Enjoy Complimentary Gift with every purchase above AED 500', '4 Enjoy Complimentary Gift with every purchase above AED 500', 'http://localhost/rich-house/category/section-5-3', 'http://localhost/rich-house/category/section-5-3', 'http://localhost/rich-house/category/section-5-3', '1608636249036_1608120159367_bnw-05.jpg', '1608636253995_1608120673410_jz-01.jpg', '1608636257661_1608120767270_LAPERLA.jpg', '1610715601405_barcode.png', '1610715669351_barcode.png', '9:00 am - 6:00 pm', '2020-10-06 06:54:07', '2021-01-15 08:01:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting_emails`
--

CREATE TABLE `setting_emails` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting_emails`
--

INSERT INTO `setting_emails` (`id`, `email`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 'info@richhouse.com', '2020-12-14 15:49:58', '2020-12-14 15:49:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting_texts`
--

CREATE TABLE `setting_texts` (
  `id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `below_section_heading` text CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `below_section_text` text CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `below_section_button` text CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `section_1_text` text CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `section_2_text` text CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `section_3_text` text CHARACTER SET utf16 COLLATE utf16_general_ci NOT NULL,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting_texts`
--

INSERT INTO `setting_texts` (`id`, `setting_id`, `title`, `below_section_heading`, `below_section_text`, `below_section_button`, `section_1_text`, `section_2_text`, `section_3_text`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(169, 1, NULL, '', '', '', '', '', '', 1, NULL, NULL, NULL),
(170, 1, NULL, '', '', '', '', '', '', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE `shippings` (
  `id` int(11) NOT NULL,
  `method` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `method`, `created_at`) VALUES
(1, 'DHL', '2020-11-23 13:09:50');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_texts`
--

CREATE TABLE `shipping_texts` (
  `id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `country` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `rate` decimal(10,2) NOT NULL,
  `cod` int(11) NOT NULL DEFAULT '0',
  `lang` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_texts`
--

INSERT INTO `shipping_texts` (`id`, `shipping_id`, `country`, `rate`, `cod`, `lang`, `created_at`) VALUES
(59, 1, 'United Arab Emirates', '25.00', 1, 1, '2021-01-15 09:08:45'),
(60, 1, 'United Arab Emirates', '25.00', 1, 2, '2021-01-15 09:08:45'),
(61, 1, 'Oman', '125.00', 0, 1, '2021-01-15 09:08:45'),
(62, 1, 'Oman', '125.00', 0, 2, '2021-01-15 09:08:45'),
(63, 1, 'Bahrain', '200.00', 0, 1, '2021-01-15 09:08:45'),
(64, 1, 'Bahrain', '200.00', 0, 2, '2021-01-15 09:08:45'),
(65, 1, 'Saudi Arabia', '230.00', 0, 1, '2021-01-15 09:08:45'),
(66, 1, 'Saudi Arabia', '230.00', 0, 2, '2021-01-15 09:08:45');

-- --------------------------------------------------------

--
-- Table structure for table `sidebar_menus`
--

CREATE TABLE `sidebar_menus` (
  `id` int(11) NOT NULL,
  `icon` text,
  `permalink` text,
  `list_order` int(11) DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '1' COMMENT '1=>active 0 => deactivate',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sidebar_menus`
--

INSERT INTO `sidebar_menus` (`id`, `icon`, `permalink`, `list_order`, `is_active`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'fragrances', 4, 1, 1, '2020-08-19 08:00:50', '2020-10-19 03:53:33', NULL),
(2, NULL, '/', 3, 1, 1, '2020-10-08 00:46:53', '2020-12-15 19:16:32', NULL),
(3, NULL, 'news-in', 1, 1, 1, NULL, '2020-12-04 12:16:30', NULL),
(6, NULL, 'home-fragrances', 5, 1, 1, NULL, NULL, NULL),
(7, NULL, 'gift-sets', 6, 1, 1, NULL, '2020-12-14 10:57:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sidebar_menu_texts`
--

CREATE TABLE `sidebar_menu_texts` (
  `id` int(11) NOT NULL,
  `sidebar_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sidebar_menu_texts`
--

INSERT INTO `sidebar_menu_texts` (`id`, `sidebar_id`, `title`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(33, 1, 'Fragrances', 1, NULL, NULL, NULL),
(34, 1, 'عطور', 2, NULL, NULL, NULL),
(43, 6, 'Home Fragrances', 1, NULL, NULL, NULL),
(44, 6, 'عطور منزلية', 2, NULL, NULL, NULL),
(47, 3, 'New In', 1, NULL, NULL, NULL),
(48, 3, 'الجديد في', 2, NULL, NULL, NULL),
(51, 7, 'Gift Sets', 1, NULL, NULL, NULL),
(52, 7, 'طقم هدايا', 2, NULL, NULL, NULL),
(57, 2, 'Explore', 1, NULL, NULL, NULL),
(58, 2, 'يكتشف', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sidebar_sub_menus`
--

CREATE TABLE `sidebar_sub_menus` (
  `id` int(11) NOT NULL,
  `sidebar_id` int(11) NOT NULL,
  `permalink` text,
  `list_order` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL COMMENT '1=>active 0 => deactivate	',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sidebar_sub_menus`
--

INSERT INTO `sidebar_sub_menus` (`id`, `sidebar_id`, `permalink`, `list_order`, `is_active`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 1, 'ead', 1, 1, 1, '2020-11-25 11:39:21', '2020-12-04 12:18:54', NULL),
(5, 1, 'hbm', 2, 1, 1, '2020-12-03 17:21:20', '2020-12-04 12:18:31', NULL),
(6, 1, 'perfume_oil', 3, 1, 1, '2020-12-03 17:21:51', '2020-12-04 13:03:54', NULL),
(7, 6, 'diffusers', 1, 1, 1, '2020-12-03 17:22:19', '2020-12-04 12:21:42', NULL),
(8, 6, 'candles', 2, 1, 1, '2020-12-03 17:22:35', '2020-12-04 12:21:28', NULL),
(9, 7, 'ts', 1, 1, 1, '2020-12-03 17:23:05', '2020-12-04 12:22:50', NULL),
(10, 7, 'ls', 2, 1, 1, '2020-12-03 17:23:23', '2020-12-04 12:22:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sidebar_sub_menu_texts`
--

CREATE TABLE `sidebar_sub_menu_texts` (
  `id` int(11) NOT NULL,
  `sidebar_id` int(11) NOT NULL,
  `sidebar_sub_menu_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sidebar_sub_menu_texts`
--

INSERT INTO `sidebar_sub_menu_texts` (`id`, `sidebar_id`, `sidebar_sub_menu_id`, `title`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(47, 1, 5, 'Hair & Body Mist', 1, NULL, NULL, NULL),
(48, 1, 5, 'Hair & Body Mist', 2, NULL, NULL, NULL),
(51, 1, 4, 'Eau De Parfum', 1, NULL, NULL, NULL),
(52, 1, 4, 'Eau De Parfum', 2, NULL, NULL, NULL),
(53, 6, 8, 'Candles', 1, NULL, NULL, NULL),
(54, 6, 8, 'Candles', 2, NULL, NULL, NULL),
(55, 6, 7, 'Diffusers', 1, NULL, NULL, NULL),
(56, 6, 7, 'Diffusers', 2, NULL, NULL, NULL),
(57, 7, 10, 'Luxury Set', 1, NULL, NULL, NULL),
(58, 7, 10, 'Luxury Set', 2, NULL, NULL, NULL),
(59, 7, 9, 'Travel Set', 1, NULL, NULL, NULL),
(60, 7, 9, 'Travel Set', 2, NULL, NULL, NULL),
(69, 1, 6, 'Perfume Oil', 1, NULL, NULL, NULL),
(70, 1, 6, 'Perfume Oil', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `city` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `days` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `timing` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lat` decimal(10,2) NOT NULL,
  `long` decimal(10,2) NOT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `email`, `phone`, `whatsapp`, `address`, `city`, `days`, `timing`, `lat`, `long`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Richhouse', 'info@richhouse.com', '042-12345678', '', 'Box No. 55408, Dubai 508, Emirates', 'UAE', 'Monday - Thursday: ', '9:00am - 7:15pm', '20.00', '50.00', '', '2020-12-14 09:49:28', NULL),
(2, 'Abbas', 'hassan@planet01.net', '3452201069', '3452201069', 'Office No 303, Business Forum', 'UAE', 'Monday:', '10pm', '20.00', '10.00', '', '2020-12-14 05:02:26', '2020-12-14 05:25:40'),
(3, 'waqar', 'waqaryounus277@gmail.com', '345313853', '+971 2251 39747', 'abc tariq road karachi pakistan', 'karachi', '', '9:00am - 7:00pm', '24.87', '67.06', '', '2020-12-14 15:51:17', '2020-12-16 16:45:16'),
(4, 'Abbas', 'hassan@planet01.net', '3452201069', '255245', 'Office No 303, Business Forum', 'Karachi', '10', '4', '4.00', '14.00', '1610554520444_download.jpeg', '2021-01-13 11:15:41', '2021-01-13 11:15:41');

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE `subscribes` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribes`
--

INSERT INTO `subscribes` (`id`, `email`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'hbjsk@kjjkh.com', '2020-10-16 02:55:28', '2020-10-16 02:55:28', NULL),
(4, 'hbjk1@kjjkh.com', '2020-10-23 00:52:55', '2020-10-23 00:52:55', NULL),
(5, 'sanamudasir@gmail.com1', '2020-11-25 11:16:13', '2021-01-15 07:23:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_settings`
--

CREATE TABLE `subscribe_settings` (
  `id` int(11) NOT NULL,
  `image` text,
  `is_active` tinyint(1) DEFAULT '1' COMMENT '1=>active 0 => deactivate',
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe_settings`
--

INSERT INTO `subscribe_settings` (`id`, `image`, `is_active`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1607516389256_TRONCO.jpg', 1, 1, '2020-10-19 07:32:07', '2020-12-09 17:20:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_setting_texts`
--

CREATE TABLE `subscribe_setting_texts` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `subscribe_setting_id` int(11) NOT NULL,
  `description` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe_setting_texts`
--

INSERT INTO `subscribe_setting_texts` (`id`, `title`, `subscribe_setting_id`, `description`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(31, 'Stay in the know', 1, '<div class=\"footer-newsletter\" style=\"-webkit-tap-highlight-color: transparent; margin-top: 1.1rem; font-family: \" byredo=\"\" sans\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 12px;=\"\" letter-spacing:=\"\" normal;\"=\"\"><div class=\"content\" style=\"-webkit-tap-highlight-color: transparent;\"><div class=\"subtitle\" style=\"text-align: center; -webkit-tap-highlight-color: transparent; margin-bottom: 0.4rem;\"><span style=\"font-size: 18px;\">Receive info on new releases, exclusive stories,</span></div><div class=\"subtitle\" style=\"text-align: center; -webkit-tap-highlight-color: transparent; margin-bottom: 0.4rem;\"><span style=\"font-size: 18px;\">collaborations, previews and special events.</span></div></div></div>', 1, NULL, NULL, NULL),
(32, 'الوافدون الجدد', 1, 'اشترك في قائمتك و كن على اطلاع على ما هو جديد و ساخن', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>active 0 => deactivate',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `is_active`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 1, 1, '2020-12-14 18:25:06', '2020-12-14 18:25:06', NULL),
(5, 1, 1, '2020-12-14 18:25:23', '2020-12-14 18:25:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tag_texts`
--

CREATE TABLE `tag_texts` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag_texts`
--

INSERT INTO `tag_texts` (`id`, `tag_id`, `title`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(27, 4, 'Gift Sets', 1, NULL, NULL, NULL),
(28, 4, 'Gift Sets', 2, NULL, NULL, NULL),
(29, 5, 'Home Fragrance', 1, NULL, NULL, NULL),
(30, 5, 'Home Fragrance', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `f_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred_language` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `email`, `phone`, `email_verified_at`, `password`, `remember_token`, `preferred_language`, `created_at`, `updated_at`) VALUES
(1, 'admin', '', 'admin@richhouse.com', '', '2020-08-17 19:00:00', '$2y$10$LvzcaSQiYAiCPwhdp1OkHeKKjwOhuvnFDPn7r2K5jWQ8QzuARISOy', NULL, 1, '2020-08-17 19:00:00', '2020-08-17 19:00:00'),
(2, 'Abbas', 'shaikh', 'abbas@gmail.com', '0314564565', NULL, '$2y$10$PQMhUviwxJ4fMVXQ36tOCuZTMOT9WJhXnKupbOtCSONhyESULmX7y', NULL, 1, '2020-12-10 12:40:46', '2021-01-29 04:41:55'),
(3, 'waqar ahmed', 'abc', 'waqarahmed@gmail.com', '9898874837', NULL, '$2y$10$lO1qrgktbfSKPCg4SZadeOu2XK6ZJNfV45jHzmlytxM588lHCnLCC', NULL, 1, '2020-12-14 16:05:02', '2020-12-14 16:05:02'),
(4, 'Mudashar', 'Mulla', 'sanamudasir@gmail.com', '0585935028', NULL, '$2y$10$4ByyD6gQbyHwJDUZ34SKPuVP5VZ2eCItdNrZyDQNbImbmvqVFnIQO', NULL, 1, '2020-12-15 20:21:03', '2020-12-16 16:36:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `state` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `city` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `primary_address` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`id`, `title`, `name`, `address`, `country`, `state`, `city`, `zip_code`, `user_id`, `primary_address`, `updated_at`) VALUES
(1, 'Home', 'Shaikh Abbas', 'ABCD . block, Ab Street, House No 1', 59, 'Sindh', 'Karachi', '75400', 2, 0, '0000-00-00 00:00:00'),
(3, 'home address', 'Waqar Ahmed Khan', 'abc suit 393 shaikh zyed road uae', 61, 'dubai', 'dubai', '704040', 2, 1, '2021-02-01 03:53:24'),
(4, 'Ajman', 'Mudashar Mulla', 'New industrial area', 61, 'Ajman', 'Ajman', '6263', 4, 0, '2021-02-01 03:51:14');

-- --------------------------------------------------------

--
-- Table structure for table `variants`
--

CREATE TABLE `variants` (
  `id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>active 0 => deactivate',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `variants`
--

INSERT INTO `variants` (`id`, `is_active`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2020-08-23 12:27:13', '2020-09-24 07:40:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `variant_texts`
--

CREATE TABLE `variant_texts` (
  `id` int(11) NOT NULL,
  `variant_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf16 COLLATE utf16_general_ci,
  `lang` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `variant_texts`
--

INSERT INTO `variant_texts` (`id`, `variant_id`, `title`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 1, 'Weight', 1, NULL, NULL, NULL),
(6, 1, 'وزن', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `weight_types`
--

CREATE TABLE `weight_types` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weight_types`
--

INSERT INTO `weight_types` (`id`, `title`, `updated_at`, `deleted_at`) VALUES
(1, 'ML', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wish_lists`
--

CREATE TABLE `wish_lists` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wish_lists`
--

INSERT INTO `wish_lists` (`id`, `product_id`, `user_id`, `updated_at`, `deleted_at`) VALUES
(1, 33, 2, '2020-12-15 18:49:55', NULL),
(2, 22, 4, '2020-12-15 20:22:12', NULL),
(3, 23, 4, '2020-12-15 20:22:16', NULL),
(4, 24, 4, '2020-12-15 20:22:19', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand_texts`
--
ALTER TABLE `brand_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_texts`
--
ALTER TABLE `coupon_texts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coupons_text_CouponID_fk` (`coupon_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_menus`
--
ALTER TABLE `footer_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_menu_bottoms`
--
ALTER TABLE `footer_menu_bottoms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_menu_bottom_texts`
--
ALTER TABLE `footer_menu_bottom_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_menu_texts`
--
ALTER TABLE `footer_menu_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notification_emails`
--
ALTER TABLE `notification_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_stories`
--
ALTER TABLE `our_stories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_story_texts`
--
ALTER TABLE `our_story_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_texts`
--
ALTER TABLE `page_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `product_texts`
--
ALTER TABLE `product_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `product_variant_texts`
--
ALTER TABLE `product_variant_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `section_menus`
--
ALTER TABLE `section_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section_menu_texts`
--
ALTER TABLE `section_menu_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_emails`
--
ALTER TABLE `setting_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_texts`
--
ALTER TABLE `setting_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_texts`
--
ALTER TABLE `shipping_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sidebar_menus`
--
ALTER TABLE `sidebar_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sidebar_menu_texts`
--
ALTER TABLE `sidebar_menu_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sidebar_sub_menus`
--
ALTER TABLE `sidebar_sub_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sidebar_sub_menu_texts`
--
ALTER TABLE `sidebar_sub_menu_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `subscribe_settings`
--
ALTER TABLE `subscribe_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe_setting_texts`
--
ALTER TABLE `subscribe_setting_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_texts`
--
ALTER TABLE `tag_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variants`
--
ALTER TABLE `variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variant_texts`
--
ALTER TABLE `variant_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `weight_types`
--
ALTER TABLE `weight_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wish_lists`
--
ALTER TABLE `wish_lists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `brand_texts`
--
ALTER TABLE `brand_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=457;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coupon_texts`
--
ALTER TABLE `coupon_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footer_menus`
--
ALTER TABLE `footer_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `footer_menu_bottoms`
--
ALTER TABLE `footer_menu_bottoms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `footer_menu_bottom_texts`
--
ALTER TABLE `footer_menu_bottom_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `footer_menu_texts`
--
ALTER TABLE `footer_menu_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `notification_emails`
--
ALTER TABLE `notification_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `our_stories`
--
ALTER TABLE `our_stories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `our_story_texts`
--
ALTER TABLE `our_story_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `page_texts`
--
ALTER TABLE `page_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `product_texts`
--
ALTER TABLE `product_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `product_variant_texts`
--
ALTER TABLE `product_variant_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `section_menus`
--
ALTER TABLE `section_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `section_menu_texts`
--
ALTER TABLE `section_menu_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting_emails`
--
ALTER TABLE `setting_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `setting_texts`
--
ALTER TABLE `setting_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shipping_texts`
--
ALTER TABLE `shipping_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `sidebar_menus`
--
ALTER TABLE `sidebar_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sidebar_menu_texts`
--
ALTER TABLE `sidebar_menu_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `sidebar_sub_menus`
--
ALTER TABLE `sidebar_sub_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sidebar_sub_menu_texts`
--
ALTER TABLE `sidebar_sub_menu_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `subscribe_settings`
--
ALTER TABLE `subscribe_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscribe_setting_texts`
--
ALTER TABLE `subscribe_setting_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tag_texts`
--
ALTER TABLE `tag_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `variants`
--
ALTER TABLE `variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `variant_texts`
--
ALTER TABLE `variant_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `weight_types`
--
ALTER TABLE `weight_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wish_lists`
--
ALTER TABLE `wish_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
